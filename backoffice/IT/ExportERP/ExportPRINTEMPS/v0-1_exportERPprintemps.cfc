<cfcomponent name="exportERPprintemps" displayname="exportERPprintemps">
	<cfset biServer="http://bip-dev.consotel.fr/xmlpserver/services/PublicReportService?wsdl">
	
	<!--- Ram�ne la liste des factures qui sont marqu�es bonnes pour export ERP --->
	<cffunction name="Main" access="remote" returntype="void" output="false">
		<cfset thread = CreateObject("java", "java.lang.Thread")>
		<!--- Cliche Par defaut --->
		<cfset idcliche=201>
		<!--- Array pour le traitement des fichiers burst --->
		<cfset stJustif=StructNew()>
		
		<cfset compteur=1>
		<!--- On r�cup�re la liste des facture --->
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_CV_REP_V3.Liste_facture_a_exporter">
			<cfprocparam value="5580" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetFactures">
		</cfstoredproc>
		
		
		<!--- On met a jour le cliche --->
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_CLICHES.create_cliche">
			<cfprocparam value="2444904" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="104" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="Refacturation" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="Batch Printemps" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="out" variable="p_retour" cfsqltype="CF_SQL_INTEGER">
		</cfstoredproc>
		<!--- <cfdump var="#qGetFactures#" label="Factures">
		<cfdump var="#stJustif#" label="Avant la boucle"> --->
		<!--- <cfflush interval="1"> --->
		<cfif qGetFactures.recordcount gt 0>
			<cfset countFact=0>
			<cfloop query="qGetFactures">
				
				<!--- On g�n�re les fichier ERP --->
				<cfset w=generateExport(idcliche,qGetFactures.numero_facture)>
				
				<!--- <cfdump var="#w#" label="#qGetFactures.numero_facture#"><br> --->
				<!--- <cfflush interval="1"> --->
				
				<!--- On g�n�re les justificatifs --->
				<cfif w neq "">
					<cfset stJob=StructNew()>
					<cfset stJob.jobStatus="Started">
					<cfset stJob.id=qGetFactures.idinventaire_periode>
					<cfset stJob.numero_facture=qGetFactures.numero_facture>
					<cfset stJob.fichier_dat=w>
					<cfset wTemp=getToken(w,1,".")>
					<cfset stJob.fichier_pdf="consoview_ar_justif-fact_"&getToken(wTemp,3,"_")&"_"&getToken(wTemp,4,"_")&".pdf">
					<cfset stJob.jobID=generateJutificatif(idcliche,qGetFactures.numero_facture,stJob.fichier_pdf)>
					<cfset structInsert(stJustif,compteur,stJob)>
					<cfset compteur=compteur+1>
					<cfset thread = CreateObject("java", "java.lang.Thread")>
					<cfset thread.sleep(1100)>
					<cfset countFact=countFact+1>
				</cfif>
				
				<cfif countFact gte 50>
					<cfbreak>
				</cfif>
			</cfloop>
			<!--- <cfdump var="#stJustif#"> --->
			<cfloop collection="#stJustif#" item="st">
				<cfset stJustif[st].jobStatus=getScheduledReportStatus(stJustif[st].JOBID)>
			</cfloop>
			<!--- <cfflush interval="1"> --->
			
			<cfdirectory action="list" directory="C:\CFT\PRINTEMPS" name="qGetDir">
			<!--- Running... --->
			<cfset jobFinished=false>
			<cfset jobNOTfinished=true>
			<!--- Polling --->
			<cfloop condition="jobFinished EQ false">
				<!--- <cfflush interval="1"> --->
				<!--- Va chercher le statut de chaque job --->
				<cfloop collection="#stJustif#" item="st">
					<cfset stJustif[st].jobStatus=getScheduledReportStatus(stJustif[st].JOBID)>
				</cfloop>
				<!--- On boucle sur les statuts pour voir si un des job n'est pas finit --->
				<cfloop collection="#stJustif#" item="st">
					<cfif stJustif[st].jobStatus neq "Completed" AND stJustif[st].jobStatus neq "Error">
						<cfset jobNOTfinished=true>
						<cfbreak>
					</cfif>
				</cfloop>
				<!--- Si il reste au moins un job non finit on continue --->
				<cfif jobNOTfinished eq true>
					<cfset jobNOTfinished=false>
				<cfelse>
					<cfset jobFinished=true>
					<cfbreak>
				</cfif>
				<!--- <cfdump var="#stJustif#"> --->
				<cfset thread.sleep(2000)>
			</cfloop>
			
			<cfloop collection="#stJustif#" item="st">
				<cfif stJustif[st].jobStatus eq "Completed">
					<!--- D�place le fichier .dat --->
					<cfftp result="resultat" action="putFile" password="K1308Sotl" username="consotel" server="80.78.3.70" localFile="C:\CFT\PRINTEMPS\#stJustif[st].fichier_dat#" remoteFile="#stJustif[st].fichier_dat#">
					<cfif resultat.Succeeded eq "YES">
						<cffile action="move" source="C:\CFT\PRINTEMPS\#stJustif[st].fichier_dat#" destination="C:\CFT\PRINTEMPS\exportes\dat\#stJustif[st].fichier_dat#" >
						<!--- d�place le fichier .pdf --->
						<cfftp result="resultat" action="putFile" password="K1308Sotl" username="consotel" server="80.78.3.70" localFile="C:\CFT\PRINTEMPS\#stJustif[st].fichier_pdf#" remoteFile="#stJustif[st].fichier_pdf#">
						<cfif resultat.Succeeded eq "YES">
							<cffile action="move" source="C:\CFT\PRINTEMPS\#stJustif[st].fichier_pdf#" destination="C:\CFT\PRINTEMPS\exportes\pdf\#stJustif[st].fichier_pdf#" >
							<cfstoredproc datasource="ROCOFFRE" procedure="PKG_CV_REP_V3.switch_to_exportee">
								<cfprocparam value="#stJustif[st].id#" type="in" cfsqltype="CF_SQL_INTEGER">
								<cfprocparam type="out" variable="t" cfsqltype="CF_SQL_INTEGER">
							</cfstoredproc>
						<cfelse>
							<cffile action="move" source="C:\CFT\PRINTEMPS\#stJustif[st].fichier_pdf#" destination="C:\CFT\PRINTEMPS\erreur\pdf\#stJustif[st].fichier_pdf#" >
							<cfmail server="192.168.3.119" port="26" type="text/html" from="production@consotel.fr" subject="[export ERP] - PRINTEMPS - Erreur" to="import@consotel.fr" password="edifact" username="infofacture">
								<table border="1">
									<cfloop collection="#stJustif#" item="st">
										<tr><td>Probl�me de connexion au FTP PRINTEMPS : fichier : #stJustif[st].fichier_pdf#</td></tr>
									</cfloop>
								</table>
							</cfmail>
						</cfif>
					<cfelse>
						<cffile action="move" source="C:\CFT\PRINTEMPS\#stJustif[st].fichier_dat#" destination="C:\CFT\PRINTEMPS\erreur\dat\#stJustif[st].fichier_dat#" >
						<cfmail server="192.168.3.119" port="26" type="text/html" from="production@consotel.fr" subject="[export ERP] - PRINTEMPS - Erreur" to="import@consotel.fr" password="edifact" username="infofacture">
							<table border="1">
								<cfloop collection="#stJustif#" item="st">
									<tr><td>Probl�me de connexion au FTP PRINTEMPS : fichier : #stJustif[st].fichier_dat#</td></tr>
								</cfloop>
							</table>
						</cfmail>
					</cfif>
				<!--- Met le .dat en erreur --->
				<cfelse>
					<cffile action="move" source="C:\CFT\PRINTEMPS\#stJustif[st].fichier_dat#" destination="C:\CFT\PRINTEMPS\erreur\dat\#stJustif[st].fichier_dat#" >
					<cfmail server="192.168.3.119" port="26" type="text/html" from="production@consotel.fr" subject="[export ERP] - PRINTEMPS - Erreur" to="import@consotel.fr" password="edifact" username="infofacture">
						<table border="1">
							<cfloop collection="#stJustif#" item="st">
								<tr><td>Probl�me de connexion au FTP PRINTEMPS : fichier : #stJustif[st].fichier_dat#</td></tr>
							</cfloop>
						</table>
					</cfmail>
				</cfif>
			</cfloop>
		<cfelse>
			<cfmail type="text/html" from="production@consotel.fr" subject="[export ERP] - PRINTEMPS - Rapport" to="import@consotel.fr" password="edifact" username="infofacture">
				<table border="1">
					<tr><td>Pas de factures � exporter</td></tr>
				</table>
			</cfmail>
		</cfif>
		<!--- <cfdump var="#stJustif#"> --->
		<!--- <cfdirectory action="list" filter="*.dat" name="qGetFile" directory="C:\CFT\PRINTEMPS">
		<cfloop query="qGetFile">
			<cfftp result="resultat" action="putFile" password="K1308Sotl" username="consotel" server="80.78.3.70" localFile="C:\CFT\PRINTEMPS\#qGetFile.name#" remoteFile="#qGetFile.name#">
			<cfif resultat.Succeeded eq "YES">
				<cffile action="move" source="C:\CFT\PRINTEMPS\#qGetFile.name#" destination="C:\CFT\PRINTEMPS\exportes\dat\#qGetFile.name#" >
			<cfelse>
				<cffile action="move" source="C:\CFT\PRINTEMPS\#qGetFile.name#" destination="C:\CFT\PRINTEMPS\erreur\dat\#qGetFile.name#" >
				<cfmail type="text/html" from="production@consotel.fr" subject="[export ERP] - PRINTEMPS - Erreur" to="import@consotel.fr" password="edifact" username="infofacture">
					<table border="1">
						<cfloop collection="#stJustif#" item="st">
							<tr><td>Probl�me de connexion au FTP PRINTEMPS : fichier : #qGetFile.name#</td></tr>
						</cfloop>
					</table>
				</cfmail>
			</cfif>
			<cfdump var="#resultat#">
		</cfloop>
		
		<cfdirectory action="list" filter="*.pdf" name="qGetFile" directory="C:\CFT\PRINTEMPS">
		<cfloop query="qGetFile">
			<cfftp result="resultat" action="putFile" password="K1308Sotl" username="consotel" server="80.78.3.70" localFile="C:\CFT\PRINTEMPS\#qGetFile.name#" remoteFile="#qGetFile.name#">
			<cfdump var="#resultat#">
			<cfif resultat.Succeeded eq "YES">
				<cffile action="move" source="C:\CFT\PRINTEMPS\#qGetFile.name#" destination="C:\CFT\PRINTEMPS\exportes\pdf\#qGetFile.name#" >
			<cfelse>
				<cffile action="move" source="C:\CFT\PRINTEMPS\#qGetFile.name#" destination="C:\CFT\PRINTEMPS\erreur\pdf\#qGetFile.name#" >
				<cfmail type="text/html" from="production@consotel.fr" subject="[export ERP] - PRINTEMPS - Erreur" to="import@consotel.fr" password="edifact" username="infofacture">
					<table border="1">
						<cfloop collection="#stJustif#" item="st">
							<tr><td>Probl�me de connexion au FTP PRINTEMPS : fichier : #qGetFile.name#</td></tr>
						</cfloop>
					</table>
				</cfmail>
			</cfif>
		</cfloop> --->
		
		<cfif qGetFactures.recordcount gt 0>
			<cfmail server="192.168.3.119" port="26" type="text/html" from="production@consotel.fr" subject="[export ERP] - PRINTEMPS - Rapport" to="import@consotel.fr" password="edifact" username="infofacture">
				<table border="1">
					<tr><td>job ID</td><td>numero facture</td><td>job Status</td>
						<td>fichier dat</td>
						<td>fichier pdf</td></tr>
					<cfloop collection="#stJustif#" item="st">
						<tr><td>#stJustif[st].jobID#</td><td>#stJustif[st].numero_facture#</td><td>#stJustif[st].jobStatus#</td>
						<td>#stJustif[st].fichier_dat#</td>
						<td>#stJustif[st].fichier_pdf#</td></tr> 
					</cfloop>
				</table>
			</cfmail>
		</cfif>
		<!--- On r�cup�re la liste des facture --->
		
		
	</cffunction>
	
	<!--- G�n�re le fichier export en csv --->
	<cffunction name="getScheduledReportStatus" access="remote" returntype="String" output="false">
		<cfargument name="P_JOBID" type="String" default="">
		<!--- scheduleReportRequest --->
		<cfset getScheduledReportStatusRequest=structNew()>
		<cfset getScheduledReportStatusRequest.scheduledJobID=P_JOBID>
		<cfset getScheduledReportStatusRequest.userID="Administrator">
		<cfset getScheduledReportStatusRequest.password="tred78">
		<cfinvoke webservice="#biServer#" returnvariable="getScheduledReportStatusRequestResponse" method="getScheduledReportStatus"
				argumentCollection="#getScheduledReportStatusRequest#">
		</cfinvoke>
		<cfreturn getScheduledReportStatusRequestResponse.getJobStatus()>
	</cffunction>
	
	<!--- G�n�re le fichier export en csv --->
	<cffunction name="generateJutificatif" access="remote" returntype="numeric" output="false">
		<cfargument name="P_IDCLICHE" type="Numeric" default="201">
		<cfargument name="P_NUM_FACTURE" type="string" default="185822690">
		<cfargument name="P_CHAINE" type="string" default="">
		<!--- getReportParameters --->
		<!--- Envoi des param�tres --->
		<cfset ArrayOfParamNameValues=ArrayNew(1)>
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_IDCLICHE">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=P_IDCLICHE>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[1]=ParamNameValue>
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_NUM_FACTURE">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=P_NUM_FACTURE>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[2]=ParamNameValue>
		<!--- Rapport --->
		<cfset ReportRequest=structNew()>
		<cfset ReportRequest.reportAbsolutePath="/CONSOTEL/Services/EXPORT_ERP/PRINTEMPS/JustificatifParFacture.xdo">
		<cfset ReportRequest.attributeTemplate="cv">
		<cfset ReportRequest.attributeLocale="fr-FR">
		<cfset ReportRequest.attributeFormat="pdf">
		<cfset ReportRequest.sizeOfDataChunkDownload=-1>
		<cfset ReportRequest.parameterNameValues=ArrayOfParamNameValues>
		<!--- File Delivery --->
		<cfset LocalDeliveryRequest=structNew()>
		<cfset LocalDeliveryRequest.destination="/opt/oracle/home/OBIEE/PRINTEMPS">
		<!--- FTP Delivery --->
		<cfset FTPDeliveryRequest=structNew()>
		<cfset FTPDeliveryRequest.ftpServerName="CFT">
		<cfset FTPDeliveryRequest.ftpUserName="printemps">
		<cfset FTPDeliveryRequest.ftpUserPassword="printemps">
		<cfset FTPDeliveryRequest.remoteFile="/" & p_chaine >
		<!--- Delivery --->
		<cfset DeliveryRequest=structNew()>
		<cfset structInsert(DeliveryRequest,"ftpOption",FTPDeliveryRequest)>
		<!--- ScheduleRequest --->
		<cfset ScheduleRequest=structNew()>
		<cfset structInsert(ScheduleRequest,"deliveryRequest",DeliveryRequest)>
		<cfset ScheduleRequest.jobLocale="fr-FR">
		<cfset structInsert(ScheduleRequest,"notificationTo","import@consotel.fr")>
		<cfset structInsert(ScheduleRequest,"notifyWhenFailed",false)>
		<cfset structInsert(ScheduleRequest,"notifyWhenSuccess",false)>
		<cfset structInsert(ScheduleRequest,"notifyWhenWarning",false)>
		<cfset structInsert(ScheduleRequest,"saveDataOption",false)>
		<cfset structInsert(ScheduleRequest,"saveOutputOption",false)>
		<cfset structInsert(ScheduleRequest,"reportRequest",ReportRequest)>
		<cfset structInsert(ScheduleRequest,"scheduleBurstringOption",false)>
		<cfset structInsert(ScheduleRequest,"userJobName","/" & p_chaine )>
		<!--- scheduleReportRequest --->
		<cfset scheduleReportRequest=structNew()>
		<cfset structInsert(scheduleReportRequest,"scheduleRequest",ScheduleRequest)>
		<cfset scheduleReportRequest.userID="concepteur">
		<cfset scheduleReportRequest.password="concepteur">
		<!--- <cfdump var="#scheduleReportRequest#" label="scheduleReportRequest"> --->
		
		<cfinvoke webservice="#biServer#" returnvariable="scheduleReportReturn" method="scheduleReport"
				argumentCollection="#scheduleReportRequest#">
		</cfinvoke>
		<cfreturn scheduleReportReturn>
	</cffunction>
	
	<!--- G�n�re en burst le justificatif par client/magasin --->
	<cffunction name="generateExport" access="remote" returntype="string" output="false">
		<cfargument name="P_IDCLICHE" type="Numeric" default="201">
		<cfargument name="P_NUM_FACTURE" type="string" default="185822690">
		<!--- getReportParameters --->
		<!--- Envoi des param�tres --->
		<cfset ArrayOfParamNameValues=ArrayNew(1)>
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_IDCLICHE">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=#P_IDCLICHE#>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[1]=ParamNameValue>
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_NUM_FACTURE">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=#P_NUM_FACTURE#>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[2]=ParamNameValue>
		<!--- Rapport --->
		<cfset myParamReportRequest=structNew()>
		<cfset myParamReportRequest.reportAbsolutePath="/CONSOTEL/Services/EXPORT_ERP/PRINTEMPS/exportERP.xdo">
		<cfset myParamReportRequest.attributeTemplate="export">
		<cfset myParamReportRequest.attributeLocale="fr-FR">
		<cfset myParamReportRequest.attributeFormat="csv">
		<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
		<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
		<!---  --->
		<!--- <cfdump var="#myParamReportRequest#"> --->
		<cfset myParamReportParameters=structNew()>
		<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
		<cfset myParamReportParameters.userID="concepteur">
		<cfset myParamReportParameters.password="concepteur">
		<cfdump var="#myParamReportParameters#" label="myParamReportParameters">
		
		<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport"
				argumentCollection="#myParamReportParameters#">
		</cfinvoke>
		
		<cfset v=resultRunReport.getReportBytes()>
		
		<cfset arrData = CSVToArray(CSV = ToString(v))>
		
		<cfdump var="#arrData#">
		<cfset thread = CreateObject("java", "java.lang.Thread")>
		<cfset thread.sleep(3100)>
		<cfif ArrayLen(arrData) gt 1>
			<cfset nomFichier="consoview_ar_" & LsDateFormat(Now(),"ddmmyyyy") & "_" & LsTimeFormat(Now(),"hhmmss") & ".dat">
		<cfelse>
			<cfset nomFichier="">
		</cfif>
		
		<cfloop from="2" to="#ArrayLen(arrData)#" index="i">
			<cfset nbr=GetToken(arrData[i][1],5,"|")>
			<cfif nbr gte 0>
				<cfset signe="+">
			<cfelse>
				<cfset signe="">
			</cfif>
			<cfset nbr=signe & LsNumberFormat(nbr,'.__')>
			<cfif right(nbr,3) eq ",00">
				<cfset nbr=getToken(nbr,1,",")>
			</cfif> 
			<cfset chaine=GetToken(arrData[i][1],1,"|")&"|"&GetToken(arrData[i][1],2,"|")&"|"&GetToken(arrData[i][1],3,"|")&"|"
					&GetToken(arrData[i][1],4,"|")&"|"&nbr&"|"&GetToken(arrData[i][1],6,"|")&"|"&GetToken(arrData[i][1],7,"|")&"|"&
						GetToken(arrData[i][1],8,"|")&"|"&GetToken(arrData[i][1],9,"|")>
			<cfif i gt 2>
				<cffile action="append" addnewline="true" file="C:\CFT\PRINTEMPS\#nomFichier#" output="#chaine#">
			<cfelse>
				<cffile action="write" file="C:\CFT\PRINTEMPS\#nomFichier#" output="#chaine#">
			</cfif>
		</cfloop>
		</table>
		<cfreturn #nomFichier#>
	</cffunction>
	
	<!--- Pour convertir un csv en array --->
	<cffunction name="CSVToArray"
		
		access="public"
		returntype="array"
		output="false"
		hint="Takes a CSV file or CSV data value and converts it to an array of arrays based on the given field delimiter. Line delimiter is assumed to be new line / carriage return related.">
		 
		
		<!--- Define arguments. --->
		<cfargument
		name="File"
		type="string"
		required="false"
		default=""
		hint="The optional file containing the CSV data."
		/>
		 
		
		<cfargument
		name="CSV"
		type="string"
		required="false"
		default=""
		hint="The CSV text data (if the file was not used)."
		/>
		 
		
		<cfargument
		name="Delimiter"
		type="string"
		required="false"
		default=","
		hint="The data field delimiter."
		/>
		 
		
		<cfargument
		name="Trim"
		type="boolean"
		required="false"
		default="true"
		hint="Flags whether or not to trim the END of the file for line breaks and carriage returns."
		/>
		 
		
		 
		
		<!--- Define the local scope. --->
		<cfset var LOCAL = StructNew() />
		 
		
		 
		
		<!---
		Check to see if we are using a CSV File. If so,
		then all we want to do is move the file data into
		the CSV variable. That way, the rest of the algorithm
		can be uniform.
		--->
		<cfif Len( ARGUMENTS.File )>
		 
		
		<!--- Read the file into Data. --->
		<cffile
		action="read"
		file="#ARGUMENTS.File#"
		variable="ARGUMENTS.CSV"
		/>
		 
		
		</cfif>
		 
		
		 
		
		<!---
		ASSERT: At this point, no matter how the data was
		passed in, we now have it in the CSV variable.
		--->
		 
		
		 
		
		<!---
		Check to see if we need to trim the data. Be default,
		we are going to pull off any new line and carraige
		returns that are at the end of the file (we do NOT want
		to strip spaces or tabs).
		--->
		<cfif ARGUMENTS.Trim>
		 
		
		<!--- Remove trailing returns. --->
		<cfset ARGUMENTS.CSV = REReplace(
		ARGUMENTS.CSV,
		"[\r\n]+$",
		"",
		"ALL"
		) />
		 
		
		</cfif>
		 
		
		 
		
		<!--- Make sure the delimiter is just one character. --->
		<cfif (Len( ARGUMENTS.Delimiter ) NEQ 1)>
		 
		
		<!--- Set the default delimiter value. --->
		<cfset ARGUMENTS.Delimiter = "," />
		 
		
		</cfif>
		 
		
		 
		
		<!---
		Create a compiled Java regular expression pattern object
		for the experssion that will be needed to parse the
		CSV tokens including the field values as well as any
		delimiters along the way.
		--->
		<cfset LOCAL.Pattern = CreateObject(
		"java",
		"java.util.regex.Pattern"
		).Compile(
		JavaCast(
		"string",
		 
		
		<!--- Delimiter. --->
		"\G(\#ARGUMENTS.Delimiter#|\r?\n|\r|^)" &
		 
		
		<!--- Quoted field value. --->
		"(?:""([^""]*+(?>""""[^""]*+)*)""|" &
		 
		
		<!--- Standard field value --->
		"([^""\#ARGUMENTS.Delimiter#\r\n]*+))"
		)
		)
		/>
		 
		
		<!---
		Get the pattern matcher for our target text (the
		CSV data). This will allows us to iterate over all the
		tokens in the CSV data for individual evaluation.
		--->
		<cfset LOCAL.Matcher = LOCAL.Pattern.Matcher(
		JavaCast( "string", ARGUMENTS.CSV )
		) />
		 
		
		 
		
		<!---
		Create an array to hold the CSV data. We are going
		to create an array of arrays in which each nested
		array represents a row in the CSV data file.
		--->
		<cfset LOCAL.Data = ArrayNew( 1 ) />
		 
		
		<!--- Start off with a new array for the new data. --->
		<cfset ArrayAppend( LOCAL.Data, ArrayNew( 1 ) ) />
		 
		
		 
		
		<!---
		Here's where the magic is taking place; we are going
		to use the Java pattern matcher to iterate over each
		of the CSV data fields using the regular expression
		we defined above.
		 
		
		Each match will have at least the field value and
		possibly an optional trailing delimiter.
		--->
		<cfloop condition="LOCAL.Matcher.Find()">
		 
		
		<!---
		Get the delimiter. We know that the delimiter will
		always be matched, but in the case that it matched
		the START expression, it will not have a length.
		--->
		<cfset LOCAL.Delimiter = LOCAL.Matcher.Group(
		JavaCast( "int", 1 )
		) />
		 
		
		 
		
		<!---
		Check for delimiter length and is not the field
		delimiter. This is the only time we ever need to
		perform an action (adding a new line array). We
		need to check the length because it might be the
		START STRING match which is empty.
		--->
		<cfif (
		Len( LOCAL.Delimiter ) AND
		(LOCAL.Delimiter NEQ ARGUMENTS.Delimiter)
		)>
		 
		
		<!--- Start new row data array. --->
		<cfset ArrayAppend(
		LOCAL.Data,
		ArrayNew( 1 )
		) />
		 
		
		</cfif>
		 
		
		 
		
		<!---
		Get the field token value in group 2 (which may
		not exist if the field value was not qualified.
		--->
		<cfset LOCAL.Value = LOCAL.Matcher.Group(
		JavaCast( "int", 2 )
		) />
		 
		
		<!---
		Check to see if the value exists. If it doesn't
		exist, then we want the non-qualified field. If
		it does exist, then we want to replace any escaped
		embedded quotes.
		--->
		<cfif StructKeyExists( LOCAL, "Value" )>
		 
		
		<!---
		Replace escpaed quotes with an unescaped double
		quote. No need to perform regex for this.
		--->
		<cfset LOCAL.Value = Replace(
		LOCAL.Value,
		"""""",
		"""",
		"all"
		) />
		 
		
		<cfelse>
		 
		
		<!---
		No qualified field value was found, so use group
		3 - the non-qualified alternative.
		--->
		<cfset LOCAL.Value = LOCAL.Matcher.Group(
		JavaCast( "int", 3 )
		) />
		 
		
		</cfif>
		 
		
		 
		
		<!--- Add the field value to the row array. --->
		<cfset ArrayAppend(
		LOCAL.Data[ ArrayLen( LOCAL.Data ) ],
		LOCAL.Value
		) />
		 
		
		</cfloop>
		 
		
		 
		
		<!---
		At this point, our array should contain the parsed
		contents of the CSV value. Return the array.
		--->
		<cfreturn LOCAL.Data />
	</cffunction>
</cfcomponent>