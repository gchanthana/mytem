package univers.inventaire.equipements.lignes.v2.views
{
	 
	import composants.util.TextInputLabeled;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionTechniqueLignesApp;
	import univers.inventaire.equipements.lignes.v2.vo.LibellePerso;
	 

	
	
	[Event(name="libelleChanged")]
	
	
	
	
	
	
	
	
	
	
	
	
	[Event(name="libelleCanceled")]
	
	
	
	
	
	
	
	
	
	
	
	[Bindable]
	public class EditLibelleImpl extends TitleWindow implements IGestionTechniqueLignesApp
	{
		public var libelle : TextInputLabeled;
		public var oldLibelle : Label;
		
		public var btValider : Button;
		public var btAnnuler : Button;
		
		protected var _libellePerso : LibellePerso;
				
		public function EditLibelleImpl()
		{
			super();
			
			
		}
		
		public function set libellePerso(libelleVo : LibellePerso):void
		{	
			_libellePerso = libelleVo;
		}
		public function get libellePerso():LibellePerso
		{	
			return _libellePerso;
		}
		
		
		private var _gestionTechnique : GestionTechniqueLignesApp;
		public function get gestionTechnique():GestionTechniqueLignesApp{
			return _gestionTechnique;
		}
		
		public function set gestionTechnique(gestion : GestionTechniqueLignesApp):void{
			_gestionTechnique = gestion;
		}
		
		
		
		
		
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			gestionTechnique.libellesPersos.addEventListener("libelleChanged",libelleChangedHandler);
		}
		//*============ HENADLER ==================================//
		
		protected function libelleChangeHandler(ev : Event):void{
			libelle.clearStyle("borderColor");	
			if (ev.currentTarget.text.length > 0){
				btValider.enabled = true;
			}else{
				btValider.enabled = false;
			}
		}
		
		protected function btValiderClickHandler(me : MouseEvent):void{			
			if (libelle.text.length > 0){								
				_libellePerso.libelle = libelle.text;
				gestionTechnique.libellesPersos.updateLibelle(_libellePerso);
			}else{				
				libelle.setStyle("borderColor","red");
			}			
		}
			
		
		protected function btAnnulerClickHandler(me : MouseEvent):void{
			PopUpManager.removePopUp(this);
		}
		
		
		protected function closeEventHandler(ce : CloseEvent):void{
			PopUpManager.removePopUp(this);
		}
		
		
		protected function libelleChangedHandler(ev : Event):void{
			dispatchEvent(new Event("libelleChanged"));
		}
		
		
		//*=============== FIN =====================================//
		
		
		
	}
}