package univers.inventaire.commande.system
{
	import composants.mail.MailBoxImpl;
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	import composants.util.ConsoviewUtil;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.commande.ihm.EnregistrerActionImpl;
	import univers.inventaire.commande.ihm.mail.ActionMailBoxIHM;
	
	[Event(name="mailSent",type="flash.events.Event")]
	
	public class MailSender extends EnregistrerActionImpl
	{
		public static const MAIL_SENT:String = "mailSent";	
		
		private var _parentRef:DisplayObject;
		public function set parentRef(value:DisplayObject):void{
			_parentRef = value;
		}	
		
		private var _infosMail:InfosObject = new InfosObject();
		public function set infosMail(value:InfosObject):void
		{
			_infosMail  = value
		}
		public function get infosMail():InfosObject
		{
			return _infosMail	
		}
		
		private var _pUpMailBox : ActionMailBoxIHM;
				
		public function MailSender()
		{
			super();
			 
		}
		
		public function afficherMailBox():void
		{			
			removePopUp();
			getListeDestinataire();
		}
		
		
		
		protected function _pUpMailBoxMailEnvoyeHandler(ev : Event):void
		{	
			/* if (infosMail.commande != null)
			{
				infosMail.commande.IDCONTACT = _pUpMailBox.contact.contactID;
				infosMail.commande.IDSOCIETE = _pUpMailBox.contact.societeID;
			} */
			
			selectedAction.COMMENTAIRE_ACTION = _pUpMailBox.txtCommentaire.text; //txtCommentaire.text;
	    	selectedAction.DATE_ACTION = new Date();//dcDateAction.selectedDate;
	    		    	
	    	var actionEvent:ActionEvent = new ActionEvent(ActionEvent.ACTION_CLICKED);
	    	actionEvent.action = selectedAction;
	    	dispatchEvent(actionEvent);
	    	
			_pUpMailBox.removeEventListener(MailBoxImpl.MAIL_ENVOYE,_pUpMailBoxMailEnvoyeHandler);
			PopUpManager.removePopUp(_pUpMailBox);
			_pUpMailBox = null;
			
			trace("(mailSender)  Mail envoyé");
		}
		
		public var listeDestinataires:String;
	    
	    public function getListeDestinatairesRevendeur():void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil
	    									.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur",
																	"getContactsRevendeur",
																	function(event:ResultEvent):void
															    	{
															    		if(event.result)	    	
															    		{	
																    		listeDestinataires = ConsoviewUtil.
																    						ICollectionViewToList(event.result as ArrayCollection,"EMAIL",",");
																    		afficher();
																    	}
																    	else
																    	{
																    		Alert.show("Pas de destinataire configuré pour ce revendeur");
																    	}	
															    	});
			RemoteObjectUtil.callService(op,infosMail.commande.IDREVENDEUR)
	    }
	    
	    protected function afficher():void
	    {
	    	_pUpMailBox = new ActionMailBoxIHM();
			
			var objet:String = "Réf. : "  + infosMail.commande.REF_CLIENT;
			
			_pUpMailBox.initMail("Commande",objet,listeDestinataires);
			
				
			_pUpMailBox.configRteMessage(infosMail,GabaritFactory.TYPE_COMMANDE_MOBILE);			
			_pUpMailBox.addEventListener(MailBoxImpl.MAIL_ENVOYE,_pUpMailBoxMailEnvoyeHandler);
			
			PopUpManager.addPopUp(_pUpMailBox,_parentRef,true);
			PopUpManager.centerPopUp(_pUpMailBox);
	    }
	    
	    
	    public function getListeDestinataire():void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil
	    									.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.inventaire.commande.GestionWorkFlow",
																	"getContactsActSuivante",
																	function(event:ResultEvent):void
															    	{
															    		if(event.result)	    	
															    		{	
																    		listeDestinataires = ConsoviewUtil.
																    						ICollectionViewToList(event.result as ArrayCollection,"LOGIN_EMAIL",",");
																    		afficher();
																    	}
																    	else
																    	{
																    		Alert.show("Pas de destinataire configuré pour ce revendeur");
																    	}	
															    	});
															    	
			RemoteObjectUtil.callService(op,Commande(infosMail.commande).IDPOOL_GESTIONNAIRE,selectedAction.IDACTION);
			
	    }
	}
}