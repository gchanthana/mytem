	<cfif IsDefined ('SESSION')>
		<cfif IsDefined ('SESSION.IS_CONNECTED')>
			<cfif SESSION.IS_CONNECTED EQ 0> 		
				<cflocation url="index_mobile.cfm">
			</cfif>
		<cfelse>	 
			<cflocation url="index_mobile.cfm">
		</cfif>	
	<cfelse>	 
		<cflocation url="index_mobile.cfm">
	</cfif>

	<?xml version="1.0" encoding="iso-8859-1" ?><!DOCTYPE html PUBLIC
		"-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
		"http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>
			TELLCOST
		</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="robots" content="all" />
		<meta name="classification" content="Services" />
		<meta name="copyright" content="ConsoTel SAS" />
		<meta name="author" content="Guillaume RAMOS" />
		<meta name="language" content="fr" />
		<meta name="description" content="TELLCOST" />
		<meta name="keywords" content="TELLCOST" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		
		<STYLE TYPE="TEXT/CSS">
			.mybouton { 
			width: 150px;} 

			.submit {
			border: solid 1px #000000;
			border-radius:5PX;
		 	}
		</style>
	</head>

<!--- Variables query --->
	
	<cfset TOTNA = "0">
	<cfset TOTM = "0">
	<cfset TOTU = "0">
	<cfset TOTK = "0">	
	<cfset TOTMONT = "0">
	<cfset TOTV = "0">
	<cfset TOTPREC = "0">

<!--- Fin variables query--->

<!--- Variables function --->

	<cfset Validate = -1>
	<cfset MessageError = "">
	
<!--- Fin variables function--->

<!--- Function --->

	<cfset obj = createobject('component','TellCostService')>	

<!--- Fin function --->

	<FONT face="Arial">
		<div align="left"> 
			<table border="0" height="100%">
				 <tr height="100%">
					<td valign="middle" height="100%" width="100%"> 
						<a href="<cfoutput>home.cfm?#SESSION.URLTOKEN#</cfoutput>">
							<< Retour
						</a>
					</td>
					<td valign="middle" height="100%"> 
						<img src="tellCostImg/ico157.gif">
					</td>
					<td valign="middle" height="100%"> 
	       				<a href="index_mobile.cfm">
		       				Déconnexion
						</a>
					</td>
				</tr>
			</table>
			

		<br/><br/>

			Votre ligne :  <cfoutput>#SESSION.PHONENUMBER#</cfoutput>

		<br/>

			<cfset SESSION = obj.zAddDate(SESSION.IDCAMPAGNE,SESSION)>
			Période de facturation des consommations du : <cfoutput>#SESSION.DATEDEBUT#</cfoutput> Au <cfoutput>#SESSION.DATEFIN#</cfoutput> <br /><br />
			<cfset dataConverted = DateFormat(#SESSION.DATEFACTURE#,"dd/mm/yyyy")>
			Date de la facture opérateur : <cfoutput>#dataConverted#</cfoutput>
		
		<br/><br/>

					<TABLE WIDTH="100%" BORDER="1" STYLE="BORDER-COLLAPSE: COLLAPSE"> 
				<cfset q = obj.zQueryConso(SESSION.IDSOUS_TETE,SESSION.IDCAMPAGNE,1,SESSION.IDRACINE)> 
				<cfset cptr = 0>
				<cfoutput>
					<cfloop query="q">
							<tr align="center">   
								<td colspan="2" height="35"> <div align="center"><B>#SUR_THEME#</B></div> </td> 
							</tr> 
							<tr align="center">
								<td width="120"> <B>Nombre d'appels</B> </td>  
								<td>  
									<cfset temp = #LSNumberFormat(val(NB_APPEL_EN_COURS), "_________")# >
									<cfif val(NB_APPEL_EN_COURS) EQ 0>
										-
									<cfelse>
										#temp#
									</cfif>
								</td>  
							</tr>
							 <tr align="center">
								<td width="120"> <B>Volumes (Minutes)</B></td>  
								<td> 
									<cfset temp = #LSNumberFormat(val(DURRE_APPEL_EN_COURS_MIN), "_________.__")#>

									<cfif val(DURRE_APPEL_EN_COURS_MIN) EQ 0>
										-
									<cfelse>
										#temp#
									</cfif>
								</td>  
							</tr>
							<tr align="center">
								<td width="120">
									<B>Volumes (Unités)</B>
								</td>  
								<td>
									<cfset temp = #LSNumberFormat(val(DURRE_APPEL_EN_COURS_UN), "_________")#>
									<cfif val(DURRE_APPEL_EN_COURS_UN) EQ 0>
										-
									<cfelse>
									
										<cfset TOTU = TOTU + val("#DURRE_APPEL_EN_COURS_UN#")>
										#temp#
									</cfif>
								</td>  
							</tr>
							
							<tr align="center">
								<td width="120">
									<B>Volumes (Ko)</B>
								</td>  
								<td> 
									<cfset temp = #LSNumberFormat(val(DURRE_APPEL_EN_COURS_KO), "_________")#>
									<cfif val(DURRE_APPEL_EN_COURS_KO) EQ 0>
										-
									<cfelse>
										#temp#
									</cfif>
								</td>  
							</tr>
							<tr align="center">
								<td width="120">
									<B>Montant (€) HT</B>
								</td>  
								<td> 
									<cfset temp = #LSNumberFormat(val(MONTANT_EN_COURS), "_________.__")#>
									<cfif val(MONTANT_EN_COURS) EQ 0>
										-
									<cfelse>
										#temp# €
									</cfif>
								</td>  
							</tr>
							<tr align="center">
								<td width="120">
									<B>Variation mois précédent</B>
								</td>  
								<td> 
									<cfset temp = #LSNumberFormat(val(VARIATION), "_________")#>
									<cfif val(VARIATION) EQ 0>
										-
									<cfelse>
										<cfif val(VARIATION) LT -100>
											-100 %
										<cfelse>
											#temp# %
										</cfif>
									</cfif>
								</td>  
							</tr>
							<cfset TOTNA = TOTNA + val("#NB_APPEL_EN_COURS#")>
							<cfset TOTM = TOTM + val("#DURRE_APPEL_EN_COURS_MIN#")>
							<cfset TOTK = TOTK + val("#DURRE_APPEL_EN_COURS_KO#")>
							<cfset TOTMONT = TOTMONT + val("#MONTANT_EN_COURS#")>
							<cfset TOTV = TOTV + val("#VARIATION#")>
							<cfset TOTPREC = TOTPREC + val("#MONTANT_PREC#")>
					</cfloop>
				</cfoutput>
					</TABLE>
					
					<TABLE WIDTH="100%" BORDER="1" STYLE="BORDER-COLLAPSE: COLLAPSE"> 
				<cfoutput>
						<tr align="center">   
								<td colspan="2" height="35"> <div align="center"><B>Total</B></div> </td> 
							</tr> 
							<tr align="center">
								<td width="120">
									<B>Nombre d'appels</B>
								</td>  
								<td>  
									<cfset temp = #LSNumberFormat(val(TOTNA), "_________.__")#>
									<cfif val(TOTNA) EQ 0>
										-
									<cfelse>
										#temp#
									</cfif>
								</td>  
							</tr>
							 <tr align="center">
								<td width="120">
									<B>Volumes (Minutes)</B>
								</td>  
								<td> 
									<cfset temp = #LSNumberFormat(val(TOTM), "_________.__")#>
									<cfif val(TOTM) EQ 0>
										-
									<cfelse>
										#temp#
									</cfif>
								</td>  
							</tr>
							 <tr align="center">
								<td width="120">
									<B>Volumes (Unités)</B>
								</td>  
								<td>
									<cfset temp = #LSNumberFormat(val(TOTU), "_________")#>
									<cfif val(TOTU) EQ 0>
										-
									<cfelse>
										#temp#
									</cfif>
								</td>  
							</tr>
							<tr align="center">
								<td width="120">
									<B>Volumes (Ko)</B>
								</td>  
								<td> 
									<cfset temp = #LSNumberFormat(val(TOTK), "_________")#>
									<cfif val(TOTK) EQ 0>
										-
									<cfelse>
										#temp#
									</cfif>
								</td>  
							</tr>
							<tr align="center">
								<td width="120">
									<B>Variation mois précédent (%)</B>
								</td>  
								<td> 
									<cfif TOTPREC EQ 0>
										<cfset VARIATION = 100>
									<cfelse>
										<cfset TOTALTEMP1 = TOTMONT - TOTPREC>
										<cfset TOTALTEMP2 = TOTALTEMP1 / TOTPREC>
										<cfset TOTALTEMP3 = TOTALTEMP2 * 100>
										<cfset VARIATION = #LSNumberFormat(val(TOTALTEMP3), "_________")#>
									</cfif>
									<cfif val(TOTALTEMP3) EQ 0>
										<B> - </B>
									<cfelse>
										<B>#VARIATION# %</B>
									</cfif>
								</td>  
							</tr>
							
							<tr align="center">
								<td width="120">
									<B>Montant (€) HT</B>
								</td>  
								<td> 
									<cfset temp = #LSNumberFormat(val(TOTMONT), "_________.__")#>
									<cfif val(TOTMONT) EQ 0>
										-
									<cfelse>
										#temp# €
									</cfif>
								</td>  
							</tr>
				</cfoutput>
					</TABLE>

		<br />
				
				<cfoutput> Pour plus de détails connectez vous depuis un poste fixe </cfoutput>
		</div>
		
	<br />
		
		<DIV ID="MONCADRE">
			<FONT face="Arial" size="2">
				<div align="left"> 
					Les montants des thèmes de consommations sont en valeur brute.<br/>
				</div>
			</FONT>
		</div>
	</FONT>
</html>