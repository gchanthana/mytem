package univers.facturation.suivi.controles.services
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	public class FicheLigneHandler
	{
		
		private var _model:FicheLigneModel;
		
		public function FicheLigneHandler(model:FicheLigneModel):void
		{
			_model = model;
		}
		
		public function getFicheLigneHandler(event:ResultEvent):void
		{
			_model.updateFicheLigne(event.result as Object);
		}
		
		public function getLibellePersoHandler(event:ResultEvent):void
		{
			_model.updateLibellePerso(event.result as Object);
		}
		
	}
}