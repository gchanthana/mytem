<cfcomponent name="FinancierMensuelConsolide" alias="fr.consotel.consoview.bi.report.financier.consolide.FinancierMensuelConsolide">
	<cffunction name="getXmlDataModel" access="remote" returntype="xml">
		<cfxml variable="xmlDataModel">
			<ROW>
			<ORGANISATION/>
			<CLICHE/>
			<CHEMIN_NOEUD_DEPART/>
			<CHEMIN/>
			<TYPE/>
			<NOEUD/>
			<PERIODE/>
			<OPERATEUR/>
			<SEGMENT/>
			<NBRE_LIGNES/>
			<ABO_SEGMENT/>
			<CONSO_SEGMENT/>
			<TOTAL_SEGMENT/>
			<COUT_MOY_LIGNE_SEGMENT/>
			<TOTAL_OPE_PERIODE/>
			<TOTAL_PERIODE/>
			<TOTAL_OPE_PERIODE_PREC/>
			<TOTAL_PERIODE_PREC/>
			<VARIATION_OPE_PERIODE/>
			<VARIATION_PERIODE/>
			</ROW>
		</cfxml>
		<cfreturn xmlDataModel>
	</cffunction>
</cfcomponent>
