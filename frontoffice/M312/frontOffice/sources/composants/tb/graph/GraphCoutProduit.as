package composants.tb.graph
{
	import composants.tb.graph.charts.PieGraph_IHM;
	import composants.util.ConsoviewFormatter;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	public class GraphCoutProduit extends PieGraphBase
	{
		private var op : AbstractOperation;
		
		[Bindable]
		private var _dataProviders : ArrayCollection;
		
		[Bindable]
		private var _dataProviders2 : ArrayCollection;
		
		private var _idTheme : int;		
							
		public function GraphCoutProduit(idtheme : String = null){ 
			super();			
			 
			_idTheme =parseInt(idtheme);
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		/**
		* Permet de passer les valeurs au composant.		
		* @param d Une collection d'objets. 
		**/						
		override public function set dataProviders(d : ArrayCollection):void{
			
			_dataProviders = _formatterDataProvider(d);// _formatterDataProvider(d);
			_dataProviders2 = _formatterDataProviderPie(d);			
			updateProviders();			
		}
		
		/**
		*  Retourne le dataproviders
		**/	
		[Bindable]			
		override public function get dataProviders():ArrayCollection{
			return _dataProviders;
		}	
	
					
		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		override public function clean():void{		
			graph.myPie.dataProvider = null;
			myGrid.dataProvider = null;				
		}
				
		/**
		 * Met à jour les donées du tableau
		 **/
		override public function update():void{			
			//chargerDonnees(); deprecated		
			 
		}	
				
		/*------------- PROTECTED ------------------------------------------------------------*/
		
		//formate les donnees
		protected override function _formatterDataProvider(d: ArrayCollection):ArrayCollection{		
			var tmpCollection : ArrayCollection = new ArrayCollection();	
			var ligne : Object;				
			
			_total = 0;
			for	(ligne in d){					
					tmpCollection.addItem(formateObject(d[ligne]));					   
			}
			
			var totalObject : Object = new Object();
			totalObject[ResourceManager.getInstance().getString('M312', 'Libell_')] = "TOTAL";
			totalObject[ResourceManager.getInstance().getString('M312', 'Montant')] = ConsoviewFormatter.formatEuroCurrency(_total,2);
			tmpCollection.addItem(totalObject);			
			
			 		
			return tmpCollection;
		}	
		
		//formate les donnees
		protected function _formatterDataProviderPie(d: ArrayCollection):ArrayCollection{		
			
			var tmpCollection : ArrayCollection = new ArrayCollection();	
			var ligne : Object;				
			var len : int = d.length;
			var obj : Object = new Object();
			var chartData : ArrayCollection = new ArrayCollection();
			
						
			_total = 0;
			
			//////////////////formatage des données
			for	(ligne in d){		
				tmpCollection.addItem(formateObjectPie(d[ligne]));					   
			}
			/////////////////
			chartData = ObjectUtil.copy(tmpCollection) as ArrayCollection;
			
			/////////////////On coupe s'il y a trop de données
			if (len > 10){
				var sum : Number  = 0;
			
				for (var i : int = 9 ; i < len; i++){
					obj = chartData.getItemAt(i);
					sum = sum + Math.abs(parseFloat(obj[ResourceManager.getInstance().getString('M312', 'Montant')]));					
				}
				
				var len2 : int =  chartData.length ;
				
				for (var j : int = 10; j < len2 ; j++){
					chartData.removeItemAt(10);
				} 	
										
				chartData.getItemAt(9)[ResourceManager.getInstance().getString('M312', 'Libell_')] = ResourceManager.getInstance().getString('M312', 'Autres___');
				chartData.getItemAt(9)[ResourceManager.getInstance().getString('M312', 'Montant')] = sum;					
			  
				
				 return chartData;
			}
			
			
			
			////////////////// si la somme des montant est null on retourne rien
			if(_total == 0){
				return null;
			}
			
			////////
			return tmpCollection;
		}	
					
		protected function formateObject(obj : Object):Object{
			var o : Object = new Object();
				
			_total = _total +  parseFloat(obj.MONTANT_FINAL);	
						
			o[ResourceManager.getInstance().getString('M312', 'Libell_')] 			= obj.LIBELLE_PRODUIT;		
			o[ResourceManager.getInstance().getString('M312', 'Type_de_produit')] 	= obj.TYPE_THEME;				
			o[ResourceManager.getInstance().getString('M312', 'Montant')]			= ConsoviewFormatter.formatEuroCurrency(obj.MONTANT_FINAL,2);											
			
			return o;
		}	
		
		protected function formateObjectPie(obj : Object):Object{
			var o : Object = new Object();
				
			_total = _total +  parseFloat(obj.MONTANT_FINAL);		
			
			if ( String(obj.LIBELLE_PRODUIT).length > 45 ) 	o[ResourceManager.getInstance().getString('M312', 'Libell_')] = String(obj.LIBELLE_PRODUIT).substr(0,40)+"....";
			else o[ResourceManager.getInstance().getString('M312', 'Libell_')] = obj.LIBELLE_PRODUIT;			
			
			o[ResourceManager.getInstance().getString('M312', 'Type_de_produit')] = obj.TYPE_THEME;					
			o[ResourceManager.getInstance().getString('M312', 'Montant')]= Math.abs(parseFloat(obj.MONTANT_FINAL));							
			return o;
		}	
		
		//-------Chargement des donnees----------- remoting ---------------------------------------------------//
		// Chargement des données par remoting
		override protected function chargerDonnees():void
		{	
		}
		 
		 
		override protected function chargerDonneesResultHandler(re :ResultEvent):void{
		 	/* try{
		 		 
		  		dataProviders =  re.result as ArrayCollection;	
		  		 
		  	 
		  		
		  	}catch(er : Error){		  	
		  		trace(er.message,"Répartition par Thèmes graph cout produit");
		  	} */
		  
		 }	
			
		/*--------------- PRIVATE ----------------*/
		
		//initialisation du composant
		private function init(fe :FlexEvent):void
		{
			title = ResourceManager.getInstance().getString('M312', 'R_partition_par_produits');
			myViewStack.width = 530;
		}
			
		private function updateProviders():void
		{
			myGrid.dataProvider = dataProviders;//dataProviders;		
			DataGridColumn(myGrid.columns[1]).setStyle("textAlign","right"); 		
			graph.callLater(updatePie);
		}
		
		private function updatePie():void
		{
			graph.myPie.dataProvider = _dataProviders2;
			graph.myLegend.visible = false;		
		}
		
	}
} 