<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.FSecureOther" extends="fr.consotel.api.mdm.partner.public.FSecureMultiLicence" hint="Implémentation Other Services FSecure">
	<!--- Other Services --->
	<cffunction access="remote" name="sendActivationSMS" returntype="String" hint="Envoi un SMS d'activation à un équipement et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var wsService=getSecurityService().getOtherService()>
		<cfreturn serializeJSON(wsService.sendActivationSMS(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="sendDownloadLink" returntype="String" hint="Envoi un lien de téléchargement à un équipement et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var wsService=getSecurityService().getOtherService()>
		<cfreturn serializeJSON(wsService.sendDownloadLink(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="getLicenseXML" returntype="String" hint="Retourne le contenu XML de la licence à utiliser pour activer le service de sécurité FSecure">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Multi License Key">
		<cfset var wsService=getSecurityService().getOtherService()>
		<cfreturn serializeJSON(wsService.getLicenseXML(ARGUMENTS["multiLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="getSMSLicenseKey" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour activer le service de sécurité FSecure">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var wsService=getSecurityService().getOtherService()>
		<cfreturn serializeJSON(wsService.getSMSLicenseKey(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="pregenerateKeycodes" returntype="String" hint="Génère NumberOfKeysToGenerate Single Key Codes dans le Multi License par défaut et retourne la liste">
		<cfargument name="numberOfKeysToGenerate" type="String" required="true" hint="Nombre de Single Key Code à générer">
		<cfset var keyCodes=[]>
		<cfset var wsService=getSecurityService().getOtherService()>
		<cfset var wskeyCodes=wsService.pregenerateKeycodes(ARGUMENTS["numberOfKeysToGenerate"])>
		<cfif isDefined("wskeyCodes")>
			<cfset keyCodes=wskeyCodes>
		</cfif>
		<cfreturn serializeJSON(keyCodes)>
	</cffunction>
	
	<cffunction access="remote" name="echo" returntype="String" hint="Retourne un ECHO du message">
		<cfargument name="echoMessage" type="String" required="true" hint="Message qui sera retourné par cette méthode">
		<cfset var wsService=getSecurityService().getOtherService()>
		<cfreturn serializeJSON(wsService.echo(ARGUMENTS["echoMessage"]))>
	</cffunction>
</cfcomponent>