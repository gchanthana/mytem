package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.fournir
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasEqpAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;

	public class RuleFournirEqp implements IRuleDisplayItem
	{
		public function RuleFournirEqp()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IsLigneMobile=new IsLigneMobile();
			var r2:IsLigneFixe=new IsLigneFixe();
			var r3:HasEqpAssociated=new HasEqpAssociated();
			
			if (r1.isDisplay())
				return true;
			else if (r2.isDisplay() && !r3.isDisplay())
				return true;
			else if (!r1.isDisplay() && !r2.isDisplay())
				return true;
			else
				return false;
		}
	}
}
