package gestionparc.services.ticketsappels
{
	import composants.util.FicheTicketVO;
	
	import flash.events.EventDispatcher;
	
	import gestionparc.event.FicheEvent;
	
	import mx.collections.ArrayCollection;

	public class TicketsAppelsDatas extends EventDispatcher
	{
		public var currencySymbol:String = '';
		private var _ficheTicket:ArrayCollection;
		private var _listeFacture:ArrayCollection;
		private var _nbTotalItem:Number = 0;
		
		public function TicketsAppelsDatas()
		{
			_ficheTicket = new ArrayCollection();
			_listeFacture = new ArrayCollection();
		}
		
		public function get nbTotalItem():Number
		{
			return _nbTotalItem;
		}
		
		public function set nbTotalItem(value:Number):void
		{
			_nbTotalItem = value;
		}
		
		public function get ficheTicket():ArrayCollection
		{
			return _ficheTicket;
		}
		
		public function set ficheTicket(value:ArrayCollection):void
		{
			_ficheTicket = value;
		}
		
		public function get listeFacture():ArrayCollection
		{
			return _listeFacture;
		}
		
		public function set listeFacture(value:ArrayCollection):void
		{
			_listeFacture = value;
		}
		
		internal function updateFicheTicket(value:ArrayCollection):void
		{		
			
			_ficheTicket = new ArrayCollection();
			
			if(value.length > 0){
				nbTotalItem = value[0].NBRECORD;
			}
			else{
				nbTotalItem = 0;
			}
			
			var temp:FicheTicketVO;
			for (var i:int=0; i < value.length; i++)
			{
				temp = new FicheTicketVO();
				temp.fill(value[i]);
				_ficheTicket.addItem(temp);
			}
			
			dispatchEvent(new FicheEvent(FicheEvent.FICHETICKET));
			
		}
		
		internal function updateListeFactures(value:ArrayCollection):void
		{
			_listeFacture = new ArrayCollection();
			
			_listeFacture = value;
			
			dispatchEvent(new FicheEvent(FicheEvent.LISTEFACTURES));
			
		}
	}
}