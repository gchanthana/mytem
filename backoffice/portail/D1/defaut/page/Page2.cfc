<cfcomponent implements="portail.class.specialClass.InterfacePage"
			 output="false"
			 displayname="Page" 
			 hint="Classe qui contient la structure de la page d'Accueil dans le template D1" >
	
	
	<!---  --->
	<cffunction name="afficher" access="public" output="true" 
				displayname="" 
				hint="Affiche les composants de la page d'Accueil / template D1" >
		<cfargument name="head" required="true" type="string" displayname="" hint="Contenu de la balise header de la page à afficher">
		<cfargument name="context" required="true" type="struct" displayname="" hint="Structure contenant les données passés dans l'URL">
		
		
		<!--- CREATION DUNE STRUC ET RECUPERATION DES DONNEES PASSEES EN URL DANS CETTE STRUCTURE --->
		<cfset structParam = createObject("component","portail.class.specialClass.AbstractComposant").getInfoSructURL(context) >
		
		<!--- METHODE DE GESTION AFFICHAGE DES DIVS DE LA PAGE 2(Acceuil) SELON LES DROITS DE LUTILISATEUR --->
		<cfset createObject("component","portail.class.classData.GestionDroitService").gestionDroitPage2(head,structParam) >
		
	</cffunction>

</cfcomponent>