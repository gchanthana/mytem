package intranet.GestionDesLogin.controlers
{
    import composants.util.ConsoviewAlert;
    import flash.display.DisplayObject;
    import flash.events.MouseEvent;
    import intranet.GestionDesLogin.AlertCreerLoginIHM;
    import intranet.GestionDesLogin.Alert_Creer_Login;
    import intranet.GestionDesLogin.Alert_Creer_LoginIHM;
    import intranet.GestionDesLogin.services.Service;
    import intranet.GestionDesLogin.vo.LoginExtranetVo;
    import mx.controls.Alert;
    import mx.core.IFlexDisplayObject;
    import mx.events.CloseEvent;
    import mx.managers.PopUpManager;
    import paginatedatagrid.columns.event.StandardColonneEvent;
    import paginatedatagrid.event.PaginateDatagridEvent;
    import paginatedatagrid.pagination.vo.ItemIntervalleVO;
    import searchpaginatedatagrid.ParametresRechercheVO;
    import searchpaginatedatagrid.event.SearchPaginateDatagridEvent;

    [Bindable]
    public class Controler
    {
        public function Controler()
        {
        }
        private var _service:Service;
        private var _itemInterval:ItemIntervalleVO;
        private var _currentParam:ParametresRechercheVO;
        private var _operateurid:Number = 0;
        private var _parentDisplayObject:DisplayObject;

        public function onOperateurComboChangeHandler(event:Event, operateurid:Number = 0):void
        {
            if (!_itemInterval) // Si c'est une nouvelle recherche
            {
                _itemInterval = new ItemIntervalleVO();
                _itemInterval.indexDepart = 0;
                _itemInterval.tailleIntervalle = 100;
            }
            _operateurid = operateurid;
            _service.getListeLogins(null, _itemInterval, operateurid);
        }

        public function onSearchHandler(event:SearchPaginateDatagridEvent, operateurid:Number = 0):void
        {
            if (!_itemInterval) // Si c'est une nouvelle recherche
            {
                event.currentTarget.paginateDatagridInstance.refreshPaginateDatagrid(true);
                _itemInterval = new ItemIntervalleVO();
                _itemInterval.indexDepart = 0;
                _itemInterval.tailleIntervalle = 100;
            }
            _operateurid = operateurid;
            _currentParam = event.parametresRechercheVO;
            _service.getListeLogins(_currentParam, _itemInterval, operateurid);
        }

        public function onIntervalleChangeHandler(event:PaginateDatagridEvent, operateurid:Number = 0):void
        {
            _service.getListeLogins(_currentParam, event.currentTarget.currentIntervalle, operateurid);
        }

        public function onDeleteHandler(data:Object):void
        {
            var deleteHandler:Function = function supprimerBtnValiderCloseEvent(e:CloseEvent):void
                {
                    if (e.detail == Alert.OK)
                    {
                        _service.deleteLogin(data as LoginExtranetVo);
                    }
                }
            ConsoviewAlert.afficherAlertConfirmation("Êtes vous sûr de vouloir supprimer le login " + data.LOGIN_EXTRANET + " ?", "Confirmation", deleteHandler);
        }

        public function onItemEdit(evt:StandardColonneEvent):void
        {
            var a:IFlexDisplayObject;
            Alert_Creer_Login.obj = evt.item;
            if (!a)
            {
                a = PopUpManager.createPopUp(_parentDisplayObject, AlertCreerLoginIHM, true);
            }
            else
            {
                PopUpManager.addPopUp(a, _parentDisplayObject, true);
            }
            PopUpManager.centerPopUp(a);
            a.addEventListener(CloseEvent.CLOSE, aCloseHandler);
        }

        public function onCreateHandler(e:MouseEvent):void
        {
            var a:IFlexDisplayObject;
            if (!a)
            {
                a = PopUpManager.createPopUp(_parentDisplayObject, AlertCreerLoginIHM, true);
            }
            else
            {
                PopUpManager.addPopUp(a, _parentDisplayObject, true);
            }
            PopUpManager.centerPopUp(a);
            a.addEventListener(CloseEvent.CLOSE, aCloseHandler);
        }

        private function aCloseHandler(e:CloseEvent):void
        {
            Alert_Creer_Login.obj = null;
            update();
        }

        public function update():void
        {
            if (_currentParam && _itemInterval)
            {
                _service.getListeLogins(_currentParam, _itemInterval, _operateurid);
            }
        }

        public function set service(value:Service):void
        {
            if (value)
            {
                _service = value;
            }
            else
            {
                _service = new Service();
            }
        }

        public function set parentDisplayObject(value:DisplayObject):void
        {
            _parentDisplayObject = value;
        }
    }
}