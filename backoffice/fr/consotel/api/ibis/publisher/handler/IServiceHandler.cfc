<cfinterface displayName="fr.consotel.api.ibis.publisher.handler.IServiceHandler" hint="OBSERVER Interface (Pattern : OBSERVER)">
	
	<!--- EVENT_HANDLER INTERFACE (Auteur : Cedric)
		Best Practices :
			- CFMX does not support OVERLOADING thus define handler per Event Component Type
	 --->
	
	<cffunction access="public" name="run" returntype="void" hint="Called with the event that triggered this handler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered this handler">
	
	</cffunction>
	
	<cffunction access="public" name="onError" returntype="void" hint="Called when an error has occurred either during creation or execution of IServiceHandler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered run() handler">
		<cfargument name="eventHandler" type="string" required="true" hint="The corresponding EVENT_HANDLER supposed to handle eventObject">
		<cfargument name="exceptionObject" type="any" required="false" hint="Optionally the corresponding catched exception (Same structure as CFCATCH scope)">
	</cffunction>
	
</cfinterface>