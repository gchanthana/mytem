package appli.login {
	import flash.events.IEventDispatcher;
	
	public interface ILoginModel extends IEventDispatcher {
		function connect(loginString:String,pwdString:String):void;
		
		function getAuthInfos():Object;
	}
}
