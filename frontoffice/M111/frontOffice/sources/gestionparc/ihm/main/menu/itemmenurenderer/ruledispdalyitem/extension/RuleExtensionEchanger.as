package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.extension
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasCollaborateurAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasEqpAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasLigneAssociated;
	
	public class RuleExtensionEchanger implements IRuleDisplayItem
	{
		public function RuleExtensionEchanger()
		{
		}
		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem = new HasEqpAssociated();
			var r2:IRuleDisplayItem = new HasCollaborateurAssociated();
			var r3:IRuleDisplayItem = new HasLigneAssociated();
			
			if((r1.isDisplay()) || (r2.isDisplay()) || (r3.isDisplay()))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}