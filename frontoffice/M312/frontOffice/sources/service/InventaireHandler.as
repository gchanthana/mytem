package service
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	public class InventaireHandler
	{
		private var _model:InventaireModel;
		
		public function InventaireHandler(model:InventaireModel)
		{
			_model = model;
		}
		
		
		internal function getMarqueEquipementResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateDatePeriodeCompleteInventaire(event.result.DATE_COMPLETE_INVENTAIRE  as Date, Number(event.result.IDDATE_COMPLETE_INVENTAIRE));
				_model.updateMarqueEquipement(event.result.LISTE_MARQUE as ArrayCollection);
			}
		} 
		
		internal function getModeleEquepementResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateModeleEquepement(event.result as ArrayCollection)
			}
		}
		
		internal function getListEquepementResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateListEquipement(event.result as ArrayCollection)	
			}
		}		
		
		
	}
}