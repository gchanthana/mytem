﻿<cfcomponent displayname="AccueilService" hint="Gère les appels backoffice du module M512" output="false">
	<cfset dataSourceName = "BI_TEST">
	<cfset AccueilPFGPItemVO = structNew()>
	<cfset ID_ORGANISATION = getIdOrga(session.perimetre.ID_PERIMETRE) >
	<cfset NIVEAU = getNiveauPerimetre(session.perimetre.ID_PERIMETRE) >
	<cfset IDNIVEAU = getIdNiveau(NIVEAU) >
	<cfset IDPERIODE = 0>
	
	<cffunction name="getDateDataCompleted" access="remote" hint="Récupère le xml qui permet la construction du menu M512" returntype="Struct">
		<cfset obj = structNew()>		
		<cfset _idracine = SESSION.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#SESSION.OFFREDSN#" blockfactor="1" procedure="pkg_m00.get_last_periode_parc">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_racine" value="#_idracine#">
			<cfprocresult name="p_date_complete_inventaire">
		</cfstoredproc>
		
		<cfif p_date_complete_inventaire.recordCount gt 0>
			<cfset obj.IDPERIODE = p_date_complete_inventaire["LAST_IDPERIODE_MOIS"][1]>	
			<cfset obj.DATE = p_date_complete_inventaire["LAST_MOIS"][1]>
						 
			<cfelse>
			
					<cfquery datasource="#SESSION.OFFREDSN#" name="rsltIdPeriodeMois">
						SELECT CV_GET_IDPERIODE_MOIS('#lsDateFormat(SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN,'yyyy/mm/dd')#')  as IDPERIOD FROM DUAL
					</cfquery>
					
					<cfset SESSION.DATES ={}>
					<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m00.get_last_periode_complete">			
						<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER" type="in">
						<cfprocresult name="qDATELASTPFGP">
					</cfstoredproc>
					
					<cfif qDATELASTPFGP.recordcount GT 0>
						<cfset SESSION.DATES.LASTPFGPLOAD = qDATELASTPFGP['LAST_MOIS'][1]>
						<cfset SESSION.DATES.LASTPFGPIDPERIODE = qDATELASTPFGP['LAST_IDPERIODE_MOIS'][1]>
					<cfelse>
						<cfset SESSION.DATES.LASTPFGPLOAD = SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN>
						<cfset SESSION.DATES.LASTPFGPIDPERIODE = rsltIdPeriodeMois.IDPERIOD>
					</cfif>
				 
				
				<cfset obj.IDRACINE=_idracine>
				<cfset obj.IDPERIODE = SESSION.DATES.LASTPFGPIDPERIODE>
				<cfset obj.DATE = SESSION.DATES.LASTPFGPLOAD>			
				
		</cfif>
		
		<cfreturn obj>
	</cffunction> 
	
	<cffunction name="getPerimetre" access="remote" hint="Récupère le xml qui permet la construction du menu M512" returntype="xml">
		<cfset currentPath = GetDirectoryFromPath(GetCurrentTemplatePath())/>
		<cfset fileName = currentPath & "MenuAccueil.xml">
		<cflog text="filename = #fileName#}"/>
		<cffile action="read" file="#fileName#" variable="XMLFileText">
		<cfset xmlAccess=XmlParse(XMLFileText)>
		<cfreturn session.XML_ACCESS_PFGP_ACCUEIL>
	</cffunction>
	
	<cffunction name="getIdOrga" output="false" access="private" returntype="Any">
		<cfargument name="idgroupe_client" type="numeric" required="true">		
		
		<cfquery datasource="#SESSION.OFFREDSN#" name="rsltOrga">
			SELECT PKG_CV_RAGO.GET_ORGA(#idgroupe_client#) as IDORGA FROM DUAL
		</cfquery>
		<cfif rsltOrga.recordCount gt 0>
		
			<cfif rsltOrga['IDORGA'][1] gt 0>
				<cfreturn rsltOrga['IDORGA'][1]>
			<cfelse>
				<cfreturn SESSION.PERIMETRE.ID_GROUPE>
			</cfif>
			
		<cfelse>
			<cfreturn -1>
		</cfif>
	</cffunction>
	<cffunction name="getNiveauPerimetre" output="false" access="private" returntype="string">
		<cfargument name="idgroupe_client" type="numeric" required="true">			 		
		
		<cfquery datasource="#SESSION.OFFREDSN#" name="rsltNiveau">
			SELECT 
			niv.NUMERO_NIVEAU
			FROM 
			groupe_client gc,
			orga_niveau niv 
			WHERE gc.idgroupe_client = #idgroupe_client#
			AND niv.idorga_niveau = gc.idorga_niveau
		</cfquery>
		
		<cfif rsltNiveau.recordCount gt 0>		
			<cfif rsltNiveau['NUMERO_NIVEAU'][1] neq ''>
				<cfreturn rsltNiveau['NUMERO_NIVEAU'][1]>
			<cfelse>
				<cfreturn 'B'>
			</cfif>
		<cfelse>
			<cfreturn 'B'>
		</cfif>
	</cffunction>
	
	<cffunction name="getIndicateurCle" access="remote" hint="Récupère les indicateurs clés de l'accueil PILOTAGE" returntype="Struct" >
		<cfargument name="idperiode" type="numeric" hint="l'id periode du dernier mois complet" default="133">
		
		<cfset VARIABLE['IDPERIODE'] = arguments['idperiode']>
		<cflog text="[ACCUEIL SERVICE]getIndicateurCle(#idperiode#)">
		
		
		<cfset 	VARIABLES['AccueilPFGPItemVO'].STATUS =1>
		<cfthread action="run" name="t1" state="#VARIABLES['AccueilPFGPItemVO'].STATUS#" periodeid="#idperiode#">
			<cfset state=state * NbLigneMobile(periodeid)>
			<cflock
				name="stateAccess"
				type="exclusive"
				timeout="20">
				<cfset VARIABLES['AccueilPFGPItemVO'].STATUS =  state>
			</cflock>
		</cfthread>
		
		<cfthread action="run" name="t2"  state="#VARIABLES['AccueilPFGPItemVO'].STATUS#"  periodeid="#idperiode#">
			<cfset state=state * NbOuvertureLigne(periodeid)>		
			<cflock
				name="stateAccess"
				type="exclusive"
				timeout="20">
				<cfset VARIABLES['AccueilPFGPItemVO'].STATUS =  state>
			</cflock>
		</cfthread>
		
		<cfthread action="run" name="t3"  state="#VARIABLES['AccueilPFGPItemVO'].STATUS#"  periodeid="#idperiode#">			
			<cfset state=state * NbResiliationLigne(periodeid)>	
			<cflock
				name="stateAccess"
				type="exclusive"
				timeout="20">
				<cfset VARIABLES['AccueilPFGPItemVO'].STATUS =  state>
			</cflock>
		</cfthread>
		
		<cfthread action="run" name="t4"  state="#VARIABLES['AccueilPFGPItemVO'].STATUS#"  periodeid="#idperiode#">
			<cfset state=state * CoutMoyenLigne(periodeid) >
			<cflock
				name="stateAccess"
				type="exclusive"
				timeout="20">
				<cfset VARIABLES['AccueilPFGPItemVO'].STATUS =  state>
			</cflock>
		</cfthread>
		
		<cfthread action="run" name="t5"  state="#VARIABLES['AccueilPFGPItemVO'].STATUS#"  periodeid="#idperiode#">
			<cfset state=state * NbLigneEligible(periodeid) >
			<cflock
				name="stateAccess"
				type="exclusive"
				timeout="20">
				<cfset VARIABLES['AccueilPFGPItemVO'].STATUS =  state>
			</cflock>		
		</cfthread>
		
		<cfthread action="join" timeout="0" name="t1,t2,t3,t4,t5" >
			 
		</cfthread> 	
			
		<cfreturn VARIABLES['AccueilPFGPItemVO']>
	</cffunction>
	
	
	<cffunction name="NbLigneMobile" access="private" returntype="Numeric"  
				hint="récupère le nombre de ligne mobile à mois M et M-1. 
					  Retourne 0 en cas d'erreur, 1 en cas de réussite, 2 en cas de résultat improbable">
		<cfargument name="idperiode_mois" type="numeric" hint="l'id periode du dernier mois complet">
		<cflog text="[ACCUEIL SERVICE]Phase 1 : KPI nombre de lignes mobiles">
		<cftry>
			<cfquery datasource="BI_TEST" name="rsltNbLigneMobile">
	      		set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
								p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idorga=#VARIABLES['ID_ORGANISATION']#,
								p_idperimetre=#SESSION.PERIMETRE.ID_PERIMETRE#,		
								p_idniveau=#VARIABLES['IDNIVEAU']#,						 
								p_niveau='#VARIABLES['NIVEAU']#';
				
				SELECT PERIODE.IDPERIODE_MOIS PER, FACTURATION.NB_MOBILE NBLIGNE
				FROM E0_AGGREGS
				WHERE PERIODE.IDPERIODE_MOIS >= #idperiode_mois# - 1
				AND PERIODE.IDPERIODE_MOIS <= #idperiode_mois#
				ORDER BY PERIODE.IDPERIODE_MOIS 
	        </cfquery>
			
			<cfset status = 0>
			<cfif rsltNbLigneMobile.RecordCount eq 2>
				<cfif rsltNbLigneMobile["PER"][1] eq VARIABLE['IDPERIODE']>
					<cfset idencours = 1>
					<cfset oldid = 2>
				<cfelse>
					<cfset idencours = 2>
					<cfset oldid = 1>
				</cfif>
				<cfif  rsltNbLigneMobile["NBLIGNE"][idencours] neq 'null'>
					<cfset VARIABLES["AccueilPFGPItemVO"].NbLigneMobile = rsltNbLigneMobile["NBLIGNE"][idencours]>
					<cfset status = 1>
				<cfelse>
					<cfset VARIABLES["AccueilPFGPItemVO"].NbLigneMobile = '0'>
					<cfset status = 2>
				</cfif>
				<cfif  rsltNbLigneMobile["NBLIGNE"][oldid] neq 'null'>
					<cfset VARIABLES["AccueilPFGPItemVO"].OldNbLigneMobile = rsltNbLigneMobile["NBLIGNE"][oldid]>
					<cfset status = 1>
				<cfelse>
					<cfset VARIABLES["AccueilPFGPItemVO"].OldNbLigneMobile = '0'>
					<cfset status = 2>
				</cfif>
			<cfelseif rsltNbLigneMobile.RecordCount eq 1>
				<cfif rsltNbLigneMobile["PER"][1] eq VARIABLE['IDPERIODE']>
					<cfif  rsltNbLigneMobile["NBLIGNE"][1] neq 'null'>
						<cfset VARIABLES["AccueilPFGPItemVO"].NbLigneMobile = rsltNbLigneMobile["NBLIGNE"][1]>
					<cfelse>
						<cfset VARIABLES["AccueilPFGPItemVO"].NbLigneMobile = '0'>
					</cfif>
					<cfset VARIABLES["AccueilPFGPItemVO"].OldNbLigneMobile = '0'>
					<cfset status = 2>
				<cfelse>
					<cfif  rsltNbLigneMobile["NBLIGNE"][1] neq 'null'>
						<cfset VARIABLES["AccueilPFGPItemVO"].OldNbLigneMobile = rsltNbLigneMobile["NBLIGNE"][1]>
					<cfelse>
						<cfset VARIABLES["AccueilPFGPItemVO"].OldNbLigneMobile = '0'>
					</cfif>
					<cfset VARIABLES["AccueilPFGPItemVO"].NbLigneMobile = '0'>
					<cfset status = 2>
				</cfif>
			<cfelse>
				<cfset VARIABLES["AccueilPFGPItemVO"].OldNbLigneMobile = '0'>
				<cfset VARIABLES["AccueilPFGPItemVO"].NbLigneMobile = '0'>
				<cfset status = 2>
			</cfif>
			<cfreturn status>
		
		<cfcatch type="any">
			<cflog text="[ACCUEIL SERVICE] NbLigneMobile.CFCATCH = #cfcatch#">
			<cfreturn 0>
		</cfcatch>
		</cftry>
	</cffunction> 
	<cffunction name="NbOuvertureLigne" access="private" returntype="Numeric" 
				hint="récupère le nombre de ligne ouverte à mois M et M-1. 
					  Retourne 0 en cas d'erreur, 1 en cas de réussite, 2 en cas de résultat improbable">
		<cfargument name="idperiode_mois" type="numeric" hint="l'id periode du dernier mois complet">
		<cflog text="[ACCUEIL SERVICE]Phase 2 : KPI nombre d'ouverture de lignes">
		<cftry>
			<cfquery datasource="BI_TEST" name="rsltNbOuvertureLigne">
	      		set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
								p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idorga=#VARIABLES['ID_ORGANISATION']#,
								p_idperimetre=#SESSION.PERIMETRE.ID_PERIMETRE#,			
								p_idniveau=#VARIABLES['IDNIVEAU']#,					 
								p_niveau='#VARIABLES['NIVEAU']#';
				
				
				SELECT saw_1 per, count(distinct saw_0) OUVERTURES 
				FROM (
				      (SELECT ORGA1CLIENT_HIST.IDSOUS_TETE saw_0,  PERIODE.IDPERIODE_MOIS saw_1
				         FROM E0_INV_LIGNES WHERE PERIODE.IDPERIODE_MOIS >= #idperiode_mois# - 1 
				         AND PERIODE.IDPERIODE_MOIS  <= #idperiode_mois# 
				       ) 
				       EXCEPT 
				      (SELECT ORGA1CLIENT_HIST.IDSOUS_TETE saw_0,  PERIODE.IDPERIODE_MOIS + 1 saw_1 
				         FROM E0_INV_LIGNES WHERE PERIODE.IDPERIODE_MOIS >= #idperiode_mois# - 2 
				         AND PERIODE.IDPERIODE_MOIS <= #idperiode_mois# -1
				       )
				     ) t1 GROUP BY saw_1
				ORDER BY saw_1
	        </cfquery>
			
			
			<cfset status = 0>
			<cfif rsltNbOuvertureLigne.RecordCount eq 2>
				<cfif rsltNbOuvertureLigne["PER"][1] eq VARIABLE['IDPERIODE']>
					<cfset idencours = 1>
					<cfset oldid = 2>
				<cfelse>
					<cfset idencours = 2>
					<cfset oldid = 1>
				</cfif>
				<cfif  rsltNbOuvertureLigne["OUVERTURES"][idencours] neq 'null'>
					<cfset VARIABLES["AccueilPFGPItemVO"].NbOuvertureLigne = rsltNbOuvertureLigne["OUVERTURES"][idencours]>
					<cfset status = 1>
				<cfelse>
					<cfset VARIABLES["AccueilPFGPItemVO"].NbOuvertureLigne = '0'>
					<cfset status = 2>
				</cfif>	
				<cfif  rsltNbOuvertureLigne["OUVERTURES"][oldid] neq 'null'>
					<cfset VARIABLES["AccueilPFGPItemVO"].OldNbOuvertureLigne = rsltNbOuvertureLigne["OUVERTURES"][oldid]>
					<cfset status = 1>
				<cfelse>
					<cfset VARIABLES["AccueilPFGPItemVO"].OldNbOuvertureLigne = '0'>
					<cfset status = 2>
				</cfif>
			<cfelseif rsltNbOuvertureLigne.RecordCount eq 1>
				<cfif rsltNbOuvertureLigne["PER"][1] eq VARIABLE['IDPERIODE']>
					<cfif  rsltNbOuvertureLigne["OUVERTURES"][1] neq 'null'>
						<cfset VARIABLES["AccueilPFGPItemVO"].NbOuvertureLigne = rsltNbOuvertureLigne["OUVERTURES"][1]>
					<cfelse>
						<cfset VARIABLES["AccueilPFGPItemVO"].NbOuvertureLigne = '0'>
					</cfif>	
					<cfset VARIABLES["AccueilPFGPItemVO"].OldNbOuvertureLigne = '0'>
					<cfset status = 2>
				<cfelse>
					<cfif  rsltNbOuvertureLigne["OUVERTURES"][1] neq 'null'>
						<cfset VARIABLES["AccueilPFGPItemVO"].OldNbOuvertureLigne = rsltNbOuvertureLigne["OUVERTURES"][1]>
					<cfelse>
						<cfset VARIABLES["AccueilPFGPItemVO"].OldNbOuvertureLigne = '0'>
					</cfif>	
					<cfset VARIABLES["AccueilPFGPItemVO"].NbOuvertureLigne = '0'>
					<cfset status = 2>
				</cfif>
			<cfelse>
				<cfset VARIABLES["AccueilPFGPItemVO"].OldNbOuvertureLigne = '0'>
				<cfset VARIABLES["AccueilPFGPItemVO"].NbOuvertureLigne = '0'>
				<cfset status = 2>
			</cfif>
			<cfreturn status>
		
		<cfcatch type="any" >
			<cflog text="[ACCUEIL SERVICE] NbOuvertureLigne.CFCATCH = #cfcatch#">
			<cfreturn 0>
		</cfcatch>
		</cftry>
	</cffunction> 
	<cffunction name="NbResiliationLigne" access="private" returntype="Numeric" 
				hint="récupère le nombre de ligne résiliée à mois M et M-1. 
					  Retourne 0 en cas d'erreur, 1 en cas de réussite, 2 en cas de résultat improbable">
		<cfargument name="idperiode_mois" type="numeric" hint="l'id periode du dernier mois complet">
		<cflog text="[ACCUEIL SERVICE]Phase 3 : KPI nombre de ligne résiliée">
		<cftry>
			<cfquery datasource="BI_TEST" name="rsltNbResiliationLigne">
	      		set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
								p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idorga=#VARIABLES['ID_ORGANISATION']#,
								p_idperimetre=#SESSION.PERIMETRE.ID_PERIMETRE#,								 
								p_niveau='#VARIABLES['NIVEAU']#',
								p_idniveau=#VARIABLES['IDNIVEAU']#;
				
				SELECT saw_1 per, count(distinct saw_0) resiliations 
				FROM (
				      (SELECT ORGA1CLIENT_HIST.IDSOUS_TETE saw_0,  PERIODE.IDPERIODE_MOIS + 1 saw_1
				         FROM E0_INV_LIGNES WHERE PERIODE.IDPERIODE_MOIS >= #idperiode_mois# - 2 
				         AND PERIODE.IDPERIODE_MOIS  <= #idperiode_mois# -1
				      ) 
				       EXCEPT 
				      (SELECT ORGA1CLIENT_HIST.IDSOUS_TETE saw_0,  PERIODE.IDPERIODE_MOIS saw_1 
				         FROM E0_INV_LIGNES WHERE PERIODE.IDPERIODE_MOIS >= #idperiode_mois# - 1 
				         AND PERIODE.IDPERIODE_MOIS <= #idperiode_mois# 
				      )
				     ) t1 GROUP BY saw_1
				ORDER BY saw_1
			
	        </cfquery>
			
			<cfset status = 0>
			<cfif rsltNbResiliationLigne.RecordCount eq 2>
				<cfif rsltNbResiliationLigne["PER"][1] eq VARIABLE['IDPERIODE']>
					<cfset idencours = 1>
					<cfset oldid = 2>
				<cfelse>
					<cfset idencours = 2>
					<cfset oldid = 1>
				</cfif>
				<cfif  rsltNbResiliationLigne["RESILIATIONS"][idencours] neq 'null'>
						<cfset VARIABLES["AccueilPFGPItemVO"].NbResiliation = rsltNbResiliationLigne["RESILIATIONS"][idencours]>
						<cfset status = 1>
					<cfelse>
						<cfset VARIABLES["AccueilPFGPItemVO"].NbResiliation = '0'>
						<cfset status = 2>
					</cfif>
					<cfif  rsltNbResiliationLigne["RESILIATIONS"][oldid] neq 'null'>
						<cfset VARIABLES["AccueilPFGPItemVO"].OldNbResiliation = rsltNbResiliationLigne["RESILIATIONS"][oldid]>
						<cfset status = 1>
					<cfelse>
						<cfset VARIABLES["AccueilPFGPItemVO"].OldNbResiliation = '0'>
						<cfset status = 2>
					</cfif>
			<cfelseif rsltNbResiliationLigne.RecordCount eq 1>
				<cfif rsltNbResiliationLigne["PER"][1] eq VARIABLE['IDPERIODE']>
					<cfif  rsltNbResiliationLigne["RESILIATIONS"][1] neq 'null'>
						<cfset VARIABLES["AccueilPFGPItemVO"].NbResiliation = rsltNbResiliationLigne["RESILIATIONS"][1]>
					<cfelse>
						<cfset VARIABLES["AccueilPFGPItemVO"].NbResiliation = '0'>
					</cfif>
					<cfset VARIABLES["AccueilPFGPItemVO"].OldNbResiliation = '0'>
					<cfset status = 2>
				<cfelse>
					<cfif  rsltNbResiliationLigne["RESILIATIONS"][1] neq 'null'>
						<cfset VARIABLES["AccueilPFGPItemVO"].OldNbResiliation = rsltNbResiliationLigne["RESILIATIONS"][1]>
					<cfelse>
						<cfset VARIABLES["AccueilPFGPItemVO"].OldNbResiliation= '0'>
					</cfif>
					<cfset VARIABLES["AccueilPFGPItemVO"].NbResiliation = '0'>
					<cfset status = 2>
				</cfif>
			<cfelse>
				<cfset VARIABLES["AccueilPFGPItemVO"].OldNbResiliation = '0'>
				<cfset VARIABLES["AccueilPFGPItemVO"].NbResiliation = '0'>
				<cfset status = 2>
			</cfif>
			<cfreturn status>
		
		<cfcatch type="any">
			<cflog text="[ACCUEIL SERVICE] NbResiliationLigne.CFCATCH = #cfcatch#">
			<cfreturn 0>
		</cfcatch>
		</cftry>
	</cffunction> 
	<cffunction name="CoutMoyenLigne" access="private" returntype="Numeric" 
				hint="Définit le cout moyen par ligne. 
					  Retourne 0 en cas d'erreur, 1 en cas de réussite, 2 en cas de résultat improbable">
		<cfargument name="idperiode_mois" type="numeric" hint="l'id periode du dernier mois complet">
		<cflog text="[ACCUEIL SERVICE]Phase 4 : KPI CoutMoyenLigne">
		<cftry>
			<cfquery datasource="BI_TEST" name="rsltCoutMoyenLigne">
	      		set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
								p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idorga=#VARIABLES['ID_ORGANISATION']#,
								p_idperimetre=#SESSION.PERIMETRE.ID_PERIMETRE#,								 
								p_niveau='#VARIABLES['NIVEAU']#',
								p_idniveau=#VARIABLES['IDNIVEAU']#;
				
				SELECT PERIODE.IDPERIODE_MOIS per, (FACTURATION.MONTANT / FACTURATION.NB_MOBILE) coutmoyen
				FROM E0_AGGREGS
				WHERE PERIODE.IDPERIODE_MOIS >= #idperiode_mois# -1
				AND PERIODE.IDPERIODE_MOIS <= #idperiode_mois#
				ORDER BY PERIODE.IDPERIODE_MOIS 
			
	        </cfquery>		
			<cfset status = 0>
			<cfif rsltCoutMoyenLigne.RecordCount eq 2>
				<cfif rsltCoutMoyenLigne["PER"][1] eq VARIABLE['IDPERIODE']>
					<cfset idencours = 1>
					<cfset oldid = 2>
				<cfelse>
					<cfset idencours = 2>
					<cfset oldid = 1>
				</cfif>
				<cfif  rsltCoutMoyenLigne["COUTMOYEN"][idencours] neq 'null'>
					<cfset VARIABLES["AccueilPFGPItemVO"].CoutMoyenLigne = rsltCoutMoyenLigne["COUTMOYEN"][idencours]>
					<cfset status = 1>
				<cfelse>
					<cfset VARIABLES["AccueilPFGPItemVO"].CoutMoyenLigne = '0'>
					<cfset status = 2>
				</cfif>
				<cfif  rsltCoutMoyenLigne["COUTMOYEN"][oldid] neq 'null'>
					<cfset VARIABLES["AccueilPFGPItemVO"].OldCoutMoyenLigne = rsltCoutMoyenLigne["COUTMOYEN"][oldid]>
					<cfset status = 1>
				<cfelse>
					<cfset VARIABLES["AccueilPFGPItemVO"].OldCoutMoyenLigne = '0'>
					<cfset status = 2>
				</cfif>
			<cfelseif rsltCoutMoyenLigne.RecordCount eq 1>
				<cfif rsltCoutMoyenLigne["PER"][1] eq VARIABLE['IDPERIODE']>
					<cfif  rsltCoutMoyenLigne["COUTMOYEN"][1] neq 'null'>
						<cfset VARIABLES["AccueilPFGPItemVO"].CoutMoyenLigne = rsltCoutMoyenLigne["COUTMOYEN"][1]>
					<cfelse>
						<cfset VARIABLES["AccueilPFGPItemVO"].CoutMoyenLigne = '0'>
					</cfif>
					<cfset VARIABLES["AccueilPFGPItemVO"].OldCoutMoyenLigne = '0'>
					<cfset status = 2>
				<cfelse>
					<cfif  rsltCoutMoyenLigne["COUTMOYEN"][1] neq 'null'>
						<cfset VARIABLES["AccueilPFGPItemVO"].OldCoutMoyenLigne = rsltCoutMoyenLigne["COUTMOYEN"][1]>
					<cfelse>
						<cfset VARIABLES["AccueilPFGPItemVO"].OldCoutMoyenLigne = '0'>
					</cfif>
					<cfset VARIABLES["AccueilPFGPItemVO"].CoutMoyenLigne = '0'>
					<cfset status = 2>
				</cfif>
			<cfelse>
				<cfset VARIABLES["AccueilPFGPItemVO"].OldCoutMoyenLigne = '0'>
				<cfset VARIABLES["AccueilPFGPItemVO"].CoutMoyenLigne = '0'>
				<cfset status = 2>
			</cfif>
			<cfreturn status>
		
		<cfcatch type="any">
			<cflog text="[ACCUEIL SERVICE] CoutMoyenLigne.CFCATCH = #cfcatch#">
			<cfreturn 0>
		</cfcatch>
		</cftry>
	</cffunction> 
	<cffunction name="NbLigneEligible" access="private" returntype="Any" 
				hint="Retourne le nombre de lignes éligibles pour le renouvellement. 
					  Retourne 0 en cas d'erreur, 1 en cas de réussite, 2 en cas de résultat improbable">
		<cfargument name="idperiode_mois" type="numeric" hint="l'id periode du dernier mois complet">
		<cflog text="[ACCUEIL SERVICE]Phase 5 : KPI Nombre de Lignes eligible au renouvellement">
		<cftry>
			<cfquery datasource="BI_TEST" name="rsltNbLigneEligible">
	      		set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
								p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idorga=#VARIABLES['ID_ORGANISATION']#,
								p_idperimetre=#SESSION.PERIMETRE.ID_PERIMETRE#,		
								p_idniveau=#VARIABLES['IDNIVEAU']#,						 
								p_niveau='#VARIABLES['NIVEAU']#';
				
				
				SELECT PFGP_PARC.IDPERIODE_MOIS per, COUNT(DISTINCT ORGA1CLIENT_HIST.IDSOUS_TETE) eligibles
				FROM E0_ENPROD_HIST_RH WHERE PFGP_PARC.IDPERIODE_MOIS >= #idperiode_mois# - 1
				AND PFGP_PARC.IDPERIODE_MOIS <= #idperiode_mois# 
				AND PFGP_PARC.DT_ELIGIBILITE <= CURRENT_TIMESTAMP
				ORDER BY PFGP_PARC.IDPERIODE_MOIS
			
	        </cfquery>
			
			<cfif rsltNbLigneEligible.RecordCount eq 2>
				<cfif rsltNbLigneEligible["PER"][1] eq VARIABLE['IDPERIODE']>
					<cfset idencours = 1>
					<cfset oldid = 2>
				<cfelse>
					<cfset idencours = 2>
					<cfset oldid = 1>
				</cfif>
				<cfif  rsltNbLigneEligible["ELIGIBLES"][idencours] neq 'null'>
					<cfset VARIABLES["AccueilPFGPItemVO"].NbLigneEligible = rsltNbLigneEligible["ELIGIBLES"][idencours]>
					<cfset status = 1>
				<cfelse>
					<cfset VARIABLES["AccueilPFGPItemVO"].NbLigneEligible = '0'>
					<cfset status = 2>
				</cfif>
				<cfif  rsltNbLigneEligible["ELIGIBLES"][oldid] neq 'null'>
					<cfset VARIABLES["AccueilPFGPItemVO"].OldNbLigneEligible = rsltNbLigneEligible["ELIGIBLES"][oldid]>
					<cfset status = 1>
				<cfelse>
					<cfset VARIABLES["AccueilPFGPItemVO"].OldNbLigneEligible = '0'>
					<cfset status = 2>
				</cfif>
			<cfelseif rsltNbLigneEligible.RecordCount eq 1>
				<cfif rsltNbLigneEligible["PER"][1] eq VARIABLE['IDPERIODE']>
					<cfif  rsltNbLigneEligible["ELIGIBLES"][1] neq 'null'>
						<cfset VARIABLES["AccueilPFGPItemVO"].NbLigneEligible = rsltNbLigneEligible["ELIGIBLES"][1]>
					<cfelse>
						<cfset VARIABLES["AccueilPFGPItemVO"].NbLigneEligible = '0'>
					</cfif>
					<cfset VARIABLES["AccueilPFGPItemVO"].OldNbLigneEligible = '0'>
					<cfset status = 2>
				<cfelse>
					<cfif  rsltNbLigneEligible["ELIGIBLES"][1] neq 'null'>
						<cfset VARIABLES["AccueilPFGPItemVO"].OldNbLigneEligible = rsltNbLigneEligible["ELIGIBLES"][1]>
					<cfelse>
						<cfset VARIABLES["AccueilPFGPItemVO"].OldNbLigneEligible = '0'>
					</cfif>
					<cfset VARIABLES["AccueilPFGPItemVO"].NbLigneEligible = '0'>
					<cfset status = 2>
				</cfif>
			<cfelse>
				<cfset VARIABLES["AccueilPFGPItemVO"].OldNbLigneEligible = '0'>
				<cfset VARIABLES["AccueilPFGPItemVO"].NbLigneEligible = '0'>
				<cfset status = 2>
			</cfif>
			<cfreturn status>
		
		<cfcatch type="any">
			<cflog text="[ACCUEIL SERVICE] NbLigneEligible.CFCATCH = #cfcatch#">
			<cfreturn 0>
		</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction name="getData1" access="remote" returntype="any" description="les donne�s provenant de E0">
		<cfargument name="source" type="string" required="true" default="ONE">
			
			<cfset dataTable = "E0_AGGREGS_DEFRAG">
			<cfswitch expression="#SESSION.PERIMETRE.NIVEAU#">
				<cfcase value="A,B,C,D,E,F" delimiters=",">
					<cfset dataTable = "E0_AGGREGS_DEFRAG">
				</cfcase>
				<cfdefaultcase>
					<cfset dataTable = "E0_ENPROD_HIST">	
				</cfdefaultcase>
			</cfswitch>
			
			<cfquery datasource="#SESSION.OFFREDSN#" name="qPeriode">
				 SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS  
			 	 FROM	PERIODE
			 	 WHERE	PERIODE.SHORT_MOIS=to_Char(#SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN#,'mm/yyyy')
			</cfquery>
			
			<cfquery datasource="#dataSourceName#" name="qQuery">				
				set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
								p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idorga=#SESSION.PERIMETRE.ID_ORGANISATION#,
								p_idperimetre=#SESSION.PERIMETRE.ID_PERIMETRE#,								 
								p_idcliche=#SESSION.PERIMETRE.ID_LAST_CLICHE#,
								p_niveau='#SESSION.PERIMETRE.NIVEAU#',
								p_idniveau=#VARIABLES['IDNIVEAU']#,
								p_langue_pays='#SESSION.USER.GLOBALIZATION#';
				
				SELECT PERIODE.LIBELLE_PERIODE saw_10, FACTURATION.MONTANT saw_11, PERIODE.IDPERIODE_MOIS saw_12, PRODUIT1GROUPE.TYPE_THEME saw_13 
				FROM #dataTable#
				WHERE PERIODE.IDPERIODE_MOIS BETWEEN #Evaluate(qPeriode.IDPERIODE_MOIS-12)# AND #qPeriode.IDPERIODE_MOIS# 
				AND PRODUIT1GROUPE.IDSEGMENT_THEME = 2
				ORDER BY saw_12
			</cfquery>
			
		<cfreturn qQuery>
	</cffunction>
	<cffunction name="getData2" access="remote" returntype="any" description="les donne�s provenant de E0">
		<cfargument name="source" type="string" required="true" default="TWO">					
			
			<cfquery datasource="#SESSION.OFFREDSN#" name="qPeriode">
				 SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS  
			 	 FROM	PERIODE
			 	 WHERE	PERIODE.SHORT_MOIS=to_Char(#SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN#,'mm/yyyy')
			</cfquery>
			
			<cfquery datasource="#dataSourceName#" name="qQuery">	
				
				set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
								p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idorga=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idperimetre=#SESSION.PERIMETRE.ID_GROUPE#,
								p_niveau='B',
								p_idniveau=1,
								p_langue_pays='#SESSION.USER.GLOBALIZATION#';

				SELECT ORGAOPERATEUR.OPERATEUR saw_0, 
							FACTURATION.NOMBRE_FACTURES saw_1, 
							PERIODE.DESC_MOIS saw_2, 
							PERIODE.SHORT_MOIS saw_3, 
							PERIODE.DESC_ANNEE saw_4, 
							PERIODE.IDPERIODE_MOIS saw_5
				FROM E0_AGGREGS_DEFRAG
				WHERE RCOUNT(PERIODE.IDPERIODE_MOIS) < 4
				AND (ORGAOPERATEUR.OPERATEURID = 1438 or ORGAOPERATEUR.OPERATEURID = 2159)
				ORDER BY saw_5 DESC
			</cfquery>

			<cfquery name="q1" dbtype="query">
				SELECT saw_1,saw_0, saw_2,saw_5,saw_3 FROM qQuery GROUP BY saw_1,saw_0, saw_2,saw_5,saw_3 ORDER BY saw_5 asc
			</cfquery>	
			
	 	<cfreturn q1>
	</cffunction>
	<cffunction name="getData3" access="remote" returntype="any" description="les donne�s provenant de E0">
		<cfargument name="source" type="string" required="true" default="THREE">
		<cfset dataTable = "E0_AGGREGS_DEFRAG">
		
			<cfswitch expression="#SESSION.PERIMETRE.NIVEAU#">
				<cfcase value="A,B,C,D,E,F" delimiters=",">
					<cfset dataTable = "E0_AGGREGS_DEFRAG">
				</cfcase>
				<cfdefaultcase>
					<cfset dataTable = "E0_ENPROD_HIST">	
				</cfdefaultcase>
			</cfswitch>
		 	
			<cfset theDate=SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN>
			<cfif theDate gt now()>
					<cfset theDate = now()>		
			</cfif>
			
			
			<cfquery datasource="#SESSION.OFFREDSN#" name="qPeriode">
				 SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS  
			 	 FROM	PERIODE
			 	 WHERE	PERIODE.SHORT_MOIS=to_Char(#theDate#,'mm/yyyy')
			</cfquery>
			
			<cfquery datasource="#dataSourceName#" name="qQuery">				
				set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
								p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idorga=#SESSION.PERIMETRE.ID_ORGANISATION#,
								p_idperimetre=#SESSION.PERIMETRE.ID_PERIMETRE#,								 
								p_idcliche=#SESSION.PERIMETRE.ID_LAST_CLICHE#,
								p_niveau='#SESSION.PERIMETRE.NIVEAU#',
								p_idniveau=#VARIABLES['IDNIVEAU']#,
								p_langue_pays='#SESSION.USER.GLOBALIZATION#';

				SELECT FACTURATION.MONTANT saw_0, PRODUIT1GROUPE.THEME saw_6 
				FROM #dataTable#
				WHERE PERIODE.IDPERIODE_MOIS = #qPeriode.idperiode_mois# 
				AND PRODUIT1GROUPE.IDSEGMENT_THEME = 2 AND PRODUIT1GROUPE.IDTYPE_THEME = 1
				AND FACTURATION.MONTANT > 0
				
			</cfquery>
			
		<cfreturn qQuery>
	</cffunction> 

<cffunction name="getIdNiveau" output="false" access="private" returntype="numeric">		
		<cfargument name="niveau" type="string" required="true">			 				
		<cfstoredproc procedure="pkg_consoview_v3.niveau_to_integer" datasource="#SESSION.OFFREDSN#">	
			<cfprocparam type="in" value="#niveau#" variable="p_niveau" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="out"  variable="p_idniveau" cfsqltype="CF_SQL_iNTEGER">
		</cfstoredproc>
		<cfreturn p_idniveau>
	</cffunction>
</cfcomponent>