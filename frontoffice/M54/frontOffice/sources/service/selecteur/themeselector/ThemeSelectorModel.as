package service.selecteur.themeselector
{
	import event.selector.ThemeEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import vo.selector.ValeurSelectorVo;

	internal class ThemeSelectorModel  extends EventDispatcher
	{
		private var _themes :ArrayCollection;
		
		public function ThemeSelectorModel():void
		{
		}
		public function get themes():ArrayCollection
		{
			return _themes;
		}
		
		internal function updateValues(value:ArrayCollection):void
		{	
			_themes= new ArrayCollection();
			var valeurSelector:ValeurSelectorVo;
			
			for(var i:int=0;i<value.length;i++)
			{
				valeurSelector = new ValeurSelectorVo();
				valeurSelector.fill(value[i],i);
				_themes.addItem(valeurSelector); 
			}
			this.dispatchEvent(new ThemeEvent(ThemeEvent.CHANGER_VALUE_THEME_EVENT));/** capturé dans ThemeSelectorImpl */
		}
	}
}