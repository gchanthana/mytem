<cfcomponent displayname="fr.consotel.api.ibis.publisher.prototype.FtpSchedulingPrototype"
		extends="fr.consotel.api.ibis.publisher.prototype.PublisherPrototype" hint="BIP FTP Scheduling Prototype (IBIS)">
	<!--- BIP Scheduling Prototype (Auteur : Cedric) --->

	<!--- EXPLICIT INITIALIZER --->
	<cfset initPrototype()>
	
	<cffunction access="private" name="initPrototype" returntype="void" hint="Initialize this prototype">
		<cfset SUPER.initPrototype()>
		<cfset VARIABLES.SCHEDULE_REQUEST.userJobName="FTP_SCHEDULING_PROTOTYPE">
		<cfset VARIABLES.SCHEDULE_REQUEST.deliveryRequest.ftpOption=structNew()>
		<cfset VARIABLES.SCHEDULE_REQUEST.deliveryRequest.ftpOption.ftpServerName="PELICAN">
		<cfset VARIABLES.SCHEDULE_REQUEST.deliveryRequest.ftpOption.sftpOption=FALSE>
		<cfset VARIABLES.SCHEDULE_REQUEST.deliveryRequest.ftpOption.ftpUserName="">
		<cfset VARIABLES.SCHEDULE_REQUEST.deliveryRequest.ftpOption.ftpUserPassword="">
		<cfset VARIABLES.SCHEDULE_REQUEST.deliveryRequest.ftpOption.remoteFile="">
	</cffunction>
</cfcomponent>