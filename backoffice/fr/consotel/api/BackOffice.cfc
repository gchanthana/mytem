<cfcomponent displayname="fr.consotel.api.BackOffice" hint="Main Class To Use to access fr.consotel.api.*">
	<!--- STATIC PROPERTIES
		VARIABLES["STATIC"]["BACKOFFICE"] : BackOffice DNS and Port Number
		VARIABLES["STATIC"]["MAIL-SERVER"] : Mail Server (Same definition as in Administration)
		VARIABLES["STATIC"]["BIP-SERVER-NAME"] : BIP Server DNS
		VARIABLES["STATIC"]["BIP-WSDL"] : BIP WSDL URL
		VARIABLES["STATIC"]["BIP-EVENT-LISTENERS"] : List of BIP Job Event Listeners. Each listener is identified by a given key.
	 --->
	
	<cffunction access="public" name="getContextMap" returntype="any" hint="Returns an associative map which contains runtime properties">
		<cfargument name="contextKey" type="string" required="true">
		<cfif UCASE(arguments["contextKey"]) EQ "REPORTING">
			<cfreturn getReportingContextMap()>
		<cfelse>
			<cflog type="error" text="BackOffice INVALID contextKey [#arguments.contextKey#]">
			<cfabort showerror="BACKOFFICE INVALID CONTEXT KEY - #arguments.contextKey# : SEE LOG FILES FOR DETAILS">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getReportingContextMap" returntype="any" hint="Returns an associative map which contains BIP reporting properties">
		<!--- 
			- BIP SCHEDULER Data are accessed using DSN (SCHED_DSN) OR in XML by running a report (SCHED_XDO. This is prefered way)
			- BIP structure keys are used to determine handler naming (e.g HTTP-NOTIF_BIP-INT)
		 --->
		<!--- 
		<cfset contextMap={
			"BIP"={
				"bip-prod.consotel.fr"={"DNS"="bip-prod.consotel.fr","SCHED_DSN"="BIP-PROD","SCHED_XDO"="NULL"},
				"bip-int.consotel.fr"={"DNS"="bip-int.consotel.fr","SCHED_DSN"="BIP-INT","SCHED_XDO"="NULL"},
				"bip-dev.consotel.fr"={"DNS"="bip-dev.consotel.fr","SCHED_DSN"="BIP-DEV","SCHED_XDO"="NULL"},
				"LABS"={"DNS"="db-5.consotel.fr:7501","SCHED_DSN"="J-SCHED-4","SCHED_XDO"="NULL"}
			}
		}>
		 --->
		<cfset contextMap=structNew()>
		<cfset contextMap["BIP"]=structNew()>
		<cfset contextMap["BIP"]["bip-prod.consotel.fr"]=structNew()>
		<cfset contextMap["BIP"]["bip-prod.consotel.fr"]["DNS"]="bip-prod.consotel.fr">
		<cfset contextMap["BIP"]["bip-prod.consotel.fr"]["SCHED_DSN"]="BIP-PROD">
		<cfset contextMap["BIP"]["bip-prod.consotel.fr"]["SCHED_XDO"]="NULL">
		<cfset contextMap["BIP"]["bip-int.consotel.fr"]=structNew()>
		<cfset contextMap["BIP"]["bip-int.consotel.fr"]["DNS"]="bip-int.consotel.fr">
		<cfset contextMap["BIP"]["bip-int.consotel.fr"]["SCHED_DSN"]="BIP-INT">
		<cfset contextMap["BIP"]["bip-int.consotel.fr"]["SCHED_XDO"]="NULL">
		<cfset contextMap["BIP"]["bip-dev.consotel.fr"]=structNew()>
		<cfset contextMap["BIP"]["bip-dev.consotel.fr"]["DNS"]="bip-dev.consotel.fr">
		<cfset contextMap["BIP"]["bip-dev.consotel.fr"]["SCHED_DSN"]="BIP-DEV">
		<cfset contextMap["BIP"]["bip-dev.consotel.fr"]["SCHED_XDO"]="NULL">
		<cfset contextMap["BIP"]["LABS"]=structNew()>
		<cfset contextMap["BIP"]["LABS"]["DNS"]="db-5.consotel.fr:7501">
		<cfset contextMap["BIP"]["LABS"]["SCHED_DSN"]="J-SCHED-4">
		<cfset contextMap["BIP"]["LABS"]["SCHED_XDO"]="NULL">
		
		<cflog type="information" text="<< BACKOFFICE - getReportingContextMap : OK">
		<cfreturn contextMap>
	</cffunction>
	
	<!--- 
	<cffunction name="getDefaultContext" access="public" returntype="struct" hint="Compatible to all CV versions">
		<cfset context=structNew()>
		<cfset context["DATASOURCES"]=structNew()>
		<cfset context["DATASOURCES"]["FACTURATION"]="ROCOFFRE">
		<cfset context["MAIL"]=structNew()>
		<cfset context["MAIL"]["CONSOTEL"]="mail.consotel.fr:26">
		<cfset context["BIP"]=getBipServerListInfos()>
		<cfreturn context>
	</cffunction>
	
	<cffunction name="getBipServerListInfos" access="private" returntype="struct">
		<cfset bipListInfos=structNew()>
		<cfset serverList="BIP-PROD,BIP-INT,BIP-DEV">
		<cfloop index="it" list="#serverList#">
			<cfset serverDNS=LCASE(it) & ".consotel.fr">
			<cfset bipListInfos[serverDNS]=structNew()>
			<cfset bipListInfos[serverDNS]["BIP_WS_URL"]="http://" & serverDNS & "/xmlpserver/PublicReportService?WSDL">
			<cfset bipListInfos[serverDNS]["LATEST_WS_URL"]="http://" & serverDNS & "/xmlpserver/PublicReportService_v11?WSDL">
			<cfset bipListInfos[serverDNS]["SCHED-DS"]=it>
		</cfloop>
		<!--- DEBUG SERVER --->
		<cfset bipListInfos["BIP-DEBUG"]=structNew()>
		<cfset bipListInfos["BIP-DEBUG"]["BIP_WS_URL"]="http://db-5.consotel.fr:7501/xmlpserver/PublicReportService?WSDL">
		<cfset bipListInfos["BIP-DEBUG"]["LATEST_WS_URL"]="http://db-5.consotel.fr:7501/xmlpserver/PublicReportService_v11?WSDL">
		<cfset bipListInfos["BIP-DEBUG"]["SCHED-DS"]="J-SCHED-4">
		<cfreturn bipListInfos>
	</cffunction>
	
	<cffunction name="getSourceList" access="public" returntype="any">
		<cfargument name="ctxRootAbsPath" type="string" required="true">
		<cfargument name="isRecursive" type="string" required="false" default="FALSE">
		<cfset sourceList=directoryList(expandPath(ctxRootAbsPath),isRecursive,"PATH","*.cf*","ASC")>
		<cfreturn sourceList>
	</cffunction>
	 --->
</cfcomponent>
