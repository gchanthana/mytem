package intranet
{
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.system.System;
    
    import fr.consotel.consoview.util.ConsoViewErrorManager;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import mx.collections.ArrayCollection;
    import mx.collections.IViewCursor;
    import mx.controls.Alert;
    import mx.controls.DataGrid;
    import mx.controls.dataGridClasses.DataGridColumn;
    import mx.controls.dataGridClasses.DataGridItemRenderer;
    import mx.events.CollectionEvent;
    import mx.events.DataGridEvent;
    import mx.events.FlexEvent;
    import mx.events.ListEvent;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    import mx.rpc.http.HTTPService;

    public class GestionCtrl extends GestionCtrlIHM
    {
        protected var segment:ArrayCollection = new ArrayCollection([ { data: 1, label: 'Fixe' }, { data: 2, label: 'Mobile' }, { data: 3, label: 'Data' } ]);
        protected var typeGroupe:ArrayCollection = new ArrayCollection([ { data: 1, label: 'Abonnements' }, { data: 2, label: 'Consommations' }, ]);
        /* Array Collections pour le filtrage */
        [Bindable]
        private var myFilterDgGroupeArray:ArrayCollection = new ArrayCollection();
        [Bindable]
        private var myFilterDgProduitCat:ArrayCollection = new ArrayCollection();
        [Bindable]
        private var myFilterDgProduitGroupe:ArrayCollection = new ArrayCollection();
		

        public function GestionCtrl()
        {
            super();
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
			addEventListener(ListEvent.ITEM_CLICK,ListEventItemClickHandler,true);
        }

        protected function initIHM(event:Event):void
        {
            /* intialise IHM */
            dgProduitGroupe.dataProvider = null;
            dgProduitCat.dataProvider = null;
            cmbSegment.dataProvider = segment;
            cmbType.dataProvider = typeGroupe;
            /* Gestion des paramètres */
            cmbOperateur.addEventListener(Event.CHANGE, onOperateurChange);
            cmbSegment.addEventListener(Event.CHANGE, onSegmentChange);
            cmbType.addEventListener(Event.CHANGE, onTypeChange);
            cmbTheme.addEventListener(Event.CHANGE, getProduitCat);
            /* Gestion des groupes */
            btnAddGroupe.addEventListener(MouseEvent.CLICK, addGroupe);
            btnDeleteGroupe.addEventListener(MouseEvent.CLICK, deleteGroupe);
            dgGroupe.addEventListener(Event.CHANGE, getProduitGroupe);
            /* Gestion des affectations */
            dgProduitCat.addEventListener(Event.CHANGE, onProduitCatChange);
            dgProduitGroupe.addEventListener(Event.CHANGE, onProduitGroupeChange);
            btnAffecter.addEventListener(MouseEvent.CLICK, affectProduits);
            btnDesaffecter.addEventListener(MouseEvent.CLICK, desaffectProduits);
            /* Gestion des produits */
            btnUpdateQte1.addEventListener(MouseEvent.CLICK, updateQte1);
            btnUpdateQte2.addEventListener(MouseEvent.CLICK, updateQte2);
            /* Gestion des filtres */
            txtFiltreDgGroupe.addEventListener(Event.CHANGE, filtreDgGroupe);
            txtFiltreDgProduitCat.addEventListener(Event.CHANGE, filtreDgProduitCat);
            txtFiltreDgProduitGroupe.addEventListener(Event.CHANGE, filtreDgProduitGroupe);
            /* Va chercher les opérateurs */
            var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.GestionGroupeControle", "getListeOperateur", processGetListeOperateur, remoteError);
            RemoteObjectUtil.callService(op2);
			
			
        }

        /* Mise à jour de la qte : a savoir si le produit compte dans le prix moyen (montant/qte) */
        private function updateQte1(event:MouseEvent):void
        {
            var arID:Array = new Array();
            var arValue:Array = new Array();
            var i:int;
            for (i = 0; i < dgProduitCat.selectedItems.length; i++)
            {
                arID[i] = dgProduitCat.selectedItems[i]["IDPRODUIT_CATALOGUE"];
                if (dgProduitCat.selectedItems[i]["BOOL_QTE"] == 'OUI')
                {
                    arValue[i] = 0;
                }
                else
                {
                    arValue[i] = 1;
                }
            }
            var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.GestionGroupeControle", "updateProduitQte", processupdateProduitQte, remoteError);
            RemoteObjectUtil.callService(op2, arID, arValue);
        }

        private function processupdateProduitQte(event:ResultEvent):void
        {
            getProduitGroupe(new Event(""));
            getProduitCat(new Event(""));
        }

        /* Mise à jour de la qte : a savoir si le produit compte dans le prix moyen (montant/qte) */
        private function updateQte2(event:MouseEvent):void
        {
            var arID:Array = new Array();
            var arValue:Array = new Array();
            var i:int;
            for (i = 0; i < dgProduitGroupe.selectedItems.length; i++)
            {
                arID[i] = dgProduitGroupe.selectedItems[i]["IDPRODUIT_CATALOGUE"];
                if (dgProduitGroupe.selectedItems[i]["BOOL_QTE"] == 'OUI')
                {
                    arValue[i] = 0;
                }
                else
                {
                    arValue[i] = 1;
                }
            }
            var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.GestionGroupeControle", "updateProduitQte", processupdateProduitQte, remoteError);
            RemoteObjectUtil.callService(op2, arID, arValue);
        }

        /* Affecte les produits catalogue sélectionnés dans le groupe sélectionné */
        private function affectProduits(event:MouseEvent):void
        {
            var arData:Array = new Array();
            var i:int;
            for (i = 0; i < dgProduitCat.selectedItems.length; i++)
            {
                arData[i] = dgProduitCat.selectedItems[i]["IDPRODUIT_CATALOGUE"];
            }
            var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.GestionGroupeControle", "affectProduits", processaffectProduits, remoteError);
            RemoteObjectUtil.callService(op2, dgGroupe.selectedItem["IDGRP_CTL"], arData);
        }

        private function processaffectProduits(event:ResultEvent):void
        {
            getProduitGroupe(new Event(""));
        }

        private function desaffectProduits(event:MouseEvent):void
        {
            var arData:Array = new Array();
            var i:int;
            for (i = 0; i < dgProduitGroupe.selectedItems.length; i++)
            {
                arData[i] = dgProduitGroupe.selectedItems[i]["IDPRODUIT_CATALOGUE"];
            }
            var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.GestionGroupeControle", "desaffectProduits", processdesaffectProduits, remoteError);
            RemoteObjectUtil.callService(op2, dgGroupe.selectedItem["IDGRP_CTL"], arData);
        }

        private function processdesaffectProduits(event:ResultEvent):void
        {
            getProduitGroupe(new Event(""));
        }

        private function onOperateurChange(event:Event):void
        {
            getGroupes(new Event(""));
            getThemes(new Event(""));
            dgProduitGroupe.dataProvider = null;
            dgCompte.dataProvider = null;
            initBtn();
        }

        private function onSegmentChange(event:Event):void
        {
            getGroupes(new Event(""));
            dgProduitGroupe.dataProvider = null;
            dgCompte.dataProvider = null;
            initBtn();
        }

        private function onTypeChange(event:Event):void
        {
            getGroupes(new Event(""));
            getThemes(new Event(""));
            dgProduitGroupe.dataProvider = null;
            dgCompte.dataProvider = null;
            initBtn();
        }

        private function onProduitCatChange(event:Event):void
        {
            if (dgProduitCat.selectedItems.length >= 1 && dgGroupe.selectedIndex != -1)
            {
                btnAffecter.enabled = true;
                btnUpdateQte1.enabled = true;
            }
            else if (dgProduitCat.selectedItems.length >= 1 && dgGroupe.selectedIndex == -1)
            {
                btnUpdateQte1.enabled = true;
            }
            else
            {
                btnAffecter.enabled = false;
                btnUpdateQte1.enabled = false;
            }
        }

        private function onProduitGroupeChange(event:Event):void
        {
            if (dgProduitGroupe.selectedItems.length >= 1)
            {
                btnDesaffecter.enabled = true;
                btnUpdateQte2.enabled = true;
            }
            else if (dgProduitGroupe.selectedItems.length >= 1 && dgGroupe.selectedIndex == -1)
            {
                btnUpdateQte2.enabled = true;
            }
            else
            {
                btnDesaffecter.enabled = false;
                btnUpdateQte2.enabled = false;
            }
        }

        private function addGroupe(event:MouseEvent):void
        {
            if (txtLibelleGroupe.text.length == 0)
            {
                Alert.show("Merci de saisir un nom pour le groupe de produit.");
            }
            else
            {
                var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.GestionGroupeControle", "AddGroupe", processAddGroupe, remoteError);
                RemoteObjectUtil.callService(op2, [ txtLibelleGroupe.text, cmbOperateur.selectedItem["data"], cmbSegment.selectedItem["data"], cmbType.selectedItem["data"] ]);
            }
        }

        private function processAddGroupe(event:ResultEvent):void
        {
            var col:ArrayCollection = (event.result as ArrayCollection);
			var cursor:IViewCursor = col.createCursor();
			
			myFilterDgGroupeArray.removeAll();
			while(!cursor.afterLast)
			{
				myFilterDgGroupeArray.addItem(cursor.current);
				cursor.moveNext();
			}
			myFilterDgGroupeArray.refresh()
			
			
        }

        private function deleteGroupe(event:MouseEvent):void
        {
            var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.GestionGroupeControle", "DeleteGroupe", processDeleteGroupe, remoteError);
            RemoteObjectUtil.callService(op2, [ dgGroupe.selectedItem["IDGRP_CTL"], cmbOperateur.selectedItem["data"], cmbSegment.selectedItem["data"], cmbType.selectedItem["data"] ]);
        }

        private function processDeleteGroupe(event:ResultEvent):void
        {
			var col:ArrayCollection = (event.result as ArrayCollection);
			var cursor:IViewCursor = col.createCursor();
			
			myFilterDgGroupeArray.removeAll();
			while(!cursor.afterLast)
			{
				myFilterDgGroupeArray.addItem(cursor.current);
				cursor.moveNext();
			}
			myFilterDgGroupeArray.refresh();
			
			updateListeProduitsGroupe();
        }

        private function dispatchLogoff(event:Event):void
        {
            dispatchEvent(new Event(Intranet.LOGOFF_EVENT));
        }

        private function processGetListeOperateur(event:ResultEvent):void
        {
            cmbOperateur.dataProvider = event.result;
            getThemes(new Event(""));
        }

        /* Va chercher les themes */
        private function getThemes(event:Event):void
        {
            var op3:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.GestionGroupeControle", "getThemesAndQte", processGetThemes, remoteError);
            RemoteObjectUtil.callService(op3, cmbOperateur.selectedItem["data"], cmbType.selectedItem["data"]);	
			
        }

        private function processGetThemes(event:ResultEvent):void
        {	 
			var col:ArrayCollection = (event.result as ArrayCollection);
			var cursor:IViewCursor = col.createCursor();
		
			listeTheme.removeAll();
			while(!cursor.afterLast)
			{
				listeTheme.addItem(cursor.current);
				cursor.moveNext();
			}
			listeTheme.refresh();
            if (listeTheme.length > 0)
            {
                getProduitCat(new Event(""));
            }
        }
		
		
		
        /* Va chercher les groupes */
        private function getGroupes(event:Event):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.GestionGroupeControle", "getGroupes", processGetGroupe, remoteError);
            RemoteObjectUtil.callService(op, [ cmbOperateur.selectedItem["data"], cmbSegment.selectedItem["data"], cmbType.selectedItem["data"] ]);
        }

        private function processGetGroupe(event:ResultEvent):void
        {
            myFilterDgGroupeArray = event.result as ArrayCollection;
            dgGroupe.dataProvider = myFilterDgGroupeArray;
        }

        private function getProduitCat(event:Event):void
        {
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.GestionGroupeControle", "getProduitCat", processGetProduitCat, remoteError);
			
			if(cmbTheme.selectedItem && cmbOperateur.selectedItem)
			{	
				RemoteObjectUtil.callService(op, [ cmbTheme.selectedItem["IDTHEME"], cmbOperateur.selectedItem["data"] ]);	
			}
			else
			{
				RemoteObjectUtil.callService(op, [0,0]);	
			}
        }

        private function processGetProduitCat(event:ResultEvent):void
        {
            myFilterDgProduitCat = event.result as ArrayCollection;
            dgProduitCat.dataProvider = myFilterDgProduitCat;
			
            initBtn();
			upDateTheme(myFilterDgProduitCat);
        }

        private function getProduitGroupe(event:Event):void
        {
            if (dgGroupe.selectedIndex != -1)
            {
                btnDeleteGroupe.enabled = true;
                var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.GestionGroupeControle", "getProduitGroupe", processGetProduitGroupe, remoteError);
                RemoteObjectUtil.callService(op, [ dgGroupe.selectedItem["IDGRP_CTL"] ]);
                var op2:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.intranet.GestionGroupeControle", "getVersionsGroupe", processGetVersionsGroupe, remoteError);
                RemoteObjectUtil.callService(op2, [ dgGroupe.selectedItem["IDGRP_CTL"] ]);
                getProduitCat(new Event(""));
            }
        }

        private function processGetProduitGroupe(event:ResultEvent):void
        {
            myFilterDgProduitGroupe = event.result as ArrayCollection;
            dgProduitGroupe.dataProvider = myFilterDgProduitGroupe;
        }

        private function processGetVersionsGroupe(event:ResultEvent):void
        {
            dgCompte.dataProvider = event.result;
        }

        private function remoteError(faultEvent:FaultEvent):void
        {
            ConsoViewErrorManager.processRemotingFaultEvent(faultEvent);
        }

        private function initBtn():void
        {
            btnUpdateQte1.enabled = false;
            btnUpdateQte2.enabled = false;
            btnAffecter.enabled = false;
            btnDesaffecter.enabled = false;
            txtFiltreDgGroupe.text = "";
        }
		
		
		//Mise à jour du nombre de produits non affecté a un groupe de controle dans la liste des thèmes
		private function upDateTheme(value:ArrayCollection):void
		{
			if(cmbTheme.selectedItem)
			{
				if(Number(cmbTheme.selectedItem.PRDT_NAF) != dgProduitCat.dataProvider.length)
				{
					cmbTheme.selectedItem.PRDT_NAF = dgProduitCat.dataProvider.length;
				}
			}
		}
		
		private function updateListeProduitsGroupe():void
		{
			if(myFilterDgProduitGroupe && myFilterDgProduitGroupe.length > 0)
			{	
				myFilterDgProduitGroupe.removeAll();
				getProduitCat(null);
			}
			
		}	
			

        /* Filtre des groupes */
        public function filtreDgGroupe(e:Event):void
        {
            myFilterDgGroupeArray.filterFunction = filtreDgGroupe_func;
            myFilterDgGroupeArray.refresh();
        }

        public function filtreDgGroupe_func(item:Object):Boolean
        {
            if (item.GRP_CTL.toLowerCase().search(txtFiltreDgGroupe.text.toLowerCase()) != -1)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        /* Fin Filtre des groupes */ /* Filtre des produits catalogue */
        public function filtreDgProduitCat(e:Event):void
        {
            myFilterDgProduitCat.filterFunction = filtreDgProduitCat_func;
            myFilterDgProduitCat.refresh();
        }

        public function filtreDgProduitCat_func(item:Object):Boolean
        {
            if (item.LIBELLE_PRODUIT.toLowerCase().search(txtFiltreDgProduitCat.text.toLowerCase()) != -1 || item.BOOL_QTE.substr(0, txtFiltreDgProduitCat.text.length).toLowerCase() == txtFiltreDgProduitCat.text.toLowerCase() || item.CODE_ARTICLE.toLowerCase().search(txtFiltreDgProduitCat.text.toLowerCase()) != -1)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        /* Fin Filtre des produits catalogue */ /* Filtre des produits groupe */
        public function filtreDgProduitGroupe(e:Event):void
        {
            myFilterDgProduitGroupe.filterFunction = filtreDgProduitGroupe_func;
            myFilterDgProduitGroupe.refresh();
        }

        public function filtreDgProduitGroupe_func(item:Object):Boolean
        {
            if (item.LIBELLE_PRODUIT.toLowerCase().search(txtFiltreDgProduitGroupe.text.toLowerCase()) != -1 || item.BOOL_QTE.substr(0, txtFiltreDgProduitGroupe.text.length).toLowerCase() == txtFiltreDgProduitGroupe.text.toLowerCase() || item.CODE_ARTICLE.toLowerCase().search(txtFiltreDgProduitGroupe.text.toLowerCase()) != -1)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }
    /* Fin Filtre des produits groupe */
		
		
		
		
		/**
		 * Copie dans le presse papier a partir d'un click d'un item de datagrid
		 * @parem event
		 */
		protected function ListEventItemClickHandler(event : ListEvent):void
		{			
				var txt : String = "";
				//si item rederer du data grid par défaut
				if(event.itemRenderer is DataGridItemRenderer)
				{
					txt = (event.itemRenderer as DataGridItemRenderer).listData.label;
					System.setClipboard(txt); 
					lblClipBoard.text = txt;
				}
				//sinon pour un autre item renderer
				else if(event.target is DataGrid)
				{
					var target:DataGrid = (event.target as DataGrid);					
					txt = (target.selectedItem)?target.selectedItem[(target.columns[event.columnIndex] as DataGridColumn).dataField]:lblClipBoard.text;
					lblClipBoard.text = txt;
				}else
				{
					System.setClipboard(""); 
					lblClipBoard.text = "";
				}
			
		}
    }
}