package univers.parametres.perimetres.mainComponent
{
	import univers.parametres.perimetres.main;
	import flash.events.Event;
	import mx.events.FlexEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	import mx.collections.ArrayCollection;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import flash.events.MouseEvent;
	import mx.controls.Alert;
	
	public class FicheSite extends FicheSiteIHM
	{
		private var _mainRef:main;
		private var _currentIdSite:Number;
		private var _idgroupe : Number;
		public function FicheSite(idgroupe : Number = -1)
		{
			_idgroupe = idgroupe;
			super();
			
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		protected function initIHM(event:Event):void
		{
			this.x = 100;
			this.y = 150;
			fillFiche();
			btUpdate.addEventListener(MouseEvent.CLICK, updateSite);
			this.addEventListener(CloseEvent.CLOSE, closeWin);
		}
		
		protected function closeWin(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function updateSite(event:MouseEvent):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"updateFicheSite",
																				updateSiteResult);
			RemoteObjectUtil.callService(op, _currentIdSite, txtLibelle.text, txtReference.text, txtCodeInterne.text,
									txtAdresse.text, "-", txtCode.text, txtCommune.text, txtCommentaire.text);	
		}
		
		
		
		private function updateSiteResult(event:ResultEvent):void
		{
			if (event.result > 0){
				Alert.show("Fiche mis à jour");
			}else{
				Alert.show("Erreur dans le remoting");
			}
		}        
		
		
				
		private function fillFiche():void
		{
			
			var idgroupe:Number = _mainRef != null ? _mainRef.getSelectedNode() : _idgroupe;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"getFicheSite",
																				getFicheSiteResult);
			RemoteObjectUtil.callService(op, idgroupe);		
		}
		
		private function getFicheSiteResult(event:ResultEvent):void
		{
			try
			{
				var data:Object = ArrayCollection(event.result)[0];
				txtLibelle.text = data.NOM_SITE;
				txtCodeInterne.text = data.SP_CODE_INTERNE;
				txtCode.text = data.SP_CODE_POSTAL;
				txtCommentaire.text = data.SP_COMMENTAIRE; 
				txtCommune.text = data.SP_COMMUNE;
				txtReference.text = data.SP_REFERENCE;
				txtAdresse.text = data.SP_ADRESSE1;
				_currentIdSite = data.IDSITE_PHYSIQUE;
			}
			catch (e:Error) {}
		}
		
		private function throwError(result:FaultEvent):void
		{
			
		}
		
		public function setParentRef(ref:main):void
		{
			_mainRef = ref;
		}
		

		
	}
}