package gestionparc.ihm.fiches.importmasse
{
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.equipement.EquipementServices;
	import gestionparc.utils.gestionparcMessages;
	
	import mx.collections.ArrayCollection;
	import mx.containers.FormItem;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	[Bindable]
	public class ChoixModeleFixeHorsCatalogue extends TitleWindow
	{

		public var cbCategorie:ComboBox;
		public var cbType:ComboBox;
		public var cbMarque:ComboBox;
		public var cbModele:ComboBox;

		public var tiAutreModele:TextInput;

		public var rbModele:RadioButton;
		public var rbAutreModele:RadioButton;

		public var btAnnuler:Button;
		public var btValider:Button;

		public var formItemModele:FormItem;
		public var formItemAutreModele:FormItem;
		public var formItemConstructeur:FormItem;

		public var idRevendeur:Number;
//		public var revendeur					:Object;
		public var parentPanel:String;

		public var oldModele:String;
		public var oldMarque:String;
		public var nouveauModele:String;

		private var modeleChoisi:Object=new Object();
		private var serv:EquipementServices=new EquipementServices();

		public function ChoixModeleFixeHorsCatalogue()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
			initListener();
			initDefaultDisplay();
		}

		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE, closePopup);
			cbMarque.addEventListener(ListEvent.CHANGE, initComboModele); // ecouteur pour la maj des modeles lorsque le revendeur change
			rbModele.addEventListener(MouseEvent.CLICK, clickRadioButton);
			rbAutreModele.addEventListener(MouseEvent.CLICK, clickRadioButton);
			btAnnuler.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValider.addEventListener(MouseEvent.CLICK, validerHandler);
			serv.myDatas.addEventListener(gestionparcEvent.LISTE_TYPE_LOADED, initComboType);
			serv.myDatas.addEventListener(gestionparcEvent.LISTE_MARQUES_LOADED, initComboMarque);
		}

		private function initDefaultDisplay():void
		{
			formItemAutreModele.enabled=false;
			rbModele.selected=true;
			initComboCategorie();
		}

		private function clickRadioButton(evt:MouseEvent):void
		{
			if (rbModele.selected)
			{
				formItemModele.enabled=true;
				formItemAutreModele.enabled=false;
			}
			else
			{
				formItemModele.enabled=false;
				formItemAutreModele.enabled=true;
			}
		}

		private function cancelPopup(evt:MouseEvent):void
		{
			closePopup(null);
		}

		private function closePopup(evt:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		private function validerHandler(evt:MouseEvent):void
		{
			modeleChoisi.idType=cbType.selectedItem.IDTYPE_EQUIPEMENT;
			modeleChoisi.idMarque=cbMarque.selectedItem.IDFOURNISSEUR;
			//Si ça n'est pas un nouveau modèle :
			if (rbModele.selected)
			{
				if (cbMarque.selectedIndex == -1 || cbModele.selectedIndex == -1)
				{
					Alert.show(ResourceManager.getInstance().getString('M111', 'S_lectionnez_un_revendeur_et_un_mod_le'));
				}
				else
				{
					var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
						"fr.consotel.consoview.M111.fiches.FicheTerminal", "insertModeleRevendeur", validerResultHandler);
					RemoteObjectUtil.callService(opData, cbModele.selectedItem.IDEQUIPEMENT_FOURNISSEUR, idRevendeur);
					modeleChoisi.modele=cbModele.selectedItem.LIBELLE_EQ;
					modeleChoisi.marque=cbMarque.selectedItem.NOM_FOURNISSEUR;
				}
			}
			else // Si c'est un nouveau modèle : 
			{
				if (cbMarque.selectedIndex == -1 || tiAutreModele.text.length == 0)
				{
					Alert.show(ResourceManager.getInstance().getString('M111', 'S_lectionnez_un_revendeur_et_une_marque_'));
				}
				else
				{
					var opDataNewModele:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.M111.fiches.FicheTerminal", "addEquipementRevendeur", validerResultHandler);
					RemoteObjectUtil.callService(opDataNewModele, tiAutreModele.text, "", "ND", cbMarque.selectedItem.IDFOURNISSEUR, cbType.selectedItem.IDTYPE_EQUIPEMENT, idRevendeur, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
					modeleChoisi.modele=tiAutreModele.text;
					modeleChoisi.marque=cbMarque.selectedItem.NOM_FOURNISSEUR;
				}
			}
		}

		private function validerResultHandler(evt:ResultEvent):void
		{
			if (evt.result && ((evt.result.hasOwnProperty("IDEQUIP") && evt.result.IDEQUIP < 0) || evt.result < 0))
			{
				ConsoviewAlert.afficherError(gestionparcMessages.MODELES_EXISTANT, gestionparcMessages.ERREUR);
				return;
			}


			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Nouveau_mod_le_ajout_'), Application.application as DisplayObject);
			closePopup(null);

			if (rbModele.selected) // pour selectionner automatiquement celui qu'on vient de chosir
			{
				if (parentPanel == "importDeMasse")
				{
					nouveauModele=cbModele.selectedItem.LIBELLE_EQ;
					dispatchEvent(new gestionparcEvent(gestionparcEvent.NEW_MODELE_ADDED, nouveauModele));
				}
				else
				{
					modeleChoisi.idModele=evt.result as int;
					dispatchEvent(new gestionparcEvent(gestionparcEvent.NEW_MODELE_ADDED, modeleChoisi));
				}
			}
			else
			{
				if (parentPanel == "importDeMasse")
				{
					nouveauModele=tiAutreModele.text;
					dispatchEvent(new gestionparcEvent(gestionparcEvent.NEW_MODELE_ADDED, nouveauModele));
				}
				else
				{
					modeleChoisi.idModele=evt.result.IDEQUIP;
					dispatchEvent(new gestionparcEvent(gestionparcEvent.NEW_MODELE_ADDED, modeleChoisi));
				}
			}
		}

		private function initComboCategorie():void
		{
			cbCategorie.dataProvider=CacheDatas.listeCategorie;
			cbCategorie.dropdown.dataProvider=CacheDatas.listeCategorie;
		}

		protected function cbCategorie_changeHandler(evt:Event):void
		{
			if (cbCategorie.selectedIndex > -1)
			{
				serv.getListeType(cbCategorie.selectedItem.IDCATEGORIE_EQUIPEMENT);
			}
		}

		private function initComboType(event:gestionparcEvent):void
		{
			var myArray:Array=new Array();
			var longueur:int=serv.myDatas.listeType.length;

			for (var index:int=longueur - 1; index > -1; index--)
			{
				var item:Object=serv.myDatas.listeType[index];
				if (item.IDTYPE_EQUIPEMENT != 70)
					myArray.push(item);
			}
			var myArrayCollection:ArrayCollection=new ArrayCollection(myArray.sortOn("TYPE_EQUIPEMENT"));

			cbType.dataProvider=myArrayCollection;
			cbType.dropdown.dataProvider=myArrayCollection;
			cbType.selectedIndex=-1;
			if (cbType.dataProvider.length == 1)
			{
				cbType.selectedIndex=0;
				cbType_changeHandler(null);
			}
		}

		protected function cbType_changeHandler(evt:Event):void
		{
			if (cbType.selectedIndex > -1)
			{
				serv.getListeMarqueWithType(cbType.selectedItem.IDTYPE_EQUIPEMENT);
			}
		}

		private function initComboMarque(evt:Event):void
		{
			cbMarque.dataProvider=serv.myDatas.listeMarque;
			cbMarque.dropdown.dataProvider=serv.myDatas.listeMarque;

			cbMarque.selectedIndex=-1;
			if (cbMarque.dataProvider.length == 1)
			{
				cbMarque.selectedIndex=0;
				initComboModele(null);
			}
			else if (modeleChoisi)
			{
				for each (var o:Object in cbMarque.dataProvider)
				{
					if (o.IDFOURNISSEUR == modeleChoisi.idMarque)
					{
						cbMarque.selectedItem=o;
						break;
					}
				}
				if (cbMarque.selectedIndex > -1)
					refreshListe();
			}
		}

		private function initComboModele(evt:Event):void
		{
			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview..M111.fiches.FicheTerminal", "getModeleHorsCatalogue", initcbModeleResultHandler);

			if (idRevendeur > 0)
			{
				RemoteObjectUtil.callService(opData, cbType.selectedItem.IDTYPE_EQUIPEMENT, cbMarque.selectedItem.IDFOURNISSEUR, idRevendeur)
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo('', ResourceManager.getInstance().getString('M111', 'Vous_devez_selectionner_un_revendeur'),null);
			}
		}

		private function initcbModeleResultHandler(evt:ResultEvent):void
		{
			if ((evt.result as ArrayCollection).length > 0)
			{
				var myArray:Array=new Array();
				var longueur:int=(evt.result as ArrayCollection).length;

				for (var index:int=0; index < longueur; index++)
				{
					myArray.push(evt.result[index]);
				}

				var myArrayCollection:ArrayCollection=new ArrayCollection(myArray.sortOn("LIBELLE_EQ"));

				cbModele.dataProvider=myArrayCollection;
				cbModele.dropdown.dataProvider=myArrayCollection;

				var tmp:int=ConsoviewUtil.getIndexByLabel(myArrayCollection, "LIBELLE_EQ", oldModele);
				if (tmp != -1)
				{
					cbModele.selectedIndex=tmp;
				}
			}
		}

		private function refreshListe():void
		{
			if (cbType.selectedIndex > -1)
			{
				if (cbMarque.selectedIndex > -1)
				{
					initComboModele(null);
				}
				else
				{
					initComboMarque(null);
				}
			}
		}
	}
}
