package rapportPilotage.service.chercherlisterapport
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;

	public class ChercherListeRapportService
	{
		private var _model:ChercherListeRapportModel;
		public var handler:ChercherListeRapportHandler;	
		
		public function ChercherListeRapportService()
		{
			this._model  = new ChercherListeRapportModel();
			this.handler = new ChercherListeRapportHandler(_model);
		}
				
		public function get model():ChercherListeRapportModel
		{
			return this._model;
		}
		
		/*
		** chercher la liste des rapports de l'utilisateur
		*/	
		public function getListeRapport():void
		{	
			// ChercherListeRapportService : nom du fichier cfc 
			//getListeRapport nom fonction dans le fichier cfc
			//getListeRapportResultHandler : une methode declarer dans le handler 		
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M332.service.RechercherRapportService","getRapport",handler.getListeRapportResultHandler); 
			
			RemoteObjectUtil.callService(op);// appel au service coldfusion
			
		}
	}
}