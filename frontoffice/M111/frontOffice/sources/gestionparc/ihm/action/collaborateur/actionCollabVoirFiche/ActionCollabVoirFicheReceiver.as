package gestionparc.ihm.action.collaborateur.actionCollabVoirFiche
{
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.ficheEntity.FicheCollaborateurIHM;
	
	public class ActionCollabVoirFicheReceiver extends EventDispatcher
	{
		private var ficheCollab:FicheCollaborateurIHM;
		
		public function ActionCollabVoirFicheReceiver()
		{
			super();
		}
		public function action():void
		{
			displayIHM();
		}
		private function displayIHM():void
		{
			ficheCollab  = new FicheCollaborateurIHM();
			ficheCollab.isNewCollabFiche = false;
			ficheCollab.idEmploye = SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			var obj:Object = SpecificationVO.getInstance().elementDataGridSelected as Object;
			ficheCollab.idRow	 = SpecificationVO.getInstance().elementDataGridSelected.ID;
			PopUpManager.addPopUp(ficheCollab, (Application.application as DisplayObject), true);
			PopUpManager.centerPopUp(ficheCollab);
		}
	}
}