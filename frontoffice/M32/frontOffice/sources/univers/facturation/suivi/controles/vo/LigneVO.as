package univers.facturation.suivi.controles.vo
{
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Value Object représentant les propriétés d'une ligne.
	 * 
	 * Auteur	: Guiou Nicolas
	 * Version 	: 1.0
	 * Date 	: 15.07.2010
	 * </pre></p>
	 *
	 */
	[Bindable]
	public class LigneVO
	{
		public var idSousTete			: int 				= -1; 
		
		public var sousTete				: String			= "";
		
		public var idTypeLigne			: int				= -1;
		public var typeLigne			: String			= ""; 

		public var idContrat			: int				= -1; 
		public var numContrat			: String			= ""; 
		public var aboPrincipale		: String			= ""; 

		public var etat					: int				= -1;
		
		public var titulaire			: int				= -1;
		public var payeur				: int				= -1;
		public var fonction				: String			= "";
		
		public var compte				: String			= "";
		public var sousCompte			: String			= "";
		public var idCompte				: int				= -1;
		public var idSousCompte			: int				= -1;
		
		//public var operateur			: OperateurVO		= null;
		
		//public var fournisseur		: RevendeurVO		= null;
		
		//public var collabRattache		: CollaborateurVO	= null; 
		
		//public var simRattache		: SIMVO				= null;
				
		public var libelleEquipement    : String			= "";
		
		public var operateurNom    		: String			= "";
		
		public var libelleOrga    		: String			= "";
		
		public var position   			: String			= "";
		
		public var dateModifPosition   	: Date				= null;
		
		public var idSite 				: int 				= -1;
		public var commentaires 		: String 			= "";

		public var listeOrganisation	: ArrayCollection	= null;
		public var positionOrga 		: String 			= "";

		public var listeAboOption		: ArrayCollection	= null;
		
		public var dureeContrat			: int				= 0; 
		public var dateOuverture		: Date				= null; 
		public var dateRenouvellement	: Date				= null; 
		public var dateResiliation		: Date				= null; 
		public var dureeEligibilite		: int				= 0; 
		public var dateEligibilite		: Date				= null; 
		public var dateFPC				: Date				= null; 
		public var dateDerniereFacture	: Date				= null;
		
		public var libelleChampPerso1 	: String 			= "";
		public var libelleChampPerso2 	: String 			= "";
		public var libelleChampPerso3 	: String 			= "";
		public var libelleChampPerso4 	: String 			= "";
		
		public var texteChampPerso1 	: String 			= "";
		public var texteChampPerso2 	: String 			= "";
		public var texteChampPerso3 	: String 			= "";
		public var texteChampPerso4 	: String 			= "";
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * La fonction remplit un objet LigneVO avec les donnees passees en parametre et retourne cette LigneVO
		 * </pre></p>
		 * 
		 * @param obj
		 * @return LigneVO
		 * 
		 */
		public function fillLigneVO(obj:Object):LigneVO
		{
			//var myLigneVO : LigneVO = new LigneVO();
			
			// Ajout des infos concernant la ligne
			this.idSousTete		= obj[1][0].IDSOUS_TETE;
			this.sousTete		 	= obj[1][0].SOUS_TETE;
			this.idContrat			= obj[1][0].IDCONTRAT_ABO;
			this.numContrat		= obj[1][0].NUM_CONTRAT_ABO;
			this.aboPrincipale 	= obj[1][0].ABO_PRINCIPAL;
			this.etat			 	= obj[1][0].ETAT_LIGNE;
			this.typeLigne			= obj[1][0].LIBELLE_TYPE_LIGNE;
			this.idTypeLigne 		= obj[1][0].IDTYPE_LIGNE;
			this.titulaire			= obj[1][0].BOOL_TITULAIRE;
			this.payeur			= obj[1][0].BOOL_PAYEUR;
			this.fonction			= obj[1][0].FONCTION;
			this.compte			= obj[1][0].COMPTE_FACTURATION;
			this.sousCompte		= obj[1][0].SOUS_COMPTE;
			this.idCompte			= (obj[1][0].IDCOMPTE_FACTURATION)?obj[1][0].IDCOMPTE_FACTURATION:-1;
			this.idSousCompte		= (obj[1][0].IDSOUS_COMPTE)?obj[1][0].IDSOUS_COMPTE:-1;
			this.idSite		 	= (obj[1][0].IDSITE_PHYSIQUE)?obj[1][0].IDSITE_PHYSIQUE:-1;
			this.commentaires	 	= obj[1][0].COMMENTAIRES;
			
						
			// Ajout des infos concernant la SIM
			
			this.libelleEquipement	= obj[1][0].NUM_SIM;
			
			/*
			this.simRattache			 		= new SIMVO();
			this.simRattache.idEquipement		= (obj[1][0].IDSIM)?obj[1][0].IDSIM:-1;
			this.simRattache.libelleEquipement	= obj[1][0].NUM_SIM;
			this.simRattache.PIN1		 		= obj[1][0].PIN1;
			this.simRattache.PIN2	 	 		= obj[1][0].PIN2;
			this.simRattache.PUK1 		 		= obj[1][0].PUK1;
			this.simRattache.PUK2	 	 		= obj[1][0].PUK2;
			*/
			// Ajout des infos concernant le contrat de la ligne
			
			this.operateurNom				= obj[1][0].OPERATEUR;
			
			
			this.dureeContrat			 	= obj[1][0].DUREE_CONTRAT;
			this.dateOuverture			 	= obj[1][0].DATE_OUVERTURE;
			this.dateRenouvellement		= obj[1][0].DATE_RENOUVELLEMENT;
			this.dateResiliation		 	= obj[1][0].DATE_RESILIATION;
			this.dureeEligibilite 		 	= obj[1][0].DUREE_ELLIGIBILITE;
			this.dateEligibilite	 	 	= obj[1][0].DATE_ELLIGIBILITE;
			this.dateFPC				 	= obj[1][0].DATE_FPC;
			this.dateDerniereFacture	 	= obj[1][0].ACCES_LAST_FACTURE;
			/*
			this.operateur					= new OperateurVO();
			this.operateur.nom				= obj[1][0].OPERATEUR;
			this.operateur.idOperateur		= obj[1][0].OPERATEURID;
			*/
			// Ajout des infos concernant la liste des abonnements et options de la ligne
			this.listeAboOption			= new ArrayCollection();
			var lenListeAboOption : int 		= (obj[2] as ArrayCollection).length;
			var aboOpt : Object;
			
			for(var index:int = 0 ;index < lenListeAboOption ; index++)
			{
				aboOpt = new Object();
				aboOpt.operateur	 			= obj[2][index].OPERATEUR_NOM;
				aboOpt.libelleProduit 			= obj[2][index].LIBELLE_PRODUIT;
				aboOpt.idInventaireProduit		= obj[2][index].IDINVENTAIRE_PRODUIT;
				aboOpt.dansInventaire			= obj[2][index].BOOL_IN_INVENTAIRE;
				aboOpt.aEntrer					= 0;
				aboOpt.aSortir					= 0;
				
				this.listeAboOption.addItem(aboOpt);
			}
			
			// Ajout des infos concernant les organisations et position
			this.listeOrganisation = new ArrayCollection();
			
			var dateLastModifPosition : Date = new Date(0);
			
			var lenListeOrga : int = (obj[3] as ArrayCollection).length;
			//var newOrga : OrgaVO;
			
			for(var i:int = 0 ;i < lenListeOrga ; i++)
			{
								
				if(obj[3][i].POSITION == 1 && obj[3][i].DATE_MODIF_POSITION != null)
				{
					if(ObjectUtil.dateCompare(obj[3][i].DATE_MODIF_POSITION ,dateLastModifPosition) == 1)
					{
						this.libelleOrga = obj[3][i].LIBELLE_ORGA;
						this.position = obj[3][i].CHEMIN_CIBLE;
						dateLastModifPosition = obj[3][i].DATE_MODIF_POSITION;
						this.dateModifPosition = dateLastModifPosition;
					}
				}
				
				/*newOrga = new OrgaVO();
				newOrga.idOrga 				= obj[3][i].IDORGA;
				newOrga.libelleOrga			= obj[3][i].LIBELLE_ORGA;
				newOrga.idCible 			= obj[3][i].IDCIBLE;
				newOrga.position			= obj[3][i].POSITION;
				newOrga.chemin 				= obj[3][i].CHEMIN_CIBLE;
				newOrga.idRegleOrga 		= obj[3][i].IDREGLE;
				newOrga.dateModifPosition 	= obj[3][i].DATE_MODIF_POSITION;
				
				this.listeOrganisation.addItem(newOrga);*/
			}
			
			// Ajout des libellés
			/*
			this.libelleChampPerso1 	= CacheDatas.libellesPersoFicheLigne1;
			this.libelleChampPerso2 	= CacheDatas.libellesPersoFicheLigne2;
			this.libelleChampPerso3 	= CacheDatas.libellesPersoFicheLigne3;
			this.libelleChampPerso4 	= CacheDatas.libellesPersoFicheLigne4;
			*/
			this.texteChampPerso1 		= obj[1][0].V1;
			this.texteChampPerso2	 	= obj[1][0].V2;
			this.texteChampPerso3 		= obj[1][0].V3;
			this.texteChampPerso4 		= obj[1][0].V4;
			
			return this;
		}
		
		public function addLibellePerso(obj:Object):void
		{
			this.libelleChampPerso1=obj[0].C1;
			this.libelleChampPerso2=obj[0].C2;
			this.libelleChampPerso3=obj[0].C3;
			this.libelleChampPerso4=obj[0].C4;
		}
		
	}

}