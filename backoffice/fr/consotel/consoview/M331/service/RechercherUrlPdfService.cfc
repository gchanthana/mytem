﻿<!-- displayname : le nom de composant -->
<cfcomponent displayname="URL_PDF" output="false">
	
	<!-- déclarer une fonction  name : le nom de la fonction ;returntype : le type du resultat retourné -->
	<!-- access : visibilité -->
	
	<cffunction access="remote" output="false" name="getUrlPdf" returntype="String">
		
			<!-- déclarer des varaibles pour passer au procédure -->

			<cfset codeApp  = SESSION.CODEAPPLICATION>
			
			<!-- dataSource: obligatoires - nom de la source de données qui pointe vers la base de données qui contient une procédure stockée. -->
			<!-- procédure : Obligatoires nom de la procédure stockée sur le serveur de base de données.-->
			 
             <cfstoredproc datasource="ROCOFFRE" procedure="pkg_m00.get_appli_properties">	
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#codeapp#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="" null="true">
				<cfprocresult name="qReturn">
			 </cfstoredproc>
			

			<cfset urlPdf="cache.consotel.fr">
			<cfloop query="qReturn"><!--- cfloop sur un query  --->
			
				<cfif comparenocase(qReturn.KEY,"URL_CACHE") eq 0 >
					<cfset urlPdf=qReturn.VALUE>
				</cfif>
			</cfloop>
			
            <cfreturn urlPdf><!-- le resultat de la procedure-->
      </cffunction>
	
</cfcomponent>