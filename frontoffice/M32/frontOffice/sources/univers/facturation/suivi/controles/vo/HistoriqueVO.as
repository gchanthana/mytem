package univers.facturation.suivi.controles.vo
{
	public class HistoriqueVO
	{
		public var idEtat:Number = 0;
		public var libelleEtat:String = "";
		public var commantaireEtat:String = "";
		public var dateAction:Date = null;
		public var utilisateur:String = "";
		public var mail:String = "";
		public var inventaire:Number = 0;
		public var dateInsert:Date = null;
		
		public function HistoriqueVO()
		{
		}
		
		public function addHistorique(value:Object):void
		{
			this.idEtat = value.IDINV_OP_ACTION;
			this.libelleEtat = value.LIBELLE_ETAT;
			this.commantaireEtat = value.COMMENTAIRE_ETAT;
			this.dateAction = value.DATE_ACTION;
			this.utilisateur = value.UTILISATEUR;
			this.mail = value.LOGIN_EMAIL;
			this.inventaire = value.DANS_INVENTAIRE;
			this.dateInsert = value.DATE_INSERT;
		}
		
	}
}