package gestionparc.ihm.action.reprendre.actionreprendreligneext
{
	import gestionparc.ihm.action.ICommand;

	public class ActionReprendreLigneExtCommand implements ICommand
	{
		private var receiver:ActionReprendreLigneExtReceiver=null;

		public function ActionReprendreLigneExtCommand(rec:ActionReprendreLigneExtReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
