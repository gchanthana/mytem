package univers.facturation.suivi.controles.vo
{
	
	import mx.resources.ResourceManager;
	
	[Bindable]
	public class HistoriqueProduitInventaireVO
	{
		public var idHistorique			 : int 				= -1; 
		
		public var dateEffet			 : Date				= null;
		
		public var description			 : String			= ""; 
		
		public var qui					 : String			= ""; 
		
		public var quand				 : Date				= null;
		
		public function fillHistorique(obj:Object):void
		{
			
			this.idHistorique	= obj.IDINV_OP_ACTION;
			this.dateEffet 		= obj.DATE_ACTION;
			this.qui			= obj.UTILISATEUR;
			this.quand	 		= obj.DATE_INSERT;
			
			if(obj.DANS_INVENTAIRE == 1 && obj.LIBELLE_ETAT == ResourceManager.getInstance().getString('M32', 'Initial'))
			{
				this.description = ResourceManager.getInstance().getString('M32', 'Le_produit_est_entr__dans_l_inventaire');
			}
			else if(obj.DANS_INVENTAIRE == 0 && obj.LIBELLE_ETAT == ResourceManager.getInstance().getString('M32', 'Initial'))
			{
				this.description = ResourceManager.getInstance().getString('M32', 'Le_produit_est_sorti_de_l_inventaire');
			}
			else
			{
				this.description = obj.LIBELLE_ETAT;
			}
						
		}
		
	}
}