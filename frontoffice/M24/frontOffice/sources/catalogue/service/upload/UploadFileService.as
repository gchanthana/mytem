package catalogue.service.upload
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import catalogue.vo.FileUpload;
	
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
		public class UploadFileService extends EventDispatcher	{				//----------- VARIABLES -----------//		private var _UUID						:String = "";
				private static var _path:String = "fr.consotel.consoview.M24.upload.UploaderFile";						//----------- METHODES -----------//		
		/* */
		public function UploadFileService()
		{
		}
		
		/**
		 * Upload only one file with a target (example upload entreprise logo / upload files of  news ...  )
		 * @ filesToUpload : an array of file
		 * @idTarget : target ID (example personID)
		 *  */		public function uploadFile(currentFile:FileUpload, idTarget:int= - 1): void
		{			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,																				_path,																				"uploadFile",																				uploadFileResultHandler);
						RemoteObjectUtil.callService(op,currentFile, idTarget);			}
		
		/**
		 * Upload many files with a target (example upload entreprise logo / upload files of  news ...  )
		 * @ filesToUpload : an array of file
		 * @idTarget : target ID (example personID)
		 *  */
		public function uploadFiles(filesToUpload:Array, idTarget:int = -1): void
		{			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_path,
																				"uploadFiles",
																				uploadFilesResultHandler);
			
			RemoteObjectUtil.callService(op,filesToUpload,idTarget);	
		}
		

		public function removeFile(currentFile:FileUpload, isDataBaseRecord:Boolean): void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_path,
																				"removeFile",
																				removeFileResultHandler);
			
			RemoteObjectUtil.callService(op,currentFile/*, Formator.formatBoolean(isDataBaseRecord)*/);	
		}		
		
		
		//----------- RETOURS - HANDLERS -----------//
		
		/* */		private function uploadFileResultHandler(re:ResultEvent):void		{			if(re.result > 0)				dispatchEvent(new Event('FILE_UPLOADED'));			else				ConsoviewAlert.afficherAlertInfo("Une erreur est survenue lors de l'upload du fichier", 'Consoview', null);
		}
		
		/* */
		private function uploadFilesResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				var bool	:Boolean = true;
				var len		:int = (re.result as Array).length;
				
				for(var i:int=0; i<len; i++)
				{
					if(re.result[i] == 0)
					{
						bool = false;
						break;
					}
				}
				
				if(bool)
					dispatchEvent(new Event('FILES_UPLOADED'));
				else
					ConsoviewAlert.afficherAlertInfo("Une erreur est survenue lors de l'upload du fichier", 'Consoview', null);
			}
			else
				ConsoviewAlert.afficherAlertInfo("Une erreur est survenue lors de l'upload du fichier", 'Consoview', null);
		}
		
		/* */
		private function renameFileResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
				dispatchEvent(new Event('FILE_RENAMED'));
			else
				ConsoviewAlert.afficherAlertInfo("Une erreur est survenue lors du renommage du fichier", 'Consoview', null);
		}
		
		/* */
		private function removeFileResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
				dispatchEvent(new Event('FILE_REMOVED'));
			else
				ConsoviewAlert.afficherAlertInfo("Une erreur est survenue lors de la suppression du fichier", 'Consoview', null);
		}
		
		public function createUUID(): void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_path,
																				"initUUID", 
																				createUUIDResultHandler);		
			RemoteObjectUtil.callService(op);	
		}
		
		/* Handler */
		private function createUUIDResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				_UUID = re.result as String;
				dispatchEvent(new Event("FILE_UUID_CREATED"));
			}
		}
		
		public function get UUID():String
		{
			return _UUID;
		}	}}