package gestionparc.ihm.fiches.popup
{
	import flash.events.Event;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.ligne.LigneServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.controls.TileList;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	
	[Bindable]
	public class PopUpChoiceSubscriptionImpl extends VBox
	{

//VARIABLES------------------------------------------------------------------------------

		// Checkbox
		public var ckbxSelectAll			:CheckBox;
		/*public var ckbxVoix					:CheckBox;
		public var ckbxData					:CheckBox;
		public var ckbxPushMail				:CheckBox;	
		*/
		// tile
		public var tFiltre					:TileList;
		// DataGrid
		public var dgListProduit			:DataGrid;
		
		// TextInput
		public var txtptSearch				:TextInput;
		
		// Label
		public var lblFavoris				:Label;
		public var lblFavorisUnder			:Label;
		public var lblTout					:Label;
		public var lblToutUnder				:Label;
		
		// Variables exterieurs
		public var myServices			 	: LigneServices;
		public var idOperateur				: Number;
		public var typeCmdSelected			: Number;
		
		// Variables Locales
		private var _listProduit			:ArrayCollection = new ArrayCollection();
		private var _listProduitFavoris		:ArrayCollection = new ArrayCollection();
		private var _listFiltre				:ArrayCollection = new ArrayCollection();
		private var _arrayTemp				:ArrayCollection = new ArrayCollection();
		private var _listProduitVisible		:ArrayCollection = new ArrayCollection();
		private var _isFavorite				:Boolean = true;
		private var _isMultiSelect			:Boolean = false;
		
//VARIABLES------------------------------------------------------------------------------

//PROPRIETEES PUBLIC---------------------------------------------------------------------		
		
		public function get isMultiSelect():Boolean
		{
			return _isMultiSelect;
		}

		public function set isMultiSelect(value:Boolean):void
		{
			_isMultiSelect = value;
		}

		public function get listProduitFavoris():ArrayCollection
		{
			return _listProduitFavoris;
		}

		public function set listProduitFavoris(value:ArrayCollection):void
		{
			_listProduitFavoris = value;
		}

		public function get listFiltre():ArrayCollection
		{
			return _listFiltre;
		}

		public function set listFiltre(value:ArrayCollection):void
		{
			_listFiltre = value;
			tFiltre.dataProvider = _listFiltre;
		}

		public function get listProduit(): ArrayCollection
	    {
	    	return _listProduit;
	    }
	    
	    public function set listProduit(value:ArrayCollection): void
	    {
	    	_listProduit = value;
	    }

//PROPRIETEES PUBLIC---------------------------------------------------------------------

//METHODE PUBLIC-------------------------------------------------------------------------

		//CONSTRUCTEUR
		public function PopUpChoiceSubscriptionImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		public function getSelectedAboOpt():ArrayCollection
		{
			var tmpArray:ArrayCollection = new ArrayCollection();
			for each(var obj:Object in listProduit)
			{
				if(obj.SELECTED)
				{
					if(this.id == "popupAbo")
						obj.ISABO = true;
					else
						obj.ISABO = false;
					tmpArray.addItem(obj);
				}
			}
			return tmpArray
		}
		public function ckbxChangeHandler():void
		{
			checkSelectAll();
			whatIsChbxSelected();
		}
//METHODE PUBLIC-------------------------------------------------------------------------

//METHODE PROTECTED----------------------------------------------------------------------
		protected function txtptSearchHandler():void
		{
			whatIsChbxSelected();
		}
		protected function btReinitialisationClickHandler():void
		{
			txtptSearch.text = "";
			_listProduitVisible = new ArrayCollection();
			ckbxSelectAll.selected = true;
			
			for each( var obj:Object in listProduit)
			{
				obj.SELECTED = false;
			}
			
			for each( var obj1:Object in listFiltre)
			{
				obj1.SELECTED = true;
			}
			
			listProduit.refresh();
			listFiltre.refresh();
			
			whatIsChbxSelected();
		}
		protected function ckbxSelectAllChangeHandler():void
		{
			for each(var obj:Object in listFiltre)
			{
				obj.SELECTED = ckbxSelectAll.selected;
			}
			listFiltre.refresh();
			whatIsChbxSelected();
		}
		protected function lblFavorisClickHandler():void
		{
			_isFavorite = true;
			
			lblFavoris.visible = false;
			lblFavoris.width = 0;
			lblFavorisUnder.visible = true;
			lblFavorisUnder.width = 60;
			
			lblTout.visible = true;
			lblTout.width = 30;
			lblToutUnder.visible = false;
			lblToutUnder.width = 0;
			
			whatIsChbxSelected();
		}
		protected function lblToutClickHandler():void
		{
			_isFavorite = false;
			
			lblTout.visible = false;
			lblTout.width = 0;
			lblToutUnder.visible = true;
			lblToutUnder.width = 30;
			
			lblFavoris.visible = true;
			lblFavoris.width = 60;
			lblFavorisUnder.visible = false;
			lblFavorisUnder.width = 0;

			whatIsChbxSelected();
		}
		protected function dgListProduitClickHandler(evt:ListEvent):void
		{
			if(dgListProduit.selectedItem != null && evt.columnIndex != 3) // columnIndex == 3 == favoris
			{
				var longueur : int = listProduit.length;
				for(var i:int = 0;i < longueur;i++)
				{
					if( listProduit[i].IDPRODUIT_CATALOGUE.toString().toLowerCase().indexOf(dgListProduit.selectedItem.IDPRODUIT_CATALOGUE.toString().toLowerCase()) != -1)
					{ 
						listProduit[i].SELECTED = !listProduit[i].SELECTED;
					}
					else if(!_isMultiSelect)
					{
						listProduit[i].SELECTED = false;
					}
				}
			}
			listProduit.refresh();
		}
		
//METHODE PROTECTED----------------------------------------------------------------------

//METHODE PRIVATE------------------------------------------------------------------------
		
		//INITIALISE APRES LA CREATION DE TOUS LES COMPOSANTS
		private function init(evt:FlexEvent):void
		{
			initListener();
		}
		private function initData(event:Event = null):void
		{
			if(this.id == "popupAbo"
				&& this.listFiltre != myServices.myDatas.listeTypeAbo)
			{
				this.listFiltre = myServices.myDatas.listeTypeAbo;
				this.listProduit = myServices.myDatas.listeAbo;
			}
			else if(this.id == "popupOpt"
				&& this.listFiltre != myServices.myDatas.listeTypeOpt)
			{
				this.listFiltre = myServices.myDatas.listeTypeOpt;
				this.listProduit = myServices.myDatas.listeOpt;
			}
			whatIsChbxSelected();
		}
		//INITIALISE LES ECOUTEURS
		private function initListener():void
		{
			myServices.myDatas.addEventListener(gestionparcEvent.LISTED_ABO_OPT_LOADED,initData);
			addEventListener(gestionparcEvent.THIS_IS_FAVORITE, thisIsAFavoriteSelected);
			addEventListener(gestionparcEvent.THIS_IS_NOT_FAVORITE, thisIsNotFavoriteSelected);
		}		
		//REGADE QUELLES CHECKBOX SONT SELECTIONNEES ET AFFICHE LES OPTIONS SUIVANT LE FILTRE
		private function whatIsChbxSelected(evt:Event=null):void
		{
			if(listProduit != null)
			{
				listProduit.filterFunction = arrayFiltre;
				listProduit.refresh();
			}
		}
		private function arrayFiltre(item:Object):Boolean
		{
			var isValid:Boolean = false;
			// Si la case "tout" estdécoché
			if(!ckbxSelectAll.selected)
			{
				for each(var obj:Object in listFiltre)
				{
					if(obj.SELECTED)
					{
						if(item.IDTHEME_PRODUIT == obj.ID
						&& isItemLikeSearch(item))
						{
							isValid = isItemFavorite(item);
							break;
						}
					}
				}
			}
			// Si la case "tout" est coché
			else 
			{
				if(isItemLikeSearch(item) && isItemFavorite(item))
				{
					isValid = true;
				}
			}
			
			return isValid;
		}
		private function isItemLikeSearch(item:Object):Boolean
		{
			if(item.LIBELLE_PRODUIT.toString().toUpperCase().search(txtptSearch.text.toUpperCase()) > -1)
				return true;
			else
				return false;
		}
		private function isItemFavorite(item:Object):Boolean
		{
			if(_isFavorite && item.FAVORI == 1 || !_isFavorite)
				return true;
			else
				return false;
		}
		//VERIFIE SI TOUS LES FILTRES SONT COCHES OU PAS
		private function checkSelectAll():void
		{
			var allSelected:Boolean = true;			
			for each(var obj:Object in listFiltre)
			{
				if(!obj.SELECTED)
				{
					allSelected = false;
					break;
				}
			}
			ckbxSelectAll.selected = allSelected;
		}
		//APPEL DE PROCEDURE PERMETTANT L'ENVOI D'UNE OPTIONS SELECTIONNE EN TANT QUE 'FAVORI'
		//APPEL DE PROCEDURE PERMETTANT DE LISTER TOUTES LES OPTIONS
		private function thisIsAFavoriteSelected(e:Event):void
		{
			if(dgListProduit.selectedItem != null && dgListProduit.selectedItem.FAVORI == 1)
			{
				myServices.ajouterProduitAMesFavoris(dgListProduit.selectedItem);
				// copie l'item dans les favoris
				myServices.myDatas.listeAboFavoris.addItem(dgListProduit.selectedItem);
			}
		}
		
		//APPEL DE PROCEDURE PERMETTANT L'ENVOI D'UNE OPTIONS SELECTIONNE EN TANT QUE 'NON FAVORI'
		//APPEL DE PROCEDURE PERMETTANT DE LISTER TOUTES LES OPTIONS
		private function thisIsNotFavoriteSelected(e:Event):void
		{
			if(dgListProduit.selectedItem != null && dgListProduit.selectedItem.FAVORI == 0)
			{
				myServices.supprimerProduitDeMesFavoris(dgListProduit.selectedItem);
				// recherche et supprime l'item des favoris
				var longueur : int = myServices.myDatas.listeAboFavoris.length;
				for(var i:int = 0; i < longueur ; i++)
				{
					if(myServices.myDatas.listeAboFavoris[i] == dgListProduit.selectedItem)
					{
						myServices.myDatas.listeAboFavoris.removeItemAt(i);
						break;
					}
				}
				whatIsChbxSelected();
			}
		}
	}
}