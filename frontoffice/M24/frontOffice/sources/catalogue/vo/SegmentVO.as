package catalogue.vo
{
	[Bindable]
	public class SegmentVO
	{
		private var _LIBELLE:String;
		private var _IDSEGMENT:Number;
		
		public function SegmentVO()
		{
		}
		
				
		public function fill(value:Object):void
		{
			LIBELLE = value.LIBELLE;
			IDSEGMENT = value.IDSEGMENT; 	
		}

		public function set LIBELLE(value:String):void
		{
			_LIBELLE = value;
		}

		public function get LIBELLE():String
		{
			return _LIBELLE;
		}
		public function set IDSEGMENT(value:Number):void
		{
			_IDSEGMENT = value;
		}

		public function get IDSEGMENT():Number
		{
			return _IDSEGMENT;
		}
		
	}
}