package univers.facturation.suivi.controles.services
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	public class SiteHandler
	{
		private var _model:SiteModel;
		
		public function SiteHandler(model:SiteModel)
		{
			_model = model;
		}
		
		internal function getSiteHandler(event:ResultEvent):void
		{
			_model.updateSite(event.result as ArrayCollection);
		}
		
	}
}