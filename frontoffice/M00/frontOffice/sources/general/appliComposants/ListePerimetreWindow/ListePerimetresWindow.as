package general.appliComposants.ListePerimetreWindow {
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.XMLListCollection;
	import mx.controls.Alert;
	import mx.controls.Tree;
	import mx.events.DropdownEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import general.appliComposants.ListePerimetreWindow.SearchPerimetreWindow.SearchPerimetreWindowImpl;
	import general.appliComposants.ListePerimetreWindow.SimplePerimetreTree.GenericPerimetreTree;
	import general.appliComposants.ListePerimetreWindow.SimplePerimetreTree.SimplePerimetreTree;
	import general.appliComposants.SpecialSearch.SpecialSearch;
	import general.appliComposants.rechercheglobale.RechObjectVO;
	import general.event.ConsoViewDataEvent;
	import general.event.PerimetreEvent;
	import general.event.PerimetreTreeEvent;
	import general.interfaceClass.IPerimetreControl;
	
	public class ListePerimetresWindow extends ListePerimetresWindowIHM implements general.interfaceClass.IPerimetreControl {
		public static const CHANGE_GROUP_ACTION:int = 1;
		public static const CHANGE_PERIMETRE_ACTION:int = 2;
		
		private var perimetreBack : XML;
		private var _selectedNode : XML;  
		private var _boolNewPerimetreFromSearch : Boolean;
		
		
		/**
		 * Référence vers la classe responsable du chargement d'un noeud donné lorsqu'il n'est pas encore 
		 * en mémoire.
		 * */
		private var _search : SpecialSearch;
		
		public function getSelectedGroupIndex():int {
			return groupListItem.selectedIndex;
		}
		
		public function getSelectedNode():XML{			
			return _selectedNode;
		}
		
		public function getListeNode():Array
		{
			return (perimetreTree as SimplePerimetreTree).listeNode
		}
		
		public function ListePerimetresWindow() {
			super();
			trace("(ListePerimetresWindow) Instance Creation");
		}
		
		protected override function initIHM(evt:FlexEvent):void 
		{
			super.initIHM(evt);
			trace("(ListePerimetresWindow) Performing IHM Initialization");
			mainView.selectedIndex = 0;
			this.setStyle("borderStyle","solid");
			this.width = ((ConsoViewModuleObject.cvWidth * 35) / 45); // Coeff : 0.77
			this.height = ConsoViewModuleObject.cvHeight / 2;
			
			perimetreTree.addEventListener(PerimetreEvent.PERIMETRE_NODE_CHANGED,handleChangeNodeEvent);
			addEventListener(PerimetreEvent.PERIMETRE_NODE_CHANGED,handleChangeNodeEvent);
			searchTree.addEventListener(ConsoViewDataEvent.BACK_NODE_SEARCH,handleSearchControlEvents);
			
			nodeSearchItem.addEventListener(MouseEvent.CLICK,handlePerimetreControlEvents);
			searchInput.addEventListener(FlexEvent.ENTER,handlePerimetreControlEvents);
			
			cancelItem.addEventListener(MouseEvent.CLICK,cancelPerimetre);
			groupListItem.addEventListener(DropdownEvent.CLOSE,changeGroup);
			groupListItem.labelField = "LIBELLE_GROUPE_CLIENT";
			groupListItem.dataProvider = CvAccessManager.getSession().GROUP_LIST;
			for each(var item:Object in CvAccessManager.getSession().GROUP_LIST)
			{
				if(item.IDGROUPE_CLIENT == CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX)
				{
					groupListItem.selectedItem = item;
					break;
				}
			}
			searchInput.text = "";
			searchInput.enabled = nodeSearchItem.visible = true;
			
			_selectedNode = perimetreTree.getSelectedItem() as XML;
			/*ICI*/
			if(evt.type == FlexEvent.CREATION_COMPLETE) {
				
				//--AJOUT SAMUEL pour les GroupeLigne SAV
				(perimetreTree as SimplePerimetreTree).addEventListener(Event.CHANGE,afficherBtSupprimerSAV);
				//--AJOUT SAMUEL pour la recherche --------------//
				searchTree.addEventListener(SearchPerimetreWindowImpl.NODE_DD_CLICKED,changePerimetreFromSearch);				
				//-- FIN AJOUT SAMUEL pour la recherche --------------//	
				vaildItem.addEventListener(MouseEvent.CLICK,validItemClickHandler);
				perimetreTree.addEventListener(MouseEvent.DOUBLE_CLICK,changePerimetre);
				
			}
			/*fin ICI*/
		}
		
		public function updatePerimetre():void {
			trace("(ListePerimetresWindow) Updating Perimetre");
			if(perimetreTree != null)
				perimetreTree.onPerimetreChange(); // If Perimetre Data has been updated
		}
		
		private function changeGroup(event:DropdownEvent):void {
			var currentGroupIndex:int = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			if(groupListItem.selectedItem.IDGROUPE_CLIENT != currentGroupIndex) {
				searchInput.text = "";
				selectGroup(groupListItem.selectedItem.IDGROUPE_CLIENT);
			}
		}
		private function changePerimetre(evt:MouseEvent):void
		{
			perimetreTree.onChangePerimetre(evt);
			PopUpManager.removePopUp(this);
		}
		public function selectGroup(groupIndex:int):void 
		{
			PopUpManager.removePopUp(this);
			CvAccessManager.changeGroupe(groupIndex);
		}
		
		private function handleChangeNodeEvent(event:general.event.PerimetreEvent):void {
			if(CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX == Number((perimetreTree.getSelectedItem() as XML).@NID)) return;
			
			_selectedNode = perimetreTree.getSelectedItem() as XML;
			PopUpManager.removePopUp(this);
			perimetreTree.onChangePerimetre(null);
		}
		
		private function handlePerimetreControlEvents(event:Event):void{
			
			if(searchInput.text.length > 2 && perimetreTree.getSelectedItem() != null) {
				
				var xmlAll : XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList;
				
				var idPerimtre : Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
				
				
				if (CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE.toUpperCase() == "GROUPELIGNE"){
					var node : XMLList = xmlAll.descendants("NODE").(@NID == idPerimtre);
					perimetreBack = ObjectUtil.copy(node[0]) as XML;
				}else{
					perimetreBack = ObjectUtil.copy(xmlAll) as XML;
				} 				
				
				_selectedNode = perimetreTree.getSelectedItem() as XML;
				trace("(ListePerimetresWindow) Searching from node " + getSelectedNode().@LBL +
					" criteria : " + searchInput.text);
				searchTree.performSearch(parseInt(getSelectedNode().@NID,10),searchInput.text);
				mainView.selectedIndex = 1;
			} else {
				Alert.show(ResourceManager.getInstance().getString('M00', 'Merci_de_saisir_au_moins_3_caract_res'),ResourceManager.getInstance().getString('M00', 'Aide'));
			}
		}
		
		private function handleSearchControlEvents(event:Event):void {
			trace("(ListePerimetresWindow) Restore Perimetre Tree Control (From Search IHM)");
			
			mainView.selectedIndex = 0;
			retourPerimetreInitiale();
			
		}
		
		private function cancelPerimetre(event:Event):void {
			PopUpManager.removePopUp(this);
		}
		
		public function logoff():void {
			if(this.initialized == true)
				PopUpManager.removePopUp(this);
		}
		
		///---- AJOUT SAMUEL ------- pour les Groupe de Lignes ////
		//Handler du click sur un noeud de l'arbre
		//(Affiche le bouton Supprimer la recherche lorsqu'on click sur un Noeud de type SAV
		//@param Event
		private function afficherBtSupprimerSAV(evt : Event):void{
			var tree : Tree = Tree(evt.currentTarget);
			if( tree != null){				
				var nid : Number =Number((tree.selectedItem as XML).@NID);
				if ((tree.selectedItem as XML).parent() != null){
					if ((Number((tree.selectedItem as XML).parent().@NID) == -4) 
						&& (nid > 0)){
						btSupGPSAV.visible = true;	
						btSupGPSAV.addEventListener(MouseEvent.CLICK,btSupGPSAVClickHandler);					
					}else{
						btSupGPSAV.visible = false;
					}
				}else{
					btSupGPSAV.visible = false;
				}				
			}else{
				btSupGPSAV.visible = false;
			}
		}
		
		private function checkDeleteNode(nid : Number):Boolean{
			var xmlPerimetreData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;	
			var parent : XML = (xmlPerimetreData.NODE[2].NODE.(@NID == nid)[0] as XML).parent();
			var parentID : int = parseInt(parent.@NID,10);
			if (CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX == parentID) return false;
			else return true;
			
		}
		
		//Handler du click sur le bouton supprimer le noeud 
		//param in MosueEvent		
		private function btSupGPSAVClickHandler(me : MouseEvent):void{
			var nid : Number = perimetreTree.getSelectedItem().@NID;
			var parent : XML = (perimetreTree.getSelectedItem() as XML).parent();
			var child : XML = ((perimetreTree.getSelectedItem() as XML).children()[0] != null)?
				(perimetreTree.getSelectedItem() as XML).children()[0] : <NODE NID="0"/> ;
			
			
			if ((CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX != nid) &&
				(CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX != child.@NID))
			{	
				var rech : RechObjectVO = new RechObjectVO();	
				rech.ID = nid;		    
				
				var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoview.rechercheglobale.RLigneGateWay",
					"deleteRech",supprimerSavResult);
				RemoteObjectUtil.callService(op,rech); 
				
			}else{
				Alert.show(ResourceManager.getInstance().getString('M00', 'Impossible_de_supprimer_un_noeud_sur_lequel_vous__tes'));
				
			}
			
		}
		
		private function supprimerSavResult(re : ResultEvent):void{
			if (re.result > 0){
				var nid : Number = perimetreTree.getSelectedItem().@NID;	
				var parent : XML = (perimetreTree.getSelectedItem() as XML).parent();
				
				var xmlPerimetreData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;	
				var node : Object = xmlPerimetreData.NODE[2].NODE.(@NID == nid);			
				
				(perimetreTree as Tree).expandItem(parent,false);
				var col : XMLListCollection = new XMLListCollection(parent.children());
				if (col.length == 1) parent.@NTY = 0;
				col.removeItemAt(col.getItemIndex(xmlPerimetreData.NODE[2].NODE.(@NID == nid)[0]));	
				(perimetreTree as Tree).selectedIndex = -1;				
				(perimetreTree as Tree).expandItem(parent,true);
				btSupGPSAV.visible = false;
				Alert.show(ResourceManager.getInstance().getString('M00', 'Sauvegarde_effac_e'));	
			}else{
				Alert.show(ResourceManager.getInstance().getString('M00', 'Echec_lors_de_la_suppression'));
			}
			
			
			
		}		
		///---- FIN AJOUT -----------------------------------////
		
		
		///---- AJOUT SAMUEL ------- pour la recherche///
		
		//retour au perimetre avant la recherche		
		private function retourPerimetreInitiale():void{
			if(_boolNewPerimetreFromSearch){
				_selectedNode = ObjectUtil.copy(perimetreBack) as XML;
				perimetreBack = null;
				_boolNewPerimetreFromSearch = false;
				dispatchEvent(new PerimetreEvent(PerimetreEvent.PERIMETRE_NODE_CHANGED));	
			}
		}
		
		
		//change le perimetre depuis la recherche
		private function changePerimetreFromSearch(event:Event):void {
			var nextNode:XML= SearchPerimetreWindowImpl(searchTree).searchTree.selectedItem as XML;
			
			if(CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX == Number(nextNode.@NID)) return;
			
			_selectedNode = SearchPerimetreWindowImpl(searchTree).searchTree.selectedItem as XML;
			if (_selectedNode != null && (Number(_selectedNode.@NID) > 0)){
				_boolNewPerimetreFromSearch = true;				
				Tree(perimetreTree).selectedItem = _selectedNode;
				
				
				var xmlPerimetreData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;			  
				var resultList:XMLList = xmlPerimetreData.descendants("NODE").(@NID == _selectedNode.@NID);
				
				if(resultList.length() == 0){//->le noeud n'est pas encore en mémoire
					deroulerLesNoeuds(Number(_selectedNode.@NID));	
				}else{
					PopUpManager.removePopUp(this);
					CvAccessManager.changePerimetre(_selectedNode.@NID);
				}
				
			}else{
				_boolNewPerimetreFromSearch = false;
				Alert.show(ResourceManager.getInstance().getString('M00', 'Vous_devez_s_lectionner_un_noeud'),ResourceManager.getInstance().getString('M00', 'Erreur'));
			}
		}
		
		//
		private function validItemClickHandler(evt:MouseEvent):void
		{
			//quelle vue
			if(mainView.selectedIndex == 0)
			{
				var obj:Object = perimetreTree.getSelectedItem(); 
				if( obj != null)
				{
					PopUpManager.removePopUp(this);
					CvAccessManager.changePerimetre(obj.@NID);
				}
			}
			else
			{	
				if(SearchPerimetreWindowImpl(searchTree).searchTree.selectedItem != null)
				{
					PopUpManager.removePopUp(this);
					changePerimetreFromSearch(null);	
				}
			}
		}
		private function deroulerLesNoeuds(idNode : Number):void{
			//aller chercher l'orga du noeud		
			
			//le noeud n'est pas encore en mémoire alors on le charge
			_search = new SpecialSearch();
			_search.addEventListener(PerimetreTreeEvent.SEARCH_ENDS,perimetreTreeEventSearchEndsHandler);	
			_search.initSearch(perimetreTree as GenericPerimetreTree,idNode);		
		}
		private function perimetreTreeEventSearchEndsHandler(evt:PerimetreTreeEvent):void
		{
			_search.removeEventListener(PerimetreTreeEvent.SEARCH_ENDS,perimetreTreeEventSearchEndsHandler);
			PopUpManager.removePopUp(this);
			dispatchEvent(new PerimetreEvent(PerimetreEvent.PERIMETRE_NODE_CHANGED));
		}
		//-------FIN AJOUT SAMUEL --------------//
	}
}
