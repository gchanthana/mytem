<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction"
			displayname="ActAL010" hint="Liste des PJs affilié à une actualité" output="false">
	
	<!--- 
		retourne la liste des PJs d'une actualité
	 --->
	<cffunction name="executeAct" access="remote" returntype="Struct" output="false"
				hint="retourne la liste des PJs d'une actualité">
		
		<cfargument name="data" type="Struct" required="true" hint="contient IDACTUALITE">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.getFileActualite">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_codeApp" type="in" value="#data.IDACTUALITE#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			<cfreturn CreateReponse("AL010", 1, qResult)>
			
		<cfcatch type="Any">
			<cfreturn CreateReponse("AL010", 0, ThrowCfCatchError("AL010",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>