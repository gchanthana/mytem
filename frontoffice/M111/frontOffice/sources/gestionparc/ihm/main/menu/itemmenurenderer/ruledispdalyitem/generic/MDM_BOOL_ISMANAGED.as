package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;

	public class MDM_BOOL_ISMANAGED implements IRuleDisplayItem
	{
		public function MDM_BOOL_ISMANAGED()
		{
		}

		public function isDisplay():Boolean
		{
			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;
			if (ligne != null && ligne.BOOL_ISMANAGED)
				return true;
			else
				return false;
		}
	}
}
