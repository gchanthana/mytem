<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.process.evenement.EvenementDemo1"
extends="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement" hint="Exemple d'implémentation d'un évènement basé sur Evenement">
	<!--- fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement --->
	<cffunction access="public" name="getName" returntype="String" hint="Retourne le nom de l'évènement">
		<cfreturn "Evènement Démo 1">
	</cffunction>

	<cffunction access="private" name="requiredProperties" returntype="Struct" hint="Retourne la liste des propriétés requises par l'évènement">
		<cfreturn SUPER.requiredProperties()>
	</cffunction>

	<cffunction access="private" name="getReportList" returntype="query" hint="Exécute et retourne la requete ramenant la liste d'exécution">
		<cfquery name="qEvenement" datasource="ROCOFFRE">
			select	/* Champs métiers */
						1 as ID, 'Container' as DELIVERY, 'E00' as POST_PROCESS, 77084 as USERID, 1 as CODE_APPLI, 2458788 as IDRACINE,
						null as EMAIL_FROM, null as EMAIL_TO, null as EMAIL_CC, null as EMAIL_SUBJECT,
						'Evenement-Test' as FILE_NAME, 'pdf' as FILE_EXT, 0 as HAVE_ATTACHEMENT,
						/* Champs pour exécution du rapport BIP */
						'/CONSOTEL/IT' as DOSSIER, 'Historique' as XDO, 'VIEW' as TEMPLATE, 'html' as FORMAT, 'fr_FR' as LOCALISATION,
						/* (Tout le reste est considéré comme) Champs Paramètres du rapport BIP */
						'F' as P_STATUS, 2 as P_NB_JOURS, 'N' as P_DELETED
			from		DUAL
			UNION
			select	/* Champs métiers */
						2 as ID, 'Mail' as DELIVERY, 'E00' as POST_PROCESS, 77084 as USERID, 1 as CODE_APPLI, 2458788 as IDRACINE,
						'api@consotel.fr' as EMAIL_FROM, 'cedric.rapiera@consotel.fr' as EMAIL_TO, null as EMAIL_CC,
						'Evènement Test (Inline)' as EMAIL_SUBJECT, null as FILE_NAME, 'html' as FILE_EXT, 0 as HAVE_ATTACHEMENT,
						/* Champs pour exécution du rapport BIP */
						'/CONSOTEL/IT' as DOSSIER, 'Historique' as XDO, 'VIEW' as TEMPLATE, 'html' as FORMAT, 'fr_FR' as LOCALISATION,
						/* (Tout le reste est considéré comme) Champs Paramètres du rapport BIP */
						'F' as P_STATUS, 2 as P_NB_JOURS, 'N' as P_DELETED
			from		DUAL
			UNION
			select	/* Champs métiers */
						3 as ID, 'Mail' as DELIVERY, 'E00' as POST_PROCESS, 77084 as USERID, 1 as CODE_APPLI, 2458788 as IDRACINE,
						'api@consotel.fr' as EMAIL_FROM, 'cedric.rapiera@consotel.fr' as EMAIL_TO, null as EMAIL_CC,
						'Evènement Test (Attachment)' as EMAIL_SUBJECT, 'Evenement-Test' as FILE_NAME, 'pdf' as FILE_EXT, 1 as HAVE_ATTACHEMENT,
						/* Champs pour exécution du rapport BIP */
						'/CONSOTEL/IT' as DOSSIER, 'Historique' as XDO, 'VIEW' as TEMPLATE, 'pdf' as FORMAT, 'fr_FR' as LOCALISATION,
						/* (Tout le reste est considéré comme) Champs Paramètres du rapport BIP */
						'F' as P_STATUS, 2 as P_NB_JOURS, 'N' as P_DELETED
			from		DUAL
		</cfquery>
		<cfreturn qEvenement>
	</cffunction>
</cfcomponent>