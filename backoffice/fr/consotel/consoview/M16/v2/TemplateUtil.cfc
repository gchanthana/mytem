﻿<cfcomponent displayname="fr.consotel.consoview.M16.v2.TemplateUtil">

	<cfset IDOPERATEUR_SPIE = 2195>	
	<cfset CODE_BDC_COMMANDE = "BDC">
	<cfset CODE_BDC_COMMANDE_SPIE = "BDC_SPIE">
	<cfset CODE_BDC_RESILIATION = "BDR">
	
	
	
	<!--- DONNE LE NOM DU TEMPLATE EN FONCTION DU TYPE DE COMMANDE ET DE L'OPÉRATEUR --->
	<cffunction name="getTemplateNameByOpeNType" access="remote" hint="DONNE LE NOM DU TEMPLATE EN FONCTION DU TYPE DE COMMANDE ET DE L'OPÉRATEUR" returntype="String">
		<cfargument name="idtypecommande" 	type="Numeric" 							required="true">
		<cfargument name="idoperateur" 		type="Numeric"  						required="true"/>
		<cfargument name="idracine" 		type="Numeric"  						required="true"/>
		
		<cfset _type = VARIABLES["CODE_BDC_COMMANDE"]/>			
		
		<cfswitch expression="#idtypecommande#">
			<cfcase value="1083;1182;1282;1382;1583;1584;1585;1587" delimiters=";">
				<cfset _type = VARIABLES["CODE_BDC_COMMANDE"]>						
			</cfcase>
			<cfcase value="1283;1383;1483;1586;1588;1589" delimiters=";">
				<cfset _type = VARIABLES["CODE_BDC_RESILIATION"]>						
			</cfcase>
			<cfdefaultcase>
				<cfset _type = VARIABLES["CODE_BDC_COMMANDE"]>
			</cfdefaultcase>
		</cfswitch>		
		<cfreturn _type & "-" & idoperateur>
	</cffunction>
	
	
	<cffunction name="getTemplateName" access="remote" hint="DONNE LE NOM DU TEMPLATE EN FONCTION DU TYPE DE COMMANDE DE LA RACINE ET DE L'OPÉRATEUR" returntype="String">
		<cfargument name="idtypecommande" 	type="Numeric" 							required="true">
		<cfargument name="idoperateur" 		type="Numeric"  						required="true"/>
		<cfargument name="idracine" 		type="Numeric"  						required="true"/>
		
		<cfset _type = VARIABLES["CODE_BDC_COMMANDE"]/>		
		
		<!--- sélection du type de commande --->
		<cfswitch expression="#idtypecommande#">
			<cfcase value="1083;1182;1282;1382;1583;1584;1585;1587" delimiters=";">
				<cfset _type = VARIABLES["CODE_BDC_COMMANDE"]>						
			</cfcase>
			<cfcase value="1283;1383;1483;1586;1588;1589" delimiters=";">
				<cfset _type = VARIABLES["CODE_BDC_RESILIATION"]>						
			</cfcase>
			<cfdefaultcase>
				<cfset _type = VARIABLES["CODE_BDC_COMMANDE"]>
			</cfdefaultcase>
		</cfswitch>		
		<cfreturn _type & "-" & idoperateur>
	</cffunction>
	
	<!--- DONNE LE TYPE DU TEMPLATE EN FONCTION DU TYPE DE COMMANDE ET DE L OPERATEUR--->
	
	<cffunction name="getTemplateType" access="remote" hint="DONNE LE TYPE DU TEMPLATE EN FONCTION DU TYPE DE COMMANDE" returntype="String">
		<cfargument name="idtypecommande" 	type="Numeric" 							required="true">
		<cfargument name="idoperateur" 		type="Numeric"  						required="false"/>
		
		<cfset _type = VARIABLES["CODE_BDC_COMMANDE"]/>
		
		
		<cfif IsDefined("idoperateur")>
			<cfif idoperateur eq  VARIABLES["IDOPERATEUR_SPIE"] >
				<cfset _type = VARIABLES["CODE_BDC_COMMANDE_SPIE"]>				
				<cfreturn _type>			
			</cfif>
		</cfif>
		
		
		
		
		<cfswitch expression="#idtypecommande#">
			<cfcase value="1083;1182;1282;1382;1583;1584;1585;1587" delimiters=";">
				<cfset _type = VARIABLES["CODE_BDC_COMMANDE"]>						
			</cfcase>
			<cfcase value="1283;1383;1483;1586;1588;1589" delimiters=";">
				<cfset _type = VARIABLES["CODE_BDC_RESILIATION"]>						
			</cfcase>
			<cfdefaultcase>
				<cfset _type = VARIABLES["CODE_BDC_COMMANDE"]>
			</cfdefaultcase>
		</cfswitch>
		
		<cfreturn _type>
	</cffunction>
	
	
	<cffunction name="findtemplate" access="private" hint="cherche un template de BDC" returntype="string">
		<cfargument name="type"			 	type="string" 	required="true">
		<cfargument name="idracine" 		type="numeric" 	required="true">
		<cfargument name="idoperateur" 		type="numeric" 	required="true">
		
		<cfset var templateArray = getRemoteTemplateList(arguments['type'])>
		<cfset var targetTemplate = arguments['type'] & '-' &  arguments['idracine']>	 
		<cfset var template = "">
				
		
		<cfif ArrayContains(templateArray,targetTemplate)>
			<cfset template = targetTemplate>		
			<cfelse>
				<cfset var targetTemplate = arguments['type'] & '-' &  arguments['idoperateur']>
				<cfif ArrayContains(templateArray,targetTemplate)>
					<cfset template = targetTemplate>
					<cfelse>
						<cfset template = "BDC">
				</cfif>
		</cfif>
		
		<cfreturn template>
	</cffunction>



	<cffunction name="getRemoteTemplateList" access="private" hint="DONNE LA LISTE DES TEMPLATE POUR UN TYPE DE COMMANDE DONNEE" returntype="array">
		<cfargument name="typeCommande" 	type="string" 	required="true">
		
		<cfset var BipCmdWebServiceFactory = new fr.consotel.api.ibis.publisher.BipCmdWebServiceFactory()>
		<cfset var webservice = BipCmdWebServiceFactory.createWebService()>
		<cfset var rapportdefinition = webservice.getReportDefinition('/consoview/gestion/flotte/#arguments["typeCommande"]#/#arguments["typeCommande"]#.xdo','consoview','public')>
		<cfset var templateArraylist = rapportdefinition.getTemplateIds()>
		
		<cfreturn templateArraylist>		
	</cffunction>
	
</cfcomponent>