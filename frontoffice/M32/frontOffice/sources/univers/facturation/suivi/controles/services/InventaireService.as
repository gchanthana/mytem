package univers.facturation.suivi.controles.services
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	import univers.facturation.suivi.controles.vo.InventaireVO;

	[Bindable]
	public class InventaireService
	{
		
		public var model:InventaireModel;
		public var handler:InventaireHandler;
		
		public function InventaireService()
		{
			model = new InventaireModel();
			handler = new InventaireHandler(model);
		}
		
		public function getInventaireFacture(paramInventaire:InventaireVO):void{
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M32.GetInventaire",
				"getHorsInventaireFacture",
				handler.getInventaireFactureHandler);
			RemoteObjectUtil.callService(opData,paramInventaire.idInventaire,
				paramInventaire.effectif);
			
		}
		
		public function getInventaireNonFacture(paramInventaire:InventaireVO):void{
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M32.GetInventaire",
				"getInventaireNonFacture",
				handler.getInventaireNonFactureHandler);
			RemoteObjectUtil.callService(opData,paramInventaire.idInventaire,
				paramInventaire.effectif,paramInventaire.solution);
			
		}
		
		public function getLibelleCompte(operateurID:Number):void{
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M32.GetInventaire",
				"getLibelleCompte",
				handler.getLibelleCompteHandler);
			RemoteObjectUtil.callService(opData,operateurID);
			
		}
		
	}
}