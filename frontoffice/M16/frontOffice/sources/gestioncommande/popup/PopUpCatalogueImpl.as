package gestioncommande.popup
{
	import composants.colorDatagrid.ColorDatagrid;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import gestioncommande.entity.EquipementsElements;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.CompteService;
	import gestioncommande.services.EquipementService;
	import gestioncommande.services.PoolService;
	import gestioncommande.services.RevendeurAutoriseService;
	import gestioncommande.services.RevendeurService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class PopUpCatalogueImpl extends TitleWindow
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var cbxTypesCommande		:ComboBox;
		public var cbxOperateurs		:ComboBox;
		public var cbxRevendeurs		:ComboBox;
		public var cbxEngagements		:ComboBox;
		public var cbxExport			:ComboBox;
		
		public var ckxTerminaux 		:CheckBox;
		public var ckxAccessoires 		:CheckBox;
		
		public var btnSearch			:Button;
		public var btnExport			:Button;

		public var tbxFiltre			:TextInput;

		public var dgListeEquipements	:ColorDatagrid;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _revAutorises		:RevendeurAutoriseService = new RevendeurAutoriseService();
		
		private var _eqptsSrv			:EquipementService = new EquipementService();		
		
		private var _poolService		:PoolService = new PoolService();
		
		private var _revService			:RevendeurService = new RevendeurService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		public var listeExports			:ArrayCollection = new ArrayCollection([
																					{LIBELLE:ResourceManager.getInstance().getString('M16', 'Export_Excel'), TYPE:"excel",	EXT:'xls', ID:0},
																					{LIBELLE:ResourceManager.getInstance().getString('M16', 'Export_PDF'), 	 TYPE:"pdf",	EXT:'pdf', ID:1},
																					{LIBELLE:ResourceManager.getInstance().getString('M16', 'Export_CSV'), 	 TYPE:"csv",	EXT:'csv', ID:2},
																				]);
		public var listeEquipements		:ArrayCollection = new ArrayCollection();
		
		public var idPool				:int = 0;
		public var idrevendeurpool		:int = 0;
		
		private var _isAutorise			:Boolean = false;
		
		private var _idSegment			:int = 1;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function PopUpCatalogueImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			
			addEventListener('DETAILS', detailsClickHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function cbxTypeCommandeChangeHandler(e:Event):void
		{
			if(_isAutorise)
				getOperateurs();
			else
			{
				if(cbxTypesCommande.selectedItem != null)
					getRevendeurs();
			}
			
			checkCombobox();
		}
		
		protected function cbxOperateurChangeHandler(e:Event):void
		{
			getEngagements();
			
			if(_isAutorise)
				getRevendeurs();
			
			checkCombobox();
		}
		
		protected function cbxRevendeurChangeHandler(e:Event):void
		{
			checkCombobox();
		}
		
		protected function rbtMobileClickHandler(me:MouseEvent):void
		{
			_idSegment = 1;
			
			if(!_isAutorise)
				getOperateurs();
		}
		
		protected function rbtFixeClickHandler(me:MouseEvent):void
		{
			_idSegment = 2;
			
			if(!_isAutorise)
				getOperateurs();
		}
		
		protected function rbtDataClickHandler(me:MouseEvent):void
		{
			_idSegment = 3;
			
			if(!_isAutorise)
				getOperateurs();
		}
		
		protected function btnSearchClickHandler(me:MouseEvent):void
		{
			getEquipements();
		}
		
		protected function ckxTerminauxClickHandler(me:MouseEvent):void
		{
			filterData();
		}
		
		protected function ckxAccessoiresClickHandler(me:MouseEvent):void
		{
			filterData();
		}
		
		protected function btnExportClickHandler(me:MouseEvent):void
		{
			if(cbxTypesCommande.selectedItem != null && cbxOperateurs.selectedItem != null && cbxRevendeurs.selectedItem != null && cbxExport.selectedItem != null)
			{
				var url:String = moduleCommandeMobileIHM.NonSecureUrlBackoffice + '/fr/consotel/consoview/M16/export/ExportCatalogue.cfm';
				
				var variables:URLVariables 		= new URLVariables();
					variables.IDPOOL			= idPool;
					variables.IDPROFIL			= cbxTypesCommande.selectedItem.IDPROFIL_EQUIPEMENT;
					variables.IDTYPEEQUIPEMENT	= 0;
					variables.IDOPERATEUR		= cbxOperateurs.selectedItem.OPERATEURID;
					variables.IDREVENDEUR		= cbxRevendeurs.selectedItem.IDREVENDEUR;
					variables.CODELANGUE		= CvAccessManager.getSession().USER.GLOBALIZATION;
					variables.IDSEGMENT			= _idSegment;
					variables.EXT				= cbxExport.selectedItem.EXT;
					variables.FORMAT			= cbxExport.selectedItem.TYPE;
					variables.GESTIONNAIRE		= CvAccessManager.getUserObject().CLIENTACCESSID;
					variables.IDCATEGORIE		= 0;
					variables.IDGAMME			= 0;
					variables.CHAINE			= '';
					variables.IDTYPEFOURNIS		= 1;
					variables.NIVEAU			= null;
					
				if(cbxEngagements.selectedItem != null)
					variables.DUREE	= cbxEngagements.selectedItem.VALUE;
				else
					variables.DUREE	= 0;
				
				var u:URLRequest = new URLRequest(url);
					u.data 		 = variables;
					u.method 	 = URLRequestMethod.POST;
				
				navigateToURL(u, '_blank');
			}
			else if(cbxExport.selectedItem == null)
				{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_type_d_export_'), 'Consoview', null);
				}
		}
		
		protected function cbxEngagementsChangeHandler(e:Event):void
		{
			var len:int = listeEquipements.length;
			
			if(len > 0)
				listeEquipements.itemUpdated(listeEquipements[0]);
		}

		protected function txtFiltreChangeHandler(e:Event):void
		{
			filterData();
		}
		
		protected function lblActualiserClickHandler(me:MouseEvent):void
		{
			tbxFiltre.text = '';
			
			filterData();
		}
		
		protected function btnCloseClickHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PRIVETE
	//--------------------------------------------------------------------------------------------//
	
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getTypeCommande():void
		{
			_poolService.fournirProfilEquipements();
			_poolService.addEventListener(CommandeEvent.LISTED_PROFILES, typeCommandeHandler);
		}
		
		private function getOperateurs():void
		{
			if(!_isAutorise)
			{
				_revAutorises.fournirListeOperateurs(_idSegment);
				_revAutorises.addEventListener(CommandeEvent.LISTED_OPERATEURS, operateursHandler);
			}
			else
			{
				if(cbxTypesCommande.selectedItem == null) return;
					
				_revAutorises.fournirOperateursAutorises(cbxTypesCommande.selectedItem.IDPROFIL_EQUIPEMENT);
				_revAutorises.addEventListener(CommandeEvent.LISTED_OPEAUTORISES, operateursHandler);
			}
		}
		
		private function getRevendeurs():void
		{
			if(!_isAutorise)
			{
				if(cbxTypesCommande.selectedItem == null) return;
				
				_revService.fournirRevendeurs(idPool, cbxTypesCommande.selectedItem.IDPROFIL_EQUIPEMENT, idrevendeurpool);
				_revService.addEventListener(CommandeEvent.LISTED_REVENDEURS, revendeursHandler);
			}
			else
			{
				if(cbxTypesCommande.selectedItem == null || cbxOperateurs.selectedItem == null) return;
				
				_revAutorises.fournirRevendeursAutorises(cbxTypesCommande.selectedItem.IDPROFIL_EQUIPEMENT, cbxOperateurs.selectedItem.OPERATEURID, idrevendeurpool);
				_revAutorises.addEventListener(CommandeEvent.LISTED_REVAUTORISES, revendeursHandler);
			}
		}
		
		private function getEngagements():void
		{
			if(cbxEngagements.dataProvider != null)
				(cbxEngagements.dataProvider as ArrayCollection).removeAll();
			
			_eqptsSrv.fournirNouvelleEngagement(cbxOperateurs.selectedItem.OPERATEURID, true);
			_eqptsSrv.addEventListener(CommandeEvent.LISTED_ENGAGEMENTS, engagementsHandler);
		}
		
		private function getEquipements():void
		{
			listeEquipements.removeAll();
			
			_eqptsSrv.fournirListeEquipementsClient(idPool, cbxTypesCommande.selectedItem.IDPROFIL_EQUIPEMENT, cbxRevendeurs.selectedItem.IDREVENDEUR, _idSegment, '');
			_eqptsSrv.addEventListener(CommandeEvent.LISTED_EQUIPEMENTS, equipementsHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function typeCommandeHandler(cmde:CommandeEvent):void
		{
			var len:int = _poolService.listeProfiles.length;
			
			attributDataToCombobox(cbxTypesCommande, _poolService.listeProfiles, ResourceManager.getInstance().getString('M16', 'Aucun_profile'));
			
			if(len == 1)
			{
				if(_isAutorise)
					getOperateurs();
				else
					getRevendeurs();
			}
		}
		
		private function operateursHandler(cmde:CommandeEvent):void
		{
			var len:int = 0;
			
			if(_isAutorise)
			{
				len = _revAutorises.listeOperateurs.length;
				
				attributDataToCombobox(cbxOperateurs,  _revAutorises.listeOperateurs, ResourceManager.getInstance().getString('M16', 'Aucun_op_rateurs'));
			}
			else
			{
				len = _revAutorises.listeOperateursNormal.length;
				
				attributDataToCombobox(cbxOperateurs,  _revAutorises.listeOperateursNormal, ResourceManager.getInstance().getString('M16', 'Aucun_op_rateurs'));
			}

			if(len == 1 && _isAutorise)
				getRevendeurs();
			
			if(len == 1)
				getEngagements();
		}
		
		private function revendeursHandler(cmde:CommandeEvent):void
		{
			var len:int = 0;
			
			if(_isAutorise)
			{
				len = _revAutorises.listeRevendeurs.length;
				
				attributDataToCombobox(cbxRevendeurs, _revAutorises.listeRevendeurs, ResourceManager.getInstance().getString('M16', 'Aucun_revendeurs'));
			}
			else
			{
				len = _revService.listeRevendeurs.length;
				
				attributDataToCombobox(cbxRevendeurs, _revService.listeRevendeurs, ResourceManager.getInstance().getString('M16', 'Aucun_revendeurs'));
			}
		}

		private function engagementsHandler(cmde:CommandeEvent):void
		{			
			attributDataToCombobox(cbxEngagements, _eqptsSrv.listEngagement, ResourceManager.getInstance().getString('M16', 'Aucun_engagement'));
		}
		
		private function equipementsHandler(cmde:CommandeEvent):void
		{
			listeEquipements = _eqptsSrv.listeEquipements;
			
			filterData();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			dgListeEquipements.rowColorFunction=selectColor;
			_isAutorise = SessionUserObject.singletonSession.BOOLDISTRIBAUTORISES;
						
			getTypeCommande();
			
			if(!_isAutorise)
			{
				getOperateurs();
				
				if(cbxTypesCommande.selectedItem != null)
					getRevendeurs();
			}
		}
		
		private function selectColor(datagrid:DataGrid, rowIndex:int, color:uint):uint			
		{				
			var rColor:uint;				
			rColor = color;				
			var item:Object =  datagrid.dataProvider.getItemAt(rowIndex);
			
			if (item != null )				
			{				
				if(item.SUSPENDU_CLT == 1)//En repture
				{	
					rColor = 0xF7FF04;
				}			
				else					
					rColor = color;					
			}				
			return rColor;				
		}
		
		private function detailsClickHandler(e:Event):void
		{
			if(dgListeEquipements.selectedItem != null)
			{
				var popUpDetails:DetailProduitIHM 	= new DetailProduitIHM();
					popUpDetails.equipement			= dgListeEquipements.selectedItem;
					popUpDetails.viewCatalog = true;
					
				
				PopUpManager.addPopUp(popUpDetails, this, true);
				PopUpManager.centerPopUp(popUpDetails);
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
	
		//---ATTRIBUT LES DONNEES A LA COMBOBOX PASSEE EN PARAMETRE
		private function attributDataToCombobox(cbx:ComboBox, dataValues:ArrayCollection, msgError:String):void
		{
			var lenDataValues:int = dataValues.length;
			
			cbx.errorString = "";
			
			if(lenDataValues != 0)
			{
				cbx.prompt 					= ResourceManager.getInstance().getString('M16', 'S_lectionnez');
				cbx.dataProvider 			= dataValues;
				cbx.dropdown.dataProvider 	= dataValues;
				
				if(lenDataValues > 1)
					cbx.selectedIndex = -1;
				else
					cbx.selectedIndex = 0;
				
				if(lenDataValues > 5)
					cbx.rowCount = 5;
				else
					cbx.rowCount = lenDataValues;
				
				(cbx.dataProvider as ArrayCollection).refresh();
			}
			else
			{
				cbx.prompt = msgError;
				cbx.selectedIndex = -1;
			}
			
			checkCombobox();
		}
		
		private function checkCombobox():void
		{
			if(cbxTypesCommande.selectedItem != null && cbxOperateurs.selectedItem != null && cbxRevendeurs.selectedItem != null)
				btnSearch.enabled = btnExport.enabled = true;
			else
				btnSearch.enabled = btnExport.enabled = false;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FILTRE
		//--------------------------------------------------------------------------------------------//
		
		private function filterData():void
		{
			if(dgListeEquipements.dataProvider != null)
			{
				(dgListeEquipements.dataProvider as ArrayCollection).filterFunction = filterFunction;
				(dgListeEquipements.dataProvider as ArrayCollection).refresh();
			}
		}
		
		private function filterFunction(item:Object):Boolean
		{
			var eq		:EquipementsElements = item as EquipementsElements;
			var rfilter	:Boolean = true;		    
			
			rfilter = rfilter && 	( 
				
										(eq.MARQUE != null && eq.MARQUE.toLocaleLowerCase().search(tbxFiltre.text.toLocaleLowerCase()) != -1)
										||
										(eq.REF_REVENDEUR != null && eq.REF_REVENDEUR.toLocaleLowerCase().search(tbxFiltre.text.toLocaleLowerCase()) != -1)
										||
										(eq.LIBELLE != null && eq.LIBELLE.toLocaleLowerCase().search(tbxFiltre.text.toLocaleLowerCase()) != -1)
										||
										(eq.PRIXC.toString().toLocaleLowerCase().search(tbxFiltre.text.toLocaleLowerCase()) != -1)
										||
										(eq.PRIX.toString().toLocaleLowerCase().search(tbxFiltre.text.toLocaleLowerCase()) != -1)
										||
										(eq.PRIXR.toString().toLocaleLowerCase().search(tbxFiltre.text.toLocaleLowerCase()) != -1)
									)
				
							&& searchMyTerminauxAndAccessoires(eq);
			
			return rfilter;
		}

		private function searchMyTerminauxAndAccessoires(item:EquipementsElements):Boolean//---> FILTRE SUR LES TERMINAUX ET LES ACCESSOIRES
		{
			var OK:Boolean = false;
			
			if(ckxTerminaux.selected == true && ckxAccessoires.selected == true)
			{
				OK = true;
			}
			else if(ckxTerminaux.selected == true && ckxAccessoires.selected == false)
			{
				if(item.IDEQUIPEMENT != 2191)
					OK = true;
			}
			else if(ckxTerminaux.selected == false && ckxAccessoires.selected == true)
			{
				if(item.IDEQUIPEMENT == 2191)
					OK = true;
			}
			
			return OK;
		}
		
	}
}