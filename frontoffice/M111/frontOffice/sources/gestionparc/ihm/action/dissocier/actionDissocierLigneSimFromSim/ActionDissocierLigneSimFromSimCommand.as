package gestionparc.ihm.action.dissocier.actionDissocierLigneSimFromSim
{
	import gestionparc.ihm.action.ICommand;

	public class ActionDissocierLigneSimFromSimCommand implements ICommand
	{
		private var receiver:ActionDissocierLigneSimFromSimReceiver=null;

		public function ActionDissocierLigneSimFromSimCommand(rec:ActionDissocierLigneSimFromSimReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
