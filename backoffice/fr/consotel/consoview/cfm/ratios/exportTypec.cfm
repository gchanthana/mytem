<cfset label="Lignes sans Conso">
<cfheader name="Content-Disposition" value="inline;filename=Ratios_#rapport#_#replace(qGetGrid.libelle_groupe_client,' ','_')#.xls">
		<cfcontent type="application/vnd.ms-excel">
		<?xml version="1.0" encoding="iso-8859-1"?>
		<?mso-application progid="Excel.Sheet"?>
		<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
		 xmlns:o="urn:schemas-microsoft-com:office:office"
		 xmlns:x="urn:schemas-microsoft-com:office:excel"
		 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
		 xmlns:html="http://www.w3.org/TR/REC-html40">
		 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
		  <LastAuthor>CONSOTEL</LastAuthor>
		  <Created>2006-02-27T14:44:16Z</Created>
		  <Version>11.6360</Version>
		 </DocumentProperties>
		 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
		  <ProtectStructure>False</ProtectStructure>
		  <ProtectWindows>False</ProtectWindows>
		 </ExcelWorkbook>
		 <Styles>
		  <Style ss:ID="Default" ss:Name="Normal">
		   <Alignment ss:Vertical="Bottom"/>
		   <Borders/>
		   <Font/>
		   <Interior/>
		   <NumberFormat/>
		   <Protection/>
		  </Style>
			
		  <Style ss:ID="s22">
		   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
		   <Borders/>
		   <Font ss:FontName="Helvetica" ss:Size="11" ss:Color="#000000" ss:Bold="1"/>
		  </Style>
		  <Style ss:ID="s23">
		   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
		   <Borders/>
		   <Font ss:FontName="Helvetica" ss:Size="11" ss:Color="#000000" ss:Bold="1"/>
		  </Style>
		  <Style ss:ID="s24">
		   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
		   <Borders/>
		   <Font ss:FontName="Helvetica" ss:Size="8" ss:Color="#000000"/>
		  </Style>
		  <Style ss:ID="s25">
		   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
		   <Borders/>
		   <Font ss:FontName="Helvetica" ss:Size="8" ss:Color="#000000"/>
		  </Style>
		<Style ss:ID="s20" ss:Name="Pourcentage">
		   <NumberFormat ss:Format="0.0%" />
			<Font ss:FontName="Helvetica" ss:Size="8" ss:Color="#000000"/>
		  </Style>
			<Style ss:ID="s211" ss:Name="Euro">
				<Font ss:FontName="Helvetica" ss:Size="8" ss:Color="#000000"/>
		   <ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
		  </Style>
		  <Style ss:ID="s16" ss:Parent="s211">
		   <NumberFormat ss:Format="_-* #,##0\ _?_-;\-* #,##0\ _?_-;_-* &quot;-&quot;??\ _?_-;_-@_-"/>
		  </Style>
		 </Styles>
		<cfset i=1>
		<cfset j=1>
		 <Worksheet ss:Name="<cfoutput>#left(label,31)#</cfoutput>">
		  <Table x:FullColumns="1"
		   x:FullRows="1" ss:DefaultColumnWidth="60">
			<Column ss:Width="120"/>
		   	<Column ss:Width="160"/>
		  	<Column ss:Width="160"/>
		   	<Column ss:Width="100"/>
			<Column ss:Width="140"/>
			<Column ss:Width="60"/>
		   <Row ss:AutoFitHeight="0" ss:Height="15">
		    <Cell ss:StyleID="s22"><Data ss:Type="String">Groupe</Data></Cell>
		    <Cell ss:StyleID="s23"><Data ss:Type="String">Sociï¿½tï¿½</Data></Cell>
			<Cell ss:StyleID="s23"><Data ss:Type="String">Compte Hiï¿½rarchique</Data></Cell>
		    <Cell ss:StyleID="s23"><Data ss:Type="String">Nbre Accï¿½s</Data></Cell>
			<Cell ss:StyleID="s23"><Data ss:Type="String">Nbre Accï¿½s sans Conso</Data></Cell>
			<Cell ss:StyleID="s23"><Data ss:Type="String">Ratio</Data></Cell>
		   </Row>
		<cfoutput query="qGetGrid">
		<cfset a=j>
		
		<cfif i mod 50000 eq 0>
		  </Table>
		  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
		   <PageSetup>
		    <PageMargins x:Bottom="0.984251969" x:Left="0.78740157499999996"
		     x:Right="0.78740157499999996" x:Top="0.984251969"/>
		   </PageSetup>
		   <Print>
		    <ValidPrinterInfo/>
		    <HorizontalResolution>300</HorizontalResolution>
		    <VerticalResolution>300</VerticalResolution>
		    <NumberofCopies>0</NumberofCopies>
		   </Print>
		   <Selected/>
		   <ProtectObjects>False</ProtectObjects>
		   <ProtectScenarios>False</ProtectScenarios>
		  </WorksheetOptions>
		 </Worksheet>
		<Worksheet ss:Name="#left(label,evaluate(30-len(j)))#_#j#">
		  <Table x:FullColumns="1"
		   x:FullRows="1" ss:DefaultColumnWidth="60">
			<Column ss:Width="120"/>
		   	<Column ss:Width="160"/>
		  	<Column ss:Width="160"/>
		   	<Column ss:Width="100"/>
			<Column ss:Width="140"/>
			<Column ss:Width="60"/>
		
		   <Row ss:AutoFitHeight="0" ss:Height="15">
		    <Cell ss:StyleID="s22"><Data ss:Type="String">Groupe</Data></Cell>
		    <Cell ss:StyleID="s23"><Data ss:Type="String">Sociï¿½tï¿½</Data></Cell>
			<Cell ss:StyleID="s23"><Data ss:Type="String">Compte Hiï¿½rarchique</Data></Cell>
		    <Cell ss:StyleID="s23"><Data ss:Type="String">Nbre Accï¿½s</Data></Cell>
			<Cell ss:StyleID="s23"><Data ss:Type="String">Nbre Accï¿½s sans Conso</Data></Cell>
			<Cell ss:StyleID="s23"><Data ss:Type="String">Ratio</Data></Cell>
		   </Row>
		<Row ss:AutoFitHeight="0" ss:Height="12.9375">
		     <Cell ss:StyleID="s24"><Data ss:Type="String">#libelle_groupe_client#</Data></Cell>
		    <Cell ss:StyleID="s24"><Data ss:Type="String">#libelle_ref_client#</Data></Cell>
			<Cell ss:StyleID="s24"><Data ss:Type="String">#ref_client#</Data></Cell>
			<Cell ss:StyleID="s25"><Data ss:Type="Number">#nbndi#</Data></Cell>
			<Cell ss:StyleID="s25"><Data ss:Type="Number">#nb_sans_conso#</Data></Cell>
			<Cell ss:StyleID="s20"><Data ss:Type="Number">#ratio#</Data></Cell>
		   </Row>
			<cfset j=j+1>
		<cfelse>
		<Row ss:AutoFitHeight="0" ss:Height="12.9375">
		    <Cell ss:StyleID="s24"><Data ss:Type="String">#libelle_groupe_client#</Data></Cell>
		    <Cell ss:StyleID="s24"><Data ss:Type="String">#libelle_ref_client#</Data></Cell>
			<Cell ss:StyleID="s24"><Data ss:Type="String">#ref_client#</Data></Cell>
			<Cell ss:StyleID="s25"><Data ss:Type="Number">#nbndi#</Data></Cell>
			<Cell ss:StyleID="s25"><Data ss:Type="Number">#nb_sans_conso#</Data></Cell>
			<Cell ss:StyleID="s20"><Data ss:Type="Number">#ratio#</Data></Cell>
		   </Row>
		</cfif>
		<cfset i=i+1>
		</cfoutput>
		</Table>
		  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
		   <PageSetup>
		    <PageMargins x:Bottom="0.984251969" x:Left="0.78740157499999996"
		     x:Right="0.78740157499999996" x:Top="0.984251969"/>
		   </PageSetup>
		   <Print>
		    <ValidPrinterInfo/>
		    <HorizontalResolution>300</HorizontalResolution>
		    <VerticalResolution>300</VerticalResolution>
		    <NumberofCopies>0</NumberofCopies>
		   </Print>
		   <Selected/>
		   <ProtectObjects>False</ProtectObjects>
		   <ProtectScenarios>False</ProtectScenarios>
		  </WorksheetOptions>
		 </Worksheet>
		</Workbook>