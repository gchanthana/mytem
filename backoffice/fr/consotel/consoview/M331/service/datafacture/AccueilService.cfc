<cfcomponent name="AccueilService">
	<cfset dataSourceName = "BI_TEST">
  	
	<cffunction name="zgetOrga" output="false" access="public" returntype="Any">
	    <cfif SESSION.PERIMETRE.ID_PERIMETRE EQ SESSION.PERIMETRE.ID_GROUPE>
	    <cfset rsltOrga = #SESSION.PERIMETRE.ID_GROUPE#>
	    <cfelse>
	         <cfquery datasource="#SESSION.OFFREDSN#" name="rsltOrga">
	              SELECT PKG_CV_RAGO.GET_ORGA(#SESSION.PERIMETRE.ID_PERIMETRE#)FROM DUAL
	         </cfquery>
	    </cfif>
		<cfreturn rsltOrga>
	</cffunction>
	
	<cffunction name="getBiAnalyticsBinaryImageFromUrl" display="true" access="private" returntype="any" description="image from Bi">
		<cfargument name="murl" type="string" required="true">	
		<cfhttp  url="#murl#" method="GET" useragent="#CGI.HTTP_USER_AGENT#"/>
		<cfset chaine=cfhttp.ResponseHeader["Set-Cookie"]>
		
		<cfset jCookie=GetToken(GetToken(chaine[1],1,";"),2,"=")>
		<cfset t=listToArray(CFHTTP.FileContent,'"')>
		<cfset path=parseArray("saw.dll?DocPart&_scid",t) />
		<cfdump var="#path#"/>
		
		<cfhttp getAsBinary="auto" url="http://sun-bi.consotel.fr/analytics/#path#" method="GET" useragent="#CGI.HTTP_USER_AGENT#">
			<cfhttpparam type="cookie" name="nQuireID" value="#jCookie#">
			<cfhttpparam type="cookie" name="sawU" value="consoview">
			<cfhttpparam type="cookie" name="sawP" value="">
		</cfhttp>
		<cfreturn 	CFHTTP.fileContent/>
	</cffunction>

		
	
	<cffunction name="getBiAnalyticsHTMLFromUrl" display="true" access="private" returntype="any" description="image from Bi">
		<cfargument name="murl" type="string" required="true">	
		<cfhttp  url="#murl#" method="GET" useragent="#CGI.HTTP_USER_AGENT#"/>
		<cfset chaine=cfhttp.ResponseHeader["Set-Cookie"]>
		<cfset jCookie=GetToken(GetToken(chaine[1],1,";"),2,"=")>
		
		<cfimage action="writeToBrowser" source="#imgTwo#" />		
		<cfreturn 	CFHTTP.fileContent/>
	</cffunction>
	
	<cffunction name="parseArray" access="private" returntype="String">
		<cfargument name="field" type="string" required="true">		
		<cfargument name="tab" type="array" required="true">
		
		<cfset l=arrayLen(tab)/>	
		
		<cfloop index="i" from="1" to="#l#">
			<cfif FindNoCase(field,tab[i]) gt 0>
				<cfreturn 	tab[i]/>
			</cfif>
		</cfloop>
		
		<cfreturn 	"notfound"/>
	</cffunction>
	
	
	
	<cffunction name="getHTMLData1" access="remote" returntype="any" description="l'url de la page HTML">
		<cfset IDRACINEMASTER = #SESSION.PERIMETRE.IDRACINE_MASTER#/>
		<cfset IDRACINE = #SESSION.PERIMETRE.ID_GROUPE#/>
		<cfset IDORGA = zgetOrga()/>
		<cfset myQuery = QueryNew("ID,URL,DESC,IMAGE","Integer,VarChar,VarChar,Binary")>
		 
		
		<cfset url1 = 'http://sun-bi.consotel.fr/analytics/saw.dll?Go&Path=/shared/CONSOTEL-OBIEE/CONSOVIEW/PageAccueil/evolution%20des%20couts%20par%20segment%20par%20periode&IDRACINEMASTER=#IDRACINEMASTER#&IDRACINE=#IDRACINE#&IDORGA=#IDORGA#&nqUser=consoview&nqPassword=public&ViewName=staticchart!1'/>
		 
		<cfscript>
			QueryAddRow(myQuery);
			QuerySetCell(myQuery,"ID","1");            
			QuerySetCell(myQuery,"URL",url1);
			QuerySetCell(myQuery,"DESC","");
			QuerySetCell(myQuery,"IMAGE",getBiAnalyticsBinaryImageFromUrl(#url1#));		
		</cfscript>
		
		<cfreturn myQuery>
	</cffunction>
	
	<cffunction name="getHTMLData2" access="remote" returntype="any" description="l'url de la page HTML">		
		<cfset IDRACINEMASTER = #SESSION.PERIMETRE.IDRACINE_MASTER#/>
		<cfset IDRACINE = #SESSION.PERIMETRE.ID_GROUPE#/>
		<cfset IDORGA = zgetOrga()/>
		<cfset myQuery = QueryNew("ID,URL,DESC,IMAGE","Integer,VarChar,VarChar,Binary")>
		
		<cfset url2 = 'http://sun-bi.consotel.fr/analytics/saw.dll?Go&Path=/shared/CONSOTEL-OBIEE/CONSOVIEW/PageAccueil/Evolution%20couts%20telephonie%20operateur%20par%20trimestre&IDRACINEMASTER=#IDRACINEMASTER#&IDRACINE=#IDRACINE#&IDORGA=#IDORGA#&nqUser=consoview&nqPassword=public&ViewName=staticchart!1'/>
		 
		<cfscript>
			QueryAddRow(myQuery);
			QuerySetCell(myQuery,"ID","2");            
			QuerySetCell(myQuery,"URL",url2);
			QuerySetCell(myQuery,"DESC","");
			QuerySetCell(myQuery,"IMAGE",getBiAnalyticsBinaryImageFromUrl(#url2#));
		</cfscript>
		
		<cfreturn myQuery>
	</cffunction>
	
	<cffunction name="getHTMLData3" access="remote" returntype="any" description="l'url de la page HTML">	
		<cfset IDRACINEMASTER = #SESSION.PERIMETRE.IDRACINE_MASTER#/>
		<cfset IDRACINE = #SESSION.PERIMETRE.ID_GROUPE#/>
		<cfset IDORGA = zgetOrga()/>
		<cfset myQuery = QueryNew("ID,URL,DESC,IMAGE","Integer,VarChar,VarChar,Binary")>
																	
		<cfset url3 = 'http://sun-bi.consotel.fr/analytics/saw.dll?Go&Path=%2Fshared%2FCONSOTEL-OBIEE%2FCONSOVIEW%2FPageAccueil%2FCoutLigne&IDRACINEMASTER=#IDRACINEMASTER#&IDRACINE=#IDRACINE#&IDORGA=#IDORGA#&nqUser=consoview&nqPassword=public&ViewName=staticchart!1'/>
		 
		<cfscript>
			QueryAddRow(myQuery);
			QuerySetCell(myQuery,"ID","3");            
			QuerySetCell(myQuery,"URL",url3);
			QuerySetCell(myQuery,"DESC","");
			QuerySetCell(myQuery,"IMAGE",getBiAnalyticsBinaryImageFromUrl(#url3#));
		</cfscript>
		
		<cfreturn myQuery>
	</cffunction>
	 
	
	<cffunction name="getData1" access="remote" returntype="any" description="les donneés provenant de E0">
		<cfargument name="source" type="string" required="true" default="ONE">
			
			<cfset dataTable = "E0_AGGREGS">
			<cfswitch expression="#SESSION.PERIMETRE.NIVEAU#">
				<cfcase value="A,B,C,D,E,F" delimiters=",">
					<cfset dataTable = "E0_AGGREGS">
				</cfcase>
				<cfdefaultcase>
					<cfset dataTable = "E0_ENPROD_HIST">	
				</cfdefaultcase>
			</cfswitch>
			
			<cfquery datasource="#SESSION.OFFREDSN#" name="qPeriode">
				 SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS  
			 	 FROM	PERIODE
			 	 WHERE	PERIODE.SHORT_MOIS=to_Char(#SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN#,'mm/yyyy')
			</cfquery>
			
			<cfquery datasource="#dataSourceName#" name="qQuery">				
				set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
								p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idorga=#SESSION.PERIMETRE.ID_ORGANISATION#,
								p_idperimetre=#SESSION.PERIMETRE.ID_PERIMETRE#,								 
								p_idcliche=#SESSION.PERIMETRE.ID_LAST_CLICHE#,
								p_niveau='#SESSION.PERIMETRE.NIVEAU#',
								p_langue_pays='#SESSION.USER.GLOBALIZATION#';
				
				SELECT PERIODE.LIBELLE_PERIODE saw_10, FACTURATION.MONTANT saw_11, PERIODE.IDPERIODE_MOIS saw_12, PRODUIT1GROUPE.SEGMENT_THEME saw_13 
				FROM #dataTable#
				WHERE PERIODE.IDPERIODE_MOIS BETWEEN #Evaluate(qPeriode.IDPERIODE_MOIS-12)# AND #qPeriode.IDPERIODE_MOIS# 
				ORDER BY saw_12
			</cfquery>
		<cfreturn qQuery>
	</cffunction>
	
	<cffunction name="getData2" access="remote" returntype="any" description="les donneés provenant de E0">
		<cfargument name="source" type="string" required="true" default="TWO">					
			
			<cfquery datasource="#SESSION.OFFREDSN#" name="qPeriode">
				 SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS  
			 	 FROM	PERIODE
			 	 WHERE	PERIODE.SHORT_MOIS=to_Char(#SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN#,'mm/yyyy')
			</cfquery>
			
			<cfquery datasource="#dataSourceName#" name="qQuery">	
				
				set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
								p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idorga=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idperimetre=#SESSION.PERIMETRE.ID_GROUPE#,
								p_niveau='B',
								p_langue_pays='#SESSION.USER.GLOBALIZATION#';

				SELECT ORGAOPERATEUR.OPERATEUR saw_0, 
							FACTURATION.NOMBRE_FACTURES saw_1, 
							PERIODE.DESC_MOIS saw_2, 
							PERIODE.SHORT_MOIS saw_3, 
							PERIODE.DESC_ANNEE saw_4, 
							PERIODE.IDPERIODE_MOIS saw_5
				FROM E0_AGGREGS
				WHERE RCOUNT(PERIODE.IDPERIODE_MOIS) <= 4
				ORDER BY saw_5 DESC
			</cfquery>

			<cfquery name="q1" dbtype="query">
				SELECT saw_1,saw_0, saw_2,saw_5,saw_3 FROM qQuery GROUP BY saw_1,saw_0, saw_2,saw_5,saw_3 ORDER BY saw_5 asc
			</cfquery>	

	 	<cfreturn q1>
	</cffunction>
	
	
	<cffunction name="getData3" access="remote" returntype="any" description="les donneés provenant de E0">
		<cfargument name="source" type="string" required="true" default="THREE">
		<cfset dataTable = "E0_AGGREGS">
		
			<cfswitch expression="#SESSION.PERIMETRE.NIVEAU#">
				<cfcase value="A,B,C,D,E,F" delimiters=",">
					<cfset dataTable = "E0_AGGREGS">
				</cfcase>
				<cfdefaultcase>
					<cfset dataTable = "E0_ENPROD_HIST">	
				</cfdefaultcase>
			</cfswitch>
		 
			<cfquery datasource="#SESSION.OFFREDSN#" name="qPeriode">
				 SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS  
			 	 FROM	PERIODE
			 	 WHERE	PERIODE.SHORT_MOIS=to_Char(#SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN#,'mm/yyyy')
			</cfquery>
			
			<cfquery datasource="#dataSourceName#" name="qQuery">				
				set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
								p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idorga=#SESSION.PERIMETRE.ID_ORGANISATION#,
								p_idperimetre=#SESSION.PERIMETRE.ID_PERIMETRE#,								 
								p_idcliche=#SESSION.PERIMETRE.ID_LAST_CLICHE#,
								p_niveau='#SESSION.PERIMETRE.NIVEAU#',
								p_langue_pays='#SESSION.USER.GLOBALIZATION#';
				
				SELECT FACTURATION.MONTANT saw_0,FACTURATION.MONTANT saw_7,PRODUIT1GROUPE.SEGMENT_THEME saw_6 
				FROM #dataTable#
				WHERE PERIODE.IDPERIODE_MOIS BETWEEN #Evaluate(qPeriode.IDPERIODE_MOIS-12)# AND #qPeriode.IDPERIODE_MOIS# 
				
			</cfquery>
		<cfreturn qQuery>
	</cffunction>
	
	
	
	
</cfcomponent>
