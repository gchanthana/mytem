package appli.connection
{
	import mx.core.Container;
	
	import composants.ConsoProgressBar.ConsoProgressBar;
	
	import entity.AppParams;
	
	import general.abstract.AbstractConnection;
	
	import interfaceClass.IConnection;
	
	public class ConnectionSSOSeSouvenir extends AbstractConnection implements IConnection
	{
		public function ConnectionSSOSeSouvenir(progress:ConsoProgressBar)
		{
			super(progress);
		}
		override public function MustBeDisplay():Boolean
		{
			return false;
		}
		override public function goLogin(param:AppParams, container:Container = null):void
		{
			trace("ConnectionSSOSeSouvenir.goLogin(param, container)");
			
			super.goLogin(param,container);
			loginService.connectViaSSOCV(params.token,params.codeApp,params.codeConnection);
		}
	}
}