package gestioncommande.popup
{	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.Transporteur;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.TransporteurSevice;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	[Bindable]
	public class PopUpLivraisonImpl extends Box
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var txtNumeroTracking	:TextInput
		
		public var cbxTransporteurs		:ComboBox;
				
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		public var myCommande			:Commande;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _transSrv			:TransporteurSevice = new TransporteurSevice();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
				
		public var listeTransporteurs	:ArrayCollection 		= new ArrayCollection();

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function PopUpLivraisonImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			if(validateTransporteur())
				setTransporteur();
		}
		
		protected function btnIgnorerClickHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event("TRANSPORTEUR_IGNORED"));
			
			PopUpManager.removePopUp(this);
		}
		
		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			getTransporteurs();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function transporteursHandler(cmde:CommandeEvent):void
		{
			var lenTrans:int = _transSrv.listeTransporteurs.length;
			
			listeTransporteurs = _transSrv.listeTransporteurs;
			
			if(lenTrans > 1)
			{
				cbxTransporteurs.prompt = ResourceManager.getInstance().getString('M16', 'S_lectionnez');
				cbxTransporteurs.selectedIndex = -1;
			}
		}
		
		private function transporteurHandler(cmde:CommandeEvent):void
		{
			dispatchEvent(new Event('TRANSPORTEUR_VALIDATE'));
			
			PopUpManager.removePopUp(this);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		public function getTransporteurs():void
		{
			_transSrv.fournirListeTransporteurs();
			_transSrv.addEventListener(CommandeEvent.LISTED_TRANSPORTS, transporteursHandler);
		}
		
		private function setTransporteur():void
		{
			_transSrv.majInfosLivraisonCommande(myCommande);
			_transSrv.addEventListener(CommandeEvent.TRANSPORTS_UPDATED, transporteurHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//

		private function validateTransporteur():Boolean
		{
			var isOK:Boolean = false;
			
			if(cbxTransporteurs.selectedItem != null)
			{
				if(txtNumeroTracking.text != "")
				{
					myCommande.IDTRANSPORTEUR 		= (cbxTransporteurs.selectedItem as Transporteur).IDTRANSPORTEUR;
					myCommande.LIBELLE_TRANSPORTEUR = (cbxTransporteurs.selectedItem as Transporteur).LIBELLE_TRANSPORTEUR;
					myCommande.NUMERO_TRACKING		= txtNumeroTracking.text;
					isOK = true;
				}
				else
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_un_num_ro_de_tracking__'), 'Consoview', null);
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_un_transporteur__'), 'Consoview', null);
			
			return isOK;
		}

	}
}