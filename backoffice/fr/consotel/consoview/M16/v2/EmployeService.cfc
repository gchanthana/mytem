<cfcomponent displayname="fr.consotel.consoview.M16.v2.EmployeService" output="false">

	<!---
	   J'AURAIS BESOIN D'UNE PROCÉDURE QUI PRENNE EN PARAM IN UN XML OU UNE STRING COMPOSÉE UNIQUEMENT DE PLUSIEURS MATRICULES ET QUI RETOURNE SOIT :
	   - UN TABLEAU AVEC TOUTES LES INFOS CONCERNANT CHAQUE MATRICULE (NOM PRÉNOM IDCOLLAB MAIL ETC..) : GOOD !
	   - UN TABLEAU AVEC TOUS LES MATRICULES QUI N'EXISTENT PAS DANS LA BASE : BAD !
	   VOIR TICKET 1842	   
	 --->
	<cffunction name="getEmployFromMat" displayname="getEmployFromMat"  access="remote" output="false" returntype="Query" hint="RECHERCHE DE LA VALIDITE DES MATRICULES">
		<cfargument name="strMatricule" 	required="true" type="string" />
		
			<cfset idracine = session.perimetre.id_groupe>
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getemployfrommat">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	value="#idracine#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 		value="#strMatricule#"/>
				<cfprocresult name="p_retour">			
			</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>

	<!---
	FOURNIT LA LISTE DES EMPLOYÉS DES POOLS DE GESTION D'UN GESTIONNAIRE SUIVANT UNE CLEF DE RECHERCHER, 
	SI ID DU GESTIONNAIRE = 0, ON PREND LES EMPLOYES DE LA RACINE.
	CLEF = | NOM | PRENOM | CODE_INTERNE | FONCTION_EMPLOYE | REFERENCE_EMPLOYE
	 --->
	<cffunction name="fournirListeEmployes" access="remote" returntype="Query" output="false" hint="FOURNIT LA LISTE DES EMPLOYÉS DES POOLS DE GESTION D'UN GESTIONNAIRE SUIVANT UNE CLEF DE RECHERCHER">
		<cfargument name="idPool" 	type="Numeric" required="true" default="-2">
		<cfargument name="cle" 		type="String"  required="true" default="">
		
			<cfset idracine = session.perimetre.id_groupe>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.find_employe">			
					<cfprocparam  cfsqltype="CF_SQL_INTEGER" value="#idPool#"/>
					<cfprocparam  cfsqltype="CF_SQL_INTEGER" value="#idracine#"/>
					<cfprocparam  cfsqltype="CF_SQL_VARCHAR" value="#cle#"/>
				<cfprocresult name="p_retour">
			</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>
	
	<cffunction name="fournirListeEmployesV2" access="remote" returntype="Query" output="false" hint="FOURNIT LA LISTE DES EMPLOYÉS DES POOLS DE GESTION D'UN GESTIONNAIRE SUIVANT UNE CLEF DE RECHERCHER">
		<cfargument name="idPool" 			type="Numeric" required="true">
		<cfargument name="columnsearch" 	type="String"  required="true">
		<cfargument name="ordercolumn" 		type="String"  required="true">
		<cfargument name="typeorder" 		type="String"  required="true">
		<cfargument name="indexdebut" 		type="Numeric" required="true">
		<cfargument name="numberofrecords" 	type="Numeric" required="true">
		<cfargument name="cle" 				type="String"  required="true">
		
			<cfset idracine = session.perimetre.id_groupe>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.find_employe_v3">			
					<cfprocparam  cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 			 value="#idPool#"/>
					<cfprocparam  cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			 value="#idracine#"/>
					<cfprocparam  cfsqltype="CF_SQL_VARCHAR" variable="p_clef" 				 value="#cle#"/>
					<cfprocparam  cfsqltype="CF_SQL_VARCHAR" variable="p_column_search" 	 value="#columnsearch#"/>
					<cfprocparam  cfsqltype="CF_SQL_VARCHAR" variable="p_order_column" 	 	 value="#ordercolumn#"/>
					<cfprocparam  cfsqltype="CF_SQL_INTEGER" variable="p_index_debut" 		 value="#indexdebut#"/>
					<cfprocparam  cfsqltype="CF_SQL_INTEGER" variable="p_number_of_records"  value="#numberofrecords#"/>
					<cfprocparam  cfsqltype="CF_SQL_VARCHAR" variable="p_type_order" 	 	 value="#typeorder#"/>
				<cfprocresult name="p_retour">
			</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>
	
	<!--- FOURNI LES DÉTAILS D'UN EMPLOYÉ --->
	<cffunction name="fournirDetailEmploye" access="remote" returntype="Array" output="false" hint="FOURNI LES DÉTAILS D'UN EMPLOYÉ">
		<cfargument name="idEmploye" 	required="true" type="numeric"/>
		<cfargument name="idGpeClient" 	required="true" type="numeric"/>
		<cfargument name="idPool" 		required="true" type="numeric"/>
	
			
			<cfset idracine = session.perimetre.id_groupe>
	
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.getfiche_collaborateur">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idEmploye#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idGpeClient#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idPool#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
							
				<cfprocresult name="employe" 		resultset="1">
				<cfprocresult name="equipement" 	resultset="2">
				<cfprocresult name="orga_cus" 		resultset="3">
			</cfstoredproc>
			
			<cfset p_retour = ArrayNew(1)>
			<cfset p_retour[1] = p_result >
			<cfset p_retour[2] = employe >
			<cfset p_retour[3] = equipement >
			<cfset p_retour[4] = orga_cus >
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- CRÉATION D'UN EMPLOYÉ --->
	<cffunction name="creationEmploye" access="remote" returntype="Numeric" output="false">
		<cfargument name="idPool" 			required="true" type="Numeric" />
		<cfargument name="idSite" 			required="true" type="numeric" />		
		<cfargument name="cleIdentifiant" 	required="true" type="String" />		
		<cfargument name="idCivilite" 		required="true" type="numeric" />		
		<cfargument name="nom" 				required="true" type="String" />		
		<cfargument name="prenom" 			required="true" type="String" />		
		<cfargument name="email" 			required="true" type="String" />		
		<cfargument name="fonction" 		required="true" type="String" />		
		<cfargument name="statut" 			required="true" type="String" />		
		<cfargument name="commentaires" 	required="true" type="String" />		
		<cfargument name="codeInterne" 		required="true" type="String" />		
		<cfargument name="reference" 		required="true" type="String" />		
		<cfargument name="matricule" 		required="true" type="String" />		
		<cfargument name="dansSociete" 		required="true" type="numeric" />		
		<cfargument name="dateEntree" 		required="true" type="String" />		
		<cfargument name="dateSortie" 		required="true" type="String" />		
		<cfargument name="texteChampPerso1" required="true" type="String" />		
		<cfargument name="texteChampPerso2" required="true" type="String" />		
		<cfargument name="texteChampPerso3" required="true" type="String" />		
		<cfargument name="texteChampPerso4" required="true" type="String" />		
		<cfargument name="niveau" 			required="true" type="numeric" />		
		<cfargument name="xmlOrganisation" 	required="true" type="String" displayname="xml de l'organisation"/>		
		
			<cfset idracine = session.perimetre.id_groupe>
			<cfset iduser = session.user.clientaccessid>
		
			<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m16.InsertEmploye">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idpool#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idSite#" null="#iif((idSite eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idUser#" > <!--- correspond a app_LoginID --->
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#cleIdentifiant#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCivilite#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#nom#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#prenom#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#email#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#fonction#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#statut#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaires#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeInterne#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#reference#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#matricule#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#dansSociete#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateEntree#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateSortie#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso1#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso2#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso3#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso4#" >			
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#niveau#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	  value="#xmlOrganisation#" >
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
		 
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="creationEmploye1" access="remote" returntype="Numeric" hint="CRÉATION D'UN EMPLOYÉ">
		<cfargument name="idpool" required="true" type="Numeric"/>
		<cfargument name="infos"  required="true" type="String"/>
		
			<cfset idracine = session.perimetre.id_groupe>
						
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.savecollab">
				<cfprocparam  type="in"  cfsqltype="CF_SQL_INTEGER" value="#idracine#">
				<cfprocparam  type="in"  cfsqltype="CF_SQL_INTEGER" value="#idpool#" >
				<cfprocparam  type="in"  cfsqltype="CF_SQL_CLOB" 	value="#tostring(infos)#">
				<cfprocparam  type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- MISE À JOUR D'UN EMPLOYÉ --->
	<cffunction name="updateEmploye" access="remote" returntype="numeric" output="false">
		<cfargument name="idGpeClient"		required="true" type="numeric" />		
		<cfargument name="idEmploye"		required="true" type="numeric" />		
		<cfargument name="idSite" 			required="true" type="numeric" />		
		<cfargument name="cleIdentifiant" 	required="true" type="String" />		
		<cfargument name="idCivilite" 		required="true" type="numeric" />		
		<cfargument name="nom" 				required="true" type="String" />		
		<cfargument name="prenom" 			required="true" type="String" />		
		<cfargument name="email" 			required="true" type="String" />		
		<cfargument name="fonction" 		required="true" type="String" />		
		<cfargument name="statut" 			required="true" type="String" />		
		<cfargument name="commentaires" 	required="true" type="String" />		
		<cfargument name="codeInterne" 		required="true" type="String" />		
		<cfargument name="reference" 		required="true" type="String" />		
		<cfargument name="matricule" 		required="true" type="String" />		
		<cfargument name="dansSociete" 		required="true" type="numeric" />		
		<cfargument name="dateEntree" 		required="true" type="String" />		
		<cfargument name="dateSortie" 		required="true" type="String" />		
		<cfargument name="texteChampPerso1" required="true" type="String" />		
		<cfargument name="texteChampPerso2" required="true" type="String" />		
		<cfargument name="texteChampPerso3" required="true" type="String" />		
		<cfargument name="texteChampPerso4" required="true" type="String" />		
		<cfargument name="niveau" 			required="true" type="numeric" />		
		<cfargument name="xmlOrganisation" 	required="true" type="String" displayname="xml de l'organisation"/>		

			<cfset idracine = session.perimetre.id_groupe>
			<cfset iduser = session.user.clientaccessid>

			<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m16.UpdateEmploye">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGpeClient#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEmploye#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idSite#" null="#iif((idSite eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idUser#" > <!--- correspond a app_LoginID --->
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#cleIdentifiant#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCivilite#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#nom#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#prenom#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#email#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#fonction#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#statut#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaires#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeInterne#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#reference#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#matricule#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#dansSociete#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateEntree#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateSortie#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso1#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso2#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso3#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#texteChampPerso4#" >			
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#niveau#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	  value="#xmlOrganisation#" >
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
		 
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="updateEmploye1" access="remote" returntype="Numeric" output="false" hint="MISE À JOUR D'UN EMPLOYÉ">
		<cfargument name="idcollaborateur" 	required="true" type="Numeric"/>				
		<cfargument name="infos"  			required="true" type="String"/>	
			
			<cfset idracine = session.perimetre.id_groupe>
				
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.updateemploye">
				<cfprocparam  type="in"  cfsqltype="CF_SQL_INTEGER" value="#idcollaborateur#">
				<cfprocparam  type="in"  cfsqltype="CF_SQL_INTEGER" value="#idracine#">
				<cfprocparam  type="in"  cfsqltype="CF_SQL_CLOB" 	value="#tostring(infos)#">
				<cfprocparam  type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
		 
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		Fournis la liste des libellés perso
	 --->
	<cffunction name="fournirChampsPersoEmploye" access="remote" returntype="Query">

		<cfset idracine = session.perimetre.id_groupe>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.Liste_Champs_Perso">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>

	<!--- 
		Fournis si oui ou non le matricul est auto et si oui alors recupération de celui ci
	 --->
	<cffunction name="getMatriculeAuto" access="remote" returntype="Struct" output="false" hint="MATRICULE AUTO">
			
		<cfset idracine = session.perimetre.id_groupe>
		<cfset matriculeStrcut 				 = structnew()>
		<cfset matriculeStrcut.PREFIXE 		 = ''>
		<cfset matriculeStrcut.CURRENT_VALUE = ''>
		<cfset matriculeStrcut.LAST_VALUE 	 = 0>
		<cfset matriculeStrcut.IDRACINE 	 = idracine>
		<cfset matriculeStrcut.IS_ACTIF 	 = 0>
		<cfset matriculeStrcut.NB_DIGIT 	 = 0>
		
		<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m21.getgestautomatinfo">
			<cfprocparam  type="in"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfif p_retour.RecordCount gt 0>				 
			<cfset matriculeStrcut.PREFIXE 		 = p_retour.PREFIXE>				
			<cfset matriculeStrcut.LAST_VALUE 	 = p_retour.LAST_VALUE>
			<cfset matriculeStrcut.IS_ACTIF 	 = p_retour.IS_ACTIF>
			<cfset matriculeStrcut.NB_DIGIT 	 = p_retour.NB_DIGIT>

			<cfif matriculeStrcut.IS_ACTIF gt 0>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m21.getmatricule">
					<cfprocparam  type="in"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#idracine#">
					<cfprocparam  type="Out" cfsqltype="CF_SQL_VARCHAR" variable="last_matricule">
				</cfstoredproc>
				<cfset matriculeStrcut.CURRENT_VALUE = last_matricule>
			</cfif>
		</cfif>		 
		<cfreturn matriculeStrcut>
	</cffunction>
	
	<cffunction name="setMatricule" access="remote" returntype="Numeric" output="false" hint="MATRICULE AUTO">
		<cfargument name="etat" required="true" type="Numeric" default="0"/>		
				
			<cfset idracine = session.perimetre.id_groupe>
				
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m21.IUGestAutoMatricule">
				<cfprocparam  type="in"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#idracine#">
				<cfprocparam  type="in"  cfsqltype="CF_SQL_VARCHAR" variable="p_prefixe"  	value="XXX">
				<cfprocparam  type="in"  cfsqltype="CF_SQL_INTEGER" variable="p_nb_digit" 	value="3">
				<cfprocparam  type="in"  cfsqltype="CF_SQL_INTEGER" variable="p_last_value" value="51">
				<cfprocparam  type="in"  cfsqltype="CF_SQL_INTEGER" variable="p_is_actif" 	value="#etat#">
				<cfprocparam  type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
		 
		<cfreturn p_retour>
	</cffunction>
	

</cfcomponent>