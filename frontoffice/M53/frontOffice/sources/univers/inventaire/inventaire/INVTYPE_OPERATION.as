package univers.inventaire.inventaire
{
	public class INVTYPE_OPERATION
	{
		//Constante définissant le type d'une opération de commande;
		 public static const COMMANDE  : int = 2;
		 
		//Constante définissant le type d'une opération de résiliation;
		 public static  const RESILIATION : int = 1;
		 
		 //Constante définissant le type d'une opération verifcation de facture apres une commande;
		 public static  const VERIF_FACT_COMMANDE  : int = 82;
		 
		//Constante définissant le type d'une opération  de verifcation de facture apres une résiliation;
		 public static  const VERIF_FACT_RESILIATION : int = 41;
		 
		 //Constante définissant le type d'une opération  de reclamation apres une résiliation;
		 public static  const RECLAM_RESILIATION : int = 61;
		 
		  //Constante définissant le type d'une opération  de reclamation apres une résiliation;
		 public static  const RECLAM_COMMANDE : int = 83;
	}
}