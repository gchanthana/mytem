<cfcomponent displayname="GetHistorique" output="false">

<cffunction name="getHistorique" access="remote" returnType="query">
	
	<cfargument name="IdInventaireProduit" type="numeric" required="true">
	
	<cfset idracine_master = session.perimetre.idracine_master>
	<cfset idgroupe_client = session.perimetre.id_groupe>
	<cfset codeLangue=session.user.globalization>  
	
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_CYCLEVIE_V3.GETHISTORIQUE_RECHERCHE_v2">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_IdInventaireProduit" value="#IdInventaireProduit#" null="false">
		<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_codeLangue" value="#codeLangue#" null="false"/>  
	        
	    <cfprocresult name="p_result">
    </cfstoredproc>
	
	<cfreturn p_result>
	
</cffunction>


</cfcomponent>