package univers.facturation.suivi.controles.services
{
	import univers.facturation.suivi.controles.vo.PieceJointeVO;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.formatters.CurrencyFormatter;
	import mx.formatters.DateFormatter;
	
	import univers.facturation.suivi.controles.controls.TypesCommandesMobile;
	import univers.facturation.suivi.controles.controls.XMLprocess;
	import univers.facturation.suivi.controles.event.CommandeEvent;
	import univers.facturation.suivi.controles.vo.CommandeHistoriqueVO;
	import univers.facturation.suivi.controles.vo.CommandeVO;
	import univers.facturation.suivi.controles.vo.OptionAbonnement;

	[Bindable]
	public class CommandeModel extends EventDispatcher
	{
		
		public var commande:CommandeVO;
		
		private var fluxXML:XMLprocess;
		
		private var pieceJointe:PieceJointeVO;
		
		private var _listeChamps:XML;
		private var _listeArticles:XML;
		
		public function CommandeModel()
		{
		}
		
		public function updateCommande(value:Array):void
		{			
			
			commande = new CommandeVO();
			
			fluxXML = new XMLprocess();
			
			pieceJointe = new PieceJointeVO();
			
			commande.addCommande(value[0][0]);
			
			_listeChamps = new XML(value[1][0].XML);
			
			commande.addRefLibelle(_listeChamps);
			
			commande.addCompteLibelle(value[2][0]);
			
			commande.addFournisseurLibelle(value[3]);
			
			this.updateHistorique(value[4] as ArrayCollection);
			
			_listeArticles = new XML(value[5][0].MODELEXML);
			
			fluxXML.myCommande = commande;
			
			fluxXML.formatXMLToArrayCollection(_listeArticles)
			
			commande.listeArticles = fluxXML.dataForList;
						
			commande.montantTotal = calculTotal();
			
			commande.listePiecesJointes = pieceJointe.mappingAttachement(value[6] as ArrayCollection);
			
			dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_GENERALE_COMPELTE));
			
		}
		
		public function updateHistorique(values:ArrayCollection):void
		{
			var histo:CommandeHistoriqueVO;
			
			for(var i:int=0;i<values.length;i++){
				histo = new CommandeHistoriqueVO();
				histo.addCommandeHistorique(values[i]);
				commande.commandHistorique.addItem(histo);
			}
		}
		
		private function calculTotal():Number
		{
			var total:int = 0;
			for (var pname:String in _listeArticles.article.total)
			{
				var totalArticle:Number = Number(_listeArticles.article.total[pname]);
				var prix:Number = (isNaN(totalArticle))?0:totalArticle; 				
				total = total + prix;
			}
			return total
		}
		
		
		
	}
}