<cflog text="BIP.CFM">
<cfset startTick=getTickCount()>
<cfif isDefined("FORM") AND (NOT structIsEmpty(FORM))>
	<cftry>
		<cfset bipHttpHandler=createObject("component","fr.consotel.api.ibis.publisher.handler.HttpReportEventHandler")>
		<cfset bipHttpHandler.onHttpNotification(FORM)>		
		<cfcatch type="any">
			<cftrace category="ERROR" type="error" text="AN ERROR OCCURRED DURING HTTP NOTIFICATION HANDLING">
			<cfmail from="cedric.rapiera@consotel.fr" to="monitoring@saaswedo.com" subject="AN ERROR OCCURRED DURING HTTP NOTIFICATION HANDLING" type="html">
				<cfdump var="#FORM#" label="FORM"><br />
				<cfdump var="#CFCATCH#" label="CFCATCH"><br />
			</cfmail>
		</cfcatch>
	</cftry>
</cfif>
<cftrace category="DEBUG" type="information" text="BIP HTTP HANDLER DURATION : #(getTickCount() - startTick)# ms">
