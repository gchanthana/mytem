<cfcomponent displayname="fr.consotel.consoview.api.app.RuntimeContext" implements="fr.consotel.consoview.api.app.IContext"
		hint="Context is stored in SESSION by API. Thus all instance updates REQUIRES user SESSION to be re-initialized (e.g USER MUST LOGOUT/LOGIN)">
	<!--- STATIC PROPERTIES
		VARIABLES["STATIC"]["BACKOFFICE"] : BackOffice DNS and Port Number
		VARIABLES["STATIC"]["MAIL-SERVER"] : Mail Server (Same definition as in Administration)
		VARIABLES["STATIC"]["BIP-SERVER-NAME"] : BIP Server DNS
		VARIABLES["STATIC"]["BIP-WSDL"] : BIP WSDL URL
		VARIABLES["STATIC"]["BIP-EVENT-HANDLERS"] : List of BIP Job Event Listeners. Each listener is identified by a given key.
	 --->
	
	<cffunction access="public" name="initInstance" returntype="fr.consotel.consoview.api.app.IContext" hint="Initialize instance : Explicit Call Only">
		<cfset var bipServerDNS="NULL">
		<!--- BACKOFFICE API --->
		<cfset var bckApi=createObject("component","fr.consotel.api.BackOffice")>
		<cfset var bipContext=bckApi.getContextMap("REPORTING")>
		<cfset VARIABLES["STATIC"]=structNew()>
		<cfset VARIABLES["STATIC"]["BACKOFFICE"]=CGI["SERVER_NAME"] & ":" & CGI["SERVER_PORT"]>
		<cfset VARIABLES["STATIC"]["MAIL-SERVER"]="mail.consotel.fr:26">
		<cfif bipServerDNS EQ "NULL">
			<cfset bipServerDNS=SESSION["bip_server_name"]>
		</cfif>
		<cfif structKeyExists(bipContext["BIP"],bipServerDNS)>
			<cfset VARIABLES["STATIC"]["BIP-SERVER-NAME"]=bipContext["BIP"][bipServerDNS]["DNS"]>
			<cflog type="information" text="RUNTIME CONTEXT set successfully BIP-SERVER-NAME : [#bipServerDNS# : #bipContext["BIP"][bipServerDNS]["DNS"]#]">
		<cfelse>
			<cflog type="error" text="RUNTIME CONTEXT IS UNABLE TO FIND BIP-SERVER-NAME : #bipServerDNS#">
			<cfabort showerror="RUNTIME CONTEXT IS UNABLE TO FIND BIP-SERVER-NAME : SEE LOG FILES FOR DETAILS">
		</cfif>
		<cfset VARIABLES["STATIC"]["BIP-WSDL"]="http://" & VARIABLES["STATIC"]["BIP-SERVER-NAME"] &
													"/xmlpserver/services/PublicReportService_v11?WSDL">
		<!--- EVENT HANDLER NAME is not always the same as defined in BIP Servers --->
		<!--- 
		<cfset VARIABLES["STATIC"]["BIP-EVENT-HANDLERS"]={"EVENT-HANDLER"="EVENT_HANDLER"}>
		 --->
		<cfset VARIABLES["STATIC"]["BIP-EVENT-HANDLERS"]=structNew()>
		<cfset VARIABLES["STATIC"]["BIP-EVENT-HANDLERS"]["EVENT-HANDLER"]="EVENT_HANDLER">
		
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="getContextInfos" returntype="any"
			hint="Returns Context Infos (Global Properties). An output error is displayed (without aborting) in log files when requesting AN INVALID NOT FOUND KEY">
		<cfargument name="contextKey" type="string" required="true">
		<cfif structKeyExists(VARIABLES["STATIC"],arguments["contextKey"])>
			<cfreturn VARIABLES["STATIC"][arguments["contextKey"]]>
		<cfelse>
			<cflog type="error" text="RUNTIME CONTEXT ILLEGAL OPERATION ERROR : KEY[#arguments["contextKey"]#] NOT FOUND">
			<cfreturn "NULL">
		</cfif>
	</cffunction>
</cfcomponent>