package gestioncommande.composants
{
	import commandemobile.composants.AddAttachementIHM;
	import commandemobile.composants.AddAttachementImpl;
	import commandemobile.composants.ConfirmationSuppressionIHM;
	import commandemobile.composants.ContainerprogressBar;
	import commandemobile.composants.PopUpEditerIHM;
	
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.FileUpload;
	import gestioncommande.events.ActionsEvent;
	import gestioncommande.events.PiecesJointesEvent;
	import gestioncommande.services.Formator;
	import gestioncommande.services.PiecesJointesService;
	import gestioncommande.services.UploadFileService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.DataGrid;
	import mx.core.IFlexDisplayObject;
	import mx.events.CollectionEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	
	[Bindable]
	public class PiecesJointesImpl extends Box
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//		
			
			//--------------------------------------------------------------------------------------------//
			//					IHMS
			//--------------------------------------------------------------------------------------------//
		
		public var dgdPiecesJointes				:DataGrid;
		
			//--------------------------------------------------------------------------------------------//
			//					POPUPS
			//--------------------------------------------------------------------------------------------//
		
		private var _addAttachementIHM			:AddAttachementIHM;
		private var _confir						:ConfirmationSuppressionIHM;
		private var _fileRef					:FileReference;
		private var _edit						:PopUpEditerIHM;
		private var _progress					:ContainerprogressBar;
		
			//--------------------------------------------------------------------------------------------//
			//					SERVICES
			//--------------------------------------------------------------------------------------------//
		
		private var _fileUploadSrv				:UploadFileService = new UploadFileService();
		
		private var _pceJSev					:PiecesJointesService = new PiecesJointesService();
		
			//--------------------------------------------------------------------------------------------//
			//					OBJETS TYPE
			//--------------------------------------------------------------------------------------------//
		
		private var _file						:FileReference;
		
		private var _selectedItem				:FileUpload;
		
			//--------------------------------------------------------------------------------------------//
			//					DIVERS
			//--------------------------------------------------------------------------------------------//
		
		public var piecesjointes				:ArrayCollection = new ArrayCollection();
				
		public var numerocommande				:String = "";
		public var UUID							:String = "";
		public var sizeTotal					:String = "";

		public var text_lblNumberPiecesjointes	:int = 0;
		public var idcommande					:int = 0;

		private var _inc						:int = -1;
		private var _filesToUpload				:Array = new Array();
		private var _isContainIdcommande		:Boolean = false;
		
			//--------------------------------------------------------------------------------------------//
			//					LIBELLES A TRADUIRE - LUPO
			//--------------------------------------------------------------------------------------------//
		
		private var text_message1				:String = ResourceManager.getInstance().getString('M16', 'La_pi_ce_jointe_');
		private var text_message2				:String = ResourceManager.getInstance().getString('M16', '_va__tre_supprim_e_');
		
		private var text_msg_rename				:String = ResourceManager.getInstance().getString('M16', 'Fichier_renomm_');
		private var text_msg_download			:String = ResourceManager.getInstance().getString('M16', 'Fichier_t_l_charg_');
		private var text_msg_upload				:String = ResourceManager.getInstance().getString('M16', 'Fichier_s__attach__s_');
		
	//--------------------------------------------------------------------------------------------//
	//					CONSRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function PiecesJointesImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		public function setProcedure(selectedCommande:Commande):void
		{
			idcommande 		= selectedCommande.IDCOMMANDE;
			numerocommande 	= selectedCommande.NUMERO_COMMANDE;
			
			createUUID();
			
			fournirAttachements();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED
		//--------------------------------------------------------------------------------------------//
		
		protected function btnAddAttachmentsClickHandler(me:MouseEvent):void
		{
			if(Number(sizeTotal) > 20)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Votre_capicit__total_de_pi_ces_jointes_e'), 'Consoview', null);
				
				return;
			}
			
			_addAttachementIHM 			 	= new AddAttachementIHM();
			_addAttachementIHM.sizeFiles 	= Number(sizeTotal);
			_addAttachementIHM.UUID			= this.UUID;
			_addAttachementIHM.IDCMD		= this.idcommande;
			
			PopUpManager.addPopUp(_addAttachementIHM, this.parentApplication as DisplayObject, true);
			PopUpManager.centerPopUp(_addAttachementIHM);
			
			_addAttachementIHM.addEventListener(AddAttachementImpl.ADD_ATTACHEMENT, addAttachementHandler);
		}
		
		protected function dgPiecesJointesItemClickHandler(le:ListEvent):void
		{
			var itemRenderer	:Object = le.itemRenderer;
			var currentItem		:Object = itemRenderer.data;
			
			if(itemRenderer.hasOwnProperty('cpstAction'))
			{
				_selectedItem = FileUpload.mappingObjectToFileUpload(currentItem);
				
				switch(itemRenderer.cpstAction.statusClicked)
				{
					case 1 : renameFile(_selectedItem);	break;
					case 2 : download(_selectedItem); 	break;
					case 3 : removeFile(_selectedItem);	break;
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS
			//--------------------------------------------------------------------------------------------//
		
		private function initialisationHandlers():void
		{
			piecesjointes.addEventListener(CollectionEvent.COLLECTION_CHANGE, collectionChangeHandler);
			
			dgdPiecesJointes.addEventListener('REFRESHGRID', refreshGridHandler);
		}
		
		private function refreshElements():void
		{
			var total:Number = 0;
			
			text_lblNumberPiecesjointes = piecesjointes.length;
			
			for(var i:int = 0; i < text_lblNumberPiecesjointes;i++)
			{
				total = total + (piecesjointes[i] as FileUpload).fileSize;
			}
			
			piecesjointes.refresh();
			
			sizeTotal = Formator.formatOctetsToMegaOctets(total);
		}
		
		private function progressBarActivation(nbrFile:int):void
		{
			_progress				= new ContainerprogressBar();
			_progress.indeterminate = true;
			_progress.totalFile		= nbrFile;
			
			PopUpManager.addPopUp(_progress, this.parentApplication as DisplayObject, true);
			PopUpManager.centerPopUp(_progress);
		}
		
		private function edition(lastName:String):void
		{
			_edit 				= new PopUpEditerIHM();
			_edit.nameFile		= lastName;
			_edit.libelle 		= ResourceManager.getInstance().getString('M16', 'Editer');
			_edit.sub_libelle 	= ResourceManager.getInstance().getString('M16', 'Modifier_le_nom_de_la_pi_ce_jointe');
			_edit.isEditable	= true;
			_edit.addEventListener(PopUpEditerIHM.EDIT_DOWNLAOD, editionHandler);
			
			PopUpManager.addPopUp(_edit, this.parentApplication as DisplayObject ,true);
			PopUpManager.centerPopUp(_edit);
		}
		
		private function download(value:FileUpload):void
		{
			_edit 				= new PopUpEditerIHM();
			_edit.nameFile		= value.fileName;
			_edit.libelle 		= ResourceManager.getInstance().getString('M16', 'T_l_charger');
			_edit.sub_libelle 	= ResourceManager.getInstance().getString('M16', 'Fichier___t_l_charger');
			_edit.isEditable	= false;
			_edit.addEventListener(PopUpEditerIHM.EDIT_DOWNLAOD, downloadHandler);
			
			PopUpManager.addPopUp(_edit, this.parentApplication as DisplayObject ,true);
			PopUpManager.centerPopUp(_edit);
		}
		
		private function isBDC(labelPiece:String):Boolean
		{
			var OK		:Boolean = true;
			var concat	:String = "BDC_" + numerocommande + ".pdf";
			
			if(labelPiece == concat)
				OK = false;
			
			return OK;
		}
		
		private function checkExtentionPresence(filename:String, extension:String):String
		{
			var myfilename	:String = "";
			var lenFileName	:int = filename.length;
			var lenExtension:int = extension.length;
			var idxSize		:int = lenFileName - lenExtension;
			var lastIdxOf	:int = filename.lastIndexOf(extension);
			
			if(lastIdxOf == idxSize)
				myfilename = filename;
			else
				myfilename = filename + extension;
			
			return myfilename;
		}
		
		private function closePopUp(display:IFlexDisplayObject):void
		{
			PopUpManager.removePopUp(display);
		}
		
		private function confirmation(name:String):void
		{
			_confir  			= new ConfirmationSuppressionIHM();
			_confir.message 	= text_message1 + " " + name + " " + text_message2;
			_confir.addEventListener(ConfirmationSuppressionIHM.ISREMOVE_ATTACHEMENT, confirmationRemoveHandler);
			
			PopUpManager.addPopUp(_confir, this.parentApplication as DisplayObject ,true);
			PopUpManager.centerPopUp(_confir);
		}
		
		private function renameFile(currentFileSelected:FileUpload):void
		{
			if(isBDC(currentFileSelected.fileName))
				edition(currentFileSelected.fileName);
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Cette_pi_ce_jointe_ne_peut_pas__tre_modi'), 'Consoview', null);
		}
		
		private function removeFile(currentFileSelected:FileUpload):void
		{
			if(isBDC(currentFileSelected.fileName))
				confirmation(currentFileSelected.fileName);
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Cette_pi_ce_jointe_ne_peut_pas__tre_supp'), 'Consoview', null);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					GESTIONS EVENTS
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			initialisationHandlers();
		}
		
		private function collectionChangeHandler(ce:CollectionEvent):void
		{
			refreshElements();
		}
		
		private function refreshGridHandler(e:Event):void
		{
			refreshElements();
		}
		
		private function editionHandler(e:Event):void
		{	
			renameFileSelected();
		}
		
		private function downloadHandler(e:Event):void
		{
			var myFileName	:String 	= checkExtentionPresence(_edit.tbxName.text, _selectedItem.fileExt);
			var urlUpload	:String 	= moduleCommandeMobileIHM.NonSecureUrlBackoffice + '/fr/consotel/consoview/M16/v2/processDownload.cfm';
			var request		:URLRequest = new URLRequest(urlUpload);
				request.method 			= URLRequestMethod.GET;
			
			_file = new FileReference();
			
			var variables:URLVariables 	= new URLVariables();
				variables.UUID			= UUID;
				variables.LIBELLEFILE	= _selectedItem.fileName;
				variables.FILEREF		= _file;
			
			request.data = variables;
			
			_file.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			_file.addEventListener(Event.COMPLETE, downloadedHandler);
			_file.addEventListener(IOErrorEvent.IO_ERROR, downloadedErrorHandler);
			_file.addEventListener(Event.SELECT, doEvent)
			_file.download(request, myFileName);
		}
		
		private function progressHandler(e:ProgressEvent):void
		{
			var len:int = 1;
			
			if(_inc < 1)
				len = 1;
			else
				len = _inc;
			
			_progress.setProgressBar(e.bytesLoaded, e.bytesTotal, len);
		}
		
		private function downloadedHandler(e:Event):void
		{
			closePopUp(_progress);
			
			ConsoviewAlert.afficherOKImage(text_msg_download, this);
			
			closePopUp(_edit);
		}
		
		private function downloadedErrorHandler(iee:IOErrorEvent):void
		{
			trace(iee.text);
			
			closePopUp(_progress);
		}	
		
		private function doEvent(e:Event):void
		{
			progressBarActivation(1);
		}
		
		private function addAttachementHandler(e:Event):void
		{
			uploadFilesSelected();
		}
		
		private function confirmationRemoveHandler(e:Event):void
		{
			removeFileSelected();
		}
		
			//--------------------------------------------------------------------------------------------//
			//					APPELS PROCEDURES
			//--------------------------------------------------------------------------------------------//
		
		private function createUUID():void
		{
			_pceJSev.createUUID();
			_pceJSev.addEventListener(PiecesJointesEvent.FILE_UUID_CREATED, createUUIDHandler);
		}
		
		private function fournirAttachements():void
		{
			piecesjointes = new ArrayCollection();
			
			_pceJSev.fournirAttachements(idcommande);
			_pceJSev.addEventListener(PiecesJointesEvent.LAST_FILES_ATTACHED, fournirAttachementsHandler);
		}
		
		private function uploadFilesSelected():void
		{
			_fileUploadSrv.uploadFiles(_addAttachementIHM.currentFiles.source, _isContainIdcommande);
			_fileUploadSrv.addEventListener('FILES_UPLOADED', uploadFilesSelectedHandler);
		}
		
		private function renameFileSelected():void
		{
			_edit.tbxName.text = checkExtentionPresence(_edit.tbxName.text,_selectedItem.fileExt)
			
			_fileUploadSrv.renameFile(_selectedItem, _edit.tbxName.text, _isContainIdcommande);
			_fileUploadSrv.addEventListener('FILE_RENAMED', renameFileSelectedHandler);
		}
		
		private function removeFileSelected():void
		{
			_fileUploadSrv.removeFile(_selectedItem, _isContainIdcommande);
			_fileUploadSrv.addEventListener('FILE_REMOVED', removeFileSelectedHandler);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					RETOURS PROCEDURES
			//--------------------------------------------------------------------------------------------//
		
		private function createUUIDHandler(pje:PiecesJointesEvent):void
		{
			UUID = _pceJSev.UUID;
			
			if(idcommande > 0)
			{
				_isContainIdcommande = true;
				
				fournirAttachements();
			}
			else
				_isContainIdcommande = false;	
		}
		
		private function fournirAttachementsHandler(pje:PiecesJointesEvent):void
		{
			var total:Number = 0;
			
			piecesjointes 				= _pceJSev.lastFilesAttached;
			text_lblNumberPiecesjointes = piecesjointes.length;
			
			if(text_lblNumberPiecesjointes > 0)
				UUID = piecesjointes[0].fileUUID;
			else
				UUID = _pceJSev.UUID;
			
			
			for(var i:int = 0; i < text_lblNumberPiecesjointes;i++)
			{
				total = total + (piecesjointes[i] as FileUpload).fileSize;
			}
			
			sizeTotal = Formator.formatOctetsToMegaOctets(total);
		}		
		
		private function uploadFilesSelectedHandler(e:Event):void
		{		
			var lenFiles	:int = _addAttachementIHM.currentFiles.length;
			
			for(var i:int = 0;i < lenFiles;i++)
			{
				piecesjointes.addItem(_addAttachementIHM.currentFiles[i]);
			}
			
			ConsoviewAlert.afficherOKImage(text_msg_upload, this);
			
			fournirAttachements();
			
			PopUpManager.removePopUp(_addAttachementIHM);
		}
		
		private function renameFileSelectedHandler(e:Event):void
		{
			var lenFiles	:int = piecesjointes.length;
			
			for(var i:int = 0;i < lenFiles;i++)
			{
				if(piecesjointes[i].fileId == _selectedItem.fileId)
				{
					piecesjointes[i].fileName = _selectedItem.fileName = _edit.tbxName.text;
					piecesjointes.itemUpdated(piecesjointes[i]);
				}
			}
			
			ConsoviewAlert.afficherOKImage(text_msg_rename, this);
			
			fournirAttachements();
			
			closePopUp(_edit);
		}
		
		private function removeFileSelectedHandler(e:Event):void
		{
			var lenFiles	:int = piecesjointes.length;
			
			for(var i:int = 0;i < lenFiles;i++)
			{
				if(piecesjointes[i].fileId == _selectedItem.fileId)
				{
					piecesjointes.removeItemAt(i);
					
					break;
				}
			}
			
			piecesjointes.refresh();
			
			fournirAttachements();
			
			closePopUp(_confir);
		}
	}
}