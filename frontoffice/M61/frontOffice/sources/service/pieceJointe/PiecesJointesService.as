package service.pieceJointe
{
	import composants.util.ConsoviewAlert;
	
	import events.pieceJointe.PiecesJointesEvent;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public class PiecesJointesService extends EventDispatcher
	{
		//----------------- VARIABLES -----------------//
		
		private var _path					:String = "fr.consotel.consoview.M61.AttachementService";
		private var _text_Error_createUUID	:String = "impossible de créer un fichier";
		private var _uuid					:String = "";
		
		
		//----------------- METHODES -----------------//
		
		/* constructeur */
		public function PiecesJointesService()
		{
		}
		
		/* creation d'un UUID */
		public function createUUID(): void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_path, "initUUID", createUUIDResultHandler);		
			RemoteObjectUtil.callService(op);	
		}
		
		/* Handler */
		private function createUUIDResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				_uuid = re.result as String;
				dispatchEvent(new PiecesJointesEvent(PiecesJointesEvent.FILE_UUID_CREATED));
			}
			else
				popup(_text_Error_createUUID);
		}
		
		/* fonction affichage erreur */
		private function popup(str:String):void
		{
			ConsoviewAlert.afficherError(str, 'Attention');
		}
		
		
		//----------------- GETTERS - SETTERS -----------------//
		
		public function get uuid():String
		{
			return _uuid;
		}
	}
}