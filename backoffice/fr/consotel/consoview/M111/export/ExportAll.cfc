<cfcomponent displayname="ExportAll">
	<cfset ID_MASTER_SNCF = 7348802>
	<cfset init()>
	
	<cfset VARIABLES["reportServiceClass"]="fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService">
	<cfset VARIABLES["containerServiceClass"]="fr.consotel.consoview.api.container.ApiContainerV1">
	<cfset VARIABLES["MODULE"]="M111">
	<cfset VARIABLES["CODE_RAPPORT"]="M111">
	<cfset VARIABLES["RAPPORTID"]=0>
		
	<cfset reportService = createObject("component",VARIABLES["reportServiceClass"])>
	<cfset ApiE0 = CreateObject("component","fr.consotel.consoview.api.E0.ApiE0")>
	<cfset containerService	= createObject("component",VARIABLES["containerServiceClass"])>
	
	<cffunction name="init" hint="pour les variables de session" access="remote">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idRacineMaster = SESSION.PERIMETRE.IDRACINE_MASTER>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
		<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
		<cfif SESSION.PERIMETRE.ID_PERIMETRE EQ idRacine>
			   <cfset idOrga=idracine>
			   <cfelse>
				   <cfset idOrga  = getOrga()>
	    </cfif>
	</cffunction>

	<cffunction name="getOrga" access="private" output="false" returntype="Any"   hint="recupere id_organisation">												   
	   <cfquery datasource="#SESSION.OFFREDSN#" name="rsltOrga">
					   SELECT PKG_CV_RAGO.GET_ORGA(#SESSION.PERIMETRE.ID_PERIMETRE#) as IDORGA FROM DUAL
	   </cfquery>
	   <cfreturn rsltOrga['IDORGA'][1]>
	</cffunction>

	<!--- FONCTION DE RAPPORT --->
	<cffunction name="exportStandard" access="remote" returntype="numeric">
		<cfargument name="idpool" required="true" type="Numeric" />
		<cfargument name="search1" required="true" type="String" />
		<cfargument name="columnSearch1" required="true" type="string" />
		<cfargument name="search2" required="true" type="String" />
		<cfargument name="columnSearch2" required="true" type="string" />
		<cfargument name="indexDebut" required="true" type="Numeric" />
		<cfargument name="nbItem" required="true" type="Numeric" />
		<cfargument name="labelSort" required="true" type="string" />
		<cfargument name="idrestriction" required="true" type="Numeric" />
		<cfargument name="orderByReq" required="true" type="string"/>
		<cfargument name="output" required="true" type="String" />
		<cfargument name="xdo" required="true" type="String" />
		<cfargument name="template" required="true" type="String" />
		<cfargument name="ext" required="true" type="String" />
		<cfargument name="format" required="true" type="String" />
	
		<cfset initStatus=reportService.init(idRacine,xdo,"ConsoView",VARIABLES["MODULE"],VARIABLES["CODE_RAPPORT"])>
				
			<cfif initStatus EQ TRUE>
			
				<cfset setStatus=TRUE>
				
			<!--- PARAMETRES RECUPERES EN SESSION --->
				
				<!--- IDRACINE --->								
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=idRacine>
					<cfset setStatus=setStatus AND reportService.setParameter("p_idracine",tmpParamValues)>

				<!--- IDUSER --->								
				<cfif arguments.idpool lt 0>
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=idUser>
					<cfset setStatus=setStatus AND reportService.setParameter("p_app_loginid",tmpParamValues)>
				</cfif>

			<!--- PARAMETRES --->
				<!--- IDPOOL --->
				<cfif arguments.idpool gt 0>
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=arguments.idpool>
					<cfset setStatus=setStatus AND reportService.setParameter("p_idpool",tmpParamValues)>
				</cfif>

				<!--- SEARCH 1--->
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=search1>
					<cfset setStatus=setStatus AND reportService.setParameter("p_chaine1",tmpParamValues)>
				
				<!--- COLUMNSEARCH 1--->
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=columnSearch1>
					<cfset setStatus=setStatus AND reportService.setParameter("p_column_search1",tmpParamValues)>

					<!--- SEARCH 2 --->
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=search2>
					<cfset setStatus=setStatus AND reportService.setParameter("p_chaine2",tmpParamValues)>
				
				<!--- COLUMNSEARCH 2 --->
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=columnSearch2>
					<cfset setStatus=setStatus AND reportService.setParameter("p_column_search2",tmpParamValues)>

				<!--- LABELSORT --->
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=labelSort>
					<cfset setStatus=setStatus AND reportService.setParameter("p_order_column",tmpParamValues)>

				<!--- INDEXDEBUT --->
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=indexDebut>
					<cfset setStatus=setStatus AND reportService.setParameter("p_index_debut",tmpParamValues)>
				
				<!--- NBITEM --->
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=nbItem>
					<cfset setStatus=setStatus AND reportService.setParameter("p_number_of_records",tmpParamValues)>
				
				<!--- ID ORGA --->
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=idOrga>
					<cfset setStatus=setStatus AND reportService.setParameter("p_idorga",tmpParamValues)>

				<!--- IDRESTRICTION --->
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=idrestriction>
					<cfset setStatus=setStatus AND reportService.setParameter("p_idrestriction",tmpParamValues)>
				
				<!--- CODE_LANGUE --->
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=idLangue>
					<cfset setStatus=setStatus AND reportService.setParameter("p_langid",tmpParamValues)>

				<!--- p_value_rest ? --->
				<cfif (arguments.idpool gt 0)>
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]="">
					<cfset setStatus=setStatus AND reportService.setParameter("p_value_rest",tmpParamValues)>
				</cfif>

				<!--- ORDER BY --->	
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1] = orderByReq>
					<cfset setStatus=setStatus AND reportService.setParameter("p_order_by",tmpParamValues)>	
				
				<!--- X_INCLUDE_CSV_HEADER --->			 
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=1>
					<cfset setStatus=setStatus AND reportService.setParameter("X_INCLUDE_CSV_HEADER",tmpParamValues)>
				 

				
					<cfset tmpParamValues=arrayNew(1)>
					<cfset tmpParamValues[1]=idRacine>
					<cfset setStatus=setStatus AND reportService.setParameter("G_IDRACINE",tmpParamValues)>
				

				<cfset pretour = -1>
				<cfif setStatus EQ TRUE>
					<cfset FILENAME=output>
					<cfset outputStatus = FALSE>
					<cfset outputStatus=reportService.setOutput(template,format,ext,FILENAME,VARIABLES["CODE_RAPPORT"])>
					<cfif outputStatus EQ TRUE>
						<cfset reportStatus = FALSE>
						<cfset reportStatus = reportService.runTask(SESSION.USER["CLIENTACCESSID"])>
						<cfset pretour = reportStatus['JOBID']>
						<cfif reportStatus['JOBID'] neq -1>		
							<cfreturn reportStatus["JOBID"]>
						</cfif>
					</cfif>
				</cfif>	
				
			</cfif>
			
			<cfreturn pretour >
			
	</cffunction>
	
	<!--- fonction pool --->

	<cffunction name="getPoolLibelle">
		<cfargument name="idpool" required="true" type="numeric" />
		
		<cfset output = "">
		
		<cfif idpool EQ 0>
			<cfset output = 'Export Fleet Global'>
		<cfelseif idpool EQ -10 >
			<cfset output = 'Export All Pool'>
		<cfelse>
			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111.getLibellePool">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idpool" value="#idpool#" null="false">
				<cfprocresult name="p_retour">
			</cfstoredproc>
		
			<cfif p_retour.recordCount gt 0 >
				<cfset output = 'Export Fleet -' & p_retour['LIBELLE_POOL'][1]>
			</cfif>
		</cfif>
		<cfreturn output>
	</cffunction>

	<cffunction name="ViewParcGlobalExport"  access="remote" returntype="numeric">
		<cfargument name="idpool" required="true" type="Numeric" />
		<cfargument name="search1" required="true" type="String" />
		<cfargument name="columnSearch1" required="true" type="string" />
		<cfargument name="search2" required="true" type="String" />
		<cfargument name="columnSearch2" required="true" type="string" />
		<cfargument name="indexDebut" required="true" type="Numeric" />
		<cfargument name="nbItem" required="true" type="Numeric" />
		<cfargument name="labelSort" required="true" type="string" />
		<cfargument name="idrestriction" required="true" type="Numeric" />
		<cfargument name="orderByReq" required="true" type="string"/>


		
		<!--- Nom rapport --->	
		
		<cfset output = getPoolLibelle(idpool)>
		
		<!--- Le chemin du rapport sous BIP --->
		<cflog text = "----------------------------------------------------   idRacineMaster : #idRacineMaster#">
		<cfif idRacineMaster EQ #ID_MASTER_SNCF#>
			
			<cfif idpool EQ 0>
				<cfset xdo="/consoview/M111/exportVue/SNCF_ParcGlobalView/SNCF_ParcGlobalView.xdo">
			<cfelseif idpool EQ -10>
				<cfset xdo="/consoview/M111/exportVue/SNCF_ParcTousPool/SNCF_ParcTousPool.xdo">
			<cfelse>
				<cfset xdo="/consoview/M111/exportVue/SNCF_MobileParcView/SNCF_MobileParcView.xdo">
			</cfif>
		<cfelse>
			<cfif idpool EQ 0>
				<cfset xdo="/consoview/M111/exportVue/ParcGlobalView/ParcGlobalView.xdo">
			<cfelseif idpool EQ -10>
				<cfset xdo="/consoview/M111/exportVue/ParcTousPool/ParcTousPool.xdo">
			<cfelse>
				<cfset xdo="/consoview/M111/exportVue/MobileParcView/MobileParcView.xdo">
			</cfif>
		</cfif>
	
		<!--- TEMPLATE BI --->
		<cfset template="default">
		<!--- Extension et FORMAT --->
		<cfset ext="CSV">
		<cfset format="CSV">
	 
		<cfset obj = createObject("component","ExportAll")>
		<cfset result = obj.exportStandard(idpool,search1,columnSearch1,search2,columnSearch2,indexDebut,nbItem,labelSort,idrestriction,orderByReq,output,xdo,template,ext,format)>
		
		<cfreturn result>
	</cffunction>

</cfcomponent>