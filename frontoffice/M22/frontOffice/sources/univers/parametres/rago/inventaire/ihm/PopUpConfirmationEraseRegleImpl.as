package univers.parametres.rago.inventaire.ihm
{
	import flash.events.Event;
	
	import mx.containers.HBox;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class PopUpConfirmationEraseRegleImpl extends HBox
	{
		
		public function PopUpConfirmationEraseRegleImpl()
		{  }
		
		protected function btnYesClickHandler(event:Event):void
		{
			dispatchEvent(new Event("eraseRegleNow",true));
			PopUpManager.removePopUp(this);
		}
		
		protected function btnNoClickHandler(event:Event):void
		{
			dispatchEvent(new Event("noEraseRegleNow",true));
			PopUpManager.removePopUp(this);
		}
	}
}