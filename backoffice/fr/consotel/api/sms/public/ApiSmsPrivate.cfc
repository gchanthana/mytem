<cfcomponent displayname="API SMS PRIVATE" output="false">

	<cffunction  name="realCreateCampagne" access="package" hint=" I return a new UUID for your campaign" output="false" returntype="string">

		<cfargument name="idracine" required="false" type="numeric"  hint="id racine client">
		<!--- creer uuid --->
		<cfset uuid=CreateUUID()>

		<!--- créer un objet de type exception --->
	    <cfset errorException=createobject("component","fr.consotel.api.sms.model.ApiException")>

		<!--- creer une structure de données --->
		<cfset campagneStruct= {UUID = #uuid#,VALID = 1,IDRACINE=#idracine#}>
		<cfset insertUUID=createobject("component","fr.consotel.api.sms.model.Campagne").create(campagneStruct)>
		<cfif insertUUID eq false>
			<cfset errorException.throwException("apiSMS_099", "Impossible de se connecter au serveur Oracle")>
		<cfelse>
			<cfreturn uuid>
		</cfif>
	</cffunction>

<!---------------------------------------------------------------------------------------------------------------------------------------->

	<cffunction  name="realSendMessageToSingleNumber" access="package" hint="I send your SMS et return FALSE ou TRUE" output="true" returntype="Any">

		<cfargument name="phoneNumber" required="false" type="any"  hint="Phone number">
		<cfargument name="contentSms"  required="false" type="any"  hint="The content of SMS">
		<cfargument name="operator"    required="false" type="any"  hint="Operator">
		<cfargument name="uuid"        required="false" type="any"  hint="UUID compaign">
		<cfargument name="withWebService"     required="false" type="numeric" >

		<!--- créer un objet de type exception --->
		<cfset errorException=createobject("component","fr.consotel.api.sms.model.ApiException")>

		<cfset valideData=createobject("component","fr.consotel.api.sms.model.Utility")>

		<cfset resSendSms="">
		<cfset exception="">

		<!--- tester s'il ne manque pas des paramètres  --->
	    <cfif not (isDefined("phoneNumber") and isDefined("contentSms") and isDefined("operator") and isDefined("uuid"))>
			<cfset exception=errorException.throwException("apiSMS_01",ARGUMENTS)>
	    </cfif>

	    <!--- tester si uuid est valide --->
    	<cfif isDefined("uuid")>
			<cfif IsInstanceof(uuid,'java.lang.String')>
				<cfset isValideUUID=valideData.isUUID(uuid)>
				<cfif isValideUUID neq true>
					<cfset exception=errorException.throwException("apiSMS_07",uuid)>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : uuid, doit etre de type String")>
			</cfif>
		</cfif>

	    <!--- tester si l'operator est valide --->
		<cfif  isDefined("operator") >
			<cfif IsInstanceof(operator,'java.lang.String')>
				<cfset isValideOperator=valideData.isOperator(operator)>
				<cfif isValideOperator neq true>
					<cfset exception=errorException.throwException("apiSMS_05",operator)>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : operator, doit etre de type String")>
			</cfif>
		</cfif>

	    <!--- si phoneNumber est définie  si phoneNumber est international l'opertateur doit être SFR --->
	    <cfif  isDefined("phoneNumber") >
	    	<cfif IsInstanceof(phoneNumber,'java.lang.String')><!--- tester le type de paramètre --->
				<cfset isValidePhoneNumber=valideData.isOkPhoneNumber(phoneNumber,operator)>
					<cfif isValidePhoneNumber neq true>
						 <cfset exception=errorException.throwException("apiSMS_03",phoneNumber)>
					</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : phoneNumber, doit etre de type String")>
			</cfif>
		</cfif>

		<!--- tester si le contentSms est valide --->
		<cfif  isDefined("contentSms") >
			<cfif IsInstanceof(contentSms,'java.lang.String')>
				<cfset isValideContent=valideData.isOkContentSms(contentSms)>
				<cfif isValideContent neq true>
					<cfset exception=errorException.throwException("apiSMS_04",contentSms)>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : contentSms, doit etre de type String")>
			</cfif>
		</cfif>

		<!---  pas d'exeception on peut envoyer les SMS --->
		<cfif exception eq "">
			<cfset resSendSms=SendSMS(operator,phoneNumber,contentSms,"",uuid,"",withWebService)><!--- appelle à une methode pour envoyer les SMS --->
			<cfreturn resSendSms>
		</cfif>

	</cffunction>


	<!---------------------------------------------------------------------------------------------------------------------------------------->

	<cffunction name="realSendMessageToMultipleNumber" access="package" hint="I send your SMS for several phone et return FALSE ou TRUE" output="true" returntype="Any">

		<cfargument name="phoneNumber" required="false" type="any"  hint="Phone number">
		<cfargument name="contentSms"  required="false" type="any"  hint="The content of SMS">
		<cfargument name="operator"    required="false" type="any"  hint="Operator">
		<cfargument name="uuid"        required="false" type="any"  hint="UUID compaign">
		<cfargument name="withWebService"     required="false" type="numeric">

		<!--- créer un objet de type exception --->
		<cfset errorException=createobject("component","fr.consotel.api.sms.model.ApiException")>

		<cfset valideData=createobject("component","fr.consotel.api.sms.model.Utility")>

		<cfset resSendSms="">
		<cfset exception="">

		<!--- tester s'il ne manque pas des paramètres  --->
	    <cfif not (isDefined("phoneNumber") and isDefined("contentSms") and isDefined("operator") and isDefined("uuid"))>
			<cfset exception=errorException.throwException("apiSMS_01",ARGUMENTS)>
	    </cfif>

	    <!--- si phoneNumber est définie  --->
	    <cfif  isDefined("phoneNumber") >
	    	<cfif isArray(phoneNumber)><!--- tester le type de paramètre = array --->
				<cfset isValidePhoneNumber=valideData.isOkPhoneNumber(phoneNumber)>
				<cfif not isArray(isValidePhoneNumber)>
					<cfif isValidePhoneNumber neq true>
						 <cfset exception=errorException.throwException("apiSMS_03",phoneNumber)>
					</cfif>
				<cfelse>
					<cfset exception=errorException.throwException("apiSMS_03",isValidePhoneNumber[1])>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : phoneNumber, doit etre de type Array")>
			</cfif>
		</cfif>

		<!--- tester si uuid est valide --->
    	<cfif isDefined("uuid")>
			<cfif IsInstanceof(uuid,'java.lang.String')>
				<cfset isValideUUID=valideData.isUUID(uuid)>
				<cfif isValideUUID neq true>
					<cfset exception=errorException.throwException("apiSMS_07",uuid)>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : uuid, doit etre de type String")>
			</cfif>
		</cfif>

		<!--- tester si l'operator est valide --->
		<cfif  isDefined("operator") >
			<cfif IsInstanceof(operator,'java.lang.String')>
				<cfset isValideOperator=valideData.isOperator(operator)>
				<cfif isValideOperator neq true>
					<cfset exception=errorException.throwException("apiSMS_05",operator)>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : operator, doit etre de type String")>
			</cfif>
		</cfif>

		<!--- tester si le contentSms est valide --->
		<cfif  isDefined("contentSms") >
			<cfif IsInstanceof(contentSms,'java.lang.String')>
				<cfset isValideContent=valideData.isOkContentSms(contentSms)>
				<cfif isValideContent neq true>
					<cfset exception=errorException.throwException("apiSMS_04",contentSms)>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : contentSms, doit etre de type String")>
			</cfif>
		</cfif>

		<!---  pas d'exeception on peut envoyer les SMS --->
		<cfif exception eq "">
			<cfset resSendSms=SendSMS(operator,phoneNumber,contentSms,"",uuid,"",withWebService)><!--- appelle à une methode pour envoyer les SMS --->
			<cfreturn resSendSms>
		</cfif>
	</cffunction>

	<!---------------------------------------------------------------------------------------------------------------------------------------->

	<!--- permet d’envoyer plusieurs message a un seul numero téléphone avec un  intervalle de temps prédéfini --->

	<cffunction  name="realSendMultipleMessageToSingleNumber" access="package" hint="I send yours SMS for one phone et return FALSE ou TRUE" output="true" returntype="Any">

		<cfargument name="phoneNumber" required="false" type="any"  hint="Phone number">
		<cfargument name="contentSms"  required="false" type="any"  hint="The content of SMS">
		<cfargument name="operator"    required="false" type="any"  hint="Operator">
		<cfargument name="delay"       required="false" type="any"  hint="Le nombre des seconds entre les envois ">
		<cfargument name="uuid"        required="false" type="any"  hint="UUID compaign">
		<cfargument name="withWebService"     required="false" type="numeric">

		<!--- créer un objet de type exception --->
		<cfset errorException=createobject("component","fr.consotel.api.sms.model.ApiException")>

		<!--- créer un objet de type Utility --->
		<cfset valideData=createobject("component","fr.consotel.api.sms.model.Utility")>
		<cfset resSendSms="">
		<cfset exception="">

		<!--- tester s'il ne manque pas des paramètres  --->
	    <cfif not (isDefined("phoneNumber") and isDefined("contentSms") and isDefined("operator") and isDefined("uuid") and isDefined("delay"))>
			<cfset exception=errorException.throwException("apiSMS_01",ARGUMENTS)>
	    </cfif>

	    <!--- si phoneNumber est définie  --->
	    <cfif  isDefined("phoneNumber") >
	    	<cfif IsInstanceof(phoneNumber,'java.lang.String')><!--- tester le type de paramètre --->
				<cfset isValidePhoneNumber=valideData.isOkPhoneNumber(phoneNumber)>
					<cfif isValidePhoneNumber neq true>
						 <cfset exception=errorException.throwException("apiSMS_03",phoneNumber)>
					</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : phoneNumber, doit etre de type String ")>
			</cfif>
		</cfif>

		 <!--- tester si uuid est valide --->
    	<cfif isDefined("uuid")>
			<cfif IsInstanceof(uuid,'java.lang.String')>
				<cfset isValideUUID=valideData.isUUID(uuid)>
				<cfif isValideUUID neq true>
					<cfset exception=errorException.throwException("apiSMS_07",uuid)>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : uuid, doit etre de type String")>
			</cfif>
		</cfif>

		<!--- tester si l'operator est valide --->
		<cfif  isDefined("operator") >
			<cfif IsInstanceof(operator,'java.lang.String')>
				<cfset isValideOperator=valideData.isOperator(operator)>
				<cfif isValideOperator neq true>
					<cfset exception=errorException.throwException("apiSMS_05",operator)>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : operator, doit etre de type String")>
			</cfif>
		</cfif>

		<!--- tester si le contenu de SMS est valide --->
		<cfif  isDefined("contentSms") >
			<cfif isArray(contentSms)>
				<cfset isValideContent=valideData.isOkContentSms(contentSms)>
				<cfif not isArray(isValideContent)>
					<cfif isValideContent neq true>
						<cfset exception=errorException.throwException("apiSMS_04",contentSms)>
					</cfif>
				<cfelse>
					<cfset exception=errorException.throwException("apiSMS_04",isValideContent[1])>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : contentSms, doit etre de type Array")>
			</cfif>
		</cfif>

		<!--- tester si delay est valide --->
		<cfif  isDefined("delay") >
			<cfif IsInstanceof(delay,'java.lang.String')>
				<cfset isValideDelay=valideData.isOkDelay(delay)>
				<cfif isValideDelay neq true>
					<cfset exception=errorException.throwException("apiSMS_02","Parametre : delay, doit etre de type String et contient que des chiffres")>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02","Parametre : delay, doit etre de type String")>
			</cfif>
		</cfif>

		<!---  pas d'exeception on peut envoyer les SMS --->
		<cfif exception eq "">
			<cfset resSendSms=SendSMS(operator,phoneNumber,contentSms,"",uuid,delay,withWebService)><!--- appelle à une methode pour envoyer les SMS --->
			<cfreturn resSendSms>
		</cfif>
	</cffunction>

	<!---------------------------------------------------------------------------------------------------------------------------------------->

	<!---------------------------------------------------------------------------------------------------------------------------------------->

	<!--- permet d’envoyer plusieurs message a plusieurs numeros téléphones --->

	<cffunction  name="realSendMultipleMessageToMultipleNumber" access="package" hint="I send yours SMS to yours phones et return FALSE ou TRUE" output="true" returntype="Any">

		<cfargument name="tabSMSNumber" required="false" type="any"  hint="Object's array of (phone number + contents SMS)">
		<cfargument name="operator"    required="false" type="any"  hint="Operator">
		<cfargument name="uuid"        required="false" type="any"  hint="UUID compaign">
		<cfargument name="withWebService"     required="false" type="numeric">

		<!--- créer un objet de type exception --->
		<cfset errorException=createobject("component","fr.consotel.api.sms.model.ApiException")>

		<!--- créer un objet de type Utility --->
		<cfset valideData=createobject("component","fr.consotel.api.sms.model.Utility")>
		<cfset resSendSms="">
		<cfset exception="">

		<!--- tester s'il ne manque pas des paramètres  --->
	    <cfif not (isDefined("tabSMSNumber") and isDefined("operator") and isDefined("uuid"))>
			<cfset exception=errorException.throwException("apiSMS_01",ARGUMENTS)>
	    </cfif>

	    <!--- tester si uuid est valide --->
    	<cfif isDefined("uuid")>
			<cfif IsInstanceof(uuid,'java.lang.String')>
				<cfset isValideUUID=valideData.isUUID(uuid)>
				<cfif isValideUUID neq true>
					<cfset exception=errorException.throwException("apiSMS_07",uuid)>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : uuid, doit etre de type String")>
			</cfif>
		</cfif>

		<!--- tester si l'operator est valide --->
		<cfif  isDefined("operator") >
			<cfif IsInstanceof(operator,'java.lang.String')>
				<cfset isValideOperator=valideData.isOperator(operator)>
				<cfif isValideOperator neq true>
					<cfset exception=errorException.throwException("apiSMS_05",operator)>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : operator, doit etre de type String")>
			</cfif>
		</cfif>

	    <!--- tester si le contenu de SMS est valide --->
		<cfif  isDefined("tabSMSNumber") >
			<cfif isArray(tabSMSNumber)>
				<cfset isValideSMSNumber=valideData.isOkTabSMSNumber(tabSMSNumber,operator)>
				<cfif isArray(isValideSMSNumber)>
					<cfif isValideSMSNumber[2] eq 1>
						<cfset exception=errorException.throwException("apiSMS_03",isValideSMSNumber[1] )>
					<cfelse>
						<cfset exception=errorException.throwException("apiSMS_04",isValideSMSNumber[1])>
					</cfif>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : tabSMSNumber, doit etre de type Array")>
			</cfif>
		</cfif>

		<!---  pas d'exeception on peut envoyer les SMS --->
		<cfif exception eq "">
			<cfset resSendSms=SendSMS(operator,"","",tabSMSNumber,uuid,"",withWebService)><!--- appelle à une methode pour envoyer les SMS --->
			<cfreturn resSendSms>
		</cfif>

	</cffunction>

	<!---------------------------------------------------------------------------------------------------------------------------------------->

	<!--- function qui permet d'envoyer le SMS --->
	<cffunction name="SendSMS" access="private" output="true" returntype="any">

		<cfargument name="operatorName"  required="true" type="any">
		<cfargument name="phoneNumber"   required="true" type="any">
		<cfargument name="contentSms"    required="true" type="any">
		<cfargument name="tabSMSNumber"  required="true" type="any">
		<cfargument name="uuid"   	required="true"  type="any">
		<cfargument name="delay" 	required="false" type="any">
		<cfargument name="withWebService"  required="false" type="numeric">

		<cfif Not isdefined("delay")>
			<cfset delay="">
		</cfif>

		<cfif Not isdefined("withWebService")>
			<cfset withWebService=0>
		</cfif>

		<cfset operatorName=LCase(operatorName)><!--- converti en lower case --->
		<cfset operatorName=UCase(left(operatorName,1))& Right(operatorName,Len(operatorName)-1)><!--- converti le 1ère lettre en MAJ --->
		<cfset newOperator=createObject("component","fr.consotel.api.sms.model.operator.Operator") >
		<cfset resSendSms=newOperator.Init(#operatorName#,phoneNumber,contentSms,tabSMSNumber,uuid,delay,withWebService)>

		<cfreturn resSendSms >

	</cffunction>

	<!---------------------------------------------------------------------------------------------------------------------------------------->

	<cffunction  name="stateByNumber" access="public" output="true" returntype="any" >

		<cfargument name="phoneNumber" required="false" type="any"  hint="Phone number">
		<cfargument name="uuid"        required="false" type="any"  hint="UUID compaign">

		<!--- créer un objet de type Utility --->
		<cfset valideData=createobject("component","fr.consotel.api.sms.model.Utility")>

		<!--- créer un objet de type exception --->
		<cfset errorException=createobject("component","fr.consotel.api.sms.model.ApiException")>

		<!--- créer un objet de type statistique --->
		<cfset statistique=createobject("component","fr.consotel.api.sms.model.Statistique")>

		<cfset exception="">

		<!--- tester s'il ne manque pas des paramètres  --->
	    <cfif not (isDefined("phoneNumber") and isDefined("uuid"))>
			<cfset exception=errorException.throwException("apiSMS_01",ARGUMENTS)>
	    </cfif>

	    <!--- si phoneNumber est définie  --->
	    <cfif  isDefined("phoneNumber") >
	    	<cfif IsInstanceof(phoneNumber,'java.lang.String')><!--- tester le type de paramètre --->
				<cfset isValidePhoneNumber=valideData.isOkPhoneNumber(phoneNumber)>
					<cfif isValidePhoneNumber neq true>
						 <cfset exception=errorException.throwException("apiSMS_03",phoneNumber)>
					</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : phoneNumber, doit etre de type String ")>
			</cfif>
		</cfif>
		<!--- tester si uuid est valide --->
    	<cfif isDefined("uuid")>
			<cfif IsInstanceof(uuid,'java.lang.String')>
				<cfset isValideUUID=valideData.isUUID(uuid)>
				<cfif isValideUUID neq true>
					<cfset exception=errorException.throwException("apiSMS_07",uuid)>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : uuid, doit etre de type String")>
			</cfif>
		</cfif>

		<!---  pas d'exeception --->
		<cfif exception eq "">
			<cfset dataStruct={uuid=uuid,phoneNumber=phoneNumber}>
			<cfset resStateByNumber=statistique.stateByNumber(dataStruct)>
			<cfreturn resStateByNumber>

		</cfif>

	</cffunction>

	<!---------------------------------------------------------------------------------------------------------------------------------------->

	<cffunction  name="stateByStatut" access="public" output="true" returntype="any" >

		<cfargument name="libelle"     required="false" type="any"  hint="Phone number">
		<cfargument name="uuid"        required="false" type="any"  hint="UUID compaign">

		<!--- créer un objet de type Utility --->
		<cfset valideData=createobject("component","fr.consotel.api.sms.model.Utility")>

		<!--- créer un objet de type exception --->
		<cfset errorException=createobject("component","fr.consotel.api.sms.model.ApiException")>

		<!--- créer un objet de type statistique --->
		<cfset statistique=createobject("component","fr.consotel.api.sms.model.Statistique")>

		<cfset exception="">

		<!--- tester s'il ne manque pas des paramètres  --->
	    <cfif not (isDefined("uuid") and isDefined("libelle"))>
			<cfset exception=errorException.throwException("apiSMS_01",ARGUMENTS)>
	    </cfif>

		<!--- tester si uuid est valide --->
    	<cfif isDefined("uuid")>
			<cfif IsInstanceof(uuid,'java.lang.String')>
				<cfset isValideUUID=valideData.isUUID(uuid)>
				<cfif isValideUUID neq true>
					<cfset exception=errorException.throwException("apiSMS_07",uuid)>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : uuid, doit etre de type String")>
			</cfif>
		</cfif>

		<!---  pas d'exeception --->
		<cfif exception eq "">
			<cfset dataStruct={uuid=uuid,libelle=libelle}>
			<cfset resStateByStatut=statistique.stateByStatut(dataStruct)>
			<cfreturn resStateByStatut>

		</cfif>

	</cffunction>

	<!---------------------------------------------------------------------------------------------------------------------------------------->

	<cffunction  name="stateByUuid" access="public"  output="true" returntype="any" >

		<cfargument name="uuid"    required="false" type="any"  hint="UUID compaign">

		<!--- créer un objet de type Utility --->
		<cfset valideData=createobject("component","fr.consotel.api.sms.model.Utility")>

		<!--- créer un objet de type exception --->
		<cfset errorException=createobject("component","fr.consotel.api.sms.model.ApiException")>

		<!--- créer un objet de type statistique --->
		<cfset statistique=createobject("component","fr.consotel.api.sms.model.Statistique")>

		<cfset exception="">

		<!--- tester s'il ne manque pas des paramètres  --->
	    <cfif not (isDefined("uuid"))>
			<cfset exception=errorException.throwException("apiSMS_01",ARGUMENTS)>
	    </cfif>

		<!--- tester si uuid est valide --->
    	<cfif isDefined("uuid")>
			<cfif IsInstanceof(uuid,'java.lang.String')>
				<cfset isValideUUID=valideData.isUUID(uuid)>
				<cfif isValideUUID neq true>
					<cfset exception=errorException.throwException("apiSMS_07",uuid)>
				</cfif>
			<cfelse>
				<cfset exception=errorException.throwException("apiSMS_02", "Parametre : uuid, doit etre de type String")>
			</cfif>
		</cfif>

		<!---  pas d'exeception --->
		<cfif exception eq "">
			<cfset resStateByUuid=statistique.stateByUuid(uuid)>
			<cfreturn resStateByUuid>

		</cfif>

	</cffunction>

	<!---------------------------------------------------------------------------------------------------------------------------------------->

</cfcomponent>