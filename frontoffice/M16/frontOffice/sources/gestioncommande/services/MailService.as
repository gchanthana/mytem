package gestioncommande.services
{
	import composants.mail.MailVO;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.registerClassAlias;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.events.CommandeEvent;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public class MailService extends EventDispatcher
	{
				
		public function MailService()
		{
		}
		
		public function sendInfosMail(myAction:Action, myInfosMail:MailVO, idsCommande:Array, myCommande:Commande, isSendMail:int, uuid:String = ''):void
		{
			registerClassAlias('fr.consotel.consoview.util.Mail', composants.mail.MailVO);
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				'fr.consotel.consoview.M16.v2.commande_sncf',
																				'envoyer',
																				sendInfosMailResultHandler);
			
			RemoteObjectUtil.callService(op,myInfosMail,
											idsCommande,
											myCommande.IDTYPE_COMMANDE,
											myAction,
											isSendMail,
											myCommande.IDOPERATEUR,
											uuid,
											myCommande.NUMERO_COMMANDE);
		}
		
		private function sendInfosMailResultHandler(re:ResultEvent):void
		{	
			if(re.result > 0)
				dispatchEvent(new CommandeEvent(CommandeEvent.MAIL_SEND));	
		}
		
	}
}