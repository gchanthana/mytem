package gestioncommande.services
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.events.CommandeEvent;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import session.SessionUserObject;
	
	public class ModeleService extends EventDispatcher
	{
		
		//------------------------------------------------------------------------------------------------------------------------------//
		//					VARIABLES GLOBALES
		//------------------------------------------------------------------------------------------------------------------------------//
		
		public var modele		:ArrayCollection;
		public var listeModeles	:ArrayCollection;
	    public var configObject	:Object;
		public var listeModele	:XML;

		//------------------------------------------------------------------------------------------------------------------------------//
		//					METHODES PUBLIC - CONSTRUCTEUR
		//------------------------------------------------------------------------------------------------------------------------------//
		
		public function ModeleService()
		{
		}

		//------------------------------------------------------------------------------------------------------------------------------//
		//					METHODES PUBLIC - APPELS DE PROCEDURES
		//------------------------------------------------------------------------------------------------------------------------------//
		
		public function sauvegardeModele(cmd:Object, articles:XML, addAllPool:Boolean = false):void
	    {
			var idFixe:int = 0;
			var idMobi:int = 0;
			var idData:int = 0;
			var idPool:int = cmd.IDPOOL_GESTIONNAIRE;
	    	
			if(addAllPool)
				idPool = 0;

			switch(SessionUserObject.singletonSession.IDSEGMENT)//--- 1-> MOBILE, 2-> FIXE, 3-> DATA
			{
				case 1: idFixe = 0;
						idMobi = 1;
						idData = 0;
						break;
				case 2: idFixe = 1;
						idMobi = 0;
						idData = 0;
						break;
				case 3: idFixe = 0;
						idMobi = 0;
						idData = 1;
						break;
			}
	    	
	    	modele = new ArrayCollection();
	    	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M16.v2.ModeleService",
	    																		"sauvegardeModele",
	    																		sauvegardeModeleResultHandler);
			
    		RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,
								    		cmd.IDGESTIONNAIRE,
								    		cmd.IDPROFIL_EQUIPEMENT,
											idPool,
								    		cmd.IDOPERATEUR,
								    		cmd.IDREVENDEUR,
								    		cmd.IDCONTACT,
    										cmd.IDSITELIVRAISON,
    										cmd.LIBELLE_CONFIGURATION,
    										cmd.REF_CLIENT1,
    										cmd.REF_CLIENT2,
    										cmd.REF_OPERATEUR,
    										cmd.COMMENTAIRES,
    										cmd.MONTANT,
    										idFixe,
    										idMobi,
    										idData,
    										cmd.IDTYPE_COMMANDE,
    										cmd.IDGROUPE_REPERE,
    										articles);	
	    }
	    
	    public function majModele(idModele:int, libelle:String, idpool:int, articles:XML):void
	    {
	    	modele = new ArrayCollection();
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M16.v2.ModeleService",
	    																		"majModele",
	    																		majModeleResultHandler);
    		RemoteObjectUtil.callService(op,idModele,
    										libelle,
    										idpool,
    										CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,
								    		CvAccessManager.getSession().USER.CLIENTACCESSID,
								    		articles);	
	    }
	    
	    public function fournirListeModele(idPool:int,idProfilEquipement:int = 0): void
	    {
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M16.v2.ModeleService",
	    																		"fournirListeModele",
	    																		fournirListeModeleResultHandler);
	    	RemoteObjectUtil.callService(op,idPool,
	    									idProfilEquipement,
	    									SessionUserObject.singletonSession.IDSEGMENT);	
	    }
	    
		public function getModeleCommande(idModele:Number): void
	    {
	    	listeModele = new XML();
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
    																			"fr.consotel.consoview.M16.v2.ModeleService",
    																			"getModeleCommande",
    																			getModeleCommandeResultHandler);
	  		RemoteObjectUtil.callService(op, idModele);	
	    }
	    
	    //supprime un modèle enregisstré
		public function deleteModeleCommande(idModele:Number): void
	    {
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.inventaire.commande.GestionCommande",//---A CHANGER
	    																		"deleteModeleCommande",
	    																		deleteModeleCommandeResultHandler);		
	    	RemoteObjectUtil.callService(op, idModele);	
	    }

		//------------------------------------------------------------------------------------------------------------------------------//
		//					METHODES PRIVATE - RESULTHANDLER
		//------------------------------------------------------------------------------------------------------------------------------//
		
	    private function sauvegardeModeleResultHandler(re:ResultEvent):void
	    {
	    	if(re.result != null)
			{
		    	modele = re.result as ArrayCollection;
				dispatchEvent(new CommandeEvent(CommandeEvent.MODELE_CREATED));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Enregistrement_du_mod_le_de_configuratio'), 'Consoview');
	    }
	    
	    private function majModeleResultHandler(re:ResultEvent):void
	    {
	    	if(re.result != null)
			{
		    	modele = re.result as ArrayCollection;
				dispatchEvent(new CommandeEvent(CommandeEvent.MODELE_UPDATED));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Enregistrement_du_mod_le_de_configuratio'), 'Consoview');
	    }
	    
	    private function fournirListeModeleResultHandler(re:ResultEvent):void
	    {
	    	if(re.result != null)
			{
				var listMod	:ArrayCollection = Formator.sortFunction(re.result as ArrayCollection, "LIBELLE");
				var lenList	:int = listMod.length;
				
				listeModeles = new ArrayCollection();
				
				for(var i:int = 0;i < lenList;i++)
				{
					if(!listeModeles.contains(listMod[i]))
						listeModeles.addItem(listMod[i]);
				}
				
				dispatchEvent(new CommandeEvent(CommandeEvent.MODELE_LISTED));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'R_cup_ration_des_mod_les_de_configuratio'), 'Consoview');
	    }
	    
	    private function getModeleCommandeResultHandler(re:ResultEvent): void
	    {
	    	if(re.result)
	    	{
	    		configObject 	= re.result[0] as Object;
	    		listeModele 	= XML(re.result[0].XML);
	    		dispatchEvent(new CommandeEvent(CommandeEvent.MODELE_ADD));
	    	}
	    	else
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es_'), 'Consoview');
	    }
	    
	    private function deleteModeleCommandeResultHandler(re:ResultEvent): void
	    {
	    	if(re.result > 0)
	    		dispatchEvent(new CommandeEvent(CommandeEvent.MODELE_ERASED));
	    	else
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Effecement_impossible_'), 'Consoview');
	    }	

	}
}