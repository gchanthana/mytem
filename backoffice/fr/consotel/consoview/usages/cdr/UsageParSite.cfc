<!--- =========================================================================
Classe: UsageParSite
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="UsageParSite" hint="" >
<!--- METHODS --->
	<cffunction name="setStrategy" access="public" returntype="void" output="false" >
		<cfargument name="TYPE_PERIMETRE" required="false" type="string" default="" displayname="string TYPE_PERIMETRE" hint="type de périmetre" />
		<cfset temp=createObject("component","com.consoview.usages.site.#TYPE_PERIMETRE#SiteUsageStrategy")>
		<cfset setSiteUsageStrategy(#temp#)>
	</cffunction>
	
	<cffunction name="getSites" access="public" returntype="query" output="false"  >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfreturn variables.instance.SiteUsageStrategy.getSites(ID)>
	</cffunction>
	
	<cffunction name="dirquery2treequery" access="public" returntype="query" output="false"  >
		<cfargument name="dquery" required="false" type="query" default="" displayname="query dquery" hint="Initial value for the dqueryproperty." />
		<cfreturn variables.instance.SiteUsageStrategy.dirquery2treequery(dquery)>
	</cffunction>
	
<!--- SETTERS --->
	<cffunction name="setSiteUsageStrategy" access="public" output="false" returntype="void" hint="Affecter une valeur a la propriete: SiteUsageStrategy">
		<cfargument name="newSiteUsageStrategy" type="SiteUsageStrategy" required="true" />
		<cfset variables.instance.SiteUsageStrategy = arguments.newSiteUsageStrategy />
	</cffunction>
</cfcomponent>