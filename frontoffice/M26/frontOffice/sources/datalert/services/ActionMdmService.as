package datalert.services
{
	import composants.util.ConsoviewAlert;
	
	import datalert.event.ActionMdmEvent;
	import datalert.vo.LigneMdmInfos;
	import datalert.vo.LigneVO;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.formatters.DateFormatter;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	public class ActionMdmService extends EventDispatcher
	{
		private const DATA_ENABLE:String = "ENABLE";
		private const DATA_DISABLE:String = "DISABLE";
		[Bindable]
		public var bool_insync:Boolean = false;
		
		public function ActionMdmService(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		public function enableDataTraffic(values:ArrayCollection):void
		{
			var opData : AbstractOperation = 	RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M26.services.mdm.DataTrafficManager",
				"enableDataTraffic",
				mangeDataTrafficHandler,null);
			
			RemoteObjectUtil.callService(opData,
				values,DATA_ENABLE);
		}
		
		
		
		public function disableDataTraffic(values:ArrayCollection):void
		{
			var opData : AbstractOperation = 	RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M26.services.mdm.DataTrafficManager",
				"disableDataTraffic",
				mangeDataTrafficHandler,null);
			
			RemoteObjectUtil.callService(opData,
				values,DATA_DISABLE);
		}
		 
		private function mangeDataTrafficHandler(r_event:ResultEvent):void
		{
			var df:DateFormatter = new DateFormatter();
			
			df.formatString= ResourceManager.getInstance().getString('M26', 'DD_MM_YYYY_JJ_NN_SS');
			
			var lignes:ArrayCollection = (r_event.token.message.body[0] as ArrayCollection);
			
			var date_action:String = df.format(new Date());
			
			var messgae:String = (r_event.token.message.body[1] == DATA_ENABLE)? ResourceManager.getInstance().getString('M26', '_Demande_de_d_blocage_du_trafic_data') 
									: ResourceManager.getInstance().getString('M26', '_Demande_de_blocage_du_trafic_data');
			
			for each(var ligne:*   in lignes)
			{
				(ligne as LigneVO).LAST_ACTION += date_action + messgae + '\n';
			}
			
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M26', 'Demeande_envoy_'));
		}
		
		public function syncServer():void
		{
			var ro:RemoteObject = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			ro.source = 'fr.consotel.consoview.M26.services.mdm.ServerManger';
			ro.addEventListener(ResultEvent.RESULT,function synchServerResultHandler():void
			{
				
				dispatchEvent(new ActionMdmEvent(ActionMdmEvent.SYNC_OK));
				bool_insync = false;
			});
			ro.addEventListener(FaultEvent.FAULT,function synchServerFaultHandler(fe:FaultEvent):void
			{
				if (fe.fault.faultString.indexOf("No devices found") > -1)
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M26', 'Aucun_terminal_n_a__t__trouv__sur_le_serv'),'Error');
				else
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M26', 'Veuillez_verifier_mdm'),'Error');
				bool_insync = false;
				
			});
			ro.syncServer();
			bool_insync = true;
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M26', 'Demeande_envoy_'));
		}
		
		public function throwHipChatPost(s:String):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M26.services.mdm.ServerManger",
				"throwHipChatPost",
				throwHipChatPost_handler,null);
			
			RemoteObjectUtil.callService(opData, s);
		}
		
		public function throwHipChatPost_handler(re:ResultEvent):void
		{
			
		}
		
		public function loadMdmInfo(infos:LigneMdmInfos):void
		{	
			
		}
		
	}

}