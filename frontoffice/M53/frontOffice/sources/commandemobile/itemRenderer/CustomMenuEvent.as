package commandemobile.itemRenderer
{
	import commandemobile.system.Action;
	
	import flash.events.Event;

	public class CustomMenuEvent extends Event
	{
		public static const MENU_ITEM_CLICK:String = "menuItemClick";
		private var _codeItemMenu:String;
		private var _actionItemMenu:Action;
		
		public function CustomMenuEvent(type:String, bubbles:Boolean=false, code:String ="",action:Action = null) {
			super(type, bubbles);
			this._codeItemMenu 		= code;
			this._actionItemMenu 	= action;
		}
		
		public function get codeItemMenu():String {
			return _codeItemMenu;
		}
		
		public function get actionItemMenu():Action {
			return _actionItemMenu;
		}
		
 		override public function clone():Event {
			return new CustomMenuEvent(type, bubbles, _codeItemMenu,_actionItemMenu);
		}
	
	}
}