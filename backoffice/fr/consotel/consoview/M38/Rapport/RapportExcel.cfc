<cfcomponent output="false">	
	<cfset VARIABLES["reportServiceClass"]="fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService">
	<cfset VARIABLES["containerServiceClass"]="fr.consotel.consoview.api.container.ApiContainerV1">
<!--- ***************************** CODE A MODIFIER ******************************* --->		
	<!--- Le chemin du rapport sous BIP --->
	<cfset VARIABLES["XDO"]="/consoview/M37/getData/getData.xdo">
	<!--- Le code rapport --->
	<cfset VARIABLES["CODE_RAPPORT"]="CARLO">
	<!--- TEMPLATE BI --->
	<cfset VARIABLES["TEMPLATE"]="excel">
	<!--- OUTPUT --->
	<cfset VARIABLES["OUTPUT"]="Export_analyse_">
	<!--- FORMAT --->
	<cfset VARIABLES["FORMAT"]="excel">
	<!--- OUTPUT --->
	<cfset VARIABLES["EXTENSION"]="xls">
	<!--- MODULE --->
	<cfset VARIABLES["MODULE"]="CarloPilotage">
	<!--- CODE_LANGUE --->
	<cfset VARIABLES["CODE_LANGUE"]="fr_FR">
	<!--- APPLI --->
	<cfset VARIABLES["APPLI"]="Pilotage">
<!--- ************************************************************ --->
	<cfset reportService=createObject("component",VARIABLES["reportServiceClass"])>
	<cfset ApiE0 = CreateObject("component","fr.consotel.consoview.api.E0.ApiE0")>
	<cfset containerService	= createObject("component",VARIABLES["containerServiceClass"])>
	
	<cffunction name="scheduleReport" access="remote" returnType="numeric">
		<cfargument name="PARAM_ARRAY" type="array" required="true">

		<cflog text="************ RAPPORT : #VARIABLES['OUTPUT']#">
	
<!--- ***************************** CODE A MODIFIER ******************************* --->		
<!--- LES PARAMETRES FOURNIT PAR L'IHM --->
	<!--- DEPEND DU RAPPORT (CF DOC) --->
		<!--- a multiplier par le nombre de paramètre nécessaire --->
		<cfset G_IDORGA = PARAM_ARRAY[1]>
		<cfset G_IDPERIMETRE = PARAM_ARRAY[2]>
		<cfset G_NIVEAU = PARAM_ARRAY[3]>
		<cfset G_IDMOISDEB = PARAM_ARRAY[4]>
		<cfset G_IDMOISFIN = PARAM_ARRAY[5]>
		<cfset G_NIVEAU_AGGREG = PARAM_ARRAY[6]>

<!--- ************************************************************ --->		
		<cfset initStatus=reportService.init(SESSION.PERIMETRE["ID_GROUPE"],VARIABLES["XDO"],VARIABLES["APPLI"],VARIABLES["MODULE"],VARIABLES["CODE_RAPPORT"])>
		<cfif initStatus EQ TRUE>
			<cfset setStatus=TRUE>
<!--- ***************************** CODE A MODIFIER ******************************* --->					
<!--- DEFINITION DES PARAMETRES DU RAPPORT --->	
		<!--- IDRACINE_MASTER --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=SESSION.PERIMETRE["IDRACINE_MASTER"]>
			<cfset setStatus=setStatus AND reportService.setParameter("P_IDRACINE_MASTER",tmpParamValues)>
		<!--- IDRACINE --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=SESSION.PERIMETRE["ID_GROUPE"]>
			<cfset setStatus=setStatus AND reportService.setParameter("P_IDRACINE",tmpParamValues)>
		<!--- IDORGA --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=G_IDORGA>
			<cfset setStatus=setStatus AND reportService.setParameter("P_IDORGA",tmpParamValues)>
		<!--- IDPERIMETRE --->			
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=G_IDPERIMETRE>
			<cfset setStatus=setStatus AND reportService.setParameter("P_IDPERIMETRE",tmpParamValues)>
		<!--- NIVEAU --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=G_NIVEAU>
			<cfset setStatus=setStatus AND reportService.setParameter("P_NIVEAU",tmpParamValues)>
		<!--- LANGUE PAYS --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=VARIABLES["CODE_LANGUE"]>
			<cfset setStatus=setStatus AND reportService.setParameter("P_LANGUE_PAYS",tmpParamValues)>
		<!--- IDPERIODEMOIS DEB --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=G_IDMOISDEB>
			<cfset setStatus=setStatus AND reportService.setParameter("P_IDPERIODE_MOIS_DEBUT",tmpParamValues)>
		<!--- IDPERIODEMOIS FIN --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=G_IDMOISFIN>
			<cfset setStatus=setStatus AND reportService.setParameter("P_IDPERIODE_MOIS_FIN",tmpParamValues)>
		<!--- NIVEAU AGGREG --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=G_NIVEAU_AGGREG>
			<cfset setStatus=setStatus AND reportService.setParameter("P_NIVEAU_AGREG",tmpParamValues)>
<!--- ************************************************************ --->
			
			<cfif setStatus EQ TRUE>
				<cfset VARIABLES["OUTPUT"] = VARIABLES["OUTPUT"]
						 & createObject("component","fr.consotel.consoview.M38.CarolService").getLibelleGroupeClient(G_IDPERIMETRE)>				
						 
				<cfset outputStatus=reportService.setOutput(VARIABLES["TEMPLATE"],VARIABLES["FORMAT"],VARIABLES["EXTENSION"],VARIABLES["OUTPUT"],VARIABLES["CODE_RAPPORT"])>
				<cfif outputStatus EQ TRUE>
					<cfset reportStatus = reportService.runTask(SESSION.USER["CLIENTACCESSID"])>
					<cflog text="************ reportStatus.JOBID = #reportStatus['JOBID']#">
					<cflog text="************ reportStatus.UUID  = #reportStatus['UUID']#">
					<cfif reportStatus['JOBID'] neq -1>
						<cfreturn reportStatus["JOBID"]>
					</cfif>
				</cfif>
			</cfif>
		</cfif>
		<cfreturn -1>
	</cffunction>
</cfcomponent>
