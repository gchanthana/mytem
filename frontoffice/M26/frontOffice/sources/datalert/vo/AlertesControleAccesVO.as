package datalert.vo
{
	public class AlertesControleAccesVO
	{
		public var is_actif:Number = 0;
		public var delai:Number = 0;
		public var idmessage_user:Number = 0;
		public var send_message_gest:Number = 0;
		public var repeat:Number = 0;
		
		public function AlertesControleAccesVO()
		{
		}
	}
}