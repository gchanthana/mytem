package gestionparc.ihm.action.ligne.actionFournirSimStock
{
	import flash.display.DisplayObject;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.ligne.FicheFournirSimStockIHM;

	public class ActionFournirSIMStokReceiver
	{
		private var srcObject:Object;
		private var fiche:FicheFournirSimStockIHM;
		
		public function ActionFournirSIMStokReceiver()
		{
		}
		
		public function action():void
		{
			setSources();
			displayIHM(srcObject);
		}
		private function displayIHM(srcObject:Object):void
		{
			fiche = new FicheFournirSimStockIHM();
			fiche.currentObjectParc = srcObject;
			PopUpManager.addPopUp(fiche,Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
		
		/**
		 * Cette fonction renseigne les sources pour réaliser l'action.
		 */
		private function setSources():void
		{
			srcObject=new Object();
			srcObject.ELEMENT_SELECTED=SpecificationVO.getInstance().elementDataGridSelected;
			srcObject.IDPOOL = SpecificationVO.getInstance().idPool;
			srcObject.IDPROFIL = SpecificationVO.getInstance().idProfil;
			srcObject.IDREVENDEUR = SpecificationVO.getInstance().idRevendeur;
			
			srcObject.LIBELLE_PROFIL = SpecificationVO.getInstance().labelProfil;
			srcObject.LABEL_POOL = SpecificationVO.getInstance().elementDataGridSelected.POOL;
			
			if(srcObject.LABEL_POOL ==  undefined)
				srcObject.LABEL_POOL = SpecificationVO.getInstance().labelPool;
			
		}
	}
}