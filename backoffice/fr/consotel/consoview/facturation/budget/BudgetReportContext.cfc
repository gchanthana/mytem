<cfcomponent name="BudgetReportContext" alias="fr.consotel.consoview.facturation.budget.BudgetReportContext">
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="PERIMETRE_INDEX" required="true" type="numeric"/>
		<cfargument name="TYPE_PERIMETRE" required="true" type="string"/>
		<cfargument name="REPORT_TYPE" required="true" type="string"/>
		<cfargument name="RAISON_SOCIALE" required="true" type="string"/>
		<cfset idPerimetre = PERIMETRE_INDEX>
		<cfif Not StructIsEmpty(FORM)>
			<cfset rapportParams = structNew()>
			<cfloop collection="#FORM#" item="i">
				<cfif i neq "FIELDNAMES" AND i neq "CHARTAREA">
					<cfset structInsert(rapportParams,"#i#","#FORM[i]#")>
				</cfif>
			</cfloop>
		</cfif>
		<cfif UCASE(TYPE_PERIMETRE) eq "GROUPELIGNE">
			<cfset reportProcessor =
					createObject("component",
						"fr.consotel.consoview.facturation.budget.#REPORT_TYPE#.Rapport#REPORT_TYPE#GroupeLigneStrategy")>
			<cfelse>
			<cfset reportProcessor =
					createObject("component",
						"fr.consotel.consoview.facturation.budget.#REPORT_TYPE#.Rapport#REPORT_TYPE#GroupeStrategy")>
		</cfif>
		<cfset reportProcessor.displayRapport(idPerimetre,rapportParams)>
	</cffunction>
	
	<cffunction name="getBudgetList" access="remote" returntype="query">
			<cfargument name="groupeId" required="true" type="numeric"/>
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_V3.get_liste_exercices">
				<cfprocparam value="#groupeId#" cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qGetBudget">
			</cfstoredproc>
			<cfreturn qGetBudget>
	</cffunction>
</cfcomponent>
