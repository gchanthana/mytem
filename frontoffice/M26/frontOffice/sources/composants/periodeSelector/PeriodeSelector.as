package composants.periodeSelector
{

	//import composants.periodeSelector.AMonth;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	
	import mx.controls.sliderClasses.Slider;
	import mx.events.FlexEvent;
	import mx.events.SliderEvent;
	import mx.formatters.DateFormatter;
	import mx.resources.ResourceManager;
	
	[Event(name="periodeChange", type="composants.periodeSelector.PeriodeEvent")]
	public class PeriodeSelector extends PeriodeSelector_IHM
	{
		private static const millisecondsPerMinute:int = 1000 * 60;
		private static const millisecondsPerHour:int = 1000 * 60 * 60;
		private static const millisecondsPerDay:int = 1000 * 60 * 60 * 24;
		
		[Bindable] public var dateDebut:Date;
		[Bindable] public var dateFin:Date;
		
		public function PeriodeSelector()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		public function onPerimetreChange():void
		{
			getPeriode();
		}
		
		public function getSelectorItemCount():int
		{
			return slider.thumbCount;
		}
		
		/**
		 *
		 * Fonction qui permet d effacer les donnees du composant
		 *
		 *
		 * */
		public function clean():void
		{
			monthData = null;
		}
		
		/**
		 * Fonction qui retourne un tableau avec les dates de la periode (les 31 jours sur lesquels peut glisser le periodeSelector)
		 * 
		 * L'initialisation de monthData se fait dans getPeriode()
		 * */
		public function getTabPeriode():Array
		{
			return monthData;
		}
		
		/**
		 *
		 * Fonction qui permet affecter un traitement au CHANGE_EVENT sur le SLIDER
		 *
		 * @param  methode : Function la fonction qui effectue le traitement
		 *
		 *
		 * */
		public function affectChangeHandler(methode:Function):void
		{
			slider.addEventListener(SliderEvent.CHANGE, methode);
		}
		
		/**
		 *
		 * Fonction qui permet de fixer la periode initiale du slider
		 *
		 * */
		public function setMini(nbMois:Number = 1):void
		{
			slider.values = [ monthData.length - (1 + nbMois), monthData.length - 1 ];
		}
		/*------------------------------------ PRIVATE ----------------------------------------------*/
		private var myDateFormatter:DateFormatter = new DateFormatter();
		[Bindable]
		private var jourDebut:String = "";
		[Bindable]
		private var jourFin:String = "";

		
		//tableau contenant les jours sur lesquels il est possible de glisser.
		[Bindable]
		private var monthData:Array;
		
		// formate les tooltips du slider	
		private function getSliderLabel(value:Number):String
		{
			var myDateRes:String = "";
			if (Math.abs(slider.getThumbAt(0).xPosition - slider.mouseX) > Math.abs(slider.getThumbAt(1).xPosition - slider.mouseX))
			{
				myDateFormatter.formatString = "DD/MM/YYYY";//Ne pas modifier avec l'internationalisation, ce n'est qu'une date de transit servant à la construction de la date internationale
				myDateRes = myDateFormatter.format(monthData[value - 1] as Date);
				return setGoodDateLocale(myDateRes);
			}
			else if (Math.abs(slider.getThumbAt(1).xPosition - slider.mouseX) >= Math.abs(slider.getThumbAt(0).xPosition - slider.mouseX))
			{
				myDateFormatter.formatString = "DD/MM/YYYY";//Ne pas modifier avec l'internationalisation, ce n'est qu'une date de transit servant à la construction de la date internationale
				myDateRes = myDateFormatter.format(monthData[value] as Date);
				return setGoodDateLocale(myDateRes);
			}
			return null;
		}
		
		/**
		 * getPeriode : jour par jour
		 */
		
		
		private function getPeriode():void
		{
			var tabPeriode:Array = new Array();//Contient désormais des jours uniques
			var lastDate:Date = new Date();
			tabPeriode[0] = lastDate;
			var addedDate:Date = lastDate;
			var i:int = 1;
			while (i < 31)//remplissage du tableau sur les 31 jours
			{
				addedDate = new Date(addedDate.getFullYear(), addedDate.getMonth(), addedDate.getDate() - 1);
				tabPeriode.unshift(addedDate);
				i++;
			}

			monthData = tabPeriode;
			slider.maximum = monthData.length;
			slider.values = [ monthData.length - 1, monthData.length];
		}
		
		private function periodeChange(e:SliderEvent):void
		{
			jourDebut = getTabPeriode()[e.currentTarget.values[0]];
			jourFin = getTabPeriode()[e.currentTarget.values[1] - 1];
			dateDebut = getTabPeriode()[e.currentTarget.values[0]];
			dateFin = getTabPeriode()[e.currentTarget.values[1] - 1];
			
			
			var periodeEvent:PeriodeEvent = new PeriodeEvent("periodeChange");
			periodeEvent.jourDeb = jourDebut;
			periodeEvent.jourFin = jourFin;
			periodeEvent.dateDeb = dateDebut;
			periodeEvent.dateFin = dateFin;
			dispatchEvent(periodeEvent);
		}
			
		private function periodeIsChanging(se:SliderEvent):void // à ne pas changer
		{
			if (slider.getThumbAt(1) != null)
			{
				if (se.thumbIndex == 1)
				{
					if (slider.getThumbAt(0).hitTestObject(slider.getThumbAt(1)))
					{
						slider.setThumbValueAt(1, slider.values[0] + 1);
					}
				}
				else if (se.thumbIndex == 0)
				{
					if (slider.getThumbAt(0).hitTestObject(slider.getThumbAt(1)))
					{
						slider.setThumbValueAt(0, slider.values[1] - 1);
					}
				}
			}
		}
		
		//Fonction qui initialise le composant. Charge les donnees (périodes);		  
		private function initIHM(fe:FlexEvent):void
		{
			affectChangeHandler(periodeChange);
			slider.addEventListener(SliderEvent.THUMB_DRAG, periodeIsChanging);
			getPeriode();
			jourDebut = getTabPeriode()[int(getTabPeriode().length) - 1];//Les 4 valeurs sont identiques car au chargement du composant seul le jour actuel est sélectionné.
			jourFin = getTabPeriode()[int(getTabPeriode().length) - 1];
			dateDebut = getTabPeriode()[int(getTabPeriode().length) - 1];
			dateFin = getTabPeriode()[int(getTabPeriode().length) - 1];
			slider.dataTipFormatFunction = getSliderLabel;
			slider.getThumbAt(0).name = "getDateDebut";
			slider.getThumbAt(1).name = "getDateFin";
			slider.getThumbAt(0).setStyle("fillColors", [ "#909587", "#909587" ]);
			slider.getThumbAt(1).setStyle("fillColors", [ "#909587", "#909587" ]);
			var periodeEvent:PeriodeEvent = new PeriodeEvent("periodeChange");
			periodeEvent.jourDeb = jourDebut;
			periodeEvent.jourFin = jourFin;
			periodeEvent.dateDeb = dateDebut;
			periodeEvent.dateFin = dateFin;
			dispatchEvent(periodeEvent);
		}
		
		private function setGoodDateLocale(myDate:String):String
		{
			var jourD : String = myDate.substr(0,2);
			var moisD : String = myDate.substr(3,2);
			var anneeD : String = myDate.substr(6,4);
			
			var tmp : Number = new Number(moisD);
			tmp--;
			
			var myDateTmp : Date = new Date(anneeD,tmp,jourD);
			
			myDateFormatter = new DateFormatter();
			myDateFormatter.formatString = ResourceManager.getInstance().getString('M26','DD_MMMM_YYYY');
			
			var myDateRes : String = myDateFormatter.format(myDateTmp);
			
			switch(moisD)
			{
				case "01" : 
					myDateRes = myDateRes.replace("January",ResourceManager.getInstance().getString('M26','MMMM_January'));
					break;
				case "02" : 
					myDateRes = myDateRes.replace("February",ResourceManager.getInstance().getString('M26','MMMM_February'));
					break;
				case "03" : 
					myDateRes = myDateRes.replace("March",ResourceManager.getInstance().getString('M26','MMMM_March'));
					break;
				case "04" : 
					myDateRes = myDateRes.replace("April",ResourceManager.getInstance().getString('M26','MMMM_April'));
					break;
				case "05" : 
					myDateRes = myDateRes.replace("May",ResourceManager.getInstance().getString('M26','MMMM_May'));
					break;
				case "06" : 
					myDateRes = myDateRes.replace("June",ResourceManager.getInstance().getString('M26','MMMM_June'));
					break;
				case "07" : 
					myDateRes = myDateRes.replace("July",ResourceManager.getInstance().getString('M26','MMMM_July'));
					break;
				case "08" : 
					myDateRes = myDateRes.replace("August",ResourceManager.getInstance().getString('M26','MMMM_August'));
					break;
				case "09" : 
					myDateRes = myDateRes.replace("September",ResourceManager.getInstance().getString('M26','MMMM_September'));
					break;
				case "10" : 
					myDateRes = myDateRes.replace("October",ResourceManager.getInstance().getString('M26','MMMM_October'));
					break;
				case "11" : 
					myDateRes = myDateRes.replace("November",ResourceManager.getInstance().getString('M26','MMMM_November'));
					break;
				case "12" : 
					myDateRes = myDateRes.replace("December",ResourceManager.getInstance().getString('M26','MMMM_December'));
					break;
			}
			
			return myDateRes;
		}
	}
}