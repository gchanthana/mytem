﻿<cfcomponent displayname="Facturation" output="false">
	
	<cffunction name="getDatasFacturation" access="remote" returntype="query" description="retourne les données de facturation selon le perimetre choisi">
		
		<cfset qDatas=queryNew('saw_1,saw_0, saw_2,saw_5,saw_3','varchar,varchar,varchar,varchar,varchar')>
		
		<cftry>
			<cfquery name="qQuery" datasource="BI_TEST" >	
				set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
								p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idorga=#SESSION.PERIMETRE.ID_GROUPE#,
								p_idperimetre=#SESSION.PERIMETRE.ID_GROUPE#,
								p_niveau='B',
								p_idniveau=1,
								p_langue_pays='#SESSION.USER.GLOBALIZATION#';
		
				SELECT ORGAOPERATEUR.OPERATEUR 	saw_0, 
						FACTURATION.NOMBRE_FACTURES saw_1, 
						PERIODE.DESC_MOIS 			saw_2, 
						PERIODE.SHORT_MOIS 			saw_3, 
						PERIODE.DESC_ANNEE 			saw_4, 
						PERIODE.IDPERIODE_MOIS 		saw_5
				FROM E0_AGGREGS_DEFRAG
				WHERE RCOUNT(PERIODE.IDPERIODE_MOIS) <= 4
				ORDER BY saw_5 DESC
			</cfquery>
			
			<cfquery name="qDatas" dbtype="query">
					SELECT saw_1,saw_0, saw_2,saw_5,saw_3 FROM qQuery GROUP BY saw_1,saw_0, saw_2,saw_5,saw_3 ORDER BY saw_5 asc
			</cfquery>
			<cfcatch type="any">
				<cfthrow object="#cfcatch#">
			</cfcatch>
		</cftry>
		<cfreturn qDatas>
	</cffunction>

</cfcomponent>