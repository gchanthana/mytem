<cfswitch expression="#form.TYPE#">				
	<cfcase value="ACF">								 
		<cfcontent type="application/force-download">
		<cfheader name="Content-Disposition" value="attachment;filename=PMU_ACCORD_CADRE_FIXE.pdf">	 
		<cfheader name="Content-Description" value="Accord cadre Fixe">
		<cflocation url="PMU_ACCORD_CADRE_FIXE.pdf">
	</cfcase>
	
	<cfcase value="ACG">						
		<cfcontent type="application/force-download">
		<cfheader name="Content-Disposition" value="attachment;filename=PMU_ACCORD_CADRE_GSM.pdf">	 
		<cfheader name="Content-Description" value="Accord cadre GSM">
		<cflocation url="PMU_ACCORD_CADRE_GSM.pdf">
	</cfcase>
	
	<cfcase value="TRFP">			
		<cfcontent type="application/force-download">
		<cfheader name="Content-Disposition" value="attachment;filename=PMU_TARIFS_PRODUITS.xls">
		<cfheader name="Content-Description" value="Tarifs produits">
		<cflocation url="PMU_TARIFS_PRODUITS.xls">				
	</cfcase>			
</cfswitch>