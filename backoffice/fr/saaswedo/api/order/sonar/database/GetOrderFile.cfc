﻿component extends="Proc"   
{
			
	public GetOrderFile function init(string datasource)
	output="false"
	{	
		super.init();
		
		if(isdefined('arguments.datasource'))
		{
			this.datasource = arguments.datasource;
		}
		
		return( this );
	}
	
	public any function execute(required numeric MytemOrderId)
	 displayname="execute" description="execute the stored procedure" hint="execute" output="true"
	{
		var spService = new storedproc();
		var result = {};
		
		spService.setDatasource(this.datasource);
		spService.setProcedure("PKG_M16.COMMANDELISTATTACHMENTS");
			  
		spService.addParam(type="in", cfsqltype="cf_sql_numeric", dbvarname= "p_idcommande", value=arguments.MytemOrderId);
		spService.addProcResult(name="p_ref_1",resultset=1);
		
		procresult = spService.execute();
		result.filelist = procresult.getProcResultSets().p_ref_1;
		result.status = 1;    
		 
		return result;
	}

}