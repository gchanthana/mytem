﻿<cfcomponent extends="AbstractAction">
	
	<!--- supprimer une fonction --->
	<cffunction name="executeAct" access="public" returntype="Struct" >
		<cfargument name="data" type="struct" required="true" hint="contient idfonction">
		
		<cftry>
			
			<!--- Supprimer une fonction --->
				<cfstoredproc procedure="pkg_m28.deleteFonction" datasource="ROCOFFRE">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#data.idfonction#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="qResult" type="out" >
				</cfstoredproc>
				
				<cfset p_retour = structNew()>
				<cfset p_retour.RESULT = qResult>
				<cfreturn CreateReponse("AS002", 1, p_retour)>
			
			<cfcatch type="Any">
				<cfreturn CreateReponse("AS002", 0, ThrowCfCatchError("AS002",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
				
	</cffunction>

</cfcomponent>