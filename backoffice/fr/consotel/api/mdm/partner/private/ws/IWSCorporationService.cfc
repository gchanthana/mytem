<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSCorporationService" hint="Interface décrivant les méthodes sur service : Corporation Management Services">
	<cffunction access="public" name="searchCorporations" returntype="Any" hint="Recherche toutes les entités Corporation qui correspondent aux critères fournis (50 résultats par page max)">
		<cfargument name="corporationName" type="String" required="true" hint="Chaine à rechercher dans le nom de l'entité Corporation">
		<cfargument name="corporationId" type="String" required="true" hint="Chaine à rechercher dans l'identifiant de l'entité Corporation">
		<cfargument name="pageNumber" type="String" required="true" hint="Numéro de la page de résultat à retourner (1 ou une chaine vide pour retourner la 1ère page de résultat)">
	</cffunction>
	
	<cffunction access="public" name="getCorporationName" returntype="String" hint="Retourne le nom correspondant à corporationId">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de l'entité Corporation">
	</cffunction>
	
	<cffunction access="public" name="getCorporationStatus" returntype="String" hint="Retourne le statut de l'entité Corporation correspondant à corporationId (Active ou Inactive)">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de l'entité Corporation">
	</cffunction>
	
	<cffunction access="public" name="createCorporation" returntype="String" hint="Crée une entité Corporation avec le nom corporationName et retourne son identifiant">
		<cfargument name="corporationName" type="String" required="true" hint="Nom de l'entité Corporation à créer">
	</cffunction>
	
	<cffunction access="public" name="deleteCorporation" returntype="boolean"
	hint="Supprime le statut de l'entité Corporation correspondant à corporationId. Retourne TRUE si l'opération s'est effectuée avec succès et FALSE sinon<br>
	<b>Une exception est levée si la Corporation contient au moins une licence multiple</b>">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de l'entité Corporation">
	</cffunction>
	
	<cffunction access="public" name="getCorporationCount" returntype="Numeric" hint="Retourne le nombre d'entité Corporation">
	</cffunction>

	<cffunction access="public" name="updateCorporationName" returntype="Boolean" hint="Renomme une entité Corporation et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de l'entité Corporation">
		<cfargument name="corporationName" type="String" required="true" hint="Nouveau nom à donner à l'entité Corporation">
	</cffunction>

	<cffunction access="public" name="updateCorporationStatus" returntype="String" hint="Met à jour le statut d'une entité Corporation et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="corporationId" type="String" required="true" hint="Identifiant de l'entité Corporation">
		<cfargument name="status" type="String" required="true" hint="Valeur à donner au statut. Valeurs possibles : Active, Inactive">
	</cffunction>
</cfinterface>