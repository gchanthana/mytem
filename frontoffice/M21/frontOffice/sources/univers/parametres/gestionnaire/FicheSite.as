package univers.parametres.gestionnaire
{
	import mx.resources.ResourceManager;
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.gestionnaire.classemetier.Site;
	
	public class FicheSite extends FicheSiteIHM
	{
		private var site : Site; 
		private var _inpup_cp :String;
		private var _input_code:String;
		private var _input_commentaire:String;
		private var _input_libelle:String;
		private var _input_ville:String;
		private var _input_adresse:String;
		private var creation : Boolean;
		private var _gestion_site :GestionSites;
		
		public function FicheSite(site : Site=null,creation :Boolean=false,gestion_site : GestionSites=null)
		{
			this._gestion_site = gestion_site;
			this.creation = creation;
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			this.site = site;
		}
		private function initIHM(evt : FlexEvent):void
		{
			if(creation==false)
			{
				inpup_cp.text=site.cp_site+"";
				input_ref.text = site.ref_site;
				input_code.text = site.code_interne_site;
				input_commentaire.text=site.commentaire_site;
				input_libelle.text=site.libelle_site;
				input_ville.text=site.commune_site;
				input_adresse.text=site.adr_site;
			}	
			bt_enregistrer.addEventListener(MouseEvent.CLICK,enregistrer_handler);
			bt_fermer.addEventListener(MouseEvent.CLICK,fermer_handler);
		}
		private function enregistrer_handler(evt : MouseEvent):void
		{
			if(creation==true)
			{
				site = new Site();
			}
			if(int(inpup_cp.text))
			{
				site.cp_site= inpup_cp.text;
				site.ref_site = input_ref.text;
				site.code_interne_site= input_code.text;
				site.commentaire_site = input_commentaire.text;
				site.libelle_site=input_libelle.text;
				site.commune_site=input_ville.text;
				site.adr_site = input_adresse.text;
				if(creation==true)
				{
					site.save_site();
				}
				else
				{
					site.save_modif_site();
				}
				_gestion_site.initData();
				PopUpManager.removePopUp(this);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M21', 'V_rifiez_le_code_postal__'),ResourceManager.getInstance().getString('M21', 'Consoview'));
			}
		}
		private function fermer_handler(evt : MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
	}
}