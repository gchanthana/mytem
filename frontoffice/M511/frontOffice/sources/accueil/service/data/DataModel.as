package accueil.service.data
{
	import mx.collections.ICollectionView;
	
	[Bindable]
	public class DataModel
	{	
		private var _data1:ICollectionView;
		private var _data2:ICollectionView;
		private var _data3:ICollectionView;
		
		public function set data1 (value:ICollectionView):void
		{
			_data1 = value;
		}

		public function get data1 ():ICollectionView
		{
			return _data1;
		}


		
		
		public function DataModel()
		{
		}


		public function set data2(value:ICollectionView):void
		{
			_data2 = value;
		}

		public function get data2():ICollectionView
		{
			return _data2;
		}
		public function set data3(value:ICollectionView):void
		{
			_data3 = value;
		}

		public function get data3():ICollectionView
		{
			return _data3;
		}
	}
}