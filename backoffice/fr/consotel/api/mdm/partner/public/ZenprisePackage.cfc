<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.ZenprisePackage" extends="fr.consotel.api.mdm.partner.public.ZenpriseCfg" hint="Implémentation Package Service">
	<!--- Package Service --->
	<cffunction access="remote" name="getPackage" returntype="String" hint="Retourne la définition du package au format JSON<br>
	Le contenu de la définition est décrit comme suit par la documentation Zenprise :<br>
<pre>General information
  id (int): package internal id
  name (string): package name
  fullPath (string): package fullpath (include package name)
  osFamily (string): package OS family (can be &quot;WINDOWS&quot;, &quot;iOS&quot;, or &quot;ANDROID&quot;)
  defaultDeploy (boolean): package used as default deployment
Rule conditions
  rule (optional string): XML describing deployment conditions. See getPackageRule(String) for details.
Scheduling conditions
  scheduling (optional string): XML describing deployment scheduling. See getPackageSchedule(String) for details.
  deployOnce (boolean): package will not be deployed if server know a successfull deployment from history) 
Target groups
  recipients (array of string): list of target groups
  deployToAnonymous (boolean): deploy to anonymous 
Sub packages
  subPackages
      array of string : list of sub package names (to get sub-packages fullpath use package fullpath, append &quot;/&quot; and the name of sub-package)
      package json object : list of package for deep request (getPackages(String, boolean)) 
Package resources
  resources (array of object): list of resources. Resource object contains:
      type (string): resource type can be &quot;SERVERGROUP&quot;, , &quot;REGISTRY&quot;, &quot;XML&quot;, &quot;CAB&quot;, &quot;APK&quot;, &quot;SCRIPT&quot;, &quot;FILE&quot;, &quot;SOFTWARE_INVENTORY&quot;
      id (int): resource internal id
      name (string): resource name
      group (string): resource group
          same as type for &quot;WINDOWS&quot; OS family
          &quot;CONFIG&quot; for &quot;iOS&quot; and &quot;ANDROID&quot; OS families for resources of type &quot;REGISTRY&quot; and &quot;XML&quot;, same as type either
  Notes:
      Only type, id and name are important.
      For &quot;SOFTWARE_INVENTORY&quot; only type is important (id and name are set with default values). Considere this resource like a boolean (present or not).
      Console use group to abstract graphic user interface. Internally and for compatiility reasons configuration will be registries or xml. You can use group for your presentation.
      But only type information will be necessary for update/delete operation.</pre>">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageJsonDef=packageService.getPackage(ARGUMENTS["pkgFullPath"])>
		<cfreturn packageJsonDef>
	</cffunction>

	<cffunction access="remote" name="setPackage" returntype="Void" hint="Spécifie la définition du package au format JSON">
		<cfargument name="jsonPkg" type="String" required="true" hint="Définition du package au format JSON<br>
		Le contenu de la définition est décrit comme suit par la documentation Zenprise :<br>
<pre>General information
  fullPath (mandatory string): package fullpath. For a rename this must be the old package path.
  name (optional string): This can be a new package name
  osFamily (mandatory string for creating a root package): package OS family (can be &quot;WINDOWS&quot;, &quot;iOS&quot;, or &quot;ANDROID&quot;)
  Inner package must follow parent&quot;s OS family. If a value is given, a check will occurs.
  defaultDeploy (optional boolean, default to false): this package must be used as default deployment. A default package cannot have recipients.
Rule conditions
  rule (optional string, default no rule): XML describing deployment conditions. See getPackageRule(String) for details.
Scheduling conditions
  scheduling (optional string, default deploy on device connection): XML describing deployment scheduling. See getPackageSchedule(String) for details.
  deployOnce (optional boolean, default deploy when scheduling condition occur): package will not be deployed if server know a successfull deployment from history) 
Target groups
  recipients (optional array of string, default no recipient): array of string recipients. A default package (defaultDeploy set to true) cannot have recipients.
  deployToAnonymous (boolean, default to false): deploy to anonymous 
Package resources
  resources (array of object, default empty resources): list of resources. Resource object contains:
      name (string): resource name
      type (string): resource type can be &quot;SERVERGROUP&quot;, &quot;TUNNEL&quot;, &quot;REGISTRY&quot;, &quot;XML&quot;, &quot;CAB&quot;,
		&quot;APK&quot;, &quot;SCRIPT&quot;, &quot;FILE&quot;, &quot;SOFTWARE_INVENTORY&quot;
  Notes:
      For &quot;SOFTWARE_INVENTORY&quot; only type is important (name will be set with default values). Considere this resource like a boolean (present or not).</pre><br>
	Voir aussi les méthodes : setPackageRule() et setPackageSchedule()">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.setPackage(ARGUMENTS["jsonPkg"])>
	</cffunction>
	
	<cffunction access="remote" name="getPackages" returntype="String" hint="Retourne la liste des définitions des packages au format JSON">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package ou une chaine vide pour indiquer <b>le package racine</b>">
		<cfargument name="deep" type="Boolean" required="true" hint="TRUE pour retourne la liste complète détaillée et FALSE sinon">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var jsonPackageList=packageService.getPackages(ARGUMENTS["pkgFullPath"],ARGUMENTS["deep"])>
		<cfreturn jsonPackageList>
	</cffunction>
	
	<cffunction access="remote" name="removePackage" returntype="Void" hint="Supprime la définition d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackage(ARGUMENTS["pkgFullPath"])>
	</cffunction>
	
	<cffunction access="remote" name="setPackageRule" returntype="Void" hint="Définit la règle d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="xmlRule" type="String" required="true" hint="Définition XML de la règle ou une chaine vide pour pour ignorer cette règle">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.setPackageRule(ARGUMENTS["pkgFullPath"],ARGUMENTS["xmlRule"])>
	</cffunction>
	
	<cffunction access="remote" name="renamePackage" returntype="Void" hint="Renomme un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Actuel chemin absolu du package">
		<cfargument name="newPackageName" type="String" required="true" hint="Nouveau nom à donner au package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.renamePackage(ARGUMENTS["pkgFullPath"],ARGUMENTS["newPackageName"])>
	</cffunction>
	
	<cffunction access="remote" name="checkPackageRule" returntype="String" hint="Vérifie la définition de la règle et retourne TRUE si elle est valide et FALSE sinon">
		<cfargument name="xmlRule" type="String" required="true" hint="Définition XML de la règle">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var checkStatus=packageService.checkPackageRule(ARGUMENTS["xmlRule"])>
		<cfreturn serializeJSON(checkStatus)>
	</cffunction>
	
	<cffunction access="remote" name="getPackageRule" returntype="String" hint="Retourne la définition XML de la règle du package (Voir documentation Zenprise pour la DTD)">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var pkgXmlRule=packageService.getPackageRule(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(pkgXmlRule)>
	</cffunction>
	
	<cffunction access="remote" name="removePackageRule" returntype="Void" hint="Supprime la règle du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackageRule(ARGUMENTS["pkgFullPath"])>
	</cffunction>
	
	<cffunction access="remote" name="isDeployOnce" returntype="String" hint="Retourne TRUE si la propriété deployOnce du package est activé et FALSE sinon. Voir setDeployOnce()">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var deployOnce=packageService.isDeployOnce(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(deployOnce)>
	</cffunction>
	
	<cffunction access="remote" name="setDeployOnce" returntype="Void" hint="Affecte la valeur deployOnce à la propriété correspondante du package<br>
	Lorsque cette propriété vaut TRUE alors le serveur ne renvoie pas de données au client si un précédent déploiement s'est déjà effectué avec succès (Via l'historique de déploiement)">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="deployOnce" type="Boolean" required="true" hint="Valeur à affecter à la propriété deployOnce du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.setDeployOnce(ARGUMENTS["pkgFullPath"],ARGUMENTS["deployOnce"])>
	</cffunction>
	
	<cffunction access="remote" name="packageDeploy" returntype="Void"
	hint="(Package Service) Effectue une demande de déploimenet pour les équipement qui sont connectés au serveur (Android et Windows). Envoie une notification PUSH pour iOS">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.deploy()>
	</cffunction>
	
	<cffunction access="remote" name="getPackageList" returntype="String" hint="Retourne la liste des noms des packages racines dans un tableau">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageList=packageService.getPackageList()>
		<cfreturn serializeJSON(packageList)>
	</cffunction>
	
	<cffunction access="remote" name="createPackage" returntype="String" hint="Crée un package avec le nom fourni dans pkgFullPath et retourne le nom">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensitive (i.e : iOS, WINDOWS, ANDROID)">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageName=packageService.createPackage(ARGUMENTS["osFamily"],ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(packageName)>
	</cffunction>
	
	<cffunction access="remote" name="getPackageTunnelList" returntype="String" hint="Retourne la liste des noms des tunnels du package dans un tableau">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var tunnelList=packageService.getPackageTunnelList(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(tunnelList)>
	</cffunction>
	
	<cffunction access="remote" name="addPackageTunnel" returntype="Void" hint="Ajoute un tunnel dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="tunnelId" type="String" required="true" hint="Nom du tunnel à ajouter">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.addPackageTunnel(ARGUMENTS["pkgFullPath"],ARGUMENTS["tunnelId"])>
	</cffunction>
	
	<cffunction access="remote" name="removePackageTunnel" returntype="Void" hint="Supprime un tunnel d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="tunnelId" type="String" required="true" hint="Nom du tunnel à supprimer">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackageTunnel(ARGUMENTS["pkgFullPath"],ARGUMENTS["tunnelId"])>
	</cffunction>
	
	<cffunction access="remote" name="getTunnelPackageList" returntype="String" hint="Retourne la liste des packages utilisant le tunnel tunnelId dans un tableau">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensitive (i.e : iOS, WINDOWS, ANDROID)">
		<cfargument name="tunnelId" type="String" required="true" hint="Nom du tunnel">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageList=packageService.getTunnelPackageList(ARGUMENTS["osFamily"],ARGUMENTS["tunnelId"])>
		<cfreturn serializeJSON(packageList)>
	</cffunction>
	
	<cffunction access="remote" name="getPackageGroupList" returntype="String" hint="Retourne la liste des groupes déployés dans le package dans un tableau">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var groupList=packageService.getPackageGroupList(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(groupList)>
	</cffunction>
	
	<cffunction access="remote" name="addPackageGroup" returntype="Void" hint="Déploie le groupe groupId dans le package pkgFullPath">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="groupId" type="String" required="true" hint="Nom hiérarchique du groupe (e.g : group.subgroup)">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.addPackageGroup(ARGUMENTS["pkgFullPath"],ARGUMENTS["groupId"])>
	</cffunction>
	
	<cffunction access="remote" name="removePackageGroup" returntype="Void" hint="Supprime un groupe du package pkgFullPath">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="groupId" type="String" required="true" hint="Nom hiérarchique du groupe (e.g : group.subgroup)">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackageGroup(ARGUMENTS["pkgFullPath"],ARGUMENTS["groupId"])>
	</cffunction>
	
	<cffunction access="remote" name="getGroupPackageList" returntype="String" hint="Retourne un tableau contenant la liste des packages dans lesquels le groupe groupId a été déployé">
		<cfargument name="groupId" type="String" required="true" hint="Nom hiérarchique du groupe (e.g : group.subgroup)">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageList=packageService.getGroupPackageList(ARGUMENTS["groupId"])>
		<cfreturn serializeJSON(packageList)>
	</cffunction>
	
	<cffunction access="remote" name="getPackageOSFamily" returntype="String" hint="Retourne l'OS cible du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var osFamily=packageService.getPackageOSFamily(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(osFamily)>
	</cffunction>
	
	<cffunction access="remote" name="getPackageSchedule" returntype="String"
	hint="Retourne la configuration XML de la planification du package ou une chaine vide s'il n'est pas planifié (Voir documentation Zenprise pour la DTD)">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var pkgSchedule=packageService.getPackageSchedule(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(pkgSchedule)>
	</cffunction>
	
	<cffunction access="remote" name="checkPackageSchedule" returntype="String" hint="Retourne TRUE si la configuration XML de planification xmlSchedule est valide et FALSE sinon">
		<cfargument name="xmlSchedule" type="String" required="true" hint="Configuration XML de planification de package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var checkStatus=packageService.checkPackageSchedule(ARGUMENTS["xmlSchedule"])>
		<cfreturn serializeJSON(checkStatus)>
	</cffunction>
	
	<cffunction access="remote" name="setPackageSchedule" returntype="Void" hint="Définit la configuration XML de planification xmlSchedule pour le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="xmlSchedule" type="String" required="true" hint="Configuration XML de planification de package ou une chaine vide pour ignorer la planification">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.setPackageSchedule(ARGUMENTS["pkgFullPath"],ARGUMENTS["xmlSchedule"])>
	</cffunction>
	
	<cffunction access="remote" name="removePackageSchedule" returntype="Void" hint="Supprime la configuration XML de planification pour le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackageSchedule(ARGUMENTS["pkgFullPath"])>
	</cffunction>
	
	<cffunction access="remote" name="getPackageFileList" returntype="String" hint="Retourne un tableau contenant la liste des fichiers de ressources utilisés dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageList=packageService.getPackageFileList(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(packageList)>
	</cffunction>
	
	<cffunction access="remote" name="addPackageFile" returntype="Void" hint="Ajoute un fichier de ressources dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier de ressources à ajouter sans préfixe">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.addPackageFile(ARGUMENTS["pkgFullPath"],ARGUMENTS["fileName"])>
	</cffunction>
	
	<cffunction access="remote" name="removePackageFile" returntype="Void" hint="Supprime le fichier de ressources fileName du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier de ressources à supprimer sans préfixe">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackageFile(ARGUMENTS["pkgFullPath"],ARGUMENTS["fileName"])>
	</cffunction>
	
	<cffunction access="remote" name="getFilePackageList" returntype="String" hint="Retourne un tableau contenant la liste des packages utilisant le fichier de ressources">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier de ressources">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageList=packageService.getFilePackageList(ARGUMENTS["fileName"])>
		<cfreturn serializeJSON(packageList)>
	</cffunction>
	
	<cffunction access="remote" name="getPackageXMLList" returntype="String" hint="Retourne un tableau contenant la liste des ressources XML utilisées dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var resList=packageService.getPackageXMLList(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(resList)>
	</cffunction>
	
	<cffunction access="remote" name="addPackageXML" returntype="Void" hint="Ajoute une ressource XML dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="xmlName" type="String" required="true" hint="Nom de la ressource XML à ajouter">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.addPackageXML(ARGUMENTS["pkgFullPath"],ARGUMENTS["xmlName"])>
	</cffunction>
	
	<cffunction access="remote" name="removePackageXML" returntype="Void" hint="Supprime une ressource XML du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="xmlName" type="String" required="true" hint="Nom de la ressource XML à suprimer">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackageXML(ARGUMENTS["pkgFullPath"],ARGUMENTS["xmlName"])>
	</cffunction>
	
	<cffunction access="remote" name="getXMLPackageList" returntype="String" hint="Retourne un tableau contenant la liste des packages utilisant la ressource XML">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensitive (i.e : iOS, WINDOWS, ANDROID)">
		<cfargument name="xmlName" type="String" required="true" hint="Nom de la ressource XML à suprimer">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageList=packageService.getXMLPackageList(ARGUMENTS["osFamily"],ARGUMENTS["xmlName"])>
		<cfreturn serializeJSON(packageList)>
	</cffunction>
	
	<cffunction access="remote" name="getPackageCABList" returntype="String" hint="Retourne un tableau contenant la liste des ressources CAB utilisées dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var resList=packageService.getPackageCABList(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(resList)>
	</cffunction>
	
	<cffunction access="remote" name="addPackageCAB" returntype="Void" hint="Ajoute une ressource CAB dans un package de type WINDOWS">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="cabName" type="String" required="true" hint="Nom de la ressource CAB à ajouter">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.addPackageCAB(ARGUMENTS["pkgFullPath"],ARGUMENTS["cabName"])>
	</cffunction>
	
	<cffunction access="remote" name="removePackageCAB" returntype="Void" hint="Supprime une ressource CAB d'un package de type WINDOWS">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="cabName" type="String" required="true" hint="Nom de la ressource CAB à supprimer">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackageCAB(ARGUMENTS["pkgFullPath"],ARGUMENTS["cabName"])>
	</cffunction>
	
	<cffunction access="remote" name="getCABPackageList" returntype="String" hint="Retourne un tableau contenant la liste des packages utilisant la ressource CAB">
		<cfargument name="cabName" type="String" required="true" hint="Nom de la ressource CAB">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageList=packageService.getCABPackageList(ARGUMENTS["cabName"])>
		<cfreturn serializeJSON(packageList)>
	</cffunction>
	
	<cffunction access="remote" name="getPackageAPKList" returntype="String" hint="Retourne un tableau contenant la liste des ressources APK d'un package de type ANDROID">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var resList=packageService.getPackageAPKList(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(resList)>
	</cffunction>
	
	<cffunction access="remote" name="addPackageAPK" returntype="Void" hint="Ajoute une ressource APK à un package de type ANDROID">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="apkName" type="String" required="true" hint="Nom de la ressource APK à ajouter">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.addPackageAPK(ARGUMENTS["pkgFullPath"],ARGUMENTS["apkName"])>
	</cffunction>
	
	<cffunction access="remote" name="removePackageAPK" returntype="Void" hint="Supprime une ressource APK d'un package de type ANDROID">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="apkName" type="String" required="true" hint="Nom de la ressource APK à ajouter">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackageAPK(ARGUMENTS["pkgFullPath"],ARGUMENTS["apkName"])>
	</cffunction>
	
	<cffunction access="remote" name="getAPKPackageList" returntype="String" hint="Retourne un tableau contenant la liste des packages utilisant la ressource APK">
		<cfargument name="apkName" type="String" required="true" hint="Nom de la ressource APK à ajouter">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageList=packageService.getAPKPackageList(ARGUMENTS["apkName"])>
		<cfreturn serializeJSON(packageList)>
	</cffunction>
	
	<cffunction access="remote" name="getPackageServerGroupList" returntype="String" hint="Retourne un tableau contenant la liste des groupes de serveurs utilisés par le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var groupList=packageService.getPackageServerGroupList(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(groupList)>
	</cffunction>
	
	<cffunction access="remote" name="addPackageServerGroup" returntype="Void" hint="Ajoute un groupe de serveur à un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="sgName" type="String" required="true" hint="Nom du groupe de serveur à ajouter">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.addPackageServerGroup(ARGUMENTS["pkgFullPath"],ARGUMENTS["sgName"])>
	</cffunction>
	
	<cffunction access="remote" name="removePackageServerGroup" returntype="Void" hint="Supprime un groupe de serveur d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="sgName" type="String" required="true" hint="Nom du groupe de serveur à supprimer">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackageServerGroup(ARGUMENTS["pkgFullPath"],ARGUMENTS["sgName"])>
	</cffunction>
	
	<cffunction access="remote" name="getServerGroupPackageList" returntype="String" hint="Retourne un tableau contenant la liste des packages utilisant le groupe de serveur">
		<cfargument name="sgName" type="String" required="true" hint="Nom du groupe de serveur">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageList=packageService.getServerGroupPackageList(ARGUMENTS["sgName"])>
		<cfreturn serializeJSON(packageList)>
	</cffunction>
	
	<cffunction access="remote" name="getPackageRegistriesList" returntype="String" hint="Retourne un tableau contenant la liste des registres utilisés par le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var regList=packageService.getPackageRegistriesList(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(regList)>
	</cffunction>
	
	<cffunction access="remote" name="addPackageRegistry" returntype="Void" hint="Ajoute un registre à un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="regName" type="String" required="true" hint="Nom du registre à ajouter">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.addPackageRegistry(ARGUMENTS["pkgFullPath"],ARGUMENTS["regName"])>
	</cffunction>
	
	<cffunction access="remote" name="removePackageRegistry" returntype="Void" hint="Supprime un registre d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="regName" type="String" required="true" hint="Nom du registre à supprimer">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackageRegistry(ARGUMENTS["pkgFullPath"],ARGUMENTS["regName"])>
	</cffunction>
	
	<cffunction access="remote" name="getRegistryPackageList" returntype="String" hint="Retourne un tableau contenant la liste des packages utilisant le registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensitive (i.e : WINDOWS, ANDROID)">
		<cfargument name="regName" type="String" required="true" hint="Nom du registre à supprimer">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageList=packageService.getRegistryPackageList(ARGUMENTS["osFamily"],ARGUMENTS["regName"])>
		<cfreturn serializeJSON(packageList)>
	</cffunction>
	
	<cffunction access="remote" name="hasPackageSoftwareInventory" returntype="String" hint="Retourne TRUE si le package contient un inventaire logiciel">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var hasPackageSoftware=packageService.hasPackageSoftwareInventory(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(hasPackageSoftware)>
	</cffunction>
	
	<cffunction access="remote" name="addPackageSoftwareInventory" returntype="Void" hint="Définit l'inventaire logiciel du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.addPackageSoftwareInventory(ARGUMENTS["pkgFullPath"])>
	</cffunction>
	
	<cffunction access="remote" name="removePackageSoftwareInventory" returntype="Void" hint="Supprime l'inventaire logiciel du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackageSoftwareInventory(ARGUMENTS["pkgFullPath"])>
	</cffunction>
	
	<cffunction access="remote" name="getSoftwareInventoryPackageList" returntype="String" hint="Retourne un tableau contenant la liste des package ayant un inventaire logiciel">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageList=packageService.getSoftwareInventoryPackageList()>
		<cfreturn serializeJSON(packageList)>
	</cffunction>
	
	<cffunction access="remote" name="getPackageCfgList" returntype="String" hint="Retourne un tableau contenant la liste des ressources CFG utilisées dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var cfgList=packageService.getPackageCfgList(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(cfgList)>
	</cffunction>
	
	<cffunction access="remote" name="addPackageCfg" returntype="Void" hint="Ajoute une ressource CFG dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="cfgName" type="String" required="true" hint="Nom de la ressource CFG à ajouter">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.addPackageCfg(ARGUMENTS["pkgFullPath"],ARGUMENTS["cfgName"])>
	</cffunction>
	
	<cffunction access="remote" name="removePackageCfg" returntype="Void" hint="Supprime une ressource CFG d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="cfgName" type="String" required="true" hint="Nom de la ressource CFG à supprimer">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackageCfg(ARGUMENTS["pkgFullPath"],ARGUMENTS["cfgName"])>
	</cffunction>
	
	<cffunction access="remote" name="getCfgPackageList" returntype="String" hint="Retourne un tableau contenant la liste des packages utilisant la ressource CFG">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Doit etre iOS pour la version actuelle de l'API Zenprise">
		<cfargument name="cfgName" type="String" required="true" hint="Nom de la ressource CFG à supprimer">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageList=packageService.getCfgPackageList(ARGUMENTS["osFamily"],ARGUMENTS["cfgName"])>
		<cfreturn serializeJSON(packageList)>
	</cffunction>
	
	<cffunction access="remote" name="getPackageExtAppList" returntype="String" hint="Retourne un tableau contenant la liste des ressources EXT d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var extList=packageService.getPackageExtAppList(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(extList)>
	</cffunction>
	
	<cffunction access="remote" name="addPackageExtApp" returntype="Void" hint="Ajoute une ressource EXT dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="extName" type="String" required="true" hint="Nom de la ressource EXT à ajouter">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.addPackageExtApp(ARGUMENTS["pkgFullPath"],ARGUMENTS["extName"])>
	</cffunction>
	
	<cffunction access="remote" name="removePackageExtApp" returntype="Void" hint="Supprime une ressource EXT d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="extName" type="String" required="true" hint="Nom de la ressource EXT à supprimer">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackageExtApp(ARGUMENTS["pkgFullPath"],ARGUMENTS["extName"])>
	</cffunction>
	
	<cffunction access="remote" name="getExtAppPackageList" returntype="String" hint="Retourne un tableau contenant la liste des packages utilisant la ressource EXT">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Doit etre iOS pour la version actuelle de l'API Zenprise">
		<cfargument name="extName" type="String" required="true" hint="Nom de la ressource EXT à supprimer">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageList=packageService.getExtAppPackageList(ARGUMENTS["osFamily"],ARGUMENTS["extName"])>
		<cfreturn serializeJSON(packageList)>
	</cffunction>
	
	<cffunction access="remote" name="getPackageIpaList" returntype="String" hint="Retourne un tableau contenant la liste des ressources IPA d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var ipaList=packageService.getPackageIpaList(ARGUMENTS["pkgFullPath"])>
		<cfreturn serializeJSON(getPackageIpaList)>
	</cffunction>
	
	<cffunction access="remote" name="addPackageIpa" returntype="Void" hint="Ajoute un ressource IPA à un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="ipaName" type="String" required="true" hint="Nom de la ressource IPA à ajouter">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.addPackageIpa(ARGUMENTS["pkgFullPath"],ARGUMENTS["ipaName"])>
	</cffunction>
	
	<cffunction access="remote" name="removePackageIpa" returntype="Void" hint="Supprime une ressource IPA d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="ipaName" type="String" required="true" hint="Nom de la ressource IPA à supprimer">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset packageService.removePackageIpa(ARGUMENTS["pkgFullPath"],ARGUMENTS["ipaName"])>
	</cffunction>
	
	<cffunction access="remote" name="getIpaPackageList" returntype="String" hint="Retourne un tableau contenant la liste des packages utilisant la ressource IPA">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Doit etre iOS pour la version actuelle de l'API Zenprise">
		<cfargument name="ipaName" type="String" required="true" hint="Nom de la ressource IPA">
		<cfset var packageService=getManagementService().getPackageService()>
		<cfset var packageList=packageService.getIpaPackageList(ARGUMENTS["osFamily"],ARGUMENTS["ipaName"])>
		<cfreturn serializeJSON(packageList)>
	</cffunction>
</cfcomponent>