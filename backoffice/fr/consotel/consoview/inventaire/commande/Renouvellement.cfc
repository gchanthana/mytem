<cfcomponent output="false">
	
	<cffunction name="findEqtsContrats" access="remote" output="false" returntype="query">
		<cfargument name="idpool" type="Numeric" required="true"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.FIND_EQCTRT">	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idpool" value="#idpool#"/>
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn qlisteContrats/>	
	</cffunction>
	
	<cffunction name="findRessourcesContrats" access="remote" output="false" returntype="query">
		<cfargument name="idcontrat" type="Numeric" required="true"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.FIND_RESSCTRT">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idContrat" value="#idpool#"/>
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn qlisteContrats/>	
	</cffunction>
	
	<cffunction name="findRessourcesSousTete" access="remote" output="false" returntype="query">
		<cfargument name="idContrat" type="Numeric" required="false"/>
								
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.FIND_RESSCTRT">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idContrat" value="#idContrat#"/>
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn qlisteRessources/>	
	</cffunction>
	
	<cffunction name="getContratAboByIDSousTete" access="remote" output="false" returntype="query">
		<cfargument name="idSousTete" type="Numeric" required="false"/>
							
								
		<cfreturn queryNew()/>	
	</cffunction>	
	
	<cffunction name="getContratAboByID" access="remote" output="false" returntype="array">
		<cfargument name="idContrat" type="Numeric" required="false"/>
				
		<!--- <cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.DETAILCONTRAT"> --->
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_CV_CONTRAT.DETAILCONTRAT">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idSousTete" value="#idSousTete#"/>
			<cfprocresult name="qDeatilContrat">
		</cfstoredproc>
		
		<cfset result = arrayNew()>
		<cfset arrayAppend(result,findEqtsContrats(idContrat))>
		<cfset arrayAppend(result,findRessourcesContrats(idContrat))>
			
		<cfreturn result/>	
	</cffunction>		
	
	<cffunction name="renewContrat" access="remote" output="false" returntype="numeric">
		<cfargument name="contrat" type="fr.consotel.consoview.inventaire.contrats.Contrat" required="true"/>
		<cfargument name="articles" type="string" required="true"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.MAJ_CTRTABO">			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_idcontrat" 				value="#contrat.getIDCONTRAT()#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_idfournisseur" 			value="#contrat.getIDFOURNISSEUR()#"> 
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" variable="p_reference_contrat" 		value="#contrat.getREFERENCE_CONTRA()T#"/> 
			<cfprocparam cfsqltype="CF_SQL_DATE"  		type="in" variable="p_date_signature" 			value="#contrat.getDATE_SIGNATURE()#" null="#iif((contrat.getDATE_SIGNATURE() neq ""), de("no"), de("yes"))#"/> 
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" variable="p_commentaire_contrat" 		value="#contrat.getCOMMENTAIRE_CONTRA()T#"/> 
			<cfprocparam cfsqltype="CF_SQL_NUMERIC"  	type="in" variable="p_montant_contrat" 			value="#contrat.getMONTANT_CONTRAT()#"/> 
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_duree_contrat" 			value="#contrat.getDUREE_CONTRAT()#"/> 
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_tacite_reonduction" 		value="#contrat.getTACITE_REONDUCTION()#"/> 
			<cfprocparam cfsqltype="CF_SQL_DATE"  		type="in" variable="p_date_debut" 				value="#contrat.getDATE_DEBUT()#" null="#iif((contrat.getDATE_DEBUT() neq ""), de("no"), de("yes"))#"/> 
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_preavis" 					value="#contrat.getPREAVIS()#"/> 
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_idcde_contact_societe" 	value="#contrat.getIDCDE_CONTACT_SOCIETE()#" null="#iif((contrat.getIDCDE_CONTACT_SOCIETE() gt 0), de("no"), de("yes"))#"/> 
			<cfprocparam cfsqltype="CF_SQL_NUMERIC"  	type="in" variable="p_loyer" 					value="#contrat.getLOYER()#" /> 
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_periodicite" 				value="#contrat.getPERIODICITE()#"/> 
			<cfprocparam cfsqltype="CF_SQL_NUMERIC"  	type="in" variable="p_montant_frais" 			value="#contrat.getMONTANT_FRAIS()#" /> 
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" variable="p_numero_client" 			value="#contrat.getNUMERO_CLIENT()#" /> 
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" variable="p_numero_fournisseur" 		value="#contrat.getNUMERO_FOURNISSEUR()#" /> 
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" variable="p_numero_facture" 			value="#contrat.getNUMERO_FACTURE()#" /> 
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" variable="p_code_interne" 			value="#contrat.getCODE_INTERNE()#" /> 
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_bool_contrat_cadre" 		value="#contrat.getBOOL_CONTRAT_CADRE()#" /> 
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_bool_avenant" 			value="#contrat.getBOOL_AVENANT()#" /> 
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_id_contrat_maitre" 		value="#contrat.getID_CONTRAT_MAITRE()#" null="#iif((contrat.getID_CONTRAT_MAITRE() gt 0), de("no"), de("yes"))#"/> 
			<cfprocparam cfsqltype="CF_SQL_NUMERIC"  	type="in" variable="p_frais_fixe_resiliation" 	value="#contrat.getFRAIS_FIXE_RESILIATION()#" /> 
			<cfprocparam cfsqltype="CF_SQL_NUMERIC"  	type="in" variable="p_montant_mensuel_pen" 		value="#contrat.getMONTANT_MENSUEL_PEN()#"/> 
			<cfprocparam cfsqltype="CF_SQL_DATE"  		type="in" variable="p_date_resiliation" 		value="#contrat.getDATE_RESILIATION()#" null="#iif((contrat.getDATE_RESILIATION() neq  ""), de("no"), de("yes"))#"/> 
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" variable="p_desigantion" 				value="#contrat.getDESIGANTION()#"> 
			<cfprocparam cfsqltype="CF_SQL_DATE"  		type="in" variable="p_date_renouvellement" 		value="#contrat.getDATE_RENOUVELLEMENT()#" null="#iif((contrat.getDATE_RENOUVELLEMENT() neq  ""), de("no"), de("yes"))#"/> 
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_operateurid" 				value="#contrat.getOPERATEURID()#" null="#iif((contrat.getOPERATEURID() gt 0), de("no"), de("yes"))#"/> 
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_idcompte_facturation" 	value="#contrat.getIDCOMPTE_FACTURATION()#" null="#iif((contrat.getIDCOMPTE_FACTURATION() gt 0), de("no"), de("yes"))#"/> 
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_idsous_compte" 			value="#contrat.getIDSOUS_COMPTE()#" null="#iif((contrat.getIDSOUS_COMPTE() gt 0), de("no"), de("yes"))#"/> 
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" variable="p_numero_contrat" 			value="#contrat.getNUMERO_CONTRAT()#"> 
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_idtype_contrat" 			value="#contrat.getIDTYPE_CONTRAT#()" null="#iif((contrat.getIDTYPE_CONTRAT() gt 0), de("no"), de("yes"))#"/> 
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_idracine" 				value="#contrat.getIDRACINE()#"> 
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" variable="p_duree_elligibilite" 		value="#contrat.getDUREE_ELLIGIBILITE()#"/> 
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" variable="p_fpc" 						value="#contrat.getFPC()#"/> 
			<cfprocparam cfsqltype="CF_SQL_CLOB"  		type="in" variable="p_articles" 				value="#tostring(articles)#"/>			 			
			<cfprocparam cfsqltype="CF_SQL_CLOB"  		type="out" variable="p_result">
		</cfstoredproc>
		
		<cfreturn p_result/>
	</cffunction>
	
	<cffunction name="manageOptions" access="remote" output="false" returntype="numeric">
		<cfargument name="contrat" type="fr.consotel.consoview.inventaire.contrats.Contrat" required="true"/>
		<cfargument name="articles" type="string" required="true"/>
		<cfset renewContrat(contrat,articles)>
		<cfreturn p_result/>	
	</cffunction>
</cfcomponent>