package intranet.GestionProduit.service
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.AbstractEvent;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import composants.util.ConsoviewAlert;

	public class GestionProduitHandler
	{
		private var _model:GestionProduitModel;
		
		
		public function GestionProduitHandler(model:GestionProduitModel)
		{
			_model = model;
		}
		
		
		internal function getProduitNonAffecte(e:AbstractEvent):void
		{
			if(e is FaultEvent)
			{
				return 
			}
			
			if(e is ResultEvent && (e as ResultEvent).result != null)
			{
				_model.getProduitNonAffecte((e as ResultEvent).result as ArrayCollection);
			}
		}
		
		internal function updPNA(e:AbstractEvent):void
		{
			if(e is FaultEvent)
			{
				var message : String = (e as FaultEvent).fault.faultCode;
				ConsoviewAlert.afficherAlertConfirmation(message,"Error",null);
				return 
			}else
			if(e is ResultEvent  && (e as ResultEvent).result is Array)
			{
				_model.updPNA((e as ResultEvent).result as Array);
			}
		}
		
		
		
	}
}