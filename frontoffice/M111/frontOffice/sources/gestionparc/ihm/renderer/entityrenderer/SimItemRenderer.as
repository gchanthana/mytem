package gestionparc.ihm.renderer.entityrenderer
{
	import flash.events.Event;
	import flash.events.MouseEvent;

	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.ficheEntity.FicheSimIHM;
	import gestionparc.ihm.main.menu.MenuContextual;
	import gestionparc.ihm.main.menu.itemmenurenderer.ItemMenuContextualOfExt;
	import gestionparc.ihm.main.menu.itemmenurenderer.ItemMenuContextualOfSim;

	/**
	 * Classe gérant l'item renderer d'un equipement.
	 * Elle hérite de la classe <code>EntityItemRenderer</code>
	 */
	public class SimItemRenderer extends EntityItemRenderer
	{
		private var _isSim:Boolean;

		public function SimItemRenderer():void
		{
			super();
		}

		/**
		 * Cette fonction surcharge le set data et met ainsi a jour le texte affiché, le style
		 * et appelle la fonction <code>fillArrayItemMenuEquipement</code> pour créer la liste des items du menu
		 * et la fonction <code>MenuContextualEntity</code> pour créer le menu.
		 */
		override public function set data(value:Object):void
		{
			img.visible=false;
			lbLibelle.text="";
			lbLibelle.toolTip="";
			lbLibelle.useHandCursor=false;

			setStyle("backgroundColor", 0xFFFFFF);
			setStyle("backgroundAlpha", 0);
			
			if (value != null)
			{
				super.data=value;

				if (value.hasOwnProperty("S_IMEI") && value.S_IMEI)
				{
					if (SpecificationVO.getInstance().idPool == SpecificationVO.POOL_PARC_GLOBAL_ID) // si vue parc global on n'affiche pas l'image action
					{
						this.img.visible=false;
					}
					else
					{
						this.img.visible=true;
						this.img.addEventListener(MouseEvent.CLICK, lineSelected);
					}

					lbLibelle.text="Sim : " + value.S_IMEI;
					lbLibelle.toolTip=lbLibelle.text;
					lbLibelle.addEventListener(MouseEvent.CLICK, ClickFicheHandler);
					lbLibelle.useHandCursor=true;
					_isSim=true;
				}

				if (value.hasOwnProperty("EX_NUMERO") && value.EX_NUMERO)
				{
					if (value.hasOwnProperty("POOL") && value.POOL) // vu parc global on n'affiche pas l'image action
					{
						this.img.visible=false;
					}
					else
					{
						this.img.visible=true;
						this.img.addEventListener(MouseEvent.CLICK, lineSelected);
					}
					lbLibelle.text="Ext : " + value.EX_NUMERO;
					lbLibelle.toolTip=lbLibelle.text;
					lbLibelle.addEventListener(MouseEvent.CLICK, ClickFicheHandler);
					lbLibelle.useHandCursor=true;
					_isSim=false;
				}

				switch (value.S_ID_STATUT)
				{
					case 4:
						setStyle("backgroundColor", 0xFEC46D); // orange 
						setStyle("backgroundAlpha", 1);
						break;

					case 3:
						setStyle("backgroundColor", 0xFE9999); // rouge
						setStyle("backgroundAlpha", 1);
						break;

					default:
						setStyle("backgroundColor", 0xFFFFFF);
						setStyle("backgroundAlpha", 0);
						break;
				}
			}
		}

		/**
		 * Cette fonction est appelée lors d'un clique sur l'image de l'item renderer,
		 * elle place les informations concernant le menu de cet item et
		 * appelle la fonction qui affiche se dernier.
		 */
		private function lineSelected(evt:Event):void
		{
			/** on met a jour les information de la ligne selectionnée */
			SpecificationVO.getInstance().elementDataGridSelected=evt.currentTarget.owner.data;
			var ligne:Object=evt.currentTarget.owner.data;
			var Context:SpecificationVO = SpecificationVO.getInstance();
			
			if((Context.validSelectedIdPool == SpecificationVO.POOL_TOUT_PARC_ID) ) //pas de profil == 'Mes pools'
			{
				Context.updateProfilCommande(ligne);//MYT-1606
				Context.idPool = ligne.IDPOOL;
				Context.labelPool = ligne.POOL;
				var userPool:Object = Context.getUserPoolInfo(ligne.IDPOOL);
				if(userPool != null)
				{
					Context.idProfil = userPool.IDPROFIL;
					Context.labelProfil =  userPool.PROFIL;
				}
			}

			this.arrayItemMenu=new Array();

			if (_isSim)
			{
				var myItemMenuContextualOfSim:ItemMenuContextualOfSim=new ItemMenuContextualOfSim();
				this.arrayItemMenu=myItemMenuContextualOfSim.returnArrayItemMenuContextual();
				this.menu=new MenuContextual(this.arrayItemMenu);
			}
			else
			{
				var myItemMenuContextualOfExt:ItemMenuContextualOfExt=new ItemMenuContextualOfExt();
				this.arrayItemMenu=myItemMenuContextualOfExt.returnArrayItemMenuContextual();
				this.menu=new MenuContextual(this.arrayItemMenu);
			}
			clickIconActionHandler(); /** afficher le menu contextuel */
		}

		override protected function setFiche():void
		{
			this.fiche=new FicheSimIHM();
			(this.fiche as FicheSimIHM).nomPrenomCollab=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR;
			(this.fiche as FicheSimIHM).matriculeCollab=SpecificationVO.getInstance().elementDataGridSelected.E_MATRICULE;
			(this.fiche as FicheSimIHM).idGpeClient=CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			(this.fiche as FicheSimIHM).idSousTete=SpecificationVO.getInstance().elementDataGridSelected.IDSOUS_TETE;
			(this.fiche as FicheSimIHM).idSim=SpecificationVO.getInstance().elementDataGridSelected.IDSIM;
			(this.fiche as FicheSimIHM).ID=SpecificationVO.getInstance().elementDataGridSelected.ID;
			if (_isSim)
			{
				(this.fiche as FicheSimIHM).lblFormItemSimExt=resourceManager.getString('M111', 'N__sim__');
			}
			else
			{
				(this.fiche as FicheSimIHM).lblFormItemSimExt=resourceManager.getString('M111', 'N__ext__');
			}
		}
	}
}
