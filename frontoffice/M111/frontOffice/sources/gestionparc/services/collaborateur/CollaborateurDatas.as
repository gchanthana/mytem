package gestionparc.services.collaborateur
{
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.core.Application;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewAlert;
	
	import gestionparc.entity.CollaborateurVO;
	import gestionparc.entity.Matricule;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;

	/**
	 * Traite les données reçues aux retours des procedures appelées dans <code>CollaborateurServices</code>. * 
	 * @see gestionparc.services.collaborateur.CollaborateurServices
	 */
	[Bindable]
	public class CollaborateurDatas extends EventDispatcher
	{
		public static var matriculeIsActif	:int = -1;
		
		private var _collaborateur			: CollaborateurVO = new CollaborateurVO();
		private var _listeEqp				:ArrayCollection = new ArrayCollection();
		private var _listeEqpFixe			:ArrayCollection = new ArrayCollection();
		private var _listeSim				:ArrayCollection = new ArrayCollection();
		private var _listeLigne				:ArrayCollection = new ArrayCollection();
		private var _matricule				:Matricule = new Matricule();
		private var _listeManagers			:ArrayCollection = new ArrayCollection();
		private var _nbreTotalManagers		:Number = 0;
		private var _listeValeursPerso:ArrayCollection = new ArrayCollection();
		
		/**
		 * Traite les données reçues au retour de la procedure <code>getInfosFicheCollaborateur</code>
		 * en appellant la fonction <code>fillCollaborateurVO()</code> pour affecter a la data "collaborateur" toutes les données concernant :
		 * - Ses informations personnelles.
		 * - Ses terminaux.
		 * - Ses lignes et SIM.
		 * - Ses organisations.
		 * @see gestionparc.services.collaborateur.CollaborateurServices#getInfosFicheCollaborateur()
		 * @see gestionparc.entity.CollaborateurVO#fillCollaborateurVO()
		 * 
		 */	
		public function treatDataGetInfosFicheCollaborateur(obj:Object):void
		{
			collaborateur = new CollaborateurVO();
			collaborateur = collaborateur.fillCollaborateurVO(obj);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_COLLAB_LOADED));
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Dispatch un événements pour signaler que la mise à jour des informations
		 * concernant la fiche collaborateur s'est bien déroulée.
		 */
		public function treatSetInfosFicheCollaborateur():void
		{
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_COLLAB_UPLOADED));
		}
		
		public function treatSetInfosManagerFicheCollaborateur():void
		{
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_COLLAB_MANAGER_UPLOADED));
		}
		
		/**
		 * Dispatch un événements pour signaler que l'ajout du nouveau
		 * collaborateur s'est bien déroulée.
		 */
		public function treatAddNewCollaborateur():void
		{
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.NEW_COLLAB_ADDED));
		}
		public function treatinitDepartEntreprise(obj:Object):void
		{
			if(obj.EQP != null
				&& obj.EQP is ArrayCollection)
				listeEqp = obj.EQP;
//			listeLigne = obj.LIGNE;
			if(obj.SIM != null
				&& obj.SIM is ArrayCollection)
				listeSim = obj.SIM;
			if(obj.EQPFIXE != null
				&& obj.EQPFIXE is ArrayCollection)
				listeEqpFixe = obj.EQPFIXE;
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INIT_DEPART_ENTREPRISE_LOADED));
		}
		public function treatvaliderDepartEntreprise(obj:Object):void
		{
			refreshData();
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'D_part_enregistr_'),Application.application as DisplayObject);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.ASSOCIATION_REALIZED));
		}
		private function refreshData():void
		{
			SpecificationVO.getInstance().boolHasChanged = true;
			SpecificationVO.getInstance().refreshData();
		}
		/**
		 * Traite les données reçues au retour de la procedure <code>getListLigneSimEqptFromCollab()</code> 
		 * en appellant la fonction <code>fillListLigneSimEqpt()</code> pour affecter a la data 
		 * "collaborateur.listeLigneSim" la liste des nouvelles informations sur les Ligne-Sim-Term de ce collaborateur
		 * Dispatch un événements pour signaler que la liste des liaisons Ligne-Sim-Term de ce collaborateur a été mise à jour.
		 * @see gestionparc.services.collaborateur.CollaborateurServices#getListLigneSimEqptFromCollab()
		 * @see gestionparc.entity.CollaborateurVO#fillListLigneSimEqpt()
		 * 
		 */
		public function treatGetListLigneSimEqptFromCollab(objResult:Object):void
		{
			collaborateur.fillListLigneSimEqpt(objResult);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LIST_LINK_LINE_SIM_TERM_LOADED));
		}
		
		/**
		 * Traite les données reçues au retour de la procedure <code>getListEqptFromCollab()</code> 
		 * en appellant la fonction <code>fillListTerm()</code> pour affecter a la data 
		 * "collaborateur.listeTerm" la liste des nouvelles informations sur les terminaux de ce collaborateur
		 * Dispatch un événements pour signaler que la liste des terminaux de ce collaborateur a été mise à jour.
		 * @see gestionparc.services.collaborateur.CollaborateurServices#getListEqptFromCollab()
		 * @see gestionparc.entity.CollaborateurVO#fillListTerm()
		 * 
		 */
		public function treatGetListEqptFromCollab(objResult:Object):void
		{
			collaborateur.fillListTerm(objResult);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LIST_TERM_COLLAB_LOADED));
		}

		/**
		 * Fonction appellée au retour de la fonction <code>getMatriculeAuto()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatGetMatriculeAuto()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * @see gestionparc.services.collaborateur.CollaborateurServices#getMatriculeAuto()
		 * @see gestionparc.services.collaborateur.CollaborateurDatas#treatGetMatriculeAuto()
		 */	
		public function treatGetMatriculeAuto(objResult:Object):void
		{
			_matricule = new Matricule();
			
			matricule = Matricule.mapping(objResult);
			
			matriculeIsActif = matricule.IS_ACTIF;
			
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.MATRICULE_AUTO));
		}
		
		public function treatGetManagers(objResult:Object):void
		{
			listeManagers.source = [];
			var listResult:ArrayCollection = objResult as ArrayCollection;
			
			for each (var objManager:Object in listResult) 
			{
				var manager:CollaborateurVO = new CollaborateurVO()
					manager.fillManager(objManager);
					listeManagers.addItem(manager);
			}
			if(listResult.length > 0)
				nbreTotalManagers = listResult[0].NB_ROWN;
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_MANAGERS));
		}
		
		public function treatGetValeursChampPerso(objResult:Object):void
		{
			listeValeursPerso.source = [];
			//listeValeursPerso = objResult as ArrayCollection;
			for each (var obj:Object in objResult)
			{
				if(obj.C1 != null)
					listeValeursPerso.addItem({LIBELLE:obj.C1});
				if(obj.C2 != null)
					listeValeursPerso.addItem({LIBELLE:obj.C2});
				if(obj.C3 != null)
					listeValeursPerso.addItem({LIBELLE:obj.C3});
				if(obj.C4 != null)
					listeValeursPerso.addItem({LIBELLE:obj.C4});
					
			}
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_VALEURS_CHAMPS_PERSO));
		}

		public function set matricule(value:Matricule):void
		{
			_matricule = value;
		}
		
		public function get matricule():Matricule
		{
			return _matricule;
		}
		
		public function set collaborateur(value:CollaborateurVO):void
		{
			_collaborateur = value;
		}
		
		public function get collaborateur():CollaborateurVO
		{
			return _collaborateur;
		}

		public function get listeEqp():ArrayCollection
		{
			return _listeEqp;
		}

		public function set listeEqp(value:ArrayCollection):void
		{
			_listeEqp = value;
		}

		public function get listeSim():ArrayCollection
		{
			return _listeSim;
		}

		public function set listeSim(value:ArrayCollection):void
		{
			_listeSim = value;
		}

		public function get listeLigne():ArrayCollection
		{
			return _listeLigne;
		}

		public function set listeLigne(value:ArrayCollection):void
		{
			_listeLigne = value;
		}

		public function get listeEqpFixe():ArrayCollection
		{
			return _listeEqpFixe;
		}
		
		public function set listeEqpFixe(value:ArrayCollection):void
		{
			_listeEqpFixe = value;
		}

		public function get listeManagers():ArrayCollection { return _listeManagers; }
		
		public function set listeManagers(value:ArrayCollection):void
		{
			if (_listeManagers == value)
				return;
			_listeManagers = value;
		}
		
		public function get nbreTotalManagers():Number { return _nbreTotalManagers; }
		
		public function set nbreTotalManagers(value:Number):void
		{
			if (_nbreTotalManagers == value)
				return;
			_nbreTotalManagers = value;
		}
		
		public function get listeValeursPerso():ArrayCollection { return _listeValeursPerso; }
		
		public function set listeValeursPerso(value:ArrayCollection):void
		{
			if (_listeValeursPerso == value)
				return;
			_listeValeursPerso = value;
		}
		
	}
}