package gestionparcmobile.resiliationLigne.validation
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.List;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.services.Formator;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class ReengagementImpl extends Box
	{
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		
		public var listConfig			:List;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var _myElements			:ElementsCommande2 = new ElementsCommande2();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var myItems				:ArrayCollection = new ArrayCollection();

		public var TOTAL				:String = "0.00";
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
				
		public function ReengagementImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		public function setMyDataToGrid():void
		{
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			
			callLater(setDataToGrid);
		}
		
		public function validateCurrentPage():Boolean
		{
			return true;
		}
		
		public function reset():void
		{
			myItems.removeAll();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHM
		//--------------------------------------------------------------------------------------------//	
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			listConfig.addEventListener('ERASE_LIGNE_SELECTED', eraseLigneSelectedHandler);
		}
		
		private function showHandler(fe:FlexEvent):void
		{
		}

		private function eraseLigneSelectedHandler(e:Event):void
		{
			if(e.target.hasOwnProperty('myData'))
			{
				var element	:Object = e.target.myData;
				var lenOptR	:int = _myElements.LINEOPTIONS.length;
				var idsstt	:int = element.IDSOUSTETE;
				var idcata	:int = element.IDCATALOGUE;
				var idinv	:int = element.IDINVENTAIRE;
				
				for(var i:int = 0;i < lenOptR;i++)
				{
					if(_myElements.LINEOPTIONS[i].IDSOUSTETE == idsstt && _myElements.LINEOPTIONS[i].IDCATALOGUE == idcata && _myElements.LINEOPTIONS[i].IDINVENTAIRE == idinv)
					{
						_myElements.LINEOPTIONS.removeItemAt(i);
						
						return;
					}
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//	
		
		private function setDataToGrid():void
		{
			myItems.removeAll();

			var elegibilite	:Object = calculNombreLignesEligible();

			addEquipements(_myElements, elegibilite.LINESELIGIBLE, elegibilite.LINESNONELIGIBLE);
		}
		
		private function addEquipements(value:ElementsCommande2, nbEligible:int, nbNonEligible:int):void
		{
			var equi	:ArrayCollection = new ArrayCollection();
			var elts	:Object = new Object();
			var lenTer	:int = value.TERMINAUX.length;
			var lenAcc	:int = value.ACCESSOIRES.length;
			var i		:int = 0;
			var prixN	:Number = 0;
			var prixC	:Number = 0;
			
			
			for(i = 0;i < lenTer;i++)//--> TERMINAUX
			{
				if(value.TERMINAUX[i].IDEQUIPEMENT > 0)
				{
					elts 			= new Object();
					elts.EQUIPEMENT = value.TERMINAUX[i].EQUIPEMENT;
					elts.IMEI		= (value.TERMINAUX[i].hasOwnProperty("IMEI"))? "   " + value.TERMINAUX[i].IMEI : "";
					elts.LIBELLE 	= (value.TERMINAUX[i].LIBELLE + "  " + elts.IMEI).toString().replace(" ","");
					elts.REFERENCE_PRODUIT = value.TERMINAUX[i].REF_REVENDEUR;
					elts.IDEQUIPMT	= value.TERMINAUX[i].IDEQUIPEMENT;
					elts.MENSUEL	= "";
					elts.PRICE		= Number(value.TERMINAUX[i].PRIX);
					elts.PRICECAT	= Number(value.TERMINAUX[i].PRIXC);
					elts.PRIX		= Formator.formatTotalWithSymbole(value.TERMINAUX[i].PRIX);
					elts.PRIXCAT	= Formator.formatTotalWithSymbole(value.TERMINAUX[i].PRIXC);
					elts.EDITABLE	= (moduleCommandeMobileIHM.userAcess.hasOwnProperty('E_PRIX_T'))? (moduleCommandeMobileIHM.userAcess.E_PRIX_T == 1) : false;
					
					prixN  = prixN + value.TERMINAUX[i].PRIX;
					prixC  = prixC + value.TERMINAUX[i].PRIXC;
					
					equi.addItem(elts);
				}
			}
			
			for(i = 0;i < lenAcc;i++)//--> ACCESSOIRES
			{
				elts 			= new Object();
				elts.EQUIPEMENT = value.ACCESSOIRES[i].EQUIPEMENT;
				elts.LIBELLE 	= value.ACCESSOIRES[i].LIBELLE;
				elts.REFERENCE_PRODUIT = value.ACCESSOIRES[i].REF_REVENDEUR;
				elts.MENSUEL	= "";
				elts.PRICE		= Number(value.ACCESSOIRES[i].PRIX);
				elts.PRICECAT	= Number(value.ACCESSOIRES[i].PRIXC);
				elts.PRIX		= Formator.formatTotalWithSymbole(value.ACCESSOIRES[i].PRIX);
				elts.PRIXCAT	= Formator.formatTotalWithSymbole(value.ACCESSOIRES[i].PRIXC);
				elts.EDITABLE	= (moduleCommandeMobileIHM.userAcess.hasOwnProperty('E_PRIX_T'))? (moduleCommandeMobileIHM.userAcess.E_PRIX_T == 1) : false;
				
				prixN  = prixN + value.ACCESSOIRES[i].PRIX;
				prixC  = prixC + value.ACCESSOIRES[i].PRIXC;
				
				equi.addItem(elts);
			}
			
			var prixRessources:Number = addRessources(value, equi);

			addItems(equi, nbEligible, prixN, nbNonEligible, prixC, prixRessources);
		}

		private function addRessources(value:ElementsCommande2, ress:ArrayCollection):Number
		{
			var elts	:Object = new Object();
			var lenOpt	:int = value.OPTIONS.length;
			var i		:int = 0;
			var qte		:int = value.CONFIGURATIONS.length;
			var prix	:Number = 0;
			
			for(i = 0;i < lenOpt;i++)//--> OPTIONS
			{
				elts 			= new Object();
				elts.EQUIPEMENT = value.OPTIONS[i].LIBELLETHEME;
				elts.LIBELLE 	= value.OPTIONS[i].LIBELLE;
				elts.REFERENCE_PRODUIT = value.OPTIONS[i].REFERENCE_PRODUIT;
				elts.QUANTITE 	= qte;
				elts.MENSUEL	= value.OPTIONS[i].PRIX_STRG;
				elts.PRIX		= "";
				
				prix = prix + value.OPTIONS[i].PRIX_UNIT;
				
				ress.addItem(elts);
			}
			
			
			
			return prix;
		}
		
		private function addItems(value:ArrayCollection, nbEligible:int, prixN:Number, nbNonEligible:int, prixC:Number, prixRessources:Number):void
		{
			var obj:Object 	= new Object();
			
			if(nbEligible > 0)
			{
				obj 		= new Object();
				obj.ITEMS 	= value;
				obj.TOTAL	= Formator.formatTotalWithSymbole(nbEligible*(prixN+prixRessources));
				obj.PRIX	= Formator.formatTotalWithSymbole((prixN+prixRessources));
				obj.PRICE	= (prixN+prixRessources);
				obj.EDIT	= (moduleCommandeMobileIHM.userAcess.hasOwnProperty('E_PRIX_T'))? (moduleCommandeMobileIHM.userAcess.E_PRIX_T == 1) : false;
				obj.NB		= nbEligible;
				obj.IS_ELI	= true;
				obj.LIBELLE = ResourceManager.getInstance().getString('M16', '_ligible');
				obj.ROPT	= getLinesEligibleOrNot(1);
				
				myItems.addItem(obj);
			}
			
			if(nbNonEligible > 0)
			{
				obj 		= new Object();
				obj.ITEMS 	= value;
				obj.TOTAL	= Formator.formatTotalWithSymbole(nbNonEligible*(prixC+prixRessources));
				obj.PRIX	= Formator.formatTotalWithSymbole((prixC+prixRessources));
				obj.PRICE	= (prixC+prixRessources);
				obj.EDIT	= (moduleCommandeMobileIHM.userAcess.hasOwnProperty('E_PRIX_T'))? (moduleCommandeMobileIHM.userAcess.E_PRIX_T == 1) : false;
				obj.NB		= nbNonEligible;
				obj.IS_ELI	= false;
				obj.LIBELLE = ResourceManager.getInstance().getString('M16', 'non__ligible');
				obj.ROPT	= getLinesEligibleOrNot(0);
				
				myItems.addItem(obj);
			}
		}
		
		private function getLinesEligibleOrNot(isEligible:int):ArrayCollection
		{
			var lines	:ArrayCollection = new ArrayCollection();
			var elts	:Object = new Object();
			var lenOptR	:int = _myElements.LINEOPTIONS.length;
			
			for(var i:int = 0;i < lenOptR;i++)
			{
				if(_myElements.LINEOPTIONS[i].ISELIGIBLE == isEligible)
				{
					elts 				= new Object();
					elts.LIGNE 			= _myElements.LINEOPTIONS[i].SOUSTETE;
					elts.LIBELLE 		= _myElements.LINEOPTIONS[i].LIBELLE;
					elts.ACTION			= _myElements.LINEOPTIONS[i].LIB_ACTION;
					elts.IDSOUSTETE		= _myElements.LINEOPTIONS[i].IDSOUSTETE;
					elts.IDCATALOGUE	= _myElements.LINEOPTIONS[i].IDCATALOGUE;
					elts.IDINVENTAIRE	= _myElements.LINEOPTIONS[i].IDINVENTAIRE;
					elts.IDPRODUIT		= _myElements.LINEOPTIONS[i].IDPRODUIT;
					
					lines.addItem(elts);
				}
			}
			
//			lines = Formator.sortFunction(lines, 'LIGNE');
			
			return lines;
		}
		
		private function calculNombreLignesEligible():Object
		{
			var eligi	:Object = new Object();
			var len		:int = _myElements.CONFIGURATIONS.length;
			var nbEl	:int 	= 0;
			var nbNEl	:int 	= 0;
			
			for(var i:int = 0;i < len;i++)
			{
				if(int(_myElements.CONFIGURATIONS[i].ELIGIBILITE) == 0)
					nbNEl++;
				else
					nbEl++;
			}
			
			eligi.LINESELIGIBLE  	= nbEl;
			eligi.LINESNONELIGIBLE 	= nbNEl;
			
			return eligi;		
		}

	}
}
