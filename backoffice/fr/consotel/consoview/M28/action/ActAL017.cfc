<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" output="false" hint="retourne la liste des intervenants du partenaire triée par intervenants principaux et date de validité du plus récent au plus ancien" author="Florian">
	
	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="contient IDPARTENAIRE">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_partenaire.List_Formation">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.IDPARTENAIRE#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#getCodeLangue()#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
				
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AL017", 1, data)/>
			<cfelseif isnumeric(p_retour)>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL017", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AL017", 0, ThrowCfCatchError("AL017",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>