<cfcomponent author="Cedric" displayname="fr.consotel.api.remoting.WSProvider" extends="fr.consotel.api.remoting.RemoteService"
hint="Implémentation abstraite d'un fournisseur de Web Services et qui gère le controle de l'authentification avec une implémentation de type fr.consotel.api.auth.AuthManager
<br>La méthode getWSProvider() doit être généralement utilisée pour instancier les implémentations de cette classe au lieu de getFactory()">
	<cffunction access="public" name="getWSProvider" returntype="fr.consotel.api.remoting.WSProvider"
	hint="Cette méthode n'est pas implémentée et lève une exception. Elle doit être redéfinie pour retourner une instance de cette classe">
		<cfargument name="authCredentials" type="Struct" required="true" hint="Credentials d'authentification">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette classe ne peut pas être instanciée">
	</cffunction>
	
	<cffunction access="public" name="isAuthenticated" returntype="boolean" hint="Retourne TRUE si les credentials sont authentifiés sinon appele authLogout() puis retourne FALSE">
		<cfset var authStatus=getAuthManager().isAuthValid(getAuthCredentials())>
		<cfif NOT authStatus>
			<cfset authLogout()>
		</cfif>
		<cfreturn authStatus>
	</cffunction>
	
	<cffunction access="public" name="authLogout" returntype="void"
	hint="Cette méthode permet d'effectuer l'opération de déconnexion des services. Cette implémentation réinitalise la valeur des credentials utilisés pour les Web Services">
		<cfset SUPER.setServiceCredentials("","")>
	</cffunction>
	
	<cffunction access="private" name="getAuthManager" returntype="fr.consotel.api.auth.AuthManager"
	hint="Retourne l'implémentation utilisée pour controler l'authentification des credentials fournis à l'instanciation">
		<cfif structKeyExists(VARIABLES,"AUTH_MANAGER")>
			<cfreturn VARIABLES["AUTH_MANAGER"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'implémentation AuthManager utilisée pour controler l'authentification n'est pas définie">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="setAuthManager" returntype="void"
	hint="Définit l'implémentation authManager utilisée par cette classe pour controler l'authentification des credentials fournis à l'instanciation">
		<cfargument name="authManager" type="fr.consotel.api.auth.AuthManager" required="true" hint="Implémentation utilisée pour controler l'authentification des credentials fournis">
		<cfset VARIABLES["AUTH_MANAGER"]=ARGUMENTS["authManager"]>
	</cffunction>
	
	<cffunction access="private" name="getAuthCredentials" returntype="Struct" hint="Retourne les credentials d'authentification fournis à l'instanciation">
		<cflock scope="REQUEST" type="readonly" timeout="300" throwontimeout="TRUE">
			<cfif structKeyExists(VARIABLES,"AUTH_CREDENTIALS")>
				<cfreturn VARIABLES["AUTH_CREDENTIALS"]>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Les credentials d'authentification ne sont pas définis">
			</cfif>
		</cflock>
	</cffunction>

	<cffunction access="private" name="setAuthCredentials" returntype="void" hint="Définit les credentials d'authentification qui seront validés avec l'implémentation getAuthManager()">
		<cfargument name="authCredentials" type="Struct" required="true" hint="Credentials d'authentification dont le contenu doit respecter les spécification de l'implémentation getAuthManager()">
		<cflock scope="REQUEST" type="exclusive" timeout="300" throwontimeout="TRUE">
			<cfset var userCredentials=ARGUMENTS["authCredentials"]>
			<cfset VARIABLES["AUTH_CREDENTIALS"]=userCredentials>
		</cflock>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="Struct" hint="Retourne une structure dans laquelle chaque clé identifie une instance d'un Web Service">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]={}>
		</cfif>
		<cfreturn VARIABLES["WS_REPOSITORY"]>
	</cffunction>
</cfcomponent>