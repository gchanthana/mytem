<cfcomponent displayname="fr.consotel.consoview.M21.WorkflowService" hint="Gestion du worflow de commande" output="false">

	<cffunction name="getInfosClotureAutomatique" access="remote" returntype="Any" output="false" hint="SET LES INFOS DE CLOTURE AUTO">
		
			<cfset idracine = session.perimetre.id_groupe>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m21.GetNbJourClotCommande">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#"/>
				<cfprocresult name="p_retour"/>
			</cfstoredproc>
			
		<cfreturn p_retour/>
	</cffunction>

	<cffunction name="setInfosClotureAutomatique" access="remote" returntype="Numeric" output="false" hint="SET LES INFOS DE CLOTURE AUTO">
		<cfargument name="isauto" 	required="true" type="Numeric"/>
		<cfargument name="nbrjours" required="true" type="Numeric"/>
		
			<cfset idracine = session.perimetre.id_groupe>

			<cfif isauto EQ 0>
			
				<cfset nbrjours = 0>
			
			</cfif>


			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m21.UpdateNbJourClotCommande">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_nbrjours" value="#nbrjours#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour/>
	</cffunction>

</cfcomponent>