package univers.facturation.suivi.controles.services
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	public class CommandeHandler
	{
		private var _model:CommandeModel;
		
		public function CommandeHandler(model:CommandeModel):void
		{
			_model = model;
		}
		
		public function getCommandeInfoGeneraleHandler(event:ResultEvent):void
		{
			_model.updateCommande(event.result as Array);
		}
		
	}
}