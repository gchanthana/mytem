package univers.facturation.suivi.controles.vo
{
	import composants.util.DateFunction;
	
	import mx.formatters.Formatter;

	[Bindable]
	public class ProduitVO
	{	
		
		public var compte:String="";
		public var sousCompte:String="";
		public var feuille:String="";
		public var idProduit:Number=0;
		public var libelleProduit:String="";
		public var typeProduit:String="";
		public var boolAcces:Number=0;
		public var etatLigne:String="";
		public var ligne:String="";
		public var idSousTete:Number=0;
		public var quantite:Number=0;
		public var actGestion:Number=0;
		public var dateSortie:Date=null;
		public var dateEntree:Date=null;
		public var idCommande:Number=0;
		public var cfCommande:String="";
		public var montant:Number=0;
		public var statut:Number=0;
		public var selected:Boolean=false;
		
		public function ProduitVO()
		{
		}
		
		public function addProduit(value:Object):void
		{
			
			if(value.COMPTE_FACTURATION != null) this.compte = value.COMPTE_FACTURATION;
			if(value.SOUS_COMPTE != null) this.sousCompte = value.SOUS_COMPTE;
			this.feuille = value.FEUILLE;
			this.idProduit = value.IDINVENTAIRE_PRODUIT;
			this.libelleProduit = value.LIBELLE_PRODUIT;
			this.typeProduit = value.TYPE_THEME;
			if(value.BOOL_ACCES != null) this.boolAcces = value.BOOL_ACCES;
			if(value.ETAT_LIGNE != null) this.etatLigne = value.ETAT_LIGNE;
			this.ligne = value.SOUS_TETE;
			this.idSousTete = value.IDSOUS_TETE;
			this.quantite = value.QTE;
			this.actGestion = value.NB_OPERATIONS_EN_COURS;
			this.dateEntree = value.DATE_ENTREE;
			this.dateSortie = value.DATE_SORTIE;
			if(value.IDCOMMANDE != null) this.idCommande = value.IDCOMMANDE;
			if(value.CF_COMMANDE != null) this.cfCommande = value.CF_COMMANDE;
			this.montant = value.MONTANT;
			this.statut = value.STATUT_IPR;
				
		}
		
		public function check(select:Boolean):void{
			this.selected = select;
		}
		
	}
}