package utils.selector.uniteselector
{
	import event.selector.UniteEvent;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.List;
	import mx.events.FlexEvent;
	
	import utils.CreateShadow;
	import utils.SelectorAccueilAbstract;
	
	import vo.selector.Parametre;
	import vo.selector.ValeurSelectorVo;

	public class UniteSelectorImpl extends SelectorAccueilAbstract
	{
		[Bindable]
		public var unites:ArrayCollection;

		private var _shadow:CreateShadow;

		[Bindable]
		public var heigthUnite:Number=70;

		public var listUnite:List;

		public function UniteSelectorImpl(selector:Object=null)
		{
			super(selector);
			addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
		}

		private function onCreationComplete(e:FlexEvent):void
		{
			_shadow=new CreateShadow(0xC3C8CC, 0.5, 2, 2, false, 2.8, 10);
			_shadow.addShadow(this); /** ajouter n shadow */
			listUnite.styleName="M54MenuSegmentWidget";
			
			if(unites!=null)
			{
				if (unites.length == 2)
				{
					heigthUnite=46;
				}
				else if (unites.length == 3)
				{
					heigthUnite=70;
				}
			}
		}

		/**
		 * permet de dispatcher un event pour recupérer la data de widget
		 * @param e
		 */
		protected function listUnite_clickHandler(e:MouseEvent):void
		{
			dispatchEvent(new UniteEvent(UniteEvent.VALIDER_VALUE_UNITE_EVENT)); /** capturé dans widgetAbstract*/
		}

		/**
		 * récupérer la valeur selectionné par l'utilisateur
		 * @return
		 */
		override public function getSelectedValue():Array
		{
			var tabValeur:Array=[];
			var param:Parametre=new Parametre();

			if (listUnite != null)
			{
				if (listUnite.selectedItem != null)
				{
					param.value=[(listUnite.selectedItem as ValeurSelectorVo).id];
					param.cle=arrayCleLibelle[0].NOM_CLE;
					param.libelle=arrayCleLibelle[0].LIBELLE_CLE;
					tabValeur[0]=param;
				}
			}
			else
			{
				param.value=[defaultValue];
				param.cle=arrayCleLibelle[0].NOM_CLE;
				param.libelle=arrayCleLibelle[0].LIBELLE_CLE;
				tabValeur[0]=param;
			}
			return tabValeur;
		}
	}
}
