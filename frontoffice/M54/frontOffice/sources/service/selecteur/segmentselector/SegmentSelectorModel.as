package service.selecteur.segmentselector
{
	import event.selector.SegmentEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import vo.selector.ValeurSelectorVo;

	internal class SegmentSelectorModel  extends EventDispatcher
	{
		private var _segments :ArrayCollection;
		
		public function SegmentSelectorModel():void
		{
		}
		public function get segments():ArrayCollection
		{
			return _segments;
		}
		
		internal function updateValues(value:ArrayCollection):void
		{	
			_segments=new ArrayCollection();
			
			var valeurSelector:ValeurSelectorVo;
			
			for(var i:int=0;i<value.length;i++)
			{
				valeurSelector = new ValeurSelectorVo();
				valeurSelector.fill(value[i],i);
				_segments.addItem(valeurSelector); 
			}
			this.dispatchEvent(new SegmentEvent(SegmentEvent.CHANGER_VALUE_SEGMENT_EVENT));/** capturé dans SegmentSelectorImpl */
		}
	}
}