<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.management.WSDeviceService" implements="fr.consotel.api.mdm.partner.private.ws.IWSDeviceService"
hint="Implémentation fournissant les méthodes du Service EveryWanDevice. Les méthodes de cette implémentation sont soumises à la vérification de l'authentification
<br><b>Pour des raisons de performance : Certaines méthodes lèvent une exception provenant du Web Service lorsqu'aucun équipement ne correspond aux infos fournies</b>
<br><i>Les méthodes deviceExists() et getAllDevices() peuvent être utilisées pour vérifier l'existence d'un équipement avant d'appeler ces méthodes</i>">
	<cffunction access="public" name="getMasterKeyList" returntype="Array" hint="Retourne la liste des clés master (Master Keys)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfset var masterKeys=deviceService.getMasterKeyList(ARGUMENTS["serial"],ARGUMENTS["imei"])>
			<cfreturn masterKeys>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="clearDeploymentHisto" returntype="Numeric" hint="Purge l'historique de déploiement. La valeur retournée n'est pas documentée par Zenprise">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfset var clearCode=deviceService.clearDeploymentHisto(ARGUMENTS["serial"],ARGUMENTS["imei"])>
			<cfreturn clearCode>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="getDeploymentHisto" returntype="Array"
	hint="Retourne l'historique de déploiement par ordre chronologique. Chaque élément est une implémentation Zenprise : BeanService.DeploymentEvent">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfset var deploymentHistory=deviceService.getDeploymentHisto(ARGUMENTS["serial"],ARGUMENTS["imei"])>
			<cfreturn deploymentHistory>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="getLastUser" returntype="String" hint="Retourne le login du dernier utilisateur Zenprise de l'équipement ou une chaine vide s'il n'est pas défini">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfreturn deviceService.getLastUser(ARGUMENTS["serial"],ARGUMENTS["imei"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="getManagedStatus" returntype="String" hint="Retourne la valeur ZDM pour un équipement géré par le MDM et UNMANAGED sinon">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfreturn deviceService.getManagedStatus(ARGUMENTS["serial"],ARGUMENTS["imei"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="authorize" returntype="Boolean" hint="Effectue l'opération AUTHORIZE pour l'équipement existant correspondant aux paramètres fournis<br>
	La valeur retournée n'est pas spécifiée par la documentation Zenprise">
		<cfargument name="serial" type="String" required="true" hint="Serial de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfreturn deviceService.authorize(ARGUMENTS["serial"],asJavaNullValue(ARGUMENTS["imei"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="deploy" returntype="Boolean"
	hint="Effectue une demande de déploiement d'Android/Windows pour un équipement connecté. Envoie un notification PUSH pour iOS<br>
	Retourne FALSE si l'équipement n'est pas connecté ou non managé et TRUE sinon">
		<cfargument name="serial" type="String" required="true" hint="Serial de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfreturn deviceService.deploy(ARGUMENTS["serial"],asJavaNullValue(ARGUMENTS["imei"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="corporateDataWipeDevice" returntype="Numeric" hint="Effectue l'opération WIPE pour l'équipement<br>
	Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfreturn VAL(deviceService.corporateDataWipeDevice(ARGUMENTS["serialNumber"],asJavaNullValue(ARGUMENTS["imei"]),ARGUMENTS["waitTime"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="createUser" returntype="String" hint="Crée un utilisateur et retourne son login">
		<cfargument name="group" type="String" required="true" hint="Nom du groupe pour l'utilisateur">
		<cfargument name="login" type="String" required="true" hint="Login pour l'utilisateur">
		<cfargument name="password" type="String" required="true" hint="Mot de passe pour l'utilisateur">
		<cfargument name="role" type="String" required="true" hint="Rôle pour l'utilisateur (i.e : ADMIN, SUPPORT, USER, GUEST)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfreturn deviceService.createUser(ARGUMENTS["group"],ARGUMENTS["login"],ARGUMENTS["password"],ARGUMENTS["role"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="locateDevice" returntype="Numeric" hint="Localise l'équipement.
	Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfreturn VAL(deviceService.locateDevice(ARGUMENTS["serialNumber"],asJavaNullValue(ARGUMENTS["imei"]),ARGUMENTS["waitTime"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="putDeviceProperties" returntype="Void" hint="Effectue l'opération putDeviceProperties() du service DeviceService Zenprise">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="properties" type="Array" required="true"
		hint="Tableau où chaque élément est une structure contenant les clés suivantes : NAME (Nom de la propriété), VALUE (Valeur de la propriété)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfset deviceService.putDeviceProperties(ARGUMENTS["serialNumber"],asJavaNullValue(ARGUMENTS["imei"]),ARGUMENTS["properties"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="registerDeviceForUser" returntype="String"
	hint="Spécifie que l'équipement est utilisé par l'utilisateur. Crée l'équipement s'il n'existe pas et retourne la valeur STRONGID associé à l'équipement">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensitive (i.e : iOS, WINDOWS, ANDROID)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="userIdentifier" type="String" required="true" hint="Identifiant utilisateur">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfreturn deviceService.registerDeviceForUser(ARGUMENTS["osFamily"],ARGUMENTS["serialNumber"],asJavaNullValue(ARGUMENTS["imei"]),ARGUMENTS["userIdentifier"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="resetDeploymentState" returntype="Void" hint="Effectue l'opération resetDeploymentState() pour l'équipement">
		<cfargument name="serial" type="String" required="true" hint="Serial de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfset deviceService.resetDeploymentState(ARGUMENTS["serial"],asJavaNullValue(ARGUMENTS["imei"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="deviceExists" returntype="boolean" hint="Retourne TRUE si un équipement correspondant aux paramètres fournis existe">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfreturn deviceService.deviceExists(ARGUMENTS["serialNumber"],asJavaNullValue(ARGUMENTS["imei"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="getAllDevices" returntype="Array" hint="Retourne un tableau contenant la liste de tous les équipements
	Chaque élément du tableau est une implémentation du Web Service correspondant à un équipement (com.sparus.ws.admin.WSDevice)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfreturn deviceService.getAllDevices()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getDeviceInfo" returntype="Any" hint="Retourne l'implémentation des infos concernant un équipement (com.sparus.ws.admin.DeviceInformation)
	<br><b>Lève une exception provenant du Web Service si la valeur imei ne correspond à aucun équipement existant</b>">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfreturn deviceService.getDeviceInfo(ARGUMENTS["serialNumber"],asJavaNullValue(ARGUMENTS["imei"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="lockDevice" returntype="Numeric" hint="Vérrouille l'équipement spécifié avec le nouveau code PIN newPinCode
	Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)
	<br><b>Lève une exception provenant du Web Service si la valeur imei ne correspond à aucun équipement existant</b>">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="newPinCode" type="String" required="true" hint="Nouveau code PIN du vérrouillage (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfset var lockStatusCode=deviceService.lockDevice(
				ARGUMENTS["serialNumber"],asJavaNullValue(ARGUMENTS["imei"]),asJavaNullValue(ARGUMENTS["newPinCode"]),ARGUMENTS["waitTime"]
			)>
			<cfreturn lockStatusCode>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="unlockDevice" returntype="Numeric" hint="Retourne TRUE si un équipement correspondant aux paramètres fournis existe
	Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)
	<br><b>Lève une exception provenant du Web Service si la valeur imei ne correspond à aucun équipement existant</b>">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfset var unlockStatusCode=deviceService.unlockDevice(ARGUMENTS["serialNumber"],asJavaNullValue(ARGUMENTS["imei"]),ARGUMENTS["waitTime"])>
			<cfreturn unlockStatusCode>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="wipeDevice" returntype="Numeric" hint="Effectue l'opération WIPE pour l'équipement existant correspondant aux paramètres fournis
	Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="erasedMemoryCard" type="Boolean" required="true" hint="TRUE pour supprimer le contenu de la carte mémoire">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfset var wipeDeviceStatus=deviceService.wipeDevice(
				ARGUMENTS["serialNumber"],asJavaNullValue(ARGUMENTS["imei"]),ARGUMENTS["erasedMemoryCard"],ARGUMENTS["waitTime"]
			)>
			<cfreturn wipeDeviceStatus>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="revokeDevice" returntype="Boolean" hint="Effectue l'opération REVOKE pour l'équipement existant correspondant aux paramètres fournis">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<!--- Version 6.1.8
		<cfreturn deviceService.revokeDevice(ARGUMENTS["serialNumber"],asJavaNullValue(ARGUMENTS["imei"]))>
		--->
		<!--- Version 6.1.9 --->
		<cftry>
			<cfreturn deviceService.revoke(ARGUMENTS["serialNumber"],asJavaNullValue(ARGUMENTS["imei"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removeDevice" returntype="void" hint="Effectue l'opération REMOVE pour l'équipement existant correspondant aux paramètres fournis">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var deviceService=zdmRepository.getDeviceService()>
		<cftry>
			<cfset deviceService.removeDevice(ARGUMENTS["serialNumber"],asJavaNullValue(ARGUMENTS["imei"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSDeviceService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="asJavaNullValue" returntype="Any" hint="Retourne la valeur nulle Java si stringValue est une chaine vide et stringValue sinon">
		<cfargument name="stringValue" type="String" required="true">
		<cfif TRIM(ARGUMENTS["stringValue"]) EQ "">
			<cfreturn javaCast("NULL",0)>
		<cfelse>
			<cfreturn ARGUMENTS["stringValue"]>
		</cfif>
	</cffunction>
</cfcomponent>