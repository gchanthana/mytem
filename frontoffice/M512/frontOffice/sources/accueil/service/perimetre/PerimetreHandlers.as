package accueil.service.perimetre
{
	import accueil.event.AccueilEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.rpc.events.ResultEvent;
	
	public class PerimetreHandlers extends EventDispatcher
	{
		private var _model : PerimetreModel;
		
		public function PerimetreHandlers(modele:PerimetreModel)
		{	
			_model = modele;
		}
		
		internal function setUpPerimetreResultHandler(event:ResultEvent):void
		{	
			if(event.result > 0)
			{
				dispatchEvent(new AccueilEvent(AccueilEvent.PERIMETRE_CHANGE));
			}
			else if(event.result == 0)
			{
				dispatchEvent(new AccueilEvent(AccueilEvent.PERIMETRE_NOT_CHANGE));
			}
			else 
			{
				dispatchEvent(new AccueilEvent(AccueilEvent.PERIMETRE_ERROR));
			}
		}
		internal function getPerimetreResultHandler(event:ResultEvent):void
		{	
			if(event.result != null)
			{
				_model.PerimetreXML = event.result as XML;
				dispatchEvent(new AccueilEvent(AccueilEvent.PERIMETRE_LOADED));
			}
			else 
			{
				_model.PerimetreXML = null;
				dispatchEvent(new AccueilEvent(AccueilEvent.PERIMETRE_LOADED_ERROR));
			}
		}

	}
}