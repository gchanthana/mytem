package univers.facturation.suivi.controles.controls.formatsclients
{
	import mx.resources.ResourceManager;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	//Class permettant d'envoyer et de récupérer tous les logs sur les diverses action des boutons
	public class LogControles extends EventDispatcher
	{
		
//****VARIABLES GLOBALES****//
	
		//Création d'un objet 'LogControl'
		static private var _lgCtrl:LogControl;

//****END VARIABLES GLOBALES****//

//****FONCTIONS PUBLIC****//

		//Permet d'appeler l'objet 'LogControl' sans en créer un autre
		public function LogControles(lgctrl:LogControl)
		{
			_lgCtrl = lgctrl;
		}
		
		//Appel de procédure pour l'envoie des logs Analtytique	(actions sur les boutons)
		public function setLogsControleFacture(IdPeriode:Number,libelleAction:String,commentaire:String):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
										"fr.consotel.consoview.facturation.suiviFacturation.controles.LogsControles",
										"setLogsControleFacture",
										zsetSendLogsControlesResultHandler);
		
			RemoteObjectUtil.callService(op,IdPeriode,_lgCtrl.LogControles_ProfileID,libelleAction,commentaire);
		}
		
		//Traitement de retour procédure de l'envoie des logs (actions sur les boutons) -1->échec, 1->ok
		private function zsetSendLogsControlesResultHandler(re:ResultEvent):void
		{	
		}
	
		//Appel de procédure pour l'envoie des logs Analtytique
		public function setLogsControleFactureAnalytique(ArrayParameters:Array,IdPeriode:Number,libelleAction:String,commentaire:String):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
										"fr.consotel.consoview.facturation.suiviFacturation.controles.LogsControles",
										"setLogsControleFactureAnalytique",
										zsetLogsControleFactureAnalytiqueResultHandler);
		
			RemoteObjectUtil.callService(op,ArrayParameters,IdPeriode,_lgCtrl.LogControles_ProfileID,libelleAction,commentaire);
		}
		
		//Traitement de retour procédure de l'envoie des logs Analytique -1->échec, 1->ok
		private function zsetLogsControleFactureAnalytiqueResultHandler(re:ResultEvent):void
		{
		}
		
		//Appel de procédure permettant de retourner l'idUser connecté
		public function getUserConnectedProfile():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
										"fr.consotel.consoview.facturation.suiviFacturation.controles.LogsControles",
										"getUserConnectedProfile",
										zUserConnectedProfileResultHandler);
		
			RemoteObjectUtil.callService(op);
		}
		
		//Traitement de retour procédure de l'envoie du 'UserId'
		private function zUserConnectedProfileResultHandler(re:ResultEvent):void
		{
			if(re.result!= null)
			{
				_lgCtrl.LogControles_ProfileID = new int(re.result);
			}else{_lgCtrl.LogControles_ProfileID = 0;}
		}
		
		//Appel de procédure permettant d'envoyer la facture vers l'ERP ou non
		public function setDecisionExport(idinventaire:int, etatexport:int, coment:String):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
										"fr.consotel.consoview.facturation.suiviFacturation.controles.LogsControles",
										"getExportDecision",
										zSentDecisionERPExport);
		
			RemoteObjectUtil.callService(op,idinventaire,etatexport,coment);
		}
		
		//Traitement de retour procédure de l'envoie de la décision ERP
		private function zSentDecisionERPExport(re:ResultEvent):void
		{
			if(re.result!= -1 || re.result!= null)
			{
				
			}else{Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur_envoie_Bouton_Etat_Export'), "CONSOVIEW")}
		}
		
		//Appel de procédure permettant de récupérer les différents log pour les affectées à 'dgControler' 
		//('datagrid' dans 'ControleAnalytiqueView.mxml') pour le suivi des factures déja contrôlées
		public function getLogsControleFactureAnalytique(IdPeriode:Number):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
										"fr.consotel.consoview.facturation.suiviFacturation.controles.LogsControles",
										"getLogVisaAnalytique",
										zgetLogsControleFactureAnalytiqueResultHandler);
			
			RemoteObjectUtil.callService(op,IdPeriode);	
		}
		
		//Affecte le résultat de la procédure dans un 'object' (traitement du retour procédure)
		private function zgetLogsControleFactureAnalytiqueResultHandler(re : ResultEvent):void
		{
			if(re.result!=null)
			{
				var tempTabRsltQuery:Object = re.result;
				var i:int = 0;
				
				_lgCtrl.arrayNumber = 0;
				_lgCtrl.ArrayQueryCtrlAna = new ArrayCollection(null);
				var obj:Object = new Object();

				for(i = 0;i < tempTabRsltQuery.length;i++)
				{
					var obje:Object = {LIBELLE_NOM_CLIENT:tempTabRsltQuery[i].NOM,IDGROUPE_CLIENT: tempTabRsltQuery[i].IDORGANISATION,LIBELLE_GROUPE_CLIENT:tempTabRsltQuery[i].NOM_GROUPE, 
										TOTAL_ANALYTIQUE:tempTabRsltQuery[i].MONTANT_ANA,TOTAL_NONAFFECTE:tempTabRsltQuery[i].MONTANT_NA,PERCENT:tempTabRsltQuery[i].POURCENTAGE,
										DATE_MODIFICATION:zformateDate(tempTabRsltQuery[i].DATE_ACTION).date,IDLOG:tempTabRsltQuery[i].IDLOG};
//					var obj:Object = {IDLOG:tempTabRsltQuery[i].IDLOG};
					_lgCtrl.ArrayQueryCtrlAna.addItem(obje);
				}
				dispatchEvent(new Event("goToPrint"));
			}
			else
			{}
		}
		
		//Formate la date en JJ/MM/AAAA
		private function zformateDate(dateToFormat:Object):Object
		{
			var realDate:Object = new Object();
			
			realDate = {date:dateToFormat.date + "/" , month:zformateMonth(dateToFormat.month) + "/" , fullyear:dateToFormat.fullYear};
			
			var dayInString:String = realDate.date.toString();
			var monthInString:String = realDate.month.toString();
			var yearInString:String = realDate.fullyear.toString();
			var realDateInString:String = dayInString + monthInString + yearInString;
			realDate = {date:realDateInString};
			
			return realDate;
		}

		//Formate le mois 0->01(janvier), 1->02(février), ..., 11->12(décembre)
		private function zformateMonth(monthToFormate:int):String
		{
			var month:String = null;
			
			switch (monthToFormate)
			{
					case 0:month = "01";break;
					case 1:month = "02";break;
					case 2:month = "03";break;
					case 3:month = "04";break;
					case 4:month = "05";break;
					case 5:month = "06";break;
					case 6:month = "07";break;
					case 7:month = "08";break;
					case 8:month = "09";break;
					case 9:month = "10";break;
					case 10:month = "11";break;
					case 11:month = "12";break;
			}
			
			return month;
		}

//****END FONCTIONS PUBLIC****//
		
	}
}