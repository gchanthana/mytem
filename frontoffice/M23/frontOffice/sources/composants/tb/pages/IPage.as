package composants.tb.pages
{
	import mx.core.UIComponent;
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	public interface IPage {
				 
		function clean():void;
		function getLibelle():String;
		function getTotal():Number;					
		function goToNextPageHandler(f : Function):void;

		function set nextPageType(t : String):void;		
		function get pageType():String;
				
		function update():void; 
		function updatePeriode(moisDeb : String = null, moisFin : String = null):void;
		function updateSegment(segment : String = ""):void;
		function updatePerimetre(perimetre : String = null, mode : String = null, identifiant : String= null):void;
		function exporterRapport(format: String, rSociale : String = ""):void;	 
	}
} 