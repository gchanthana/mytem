package intranet.M25.entity
{

    [Bindable]
    /**
     * Classe regroupant les Ressources statiques ( icones , images , variables de styles)
     *
     */
    public class StaticData
    {
        // icones
        [Embed(source="/intranet/M25/assets/icones/conso_pic_40.png")]
        public static const consoPicto:Class;
        [Embed(source="/intranet/M25/assets/icones/modifier.png")]
        public static const imgEdit:Class;
        [Embed(source="/intranet/M25/assets/icones/add2.png")]
        public static const imgNew:Class;
        [Embed(source="/intranet/M25/assets/icones/delete2.png")]
        public static const imgDelete:Class;
        //[Embed(source="/intranet/M25/assets/icones/pj2-32.PNG")]
        //[Embed(source="/intranet/M25/assets/icones/pjo_16.PNG")]
        [Embed(source="/intranet/M25/assets/icones/publish.png")]
        public static const imgPJ:Class;
        [Embed(source="/intranet/M25/assets/icones/link.png")]
        public static const imgLink:Class;
        [Embed(source="/intranet/M25/assets/icones/cv.png")]
        public static const imgPJSmall:Class;
        [Embed(source="/intranet/M25/assets/icones/share.png")]
        public static const imgShare:Class;
        // [Embed(source="/intranet/M25/assets/icones/Info-32.PNG")]
        [Embed(source="/intranet/M25/assets/icones/info_16.PNG")]
        public static const imgInfo:Class;
        [Embed(source="/intranet/M25/assets/icones/info_16.PNG")]
        public static const imgInfoSmall:Class;
        [Embed(source="/intranet/M25/assets/icones/process.png")]
        public static const imgActiv:Class;
        [Embed(source="/intranet/M25/assets/icones/csv_icon.png")]
        public static const imgExportXls:Class;
        [Embed(source="/intranet/M25/assets/icones/address.png")]
        public static const imgContact:Class;
        [Embed(source="/intranet/M25/assets/icones/imgOk.gif")]
        public static const imgOk:Class;
        [Embed(source="/intranet/M25/assets/icones/imgKo.gif")]
        public static const imgKO:Class;
        [Embed(source="/intranet/M25/assets/icones/hire-me.png")]
        public static const imgNoValid:Class;
        [Embed(source="/intranet/M25/assets/icones/finished-work.png")]
        public static const imgValid:Class;
        [Embed(source="/intranet/M25/assets/icones/old-versions.png")]
        public static const imgTodo:Class;
        [Embed(source="/intranet/M25/assets/icones/rond_vert.png")]
        public static const imgOn:Class;
        [Embed(source="/intranet/M25/assets/icones/rond_rouge.png")]
        public static const imgOff:Class;
        [Embed(source="/intranet/M25/assets/icones/Alert-32.PNG")]
        public static const imgError:Class;
        [Embed(source="/intranet/M25/assets/icones/Info-32.PNG")]
        public static const imgInfo32:Class;
        // données de style
        public static const fontSizeTitle:int = 13;
        public static const paddingBox:int = 5;
        public static const paddingBox_2:int = 10;
        public static const BG_COLOR_0:uint = 0xDFE8E8;
        public static const BG_COLOR_1:uint = 0xF5F5F5; // pastel
        public static const BG_COLOR:uint = 0xFAFAFA; // gris
        public static const W_CB_SMALL:Number = 80;
        /**
         * boolean masquant certaines parties de l'IHM en
         * fonction des infos dispo concernant le suivi
         * des comptes de facturation
         */
        public static const DATA_CF_AVAILABLE:Boolean = false;

        public function StaticData()
        {
        }
    }
}