<cfcomponent author="Cedric" displayName="fr.saaswedo.api.mdm.impl.zenprise.DeviceInfo" extends="fr.saaswedo.api.mdm.impl.zenprise.MdmService"
implements="fr.saaswedo.api.mdm.core.services.IDeviceInfo" hint="Implémentation Zenprise.
<br>Une instance de ce composant ne référence que les services d'un seul serveur MDM et utilise les mêmes crédentiels pour y accéder">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.zenprise.DeviceInfo">
		<cfreturn "fr.saaswedo.api.mdm.impl.zenprise.DeviceInfo">
	</cffunction>

	<cffunction access="public" name="getAllDevices" returntype="Array">
		<cfset var devices=[]>
		<cftry>
			<cfset var deviceList=[]>
			<!--- Librairie Remoting : SOAP --->
			<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
			<cfset var result=remoting.invoke(getDeviceService(),"getAllDevices",[])>
			<cfif (result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT())>
				<cfthrow type="Custom" message="WebService invocation error" detail="API MDM: getAllDevices()">
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset deviceList=result[remoting.RESULT()]>
			</cfif>
			<cfset var deviceCount=arrayLen(deviceList)>
			<cfloop index="i" from="1" to="#deviceCount#">
				<cfset arrayAppend(devices,{
					SerialNumber=toString(deviceList[i].getSerialNumber()),IMEI=toString(deviceList[i].getImei())
				})>
			</cfloop>
			<cfreturn devices>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH}>
				<cfif isDefined("remoting") AND isDefined("result")>
					<!--- Exception provenant du WebService --->
					<cfif ((result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT()))
						AND structKeyExists(result,remoting.RESULT())>
						<cfset errorLog.EXCEPTION=result[remoting.RESULT()]>
					</cfif>
					<cfif structKeyExists(result,remoting.CLIENT_REQUEST())>
						<cfset errorLog.CLIENT_REQUEST=result[remoting.CLIENT_REQUEST()]>
					</cfif>
					<cfif structKeyExists(result,remoting.SERVER_RESPONSE())>
						<cfset errorLog.SERVER_RESPONSE=result[remoting.SERVER_RESPONSE()]>
					</cfif>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn devices>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="deviceExists" returntype="Boolean" hint="Retourne TRUE le device existe et FALSE sinon">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cftry>
			<cfset var serialValue=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<!--- Librairie Remoting : SOAP --->
			<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
			<cfset var params=[{NAME="serialNumber",VALUE=serialValue},{NAME="imei",VALUE=imeiValue}]>
			<cfset var result=remoting.invoke(getDeviceService(),"deviceExists",params)>
			<cfif (result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT())>
				<cfthrow type="Custom" message="WebService invocation error" detail="API MDM: deviceExists()">
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfreturn result[remoting.RESULT()]>
			<cfelse>
				<cfreturn FALSE>
			</cfif>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=serialValue,IMEI=imeiValue}>
				<cfif isDefined("remoting") AND isDefined("result")>
					<!--- Exception provenant du WebService --->
					<cfif ((result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT()))
						AND structKeyExists(result,remoting.RESULT())>
						<cfset errorLog.EXCEPTION=result[remoting.RESULT()]>
					</cfif>
					<cfif structKeyExists(result,remoting.CLIENT_REQUEST())>
						<cfset errorLog.CLIENT_REQUEST=result[remoting.CLIENT_REQUEST()]>
					</cfif>
					<cfif structKeyExists(result,remoting.SERVER_RESPONSE())>
						<cfset errorLog.SERVER_RESPONSE=result[remoting.SERVER_RESPONSE()]>
					</cfif>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn FALSE>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="getManagedStatus" returntype="String"
	hint="Retourne un des status :<br>- MANAGED() : Enrollé<br>- UNMANAGED() : Non Enrollé">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cftry>
			<cfset var serialValue=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<!--- Librairie Remoting : SOAP --->
			<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
			<cfset var params=[{NAME="serial",VALUE=serialValue},{NAME="imei",VALUE=imeiValue}]>
			<cfset var result=remoting.invoke(getDeviceService(),"getManagedStatus",params)>
			<cfif (result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT())>
				<cfthrow type="Custom" message="WebService invocation error" detail="API MDM: getManagedStatus()">
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfreturn (result[remoting.RESULT()] EQ "ZDM") ? MANAGED():UNMANAGED()>
			<cfelse>
				<cfreturn UNMANAGED()>
			</cfif>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=serialValue,IMEI=imeiValue}>
				<cfif isDefined("remoting") AND isDefined("result")>
					<!--- Exception provenant du WebService --->
					<cfif ((result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT()))
						AND structKeyExists(result,remoting.RESULT())>
						<cfset errorLog.EXCEPTION=result[remoting.RESULT()]>
					</cfif>
					<cfif structKeyExists(result,remoting.CLIENT_REQUEST())>
						<cfset errorLog.CLIENT_REQUEST=result[remoting.CLIENT_REQUEST()]>
					</cfif>
					<cfif structKeyExists(result,remoting.SERVER_RESPONSE())>
						<cfset errorLog.SERVER_RESPONSE=result[remoting.SERVER_RESPONSE()]>
					</cfif>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn UNMANAGED()>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getLastUser" returntype="String" hint="Retourne le login d'enrollment ou une chaine vide s'il n'est pas enrollé">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var lastUser="">
		<cftry>
			<cfset var serialValue=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<cfif deviceExists(serialValue,imeiValue)>
				<!--- Librairie Remoting : SOAP --->
				<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
				<cfset var params=[{NAME="serial",VALUE=serialValue},{NAME="imei",VALUE=imeiValue}]>
				<cfset var result=remoting.invoke(getDeviceService(),"getLastUser",params)>
				<cfif (result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT())>
					<cfthrow type="Custom" message="WebService invocation error" detail="API MDM: getLastUser()">
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset lastUser=toString(result[remoting.RESULT()])>
				</cfif>
			</cfif>
			<cfreturn lastUser>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=serialValue,IMEI=imeiValue}>
				<cfif isDefined("remoting") AND isDefined("result")>
					<!--- Exception provenant du WebService --->
					<cfif ((result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT()))
						AND structKeyExists(result,remoting.RESULT())>
						<cfset errorLog.EXCEPTION=result[remoting.RESULT()]>
					</cfif>
					<cfif structKeyExists(result,remoting.CLIENT_REQUEST())>
						<cfset errorLog.CLIENT_REQUEST=result[remoting.CLIENT_REQUEST()]>
					</cfif>
					<cfif structKeyExists(result,remoting.SERVER_RESPONSE())>
						<cfset errorLog.SERVER_RESPONSE=result[remoting.SERVER_RESPONSE()]>
					</cfif>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn lastUser>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getDeviceInfo" returntype="Struct">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var serialValue=ARGUMENTS.serialNumber>
		<cfset var imeiValue=ARGUMENTS.imei>
		<cfset var mdmDevice={}>
		<cftry>
			<cfif deviceExists(serialValue,imeiValue)>
				<cfset var device="">
				<!--- Librairie Remoting : SOAP --->
				<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
				<cfset var params=[{NAME="serialNumber",VALUE=serialValue},{NAME="imei",VALUE=imeiValue}]>
				<cfset var result=remoting.invoke(getDeviceService(),"getDeviceInfo",params)>
				<cfif (result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT())>
					<cfthrow type="Custom" message="WebService invocation error" detail="API MDM: getDeviceInfo()">
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset device=result[remoting.RESULT()]>
					<!--- GENERAL --->
					<cfset var firstAuth=device.getFirstConnectionDate()>
					<cfset var lastAuth=device.getLastAuthDate()>
					<cfset var mdmDevice.GENERAL={
						Serial_Number=toString(device.getSerialNumber()),IMEI=toString(device.getIMEI()),
						STATUS=getManagedStatus(serialValue,imeiValue),OS="",firstConnectionDate=isDefined("firstAuth") ? firstAuth.getTime():"",
						lastAuthDate=isDefined("lastAuth") ? lastAuth.getTime():""
					}>
					<!--- PROPERTIES --->
					<cfset var deviceProperties=[]>
					<cfset var properties=device.getDeviceProperties()>
					<cfset var propCount=arrayLen(properties)>
					<cfloop index="i" from="1" to="#propCount#">
						<cfset var property=properties[i]>
						<cfset var propName=toString(property.getName())>
						<cfset var propValue=toString(property.getValue())>
						<!--- OS du device --->
						<cfif propName EQ "SYSTEM_PLATFORM">
							<cfif REFindNoCase("ANDROID",propValue) GT 0>
								<cfset propValue=ANDROID()>
							<cfelseif REFindNoCase("IOS",propValue) GT 0>
								<cfset propValue=IOS()>
							</cfif>
							<cfset mdmDevice.GENERAL.OS=propValue>
						</cfif>
						<cfset arrayAppend(deviceProperties,{NAME=propName,VALUE=propValue})>
					</cfloop>
					<!--- Ajout du provider MDM dans les propriétés du device --->
					<cfset arrayAppend(deviceProperties,{NAME="MDM_PROVIDER",VALUE=getMdmServer().getMdmServerInfo().getProvider()})>
					<cfset mdmDevice.PROPERTIES=deviceProperties>
					<!--- SOFTWARE --->
					<cfset var deviceSoftwares=[]>
					<cfset var softwares=device.getSoftwareInventory()>
					<cfset var softwareCount=arrayLen(softwares)>
					<cfloop index="i" from="1" to="#softwareCount#">
						<cfset var software=softwares[i]>
						<cfset var installTS=software.getInstallTimeStamp()>
						<cfset arrayAppend(deviceSoftwares,{
							NAME=toString(software.getName()),AUTHOR=toString(software.getAuthor()),VERSION=toString(software.getversion()),
							SIZE=VAL(toString(software.getSize())),InstallTimeStamp=isDefined("installTS") ? createObject("java","java.util.Date").init():""
						})>
					</cfloop>
					<cfset mdmDevice.SOFTWARE=deviceSoftwares>
				<cfelse>
					<cfset var code=GLOBALS().CONST("Zenprise.ERROR")>
					<cfset var deviceMsg="SERIAL='#serialValue#' and IMEI='#imeiValue#'">
					<cflog type="Warning" text="Unable to get device info for #deviceMsg# (WebService result not defined)">
				</cfif>
			<cfelse>
				<cfset var code=GLOBALS().CONST("Zenprise.ERROR")>
				<cfset var deviceMsg="SERIAL='#serialValue#' and IMEI='#imeiValue#'">
				<cflog type="Warning" text="Unable to get device info for #deviceMsg# (Device not found)">
			</cfif>
			<cfreturn mdmDevice>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=serialValue,IMEI=imeiValue}>
				<cfif isDefined("remoting") AND isDefined("result")>
					<!--- Exception provenant du WebService --->
					<cfif ((result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT()))
						AND structKeyExists(result,remoting.RESULT())>
						<cfset errorLog.EXCEPTION=result[remoting.RESULT()]>
					</cfif>
					<cfif structKeyExists(result,remoting.CLIENT_REQUEST())>
						<cfset errorLog.CLIENT_REQUEST=result[remoting.CLIENT_REQUEST()]>
					</cfif>
					<cfif structKeyExists(result,remoting.SERVER_RESPONSE())>
						<cfset errorLog.SERVER_RESPONSE=result[remoting.SERVER_RESPONSE()]>
					</cfif>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn mdmDevice>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>