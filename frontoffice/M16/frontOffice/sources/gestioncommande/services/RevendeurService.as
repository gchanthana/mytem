package gestioncommande.services
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.events.CommandeEvent;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import session.SessionUserObject;
	
	public class RevendeurService extends EventDispatcher
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					GLOBALES
		//--------------------------------------------------------------------------------------------//
		
		public var listeRevendeurs	:ArrayCollection;
		public var listeAgences		:ArrayCollection;
		public var listeContacts	:ArrayCollection;
		public var listeActePour	:ArrayCollection;
		public var adresseRevendeur	:ArrayCollection;
		
		public var idtypeprofil		:int = 0;

		//--------------------------------------------------------------------------------------------//
		//					LIBELLE A TRADUIRE - LUPO
		//--------------------------------------------------------------------------------------------//
		
		private var text_libelleError	:String = ResourceManager.getInstance().getString('M16', 'Erreur');
		private var text_errorRevendeur	:String = ResourceManager.getInstance().getString('M16', 'Erreur_revendeur__');
		private var text_errorAgences	:String = ResourceManager.getInstance().getString('M16', 'Erreur_agence__');
		private var text_errorContacts	:String = ResourceManager.getInstance().getString('M16', 'Erreur_contact__');	

	//--------------------------------------------------------------------------------------------//
	//					CONSRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function RevendeurService()
		{
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLICS
	//--------------------------------------------------------------------------------------------//
	
		//--------------------------------------------------------------------------------------------//
		//					PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private var _myRevendeurPool:int = 0;
		
		public function fournirRevendeurs(idPoolGestion:int, idProfilEquipement:int, idrevendeurpool:int, clefRecherche:String = ""): void
	    {
	    	listeRevendeurs = new ArrayCollection();
	    	
			_myRevendeurPool = idrevendeurpool;
			
			if(idProfilEquipement > 0)
			{
		    	var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
		    																		"fr.consotel.consoview.M16.v2.DistributeurService",
		    																		"fournirRevendeurs",
		    																		fournirRevendeursResultHandler);
		    	RemoteObjectUtil.callService(op1,CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,
										    	idPoolGestion,
										    	idProfilEquipement,
										    	clefRecherche);	
			}
			else
			{
				var op2:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.M16.v2.DistributeurService",
																					"getUserRevendeurs",
																					fournirRevendeursResultHandler);
				RemoteObjectUtil.callService(op2,SessionUserObject.singletonSession.IDSEGMENT,
												idPoolGestion);	
			}
	    }

		public function fournirAgencesRevendeur(idContact:int, chaine:String = ""): void
	    {
	    	listeAgences = new ArrayCollection();
	    	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.revendeurs.FicheSociete",
																					"get_liste_agence", 
																					fournirAgencesRevendeurResultHandler);
			
			RemoteObjectUtil.callService(op,idContact,
											chaine);
	    }
	    
	    public function fournirContactsRevendeur(idRevendeur:int, idrole:int): void
	    {
	    	listeContacts = new ArrayCollection();
	    	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur",
																					"getContactsRevendeurWithRole", 
																					fournirContactsRevendeurResultHandler);
			
			RemoteObjectUtil.callService(op,idRevendeur, 
											idrole);
	    }
		
		public function fournirActePour(profil:int, idrevendeur:int):void
		{
			listeActePour = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.commande_sncf",
																				"functionActePour",
																				fournirActePourHandler);
			RemoteObjectUtil.callService(op,profil,
											idrevendeur);
		}

		public function getUserProfil(): void
		{
			idtypeprofil = 1;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.M16.v2.DistributeurService",
																					"getUserProfil", 
																					getUserProfilResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
		
		public function getRevendeurAdresse(idsegment:int, idrevendeur:int): void
		{
			adresseRevendeur = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.DistributeurService",
																				"getRevendeurAdresse", 
																				getRevendeurAdresseResultHandler);
			
			RemoteObjectUtil.callService(op,idsegment,
											idrevendeur);
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATES
	//--------------------------------------------------------------------------------------------//
	
		//--------------------------------------------------------------------------------------------//
		//					RESULT EVENT PROCEDURES
		//--------------------------------------------------------------------------------------------//	   
	    
	    private function fournirRevendeursResultHandler(re:ResultEvent):void
	    {
	    	if(re.result)
			{
				var value:ArrayCollection = new ArrayCollection();
				
				if(re.result as ArrayCollection)
					value = re.result as ArrayCollection;
				else
					value = new ArrayCollection(re.result as Array);
				
				var myrevs	:ArrayCollection = Formator.sortFunction(value, "IDREVENDEUR");
				var sortrev	:ArrayCollection = new ArrayCollection();
				var lenRev	:int = myrevs.length;
				var lastid	:int = -1;
				
				for(var i:int = 0;i < lenRev;i++)
				{
					if(lastid != myrevs[i].IDREVENDEUR)
					{
						sortrev.addItem(myrevs[i]);
						
						lastid = myrevs[i].IDREVENDEUR;
					}
				}
				
				listeRevendeurs = Formator.sortFunction(sortrev, "LIBELLE");
				
				if(_myRevendeurPool > 0)
				{
					var myObjRev:Object = null;
					
					lenRev = listeRevendeurs.length;
					
					for(var j:int = 0;j < lenRev;j++)
					{
						if(listeRevendeurs[j].IDREVENDEUR == _myRevendeurPool)
						{
							myObjRev = new Object();
							myObjRev = listeRevendeurs[j];
						}
					}
					
					if(myObjRev != null)
					{
						listeRevendeurs = new ArrayCollection();
						listeRevendeurs.addItem(myObjRev);
					}
					else
						listeRevendeurs = new ArrayCollection();
				}

				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_REVENDEURS));
			}
			else
	    		ConsoviewAlert.afficherError(text_errorRevendeur, text_libelleError);
	    }
	    
	    private function fournirAgencesRevendeurResultHandler(re:ResultEvent): void
	    {
	    	if(re.result)
	    	{
	           	listeAgences = re.result as ArrayCollection;
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_AGENCES));
	    	}
	    	else
	    		ConsoviewAlert.afficherError(text_errorAgences, text_libelleError);
	    }
	    
	    private function fournirContactsRevendeurResultHandler(re:ResultEvent): void
	    {
	    	if(re.result)
	    	{
	           	listeContacts = re.result as ArrayCollection;
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_CONTACTS));
			}
	    	else
	    		ConsoviewAlert.afficherError(text_errorContacts, text_libelleError);
	    }
		
		private function fournirActePourHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				listeActePour = mappinglistActePour(re.result as ArrayCollection);
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_ACTE_POUR));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Impossible_de_r_cup_rer_la_liste_des_ges'), text_libelleError);
		}
		
		private function getUserProfilResultHandler(re:ResultEvent): void
		{
			if(re.result)
			{
				idtypeprofil = re.result as int;
				dispatchEvent(new CommandeEvent(CommandeEvent.IDTYPEPROFIL));
			}
		}
		
		private function getRevendeurAdresseResultHandler(re:ResultEvent): void
		{
			if(re.result)
			{
				adresseRevendeur = new ArrayCollection(re.result as Array);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.ADRESSE_REVENDEUR));
			}
		}
	
		//--------------------------------------------------------------------------------------------//
		//					MAPPINGS
		//--------------------------------------------------------------------------------------------//	
		
		private function mappinglistActePour(listGestionnaire:ArrayCollection):ArrayCollection
		{
			var object:Object 					= new Object();
			var gestionnaires:ArrayCollection 	= new ArrayCollection();
			
			for(var i:int = 0;i < listGestionnaire.length;i++)
			{
				object = new Object();
				object.LIBELLE 			= listGestionnaire[i].LOGIN_PRENOM + " " + listGestionnaire[i].LOGIN_NOM + " / " + listGestionnaire[i].LIBELLE_POOL + " / " + listGestionnaire[i].LIBELLE_PROFIL_COMMANDE;
				object.NOM 				= listGestionnaire[i].LOGIN_NOM;
				object.PRENOM 			= listGestionnaire[i].LOGIN_PRENOM;
				object.LOGIN_EMAIL 		= listGestionnaire[i].LOGIN_EMAIL;
				object.APPLOGINID 		= listGestionnaire[i].APP_LOGINID;
				object.IDPROFILE 		= listGestionnaire[i].IDPROFIL_COMMANDE;
				object.LIBELLE_PROFILE 	= listGestionnaire[i].LIBELLE_PROFIL_COMMANDE;
				object.IDPOOL 			= listGestionnaire[i].IDPOOL;
				object.LIBELLE_POOL 	= listGestionnaire[i].LIBELLE_POOL;
				
				gestionnaires.addItem(object);
			}
			
			return gestionnaires;
		}
	    
	}
}