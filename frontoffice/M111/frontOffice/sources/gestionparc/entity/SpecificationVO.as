package gestionparc.entity
{
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.controls.DataGrid;
	import mx.utils.ObjectUtil;
	
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.entity.vue.MatriceParcVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.cache.CacheDatas;

	[Bindable]
	public class SpecificationVO extends EventDispatcher
	{
		public static const ANDROID:String			="ANDROID";
		public static const IOS:String				="iOS";
		public static const UNKNOWN_OS:String		="N.D";
		public static const POOL_PARC_GLOBAL_ID:Number 	= 0;
		public static const POOL_TOUT_PARC_ID:Number 	= -10;
		public static var pool_is_tout_parc:Boolean = false;

		private static var _instance:SpecificationVO;
		private var _gpDroitUser:GpDroitUserVO; /** Tableau contenant l'ensemble des containers dont le background doit être orangé. */
		private var listeContainer:Array=[];
		private var _listePool:ArrayCollection;

		private var _boolHasChanged:Boolean=false; /** précise si les spécifications ont été modifié ou pas. */ 
		private var _boolFirstUse:Boolean=true;   /** précise si l'utilisateur a déjà appuyé sur le bouton de recherche ou pas*/	
		private var _orderByReq : String= 'ASC';

		private var _elementDataGridSelected:AbstractMatriceParcVO; /** la ligne du datagrid sélectionné */
		private var _dataGridView: DataGrid;
		private var _stateButton:int;  /**  Variables inutilisées*/
		private var _labelSort:String; /**  libellè du tri -champ de tri*/
		
		private var _view:String;	/** libelle de la vue sélectionnée */ 	
		private var _idView:int;   /**  id de la vue sélectionnée */
		private var _ViewObject:AbstractVue;	/** l'objet VUE */

		private var _restriction:String;     /**	libelle de la restriction */
		private var _idRestriction:int;      /** l'id de la restriction */
		private var _valueRestriction:String;/** valeur de la saisie utilisateur */
	
		
		private var _labelPool:String; 		/** le libelle du pool sélectionné */
		private var _labelPoolNotValid:String;	/** le libelle du pool sélectionné*/	
		private var _idPool:int;/** l'id du pool sélectionné*/
		
		/** 
		 * l'id du pool actuellement selectionné dans la combo Pools 
		 * et validé après un click sur Afficher ou Actualiser
		 * cet id est conservé et ne change que si on choisi un autre pool via la combo et qu'on click sur afficher
		 * Utile quand on choisi le pool 'Mes Pools' car dans ce cas idpool devient celui de la ligne (row) du GRID 
		 **/
		private var _validSelectedIdPool:int =0; 
		private var _idRevendeur:Number; /** l id du revendeur du pool */  
		private var _idRevendeurNotValid:Number; /** l id de revendeur du pool selectionné */
		
		private var _idProfilNotValid:Number;/** l 'id du profil de l utilisateur dans le pool selectionné */
		private var _labelProfilNotValid:String;/** le libellé du profil de l utilisateur dans le pool selectionné */
		
		private var _idProfil:Number; /** l'id du profil dans le pool selectionné */ 
		private var _labelProfil:String; /**le libelle du proifil dans le pool selectionné */

		private var _idPoolNotValid:int; /** l'id du pool sélectionné mais NON VALIDE */
	
		private var _keyWord:String; /** le libellè saisi par le client */	
		private var _idKeyWordScope:String; /** le code du scope sélectionné -la colonne dans laquelle on recherche */ 
		
		private var _idKeyWordScope2:String; /** le code de la colonne sélectionnée dans laquelle on recherche */
		private var _keyWord2:String; /** la valeur choisie de la colonne */	
		
		private var _boolNewInstance:Boolean=true;

		/** possibilité de faire des commandes */
		private var _hasCommandeMobile:Boolean;
		private var _hasCommandeFixe:Boolean;
		
		private var _nbRowDataGrid :Number;

		public function SpecificationVO()
		{
			if (_instance == null || (_instance && _instance._boolNewInstance))
			{
				_instance=this;
				_instance.boolNewInstance=false;
			}
		}

		public function dispatchEventRefresh():void
		{
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_MAIN_GRID, true));
		}
		
		//appliqué au changement de combo pool MYT-1606 à changer pour application après validation
		public function initDroitCommandes(values:ArrayCollection):void
		{
			var cursor:IViewCursor=values.createCursor();
			_hasCommandeFixe=false;
			_hasCommandeMobile=false;
			while (!cursor.afterLast)
			{
				if (cursor.current.IDINV_ACTIONS == 2066 && cursor.current.SELECTED == 1)
					_hasCommandeMobile=true
				if (cursor.current.IDINV_ACTIONS == 2216 && cursor.current.SELECTED == 1)
					_hasCommandeFixe=true;
				if (_hasCommandeMobile == true && _hasCommandeFixe == true)
				{
					break;
				}
				cursor.moveNext();
			}
		}
		
		
		/**
		 *  Mets à jour les droits sur la commande en fonction du pool et de la matrice de droits du profil utilisateur
		 * 
		 * 	item <Object> grid line selected
		 * 	IDPOOL
		 * 	POOL
		 *  
		 */
		public function updateProfilCommande(item:Object):void
		{
			/*
			*	values <Object>
			*  	ACTION = "Preparer la commande"
			*	IDACTION = "2066"
			*	IDPOOL = "51038"
			*	IDPROFIL = "28215"
			*	POOL = "35680 FERROV. INFRAPOLE BRETAGNE"
			*	PROFIL = "Default Profile"
			*/
			var values:ArrayCollection = CacheDatas.matricePoolProfil;			
			var cursor:IViewCursor=values.createCursor();
			var _idpool:Number = 0;
			_hasCommandeFixe=false;
			_hasCommandeMobile=false;
			
			if(validSelectedIdPool == POOL_PARC_GLOBAL_ID){
				_hasCommandeFixe=false;
				_hasCommandeMobile=false;
				return;
			}else
			
			if(validSelectedIdPool === POOL_TOUT_PARC_ID && //Mes Pools >> //get the pool of the selected line
				 item.hasOwnProperty("IDPOOL") && item.IDPOOL > 0){
				
				_idpool = item.IDPOOL;
					
				while (!cursor.afterLast)
				{
					if (cursor.current.IDACTION == 2066 && cursor.current.IDPOOL == _idpool){
						_hasCommandeMobile=true
						trace(ObjectUtil.toString(cursor.current));
					}
						
					if (cursor.current.IDACTION == 2216 && cursor.current.IDPOOL == _idpool){
						_hasCommandeFixe=true;
						trace(ObjectUtil.toString(cursor.current));
					}
						
					if (_hasCommandeMobile == true && _hasCommandeFixe == true)
					{
						break;
					}
					cursor.moveNext();
				}
					
			}
			else{
				
				initDroitCommandes(CacheDatas.listeDroitCommande);
			}
			
		}

		/** RESET l'instance */
		
		public static function resetSpecificationVO():void
		{
			if (_instance)
			{
				_instance.boolNewInstance=true;
				_instance=new SpecificationVO();
			}
		}

		/** Définit un objet SpecificationVO par défaut */
		
		public function setDefaultSpecification():void
		{
			idPool=-1;
			labelPool="";
		}

		/** Avertit qu'une association ou dissociation a eu lieu, on rafraichit les données */ 
		
		public function refreshData():void
		{
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_MAIN_GRID));
		}
		
		
		/** 
		 *  Renvoi un objet Matrice / Profil / Action en foncton du pool pour l' utilisateur courant <br>
		 *  Renvoi null si pas de profil pour le pool<br>
		 *  En Option si idaction > 0, permet de savvoir si pour le pool en quetsion on peut faire l'action passé en paramètre
		 *  
		 *  @param
		 * 	<strong>IDPOOL</strong> Le pool<br>
		 * 
		 * 	<strong>[idAction]</strong> optionnelle
		 * 
		 *  @return 
		 *  <strong>Object</strong><br>
		 *  ACTION = "Preparer la commande"<br>
		 *	IDACTION = "2066"<br>
		 *	IDPOOL = "51038"<br>
		 *	IDPROFIL = "28215"<br>
		 *	POOL = "35680 FERROV. INFRAPOLE BRETAGNE"<br>
		 *	PROFIL = "Default Profile"<br>
		 */
		public  function getUserPoolInfo(idPool:int, idaction:int=0):Object
		{
			var matrice:ArrayCollection = CacheDatas.matricePoolProfil;
			var optionalCondition:Boolean = (idaction > 0)?true:false;
			var infos:Object;
			var message:String ="";
			
			if(optionalCondition){
				for(var i:int  = 0; i < matrice.length; i ++)
				{
					if (idPool == matrice[i].IDPOOL && (idaction == matrice[i].IDACTION)  ){
						
						message = "IDPOOL :" + idPool + "  " + "IDPROFIL :" + matrice[i].IDPROFIL + "  " + "IDACTION :" + matrice[i].IDACTION;
						trace(message);
						infos =  matrice[i];
						break;
						
					}
				}
			}else{
				for(var j:int  = 0; j < matrice.length; j ++)
				{
					if (idPool == matrice[j].IDPOOL ){
						
						message = "IDPOOL :" + idPool + "  " + "IDPROFIL :" + matrice[j].IDPROFIL;
						trace(message);
						infos =  matrice[j];
						break;
						
					}
				}
			}
				
			
			
			return infos;
		}

		public function get hasCommandeFixe():Boolean
		{
			return _hasCommandeFixe;
		}

		public function get hasCommandeMobile():Boolean
		{
			return _hasCommandeMobile;
		}

		public function get idRevendeurNotValid():Number
		{
			return _idRevendeurNotValid;
		}

		public function set idRevendeurNotValid(value:Number):void
		{
			_idRevendeurNotValid=value;
		}

		public function get idRevendeur():Number
		{
			return _idRevendeur;
		}

		public function set idRevendeur(value:Number):void
		{
			_idRevendeur=value;
		}

		public function get idProfil():Number
		{
			return _idProfil;
		}

		public function set idProfil(value:Number):void
		{
			_idProfil=value;
		}

		public function get idProfilNotValid():Number
		{
			return _idProfilNotValid;
		}

		public function set idProfilNotValid(value:Number):void
		{
			_idProfilNotValid=value;
		}

		public function get labelProfil():String
		{
			return _labelProfil;
		}

		public function set labelProfil(value:String):void
		{
			_labelProfil=value;
		}

		public function get labelProfilNotValid():String
		{
			return _labelProfilNotValid;
		}

		public function set labelProfilNotValid(value:String):void
		{
			_labelProfilNotValid=value;
		}

		private function setBoolHasChanged():void
		{
			if (!boolFirstUse)
			{
				if (!boolHasChanged)
					boolHasChanged=true;
			}
			else
				boolHasChanged=false;
		}

		public function set gpDroitUser(value:GpDroitUserVO):void
		{
			_gpDroitUser=value;
			setBoolHasChanged();
		}

		public function get gpDroitUser():GpDroitUserVO
		{
			return _gpDroitUser;
		}

		public function set labelPool(value:String):void
		{
			_labelPool=value;
			setBoolHasChanged();
		}

		public function get labelPool():String
		{
			return _labelPool;
		}

		public function set idPool(value:int):void
		{
			_idPool=value;
			setBoolHasChanged();
		}

		public function get idPool():int
		{
			return _idPool;
		}
		
		/** 
		 * l'id du pool actuellement selectionné dans la combo Pools 
		 * et validé après un click sur Afficher ou Actualiser
		 * cet id est conservé et ne change que si on choisi un autre pool via la combo et qu'on click sur afficher
		 * Utile quand on choisi le pool 'Mes Pools' car dans ce cas idpool devient celui de la ligne (row) du GRID 
		 **/
		public function set validSelectedIdPool(value:int):void
		{
			_validSelectedIdPool=value;
		}
		
		public function get validSelectedIdPool():int
		{
			return _validSelectedIdPool;
		}

		public function set keyWord(value:String):void
		{
			_keyWord=value;
			setBoolHasChanged();
		}

		public function get keyWord():String
		{
			return _keyWord;
		}

		public function set idKeyWordScope(value:String):void
		{
			_idKeyWordScope=value;
			setBoolHasChanged();
		}

		public function get idKeyWordScope():String
		{
			return _idKeyWordScope;
		}
		
		public function set keyWord2(value:String):void
		{
			_keyWord2=value;
			setBoolHasChanged();
		}
		
		public function get keyWord2():String
		{
			return _keyWord2;
		}
		
		public function set idKeyWordScope2(value:String):void
		{
			_idKeyWordScope2=value;
			setBoolHasChanged();
		}
		
		public function get idKeyWordScope2():String
		{
			return _idKeyWordScope2;
		}

		public function set restriction(value:String):void
		{
			_restriction=value;
			setBoolHasChanged();
		}

		public function get restriction():String
		{
			return _restriction;
		}

		public function set idRestriction(value:int):void
		{
			_idRestriction=value;
			setBoolHasChanged();
		}

		public function get idRestriction():int
		{
			return _idRestriction;
		}

		public function set valueRestriction(value:String):void
		{
			_valueRestriction=value;
			setBoolHasChanged();
		}

		public function get valueRestriction():String
		{
			return _valueRestriction;
		}

		public function set view(value:String):void
		{
			_view=value;
			setBoolHasChanged();
		}

		public function get view():String
		{
			return _view;
		}

		public function set idView(value:int):void
		{
			_idView=value;
			setBoolHasChanged();
		}

		public function get idView():int
		{
			return _idView;
		}

		public function set labelSort(value:String):void
		{
			_labelSort=value;
			setBoolHasChanged();
		}

		public function get labelSort():String
		{
			return _labelSort;
		}

		public function set elementDataGridSelected(value:AbstractMatriceParcVO):void
		{
			_elementDataGridSelected=value;
		}
		
		public function get elementDataGridSelected():AbstractMatriceParcVO
		{
			return _elementDataGridSelected;
		}
		
		public function set boolHasChanged(value:Boolean):void
		{
			_boolHasChanged=value;
		}

		public function get boolHasChanged():Boolean
		{
			return _boolHasChanged;
		}

		public function set boolFirstUse(value:Boolean):void
		{
			_boolFirstUse=value;
		}

		public function get boolFirstUse():Boolean
		{
			return _boolFirstUse;
		}

		public function get ViewObject():AbstractVue
		{
			return _ViewObject;
		}

		public function set ViewObject(value:AbstractVue):void
		{
			_ViewObject=value;
		}
	
		public static function getInstance():SpecificationVO 
		{
			return SpecificationVO._instance;
		}

		public function get listePool():ArrayCollection
		{
			return _listePool;
		}

		public function set listePool(value:ArrayCollection):void
		{
			_listePool=value;
		}

		public function get idPoolNotValid():int
		{
			return _idPoolNotValid;
		}

		public function set idPoolNotValid(value:int):void
		{
			_idPoolNotValid=value;
		}

		public function get labelPoolNotValid():String
		{
			return _labelPoolNotValid;
		}

		public function set labelPoolNotValid(value:String):void
		{
			_labelPoolNotValid=value;
		}

		private var _ref:DisplayObject;

		public function get displayObjectRef():DisplayObject
		{
			return _ref;
		}

		public function set displayObjectRef(ref:DisplayObject):void
		{
			_ref=ref;
		}

		public function get boolNewInstance():Boolean
		{
			return _boolNewInstance;
		}

		public function set boolNewInstance(value:Boolean):void
		{
			_boolNewInstance=value;
		}

		public function get orderByReq():String
		{
			return _orderByReq;
		}
		
		public function set orderByReq(value:String):void
		{
			_orderByReq = value;
		}
		
		public function get nbRowDataGrid():Number
		{
			return _nbRowDataGrid;
		}
		
		public function set nbRowDataGrid(value:Number):void
		{
			_nbRowDataGrid = value;
		}
		
		public function get dataGridView():DataGrid
		{
			return _dataGridView;
		}
		
		public function set dataGridView(value:DataGrid):void
		{
			_dataGridView = value;
		}
		
	}
}
