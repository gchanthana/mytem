package gestionparc.ihm.action.reprendre.actionReprendreLigne
{
	import flash.display.DisplayObject;

	import gestionparc.ihm.fiches.collaborateur.FicheReprendreLigneIHM;

	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionReprendreLigneReceiver
	{
		private var popup:FicheReprendreLigneIHM;

		public function ActionReprendreLigneReceiver()
		{
		}
		/**
		 * Cette fonction appelle l'affichage de l'IHM et renseigne au préalable les sources de l'action.
		 */
		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			popup=new FicheReprendreLigneIHM();
			PopUpManager.addPopUp(popup, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(popup);
		}
	}
}
