package composants.tb.tableaux
{
	import mx.events.FlexEvent;
	import mx.collections.ArrayCollection;
	import flash.events.Event;
	import mx.utils.ObjectUtil;
	import mx.formatters.Formatter;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import composants.tb.effect.EffectProvider;
	
	public class TableauConsoSegment extends TableauConso
	{
		private var _segment : String;
		private var _tiltle : String;
		
		public function TableauConsoSegment(ev : TableauChangeEvent)
		{
			super();
			_segment = ev.SEGMENT;
			_tiltle = "Consommations Segment "+ev.SEGMENT.toLowerCase();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
				
		/*---------- Protected -----------------------*/
		
		//initialisation du composant
		protected override function init(fe :  FlexEvent):void{
			title = _tiltle;
			//update();			
		}
		
		protected function drawTabs():void{ 
			rep.dataProvider = dataProviders;
			if (rep.dataProvider != null)	
				for (var i:int = 0; i < rep.dataProvider.length; i++){
						//myGrid[i].addEventListener(Event.CHANGE,_changeHandlerFunction);
						myGrid[i].addEventListener(Event.CHANGE,foo);	
						myGrid[i].columns[0].itemRenderer = null;	
						myGrid[i].columns[3].itemRenderer = null;			
				}  			
			myFooterGrid.columns[1].headerText = nf.format(_totalQte);
			myFooterGrid.columns[2].headerText = nf.format(_totalVol);	
			myFooterGrid.columns[3].headerText = nf.format(_total);
		}
		
		//formatte les donnees
	 	protected override function _formatterDataProvider(d: ArrayCollection):ArrayCollection{		

			var tmpCollection : ArrayCollection = new ArrayCollection();
			var o : Object;
		
			var segmentArray : Array = new Array();
			
			_total = 0;
			_totalQte = 0;
			_totalVol = 0;
			
			for	(o in d){					
				segmentArray.push(formateObject(d[o]));						 	   
			}
		
			tmpCollection.addItem(segmentArray);
						
			return tmpCollection;
		}  	
		
		
		protected override function formateObject(obj : Object):Object{

			_totalVol = _totalVol + (parseFloat(obj.duree_appel));	
			_totalQte = _totalQte + parseInt(obj.nombre_appel);	
			_total = _total +  parseFloat(obj.montant_final);	

			var o : Object = new Object();			
			o["NBAPPELS"]= nf.format(obj.nombre_appel);
			o["LIBELLE"] = obj.theme_libelle;
			o["SEGMENT"] = obj.segment_theme;
			o["QUANTITE"] = nf.format((parseFloat(obj.duree_appel)));
			o["TYPE_THEME"] = obj.type_theme;
			o["SUR_THEME"] = obj.sur_theme;
			o["MONTANT_TOTAL"]= obj.montant_final;
			o["TYPE"] = "THEME";	
			o["ID"] = obj.idtheme_produit;	
			return o;
		}	
		//-------Chargement des donnees----------- remoting ---------------------------------------------------//
		// Chargement des données par remoting
		protected override function chargerDonnnees():void{
			var op : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.tb.accueil.facade",
																			"getConsoBySegment",
																			chargerDonnneesResultHandler,null);
			
			
			RemoteObjectUtil.callService(op,_segment);
			
		}
		
	}
}