<cfcomponent author="Cedric" displayname="fr.consotel.consoview.M311.reporting.RapportLigne" implements="fr.consotel.consoview.M311.reporting.IParamStrategy"
hint="Stratégie utilisée pour définie et mettre à jour les paramètres du rapport ligne">
	<cffunction access="public" name="setParameters" returntype="Struct" hint="Définit et met à jour les valeurs des paramètres dans report. Retourne report">
		<cfargument name="report" type="Struct" required="true" hint="Structure qui représente le reporting à effectuer.
		Si elle contient une clé RAPPORT alors l'API de la bibliothèque de rapport est utilisée et sinon c'est l'API standard">
		<cfset var reportProp=ARGUMENTS["report"]> 
		<!--- RAPPORT --->
		<cfset reportProp["RAPPORT"]["TEMPLATE"]="default">
		<cfset reportProp["RAPPORT"]["FORMAT"]="pdf">
		<cfset reportProp["RAPPORT"]["FILE_EXT"]="pdf">
		<cfset reportProp["RAPPORT"]["USER_ID"]=SESSION.USER.APP_LOGINID>
		<cfset reportProp["RAPPORT"]["IDRACINE"]=SESSION.PARAMS_EXPORT.IDRACINE>
		<cfset reportProp["RAPPORT"]["LOCALIZATION"]=SESSION.USER.GLOBALIZATION>
		<!--- PARAMETRE --->
		<cfset reportProp["PARAMETRE"]["G_IDMASTER"]={LIBELLE="", VALEUR=[SESSION.PARAMS_EXPORT.IDRACINE_MASTER], IS_SYSTEM=TRUE}>
		<cfset reportProp["PARAMETRE"]["G_IDRACINE"]={LIBELLE="", VALEUR=[SESSION.PARAMS_EXPORT.IDRACINE], IS_SYSTEM=TRUE}>
		<cfset reportProp["PARAMETRE"]["G_IDORGA"]={LIBELLE="", VALEUR=[SESSION.PARAMS_EXPORT.IDORGA], IS_SYSTEM=TRUE}>
		<cfset reportProp["PARAMETRE"]["G_IDPERIMETRE"]={LIBELLE="", VALEUR=[SESSION.PARAMS_EXPORT.IDPERIMETRE], IS_SYSTEM=TRUE}>
		<cfset reportProp["PARAMETRE"]["G_NIVEAU"]={LIBELLE="", VALEUR=[SESSION.PARAMS_EXPORT.NIVEAU], IS_SYSTEM=TRUE}>
		<cfset reportProp["PARAMETRE"]["G_IDCLICHE"]={LIBELLE="", VALEUR=[0], IS_SYSTEM=TRUE}>
		<cfset reportProp["PARAMETRE"]["L_PERIODE"]={LIBELLE="", VALEUR=[SESSION.PERIMETRE.GROUPE], IS_SYSTEM=TRUE}><!--- Préfixe du nom de fichier container --->
		<cfset reportProp["PARAMETRE"]["U_IDPERIOD_DEB"]={LIBELLE="", VALEUR=[SESSION.PERIMETRE.SELECTED_IDPERIOD_DEB], IS_SYSTEM=TRUE}>
		<cfset reportProp["PARAMETRE"]["U_IDPERIOD_FIN"]={LIBELLE="", VALEUR=[SESSION.PERIMETRE.SELECTED_IDPERIOD_FIN], IS_SYSTEM=TRUE}>
		<cfset reportProp["PARAMETRE"]["G_LANGUE_PAYS"]={LIBELLE="", VALEUR=[SESSION.USER.GLOBALIZATION], IS_SYSTEM=TRUE}>
		<cfreturn reportProp>
	</cffunction>
</cfcomponent>