<!--- TODO : Modifier la spécification (i.e : attribut hint) pour autoriser les méthode getContent() et getnContentAsLocalFile() --->
<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.event.ContainerEvent"
extends="fr.consotel.api.ibis.publisher.handler.event.FtpEvent"
hint="Evènement contenant les infos liées à une diffusion du contenu d'un reporting dans le container ConsoView
Le contenu du reporting n'est accessible qu'à partir du module container de ConsoView et ne peut etre obtenu à partir de cet évènement
La méthode getContent() retourne le contenu retourné par la méthode getDeliveryInfos()
La méthode getDeliveryInfos() retourne un contenu générique contenant entre autres les infos FTP et Container
La méthode getContentAsLocalFile() lève une exception">
	<!--- fr.consotel.api.ibis.publisher.handler.event.ContainerEvent --->
	<cffunction access="public" name="getContainerStatus" returntype="String"
	hint="Retourne la valeur correspondante de getStatus() pour container. Les valeurs possibles sont :
	Completed : Correspond au statut SUCCESS()
	Error : Correspond aux statuts FAILURE() et EXCEPTION()
	Unknown : Correspond à toutes autre valeurs de getStatus()">
		<cfset var CONTAINER_STATUS={S="Completed",F="Error",X="Error"}>
		<cfset var containerStatus="Unknown">
		<cfif structKeyExists(CONTAINER_STATUS,getStatus())>
			<cfset containerStatus=CONTAINER_STATUS[getStatus()]>
		</cfif>
		<cfreturn containerStatus>
	</cffunction>
	
	<cffunction access="public" name="getFileUUID" returntype="String"
	hint="Retourne le nom du fichier qui est généré en FTP ou une chaine vide s'il n'est pas défini. La propriété correspondante est FILE_NAME
	Sa valeur correspond à un UUID unique généré par la stratégie de diffusion : fr.consotel.api.ibis.publisher.delivery.Container">
		<cfreturn getDeliveryKeyValue("FILE_NAME")>
	</cffunction>
	
	<cffunction access="public" name="getUserId" returntype="Numeric" hint="Retourne l'identifiant de l'utilisateur container ou 0 s'il n'est pas défini">
		<cfreturn VAL(getDeliveryKeyValue("USERID"))>
	</cffunction>
	
	<cffunction access="public" name="getIdRacine" returntype="Numeric"
	hint="Retourne l'identifiant de la racine utilisé dans les notifications ou 0 s'il n'est pas défini">
		<cfreturn VAL(getDeliveryKeyValue("IDRACINE"))>
	</cffunction>
	
	<cffunction access="public" name="getCodeApp" returntype="Numeric" hint="Retourne le CodeApp utilisé dans les notifications ou 0 s'il n'est pas défini">
		<cfreturn VAL(getDeliveryKeyValue("CODE_APP"))>
	</cffunction>

	<cffunction access="public" name="getFichier" returntype="String" hint="Retourne le nom sous lequel le fichier est visible dans le container">
		<cfreturn getDeliveryKeyValue("FICHIER")>
	</cffunction>
	
	<cffunction access="public" name="getExt" returntype="String" hint="Retourne l'extension du fichier dans le container">
		<cfreturn getDeliveryKeyValue("FILE_EXT")>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent --->
	<cffunction access="public" name="getDeliveryInfos" returntype="any"
	hint="Retourne le contenu retourné par getDeliveryInfos() incluant des infos FTP et Container">
		<cfset var deliveryInfos="">
		<cfset var deliveryContent=SUPER.getDeliveryInfos()>
		<cfsavecontent variable="deliveryInfos">
			<cfoutput>
				#deliveryContent#<br><br>
				<b>Les propriétés de diffusion Container provenant de l'évènement sont :</b><br>
				Statut container : #getContainerStatus()#<br>
				CodeApp : #getCodeApp()#<br>
				UserId : #getUserId()#<br>
				IdRacine : #getIdRacine()#<br>
				Nom du fichier généré en FTP (également UUID utilisé par les procédures stockées container) : #getFileUUID()#<br>
				Nom sous lequel le fichier est visible dans container : #getFichier()#<br>
				Extension du fichier visible dans container : #getExt()#
			</cfoutput>
		</cfsavecontent>
		<cfreturn deliveryInfos>
	</cffunction>
	
	<cffunction access="public" name="getContent" returntype="any" hint="Retourne getDeliveryInfos()">
		<cfargument name="localFileName" type="String"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfargument name="localDirAbsPath" type="String"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfset var deliveryInfos="">
		<cfset var deliveryContent=getDeliveryInfos()>
		<cfsavecontent variable="deliveryInfos">
			<cfoutput>
				<center><b><i>(Le contenu du reporting n'est accessible qu'à partir du module container de ConsoView)</i></b></center><br>
				#deliveryContent#
			</cfoutput>
		</cfsavecontent>
		<cfreturn deliveryInfos>
		<cfreturn >
	</cffunction>

	<cffunction access="public" name="getContentAsLocalFile" returntype="String"
	hint="Le contenu n'est accessible qu'à partir du module container de ConsoView. Lève une exception">
		<cfargument name="localFileName" type="String"  required="true">
		<cfargument name="localDirAbsPath" type="String"  required="true">
		<!---
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le fichier n'est accessible qu'à partir du module container de ConsoView">
		--->
		<cfreturn SUPER.getContentAsLocalFile(ARGUMENTS["localFileName"],ARGUMENTS["localDirAbsPath"])>
	</cffunction>
</cfcomponent>