<cfset objProduit = createObject("component","fr.consotel.consoview.M311.theme.facade")>
<cfset dataset = objProduit.getListeProduitsTheme(perimetre,modeCalcul,numero,idproduit_client,datedeb,datefin)> 

<cfif format eq "excel" or format eq "csv">

	<cfinclude template="/fr/consotel/consoview/M311/report/tb_inventaire_theme_xls.cfm">
<cfelse>
	<cfcontent type="application/pdf"> 
	<cfheader name="Content-Disposition" value="inline;filename=inventaire_produit.pdf" charset="utf-8">

	<cfdocument pagetype="legal" format="#format#" backgroundvisible="yes" fontembed = "yes"  unit="CM" margintop="5" marginleft="0.5" marginright="0.5" orientation="portrait" >
	
	<cfoutput> 
	<html>
		<head>
		
		</head>
		
		<body>
		<style>
			@page { 
				size:portrait; margin-top:2cm; margin-bottom:2cm; margin-left:1cm; margin-right:1cm;
			 }
			 
			html body {
			  margin		: 0;
			  padding		: 0;
			  background	: ##FFFFFF;
			  font			: x-small Verdana,Georgia, Sans-serif;
			  voice-family	: "\"}\""; voice-family:inherit;
			  font-size:x-small;
			  } html>body {
				font-size	: x-small;
			}
			
			.cadre {
				border-color:##000000;
				border-style:solid;
				border-collapse:collapse;
				
				border-top-width:1px;
				border-left-width:1px;
				border-right-width:1px;
				border-bottom-width:1px;
			}
			
			.cadre1{
				background		: rgb(255,255,255);
				border-bottom	:1px solid rgb(0,0,0);
				border-left		:1px solid rgb(0,0,0);
				border-right	:1px solid rgb(0,0,0);
				border-top		:1px solid rgb(0,0,0);
				border-collapse : collapse;
			}
			
			table .menuconsoview
			{
				border 		: none;
				background	: transparent;
				font		: normal 8px Tahoma,Arial, sans-serif;
				color		: ##005EBB;
			}
			
			.menuconsoview caption{
				background		:##036 ;
				color			: white;
				font			: bold 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
				padding			: 4px;
				letter-spacing	: 1px;
			
			}
			
			.menuconsoview th{
				background   	:##99CCFF ;
				color        	: ##036;
				font        	: normal 9px EurostileBold,verdana,arial,helvetica,serif;
				padding      	: 3px;
				border-collapse	: collapse;
			}
			
			.menuconsoview .th_stt{
				background   	:##C0DEF1 ;
				color        	: ##036;
				font        	: normal 9px EurostileBold,verdana,arial,helvetica,serif;
				padding      	: 3px;
				border-collapse	: collapse;
			}
			
			.menuconsoview .th_bottom{
				vertical-align	: middle;
				padding			: 3px;
				border-bottom	: 1px solid ##808080;
				font			: normal 9px Tahoma,Arial, sans-serif;
				color        	: ##036;
				background		: ##99CCFF;
			}
			
			.menuconsoview .th_top{
				vertical-align	: middle;
				padding			:3px;
				border-top		: 1px solid ##808080;
				font			: normal 9px Tahoma,Arial, sans-serif;
				color       	: ##036;
				background		: ##99CCFF;
			}
			
			.menuconsoview .grid_normal{
				background	: ##FDFDFB;
				color		: rgb(0 , 0, 0);
				font		: normal 8px Tahoma,Arial, sans-serif;
				padding		: 2px;
			}
			
			.menuconsoview .grid_small{
				background	: ##FDFDFB;
				color		: rgb(0 , 0, 0);
				font		: normal 8px Tahoma,Arial, sans-serif;
				padding		: 0px;
			}
			
			.menuconsoview .grid_medium {
				background	: ##FDFDFB;
				color		: rgb(0 , 0, 0);
				font		: normal 8px Tahoma,Arial, sans-serif;
				padding-top		: 1px;
			}
			
			.menuconsoview .grid_total{
				background	: ##008ACC;
				color		: rgb(0 , 0, 0);
				font		: normal 8px Tahoma,Arial, sans-serif;
				padding-top	: 2px;
			}
			
			.menuconsoview .grid_bold{
				background	: ##FDFDFB;
				color		: rgb(0 , 0, 0);
				font		: bold 8px Tahoma,Arial, sans-serif;
				padding		: 0px;
			}
			
			/* caption impossible pour liste des produits */
			.td5{
				padding		:5px;
				background	:##036;
				color		:rgb(255,255,255);
				font		:normal 9px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			}
			
		</style>
		<cfdocumentitem type="header">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
				<tr>
					<td align="left" width="40%" height="50"><img src="/fr/consotel/consoview/images/consoview_new.gif" height="40"></td>
					<td width="17%">&nbsp;</td>
					<td width="3%" bgcolor="F4F188">&nbsp;</td>
					<td align="right" bgcolor="FDFCE1" width="40%"><font face="Arial" size="2">INVENTAIRE DES THEMES&nbsp;</font></td>
				</tr>
				<tr><td height="40">&nbsp;</td></tr>
				<tr>
					<td colspan="4">
						<font face="Tahoma" size="2"><strong>#raisonsociale#</strong></font><br>
						<font face="Tahoma" size="1">Editï¿½ par&nbsp<strong>#session.user.prenom# #session.user.nom#</strong></font><br>
						<font face="Tahoma" size="1">Le&nbsp;#Lsdateformat(now(), 'dd mmmm yyyy')#</font>
					</td>
				</tr>
				<tr>
					<td colspan="4">						
						<font face="Tahoma" size="1">
							<u>P&eacute;riode:</u>
							&nbsp;#Lsdateformat(datedeb, 'dd mmmm yyyy')# - #Lsdateformat(datefin, 'dd mmmm yyyy')#
						</font>
					</td>
				</tr>
			</table>
		</cfdocumentitem>
		<cfinclude template="/fr/consotel/consoview/M311/pdf/theme/inventaire_cat_pdf.cfm">
		<cfdocumentitem type="footer">
			<style>
			.tfooter {
				color: rgb(0 , 0, 0);
				font: normal 11px Tahoma,Arial, sans-serif;
			}
			</style>
			<center>
			<table border="0" cellpadding="0" cellspacing="0" width="560" align="center">
				<tr>
					<td align="center" class="tfooter" width="560">copyright CONSOTEL  2000 / 2007 - page #cfdocument.currentpagenumber# / #cfdocument.totalpagecount#</td>
				</tr>
			</table>
			</center>
		</cfdocumentitem>
		
		</body>
	</html> 
	</cfoutput>
	</cfdocument>
</cfif>
