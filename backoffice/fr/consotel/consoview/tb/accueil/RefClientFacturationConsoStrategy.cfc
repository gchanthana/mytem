<!--- =========================================================================
Name: RefclientAboStrategy
Original Author: Brice Miramont
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent displayname="RefclientFacturationConsoStrategy" hint="" extends="Strategy">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="RefclientAboStrategy" displayname="RefclientAboStrategy init()" hint="Initialize the RefclientAboStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(integer, string, date, date)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfquery name="qGetDetailFactureAbo" datasource="#application.OFFREDSN#">
			SELECT 	t.type_theme, t.theme_libelle,t.idtheme_produit, nvl(sum(tpc.qte), 0) AS qte,
			       	nvl(sum(tpc.nombre_appel), 0) AS nombre_appel, nvl(sum(tpc.duree_appel), 0) AS duree_appel,
		    	   	sum(nvl(tpc.montant_final,0)) AS montant_final
		    FROM 	theme_produit t, 
		       		( 
		         		SELECT tp.*, dfa.nombre_appel, dfa.duree_appel, dfa.qte, dfa.montant_final
		         		FROM THEME_PRODUIT_CATALOGUE tp, 
		         			( 
				           		SELECT 	SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
		        		   				SUM(df.duree_appel) AS duree_appel, df.idproduit_client, 
				           				pc.idproduit_catalogue
		           				FROM 	detail_facture_abo df, produit_client pc, produit_catalogue pca, inventaire_periode ip
		           				WHERE 	df.idproduit_client=pc.idproduit_client 
						        		AND pca.idproduit_catalogue=pc.idproduit_catalogue
										AND df.idinventaire_periode=ip.idinventaire_periode
						        		AND trunc(ip.datedeb,'MM')>=trunc(#DateDebut#,'MM')
						        		AND trunc(ip.datedeb,'MM')<=trunc(last_day(#DateFin#),'MM')
										AND df.idref_client=#ID#
						        GROUP BY df.idproduit_client, pc.idproduit_catalogue
		         			) dfa 
		         		WHERE tp.idproduit_catalogue=dfa.idproduit_catalogue (+)
		       		) tpc 
			WHERE t.idtheme_produit=tpc.idtheme_produit (+)
			AND lower(t.type_theme)=lower('Consommations')
			GROUP BY t.type_theme,t.theme_libelle,t.ordre_affichage,t.idtheme_produit
			ORDER BY t.ordre_affichage
		</cfquery>
		<cfreturn qGetDetailFactureAbo>
	</cffunction>
	
	<cffunction name="getStrategy" returntype="string" access="public" output="false">
		<cfreturn "Consommations">
	</cffunction>
	
	<cffunction name="getNumero" returntype="string" access="public" output="false">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
			<cfquery name="qGetCompte" datasource="#application.OFFREDSN#">
				select ref_client
				from ref_client rc
				where rc.idref_client=#ID#
			</cfquery>
			<cfreturn qGetCompte.ref_client>
	</cffunction>
</cfcomponent>