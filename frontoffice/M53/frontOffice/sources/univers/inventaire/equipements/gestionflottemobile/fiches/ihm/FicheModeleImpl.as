package univers.inventaire.equipements.gestionflottemobile.fiches.ihm
{
	import composants.util.equipements.EquipementsUtils;
	
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;

	[Bindable]
	public class FicheModeleImpl extends TitleWindow
	{
		 
		private var _editabel:Boolean;
		
		public var equipementUtils:EquipementsUtils = new EquipementsUtils();
		
		public function FicheModeleImpl()
		{
			super();
		}
		
		public function set selectedTerminal(value:Object):void
		{
			equipementUtils.selectedEquipement = value;
		}

		public function get selectedTerminal():Object
		{
			return equipementUtils.selectedEquipement;
		}
		
		protected function _localeCloseHandler(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		public function set editabel(value:Boolean):void
		{
			_editabel = value;
		}

		public function get editabel():Boolean
		{
			return _editabel;
		}
		
		protected function btAnnulerClickHandler(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btValiderClickHandler(event:MouseEvent):void
		{
			
		}
	}
}