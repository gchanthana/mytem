﻿<cfcomponent extends="AbstractConnectToApp">
<!---- CHAQUE FONCTION GERE CES ERREURS ET RETOURNE UN OBJECT ERROR AVEC UN CODEERREUR
	   IL EST POSSIBLE DE PRECISER LE TYPE, LE MESSAGE ET LE DETAIL
	   <cfreturn ThrowCfCatchError(CODEERREUR,TYPE,MESSAGE,DETAIL)>
	   codeerreur : int, type : string, message : string, detail  : string --->
	<cffunction name="connectTo" 
				displayname="ConnectToApp" 
				description="Permet de se logger à une application" 
				access="public" 
				output="false" 
				returntype="struct">
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
		
		<cflog text="Abstractconnection3.connectToApp()">
		
		<cfset result = super.connectTo(objConnection)>
		<cfreturn result>
		
		<!---<cfset res = super.connectTo(objConnection)>
		<cfreturn res>--->
	</cffunction>	   
	<cffunction name="ChangeGroupe" 
				displayname="ChangeGroupe" 
				description="Gère le changement de Groupe de la personne connectée" 
				access="public" 
				output="false" 
				returntype="struct">
					
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
		
		<cflog text="AbstractConnection3.ChangeGroupe(objConnection.idperimetre = #objConnection.idperimetre#)">
		
		<cftry>
			<!--- CONNECTONGROUP : se connecte au group correspondant à la racine rattaché à l'utilisateur à la création --->
			<cfset result = 0>
			<cfset result = connectOnGroup(objConnection.idperimetre)>
			 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
				<cfreturn result>
			<cfelse>
				<!--- SETPERIMETRE : définit le périmètre --->
				<cfset result = setPerimetre(SESSION.PERIMETRE.ID_GROUPE)>
				 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
					<cfreturn result>
				<cfelse>
					<!--- SETXMLACCESS : créé la liste des modules accessibles selon le périmètre et l'utilisateur --->
					<cfset result = setXMLAccess()>
					<cfif isDefined("result") AND isStruct(result) AND structKeyExists(result,"CODEERREUR")>
						<cfreturn result>
					 </cfif>
				</cfif>
			</cfif>
			<cfreturn SESSION>
			<cfcatch type="any" >
				<cfreturn ThrowCfCatchError(-16,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
			</cfcatch>
		</cftry>
	</cffunction>		
	<cffunction name="ChangePerimetre" 
				displayname="ChangePerimetre" 
				description="Gère le changement de périmètre de la personne connectée" 
				access="public" 
				output="false" 
				returntype="struct">
					
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
		
		<cflog text="AbstractConnection3.ChangePerimetre(objConnection.idgroupe = #objConnection.idgroupe#, objConnection.idperimetre=#objConnection.idperimetre#)">
		
		<cftry>
			<!--- CONNECTONGROUP : se connecte au group correspondant à la racine rattaché à l'utilisateur à la création --->
			<cfset result = 0>
			<cfif objConnection.idgroupe neq SESSION.PERIMETRE.ID_GROUPE>
				<cfset result = connectOnGroup(objConnection.idgroupe)>
			</cfif>
			 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
				<cfreturn result>
			<cfelse>
				<!--- SETPERIMETRE : définit le périmètre --->
				<cfset result = setChangePerimetre(objConnection.idperimetre)>
				 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
					<cfreturn result>
				</cfif>
			</cfif>
			<cfreturn SESSION>
			<cfcatch type="any" >
				<cfreturn ThrowCfCatchError(-15,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
			</cfcatch>
		</cftry>
	</cffunction>	
	<cffunction name="validateLogin" access="private" output="false" returntype="Any" >
		<cfargument name="objConnection" required="true" type="struct" >
		<cftry>
			<cflog text="AbstractConnection3.validateLogin(objConnection.token = #objConnection.token# - objConnection.codeapp = #objConnection.codeapp#)">
			<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.connectClient_v6">
				<cfprocparam type="in" value="#objConnection.token#" cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="in" value="#objConnection.codeapp#" cfsqltype="CF_SQL_NUMERIC">
				<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qValidateLogin">
			</cfstoredproc>
			<cfif p_retour eq 1>
				<cfif qValidateLogin.recordcount EQ 1>
					<cfset boolCheckIP=qValidateLogin['RESTRICTION_IP'][1]>
					<cfif boolCheckIP EQ 1>
						<cfset checkIpResult=checkIpAddress(qValidateLogin['CLIENTACCESSID'][1],#CGI.REMOTE_ADDR#)>
						<cfif checkIpResult LT 1>
							<cfreturn ThrowCfCatchError(-2)>
						<cfelse>
							<cfif setupSessionUser(qValidateLogin)>
								<cfreturn 1>
							<cfelse>
								<cfreturn ThrowCfCatchError(-5)>
							</cfif>
							<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#"  expires="never">
						</cfif>
					<cfelse>
						<cfif setupSessionUser(qValidateLogin)>
							<cfreturn 1>
						<cfelse>
							<cfreturn ThrowCfCatchError(-5)>
						</cfif>
					</cfif>
			<!--- 11/07/2012 : AJOUT SSO SE SOUVENIR  --->	
				<cfelse>
					<cfreturn ThrowCfCatchError(-3)>
				</cfif>
			<cfelse>
				<cfreturn ThrowCfCatchError(p_retour)>
			</cfif>
		<cfcatch type="any" >
			<cfreturn ThrowCfCatchError(-4,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>	
	</cffunction>

</cfcomponent>