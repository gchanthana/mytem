<cfcomponent author="Cedric" displayName="fr.saaswedo.api.mdm.impl.zenprise.DeviceManagement" extends="fr.saaswedo.api.mdm.impl.zenprise.MdmService"
implements="fr.saaswedo.api.mdm.core.services.IDeviceManagement" hint="Implémentation Zenprise.
<br>Une instance de ce composant ne référence que les services d'un seul serveur MDM et utilise les mêmes crédentiels pour y accéder">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.zenprise.DeviceManagement">
		<cfreturn "fr.saaswedo.api.mdm.impl.zenprise.DeviceManagement">
	</cffunction>
	
	<cffunction access="public" name="lock" returntype="Boolean"
	hint="Verrouille un device et remplace le code de déverrouillage. Retourne TRUE si l'action a été effectuée avec succès ou sur un device connecté">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="newPinCode" type="String" required="false" default="0000" hint="Nouveau code de déverrouillage">
		<cfset var statusCode=-1>
		<cftry>
			<cfset var serialValue=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<!--- Librairie Remoting : SOAP --->
			<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
			<cfset var params=[
				{NAME="serialNumber",VALUE=serialValue},{NAME="imei",VALUE=imeiValue},
				{NAME="newPinCode",VALUE=ARGUMENTS.newPinCode},{NAME="waitTime",VALUE=defaultWaitTime()}
			]>
			<cfset var result=remoting.invoke(getDeviceService(),"lockDevice",params)>
			<cfif (result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT())>
				<cfthrow type="Custom" message="WebService invocation error" detail="API MDM: lock()">
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset statusCode=VAL(result[remoting.RESULT()])>
			</cfif>
			<cfset var actionLog="LOCK">
			<cfset var deviceLog="SERIAL='#serialValue#' and IMEI='#imeiValue#'">
			<cfif (statusCode EQ 3) OR (statusCode EQ -1)>
				<cfset var code=GLOBALS().CONST("Zenprise.RESULT_ERROR")>
				<cfthrow type="Custom" errorCode="#code#"
				message="#actionLog# failed. StatusCode #statusCode# for #deviceLog#" detail="#actionLog# failed">
			<cfelse>
				<!--- 0 if action is done; 1 for a device connected; 2 if action is asked for a device not connected; 3 if action failed --->
				<cflog type="information" text="lock('#serialValue#','#imeiValue#'): #statusCode#"> 
				<cfreturn statusCode NEQ 2>
			</cfif>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=serialValue,IMEI=imeiValue}>
				<cfif isDefined("remoting") AND isDefined("result")>
					<!--- Exception provenant du WebService --->
					<cfif ((result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT()))
						AND structKeyExists(result,remoting.RESULT())>
						<cfset errorLog.EXCEPTION=result[remoting.RESULT()]>
					</cfif>
					<cfif structKeyExists(result,remoting.CLIENT_REQUEST())>
						<cfset errorLog.CLIENT_REQUEST=result[remoting.CLIENT_REQUEST()]>
					</cfif>
					<cfif structKeyExists(result,remoting.SERVER_RESPONSE())>
						<cfset errorLog.SERVER_RESPONSE=result[remoting.SERVER_RESPONSE()]>
					</cfif>
				</cfif>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="unlock" returntype="Boolean"
	hint="Déverrouille un device. Retourne TRUE si l'action a été effectuée avec succès ou sur un device connecté">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var statusCode=-1>
		<cftry>
			<cfset var serialValue=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<!--- Librairie Remoting : SOAP --->
			<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
			<cfset var params=[
				{NAME="serialNumber",VALUE=serialValue},{NAME="imei",VALUE=imeiValue},{NAME="waitTime",VALUE=defaultWaitTime()}
			]>
			<cfset var result=remoting.invoke(getDeviceService(),"unlockDevice",params)>
			<cfif (result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT())>
				<cfthrow type="Custom" message="WebService invocation error" detail="API MDM: unlock()">
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset statusCode=VAL(result[remoting.RESULT()])>
			</cfif>
			<cfset var actionLog="UNLOCK">
			<cfset var deviceLog="SERIAL='#serialValue#' and IMEI='#imeiValue#'">
			<cfif (statusCode EQ 3) OR (statusCode EQ -1)>
				<cfset var code=GLOBALS().CONST("Zenprise.RESULT_ERROR")>
				<cfthrow type="Custom" errorCode="#code#"
				message="#actionLog# failed. StatusCode #statusCode# for #deviceLog#" detail="#actionLog# failed">
			<cfelse>
				<!--- 0 if action is done; 1 for a device connected; 2 if action is asked for a device not connected; 3 if action failed --->
				<cflog type="information" text="unlock('#serialValue#','#imeiValue#'): #statusCode#"> 
				<cfreturn statusCode NEQ 2>
			</cfif>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=serialValue,IMEI=imeiValue}>
				<cfif isDefined("remoting") AND isDefined("result")>
					<!--- Exception provenant du WebService --->
					<cfif ((result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT()))
						AND structKeyExists(result,remoting.RESULT())>
						<cfset errorLog.EXCEPTION=result[remoting.RESULT()]>
					</cfif>
					<cfif structKeyExists(result,remoting.CLIENT_REQUEST())>
						<cfset errorLog.CLIENT_REQUEST=result[remoting.CLIENT_REQUEST()]>
					</cfif>
					<cfif structKeyExists(result,remoting.SERVER_RESPONSE())>
						<cfset errorLog.SERVER_RESPONSE=result[remoting.SERVER_RESPONSE()]>
					</cfif>
				</cfif>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="locate" returntype="Boolean" hint="Retourne TRUE si l'action a été effectuée avec succès ou sur un device connecté">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var statusCode=-1>
		<cftry>
			<cfset var serialValue=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<!--- Librairie Remoting : SOAP --->
			<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
			<cfset var params=[
				{NAME="serialNumber",VALUE=serialValue},{NAME="imei",VALUE=imeiValue},{NAME="waitTime",VALUE=defaultWaitTime()}
			]>
			<cfset var result=remoting.invoke(getDeviceService(),"locateDevice",params)>
			<cfif (result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT())>
				<cfthrow type="Custom" message="WebService invocation error" detail="API MDM: locate()">
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset statusCode=VAL(result[remoting.RESULT()])>
			</cfif>
			<cfset var actionLog="LOCATE">
			<cfset var deviceLog="SERIAL='#serialValue#' and IMEI='#imeiValue#'">
			<cfif (statusCode EQ 3) OR (statusCode EQ -1)>
				<cfset var code=GLOBALS().CONST("Zenprise.RESULT_ERROR")>
				<cfthrow type="Custom" errorCode="#code#"
				message="#actionLog# failed. StatusCode #statusCode# for #deviceLog#" detail="#actionLog# failed">
			<cfelse>
				<!--- 0 if action is done; 1 for a device connected; 2 if action is asked for a device not connected; 3 if action failed --->
				<cflog type="information" text="locate('#serialValue#','#imeiValue#'): #statusCode#"> 
				<cfreturn statusCode NEQ 2>
			</cfif>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=serialValue,IMEI=imeiValue}>
				<cfif isDefined("remoting") AND isDefined("result")>
					<!--- Exception provenant du WebService --->
					<cfif ((result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT()))
						AND structKeyExists(result,remoting.RESULT())>
						<cfset errorLog.EXCEPTION=result[remoting.RESULT()]>
					</cfif>
					<cfif structKeyExists(result,remoting.CLIENT_REQUEST())>
						<cfset errorLog.CLIENT_REQUEST=result[remoting.CLIENT_REQUEST()]>
					</cfif>
					<cfif structKeyExists(result,remoting.SERVER_RESPONSE())>
						<cfset errorLog.SERVER_RESPONSE=result[remoting.SERVER_RESPONSE()]>
					</cfif>
				</cfif>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="corporateWipe" returntype="Boolean"
	hint="Retourne TRUE si l'action a été effectuée avec succès ou sur un device connecté">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var statusCode=-1>
		<cftry>
			<cfset var serialValue=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<!--- Librairie Remoting : SOAP --->
			<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
			<cfset var params=[
				{NAME="serialNumber",VALUE=serialValue},{NAME="imei",VALUE=imeiValue},{NAME="waitTime",VALUE=defaultWaitTime()}
			]>
			<cfset var result=remoting.invoke(getDeviceService(),"corporateDataWipeDevice",params)>
			<cfif (result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT())>
				<cfthrow type="Custom" message="WebService invocation error" detail="API MDM: corporateWipe()">
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset statusCode=VAL(result[remoting.RESULT()])>
			</cfif>
			<cfset var actionLog="CORPORATE WIPE">
			<cfset var deviceLog="SERIAL='#serialValue#' and IMEI='#imeiValue#'">
			<cfif (statusCode EQ 3) OR (statusCode EQ -1)>
				<cfset var code=GLOBALS().CONST("Zenprise.RESULT_ERROR")>
				<cfthrow type="Custom" errorCode="#code#"
				message="#actionLog# failed. StatusCode #statusCode# for #deviceLog#" detail="#actionLog# failed">
			<cfelse>
				<!--- 0 if action is done; 1 for a device connected; 2 if action is asked for a device not connected; 3 if action failed --->
				<cflog type="information" text="corporateWipe('#serialValue#','#imeiValue#'): #statusCode#"> 
				<cfreturn statusCode NEQ 2>
			</cfif>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=serialValue,IMEI=imeiValue}>
				<cfif isDefined("remoting") AND isDefined("result")>
					<!--- Exception provenant du WebService --->
					<cfif ((result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT()))
						AND structKeyExists(result,remoting.RESULT())>
						<cfset errorLog.EXCEPTION=result[remoting.RESULT()]>
					</cfif>
					<cfif structKeyExists(result,remoting.CLIENT_REQUEST())>
						<cfset errorLog.CLIENT_REQUEST=result[remoting.CLIENT_REQUEST()]>
					</cfif>
					<cfif structKeyExists(result,remoting.SERVER_RESPONSE())>
						<cfset errorLog.SERVER_RESPONSE=result[remoting.SERVER_RESPONSE()]>
					</cfif>
				</cfif>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="wipe" returntype="Boolean"
	hint="Réinitialise un device. Retourne TRUE si l'action a été effectuée avec succès ou sur un device connecté">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="erasedMemoryCard" type="Boolean" required="false" default="false" hint="TRUE pour supprimer le contenu de la carte mémoire">
		<cfset var statusCode=-1>
		<cftry>
			<cfset var serialValue=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<!--- Librairie Remoting : SOAP --->
			<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
			<cfset var params=[
				{NAME="serialNumber",VALUE=serialValue},{NAME="imei",VALUE=imeiValue},
				{NAME="erasedMemoryCard",VALUE=ARGUMENTS.erasedMemoryCard},{NAME="waitTime",VALUE=defaultWaitTime()}
			]>
			<cfset var result=remoting.invoke(getDeviceService(),"wipeDevice",params)>
			<cfif (result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT())>
				<cfthrow type="Custom" message="WebService invocation error" detail="API MDM: wipe()">
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset statusCode=VAL(result[remoting.RESULT()])>
			</cfif>
			<cfset var actionLog="WIPE">
			<cfset var deviceLog="SERIAL='#serialValue#' and IMEI='#imeiValue#'">
			<cfif (statusCode EQ 3) OR (statusCode EQ -1)>
				<cfset var code=GLOBALS().CONST("Zenprise.RESULT_ERROR")>
				<cfthrow type="Custom" errorCode="#code#"
				message="#actionLog# failed. StatusCode #statusCode# for #deviceLog#" detail="#actionLog# failed">
			<cfelse>
				<!--- 0 if action is done; 1 for a device connected; 2 if action is asked for a device not connected; 3 if action failed --->
				<cflog type="information" text="wipe('#serialValue#','#imeiValue#'): #statusCode#"> 
				<cfreturn statusCode NEQ 2>
			</cfif>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=serialValue,IMEI=imeiValue}>
				<cfif isDefined("remoting") AND isDefined("result")>
					<!--- Exception provenant du WebService --->
					<cfif ((result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT()))
						AND structKeyExists(result,remoting.RESULT())>
						<cfset errorLog.EXCEPTION=result[remoting.RESULT()]>
					</cfif>
					<cfif structKeyExists(result,remoting.CLIENT_REQUEST())>
						<cfset errorLog.CLIENT_REQUEST=result[remoting.CLIENT_REQUEST()]>
					</cfif>
					<cfif structKeyExists(result,remoting.SERVER_RESPONSE())>
						<cfset errorLog.SERVER_RESPONSE=result[remoting.SERVER_RESPONSE()]>
					</cfif>
				</cfif>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="revoke" returntype="Boolean" hint="Révoque un device. Retourne TRUE si l'action a été effectuée avec succès">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var statusCode=FALSE>
		<cftry>
			<cfset var serialValue=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<!--- Librairie Remoting : SOAP --->
			<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
			<cfset var params=[
				{NAME="serial",VALUE=serialValue},{NAME="imei",VALUE=imeiValue}
			]>
			<cfset var result=remoting.invoke(getDeviceService(),"revoke",params)>
			<cfif (result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT())>
				<cfthrow type="Custom" message="WebService invocation error" detail="API MDM: revoke()">
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset statusCode=result[remoting.RESULT()]>
			</cfif>
			<cfset var actionLog="REVOKE">
			<cfset var deviceLog="SERIAL='#serialValue#' and IMEI='#imeiValue#'">
			<cflog type="information" text="revoke('#serialValue#','#imeiValue#'): #statusCode#"> 
			<cfreturn statusCode>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=serialValue,IMEI=imeiValue}>
				<cfif isDefined("remoting") AND isDefined("result")>
					<!--- Exception provenant du WebService --->
					<cfif ((result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT()))
						AND structKeyExists(result,remoting.RESULT())>
						<cfset errorLog.EXCEPTION=result[remoting.RESULT()]>
					</cfif>
					<cfif structKeyExists(result,remoting.CLIENT_REQUEST())>
						<cfset errorLog.CLIENT_REQUEST=result[remoting.CLIENT_REQUEST()]>
					</cfif>
					<cfif structKeyExists(result,remoting.SERVER_RESPONSE())>
						<cfset errorLog.SERVER_RESPONSE=result[remoting.SERVER_RESPONSE()]>
					</cfif>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn FALSE>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="register" returntype="Boolean" hint="Autorise l'enrollment d'un device. Retourne TRUE en cas de succès">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var statusCode=FALSE>
		<cftry>
			<cfset var serialValue=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<!--- Librairie Remoting : SOAP --->
			<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
			<cfset var params=[
				{NAME="serial",VALUE=serialValue},{NAME="imei",VALUE=imeiValue}
			]>
			<cfset var result=remoting.invoke(getDeviceService(),"authorize",params)>
			<cfif (result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT())>
				<cfthrow type="Custom" message="WebService invocation error" detail="API MDM: register()">
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset statusCode=result[remoting.RESULT()]>
			</cfif>
			<cfset var actionLog="REGISTER">
			<cfset var deviceLog="SERIAL='#serialValue#' and IMEI='#imeiValue#'">
			<cflog type="information" text="register('#serialValue#','#imeiValue#'): #statusCode#"> 
			<cfreturn statusCode>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=serialValue,IMEI=imeiValue}>
				<cfif isDefined("remoting") AND isDefined("result")>
					<!--- Exception provenant du WebService --->
					<cfif ((result[remoting.STATUS()] EQ remoting.ERROR()) OR (result[remoting.STATUS()] EQ remoting.FAULT()))
						AND structKeyExists(result,remoting.RESULT())>
						<cfset errorLog.EXCEPTION=result[remoting.RESULT()]>
					</cfif>
					<cfif structKeyExists(result,remoting.CLIENT_REQUEST())>
						<cfset errorLog.CLIENT_REQUEST=result[remoting.CLIENT_REQUEST()]>
					</cfif>
					<cfif structKeyExists(result,remoting.SERVER_RESPONSE())>
						<cfset errorLog.SERVER_RESPONSE=result[remoting.SERVER_RESPONSE()]>
					</cfif>
				</cfif>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="add" returntype="String" hint="Ajoute un device dans le MDM et retourne son identifiant Zenprise (StringID)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="osFamily" type="String" required="true" hint="OS du device">
		<cfset var code=GLOBALS().CONST("Zenprise.RESULT_ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="Function 'Add device' is not implemented">
	</cffunction>
	
	<cffunction access="public" name="remove" returntype="void" hint="Supprime un device du MDM">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var code=GLOBALS().CONST("Zenprise.RESULT_ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="Function 'Remove device' is not implemented">
	</cffunction>
	
	<cffunction access="public" name="putDeviceProperties" returntype="void" hint="Ajoute les propriétés properties dans celles d'un device">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="properties" type="Array" required="true">
		<cfset var code=GLOBALS().CONST("Zenprise.RESULT_ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="Function 'Put device properties' is not implemented">
	</cffunction>

	<!--- =========== Fin des fonction publiques =========== --->
</cfcomponent>