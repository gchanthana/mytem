package gestioncommande.events
{
	import flash.events.Event;
	
	public class CompteEvent extends Event
	{
		public static const VALID_CHOIX_COMPTE :String = 'VALID_CHOIX_COMPTE';
		
		public var idCompte		:String = '';
		public function CompteEvent(type:String, idcompte:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.idCompte = idcompte;
		}
	}
}