package univers.inventaire.inventaire.recherche
{
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Menu;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;
	import mx.events.NumericStepperEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.inventaire.menu.RechercheMenuEvent;
	
	/**
	 * Classe principale de la Gestion Directe
	 * Permet de sortir ou d'enter dans l'inventaire un produit qui ne fait pas partie d'une opération.
	 * Permet aussi de visualiser l'historique des actions faites sur une ressource
	 * */
	public class PanelResilierProduit extends PanelResilierProduitIHM implements IPanelRecherche
	{
		//Constante qualifiant un produit qui est dans une operation
		private const DANS_OPERATION : int = 1; 
		
		//Constante qualifiant un produit qui n'est pas dans une operation
		private const HORS_OPERATION : int = 0;
		
		//Identifiant de laction commanderProduit
		private const COMMANDER_PRODUIT : int = 29;
		
		//Identifiant de laction résilierProduit
		private const DEMANDE_RESILIATION : int = 9;
		
		//Identifiant arbitraire pour la sauvegarde d'une action
		//supposé etre le parametre "dernier action"
		private const LAST_ACTION : int = 1;
		
		//reference vers l'id de la ressource selectionnée 
		private var currentRessource : int; 
		
		//la liste des ressources 
		[Bindable]
		private var listeRessources : ArrayCollection;
		
		//la liste des ressources sélectionnées
		private var ressourcesSelectionnees : Array;
		
		//La date de la derniere action effectuee sur la ressource
		private var dateCurrentInfoEtape : Date;
		
		
		//L'identifiant du noeud sur lequel on est 
		private var nodeId : int; 
		
		
		//Le libellé du noeud sur lequel on est 
		private var nodeLibelle : String; 
		
		
		//Le type de perimetre pour le noeud sur lequel on est 
		private var nodePerimetre : String;	
				
		//Historique des actions sur une ressource		
		[Bindable]
		private var tabHistorique : ArrayCollection;
		
		
		//Index, dans l'historique, de la derniere action effectuée sur la ressource sélectionnée 
		private var HISTORIQUE_LAST_ELEMENT : int = 0;
		
		
		//Tableau des actions possibles
		[Bindable]
		private var tabActionsPossible : ArrayCollection;
		
		
		//Reference vers un remoting
		private var op : AbstractOperation;	
		
		//Reference vers le remoting de chargement de lhistorique
		private var opHisto : AbstractOperation;
		
		//Reference vers le remoting de chargement des actions possibles		
		private var opActionsPoss : AbstractOperation;
		
		//Reference vers le remoting enregistrat une action
		private var opDoAction : AbstractOperation;
		
		//Reference vers le remoting mettant à jopur une action
		private var opUpDateAction : AbstractOperation;
		
		//Reference vers le menu du PopUpMenuButton des actions possibles
		private var myMenu : Menu;//
		
		//Reference vers l'id de l'action selectionné dans le menu des actions possibles
		private var selectedAction : int;
		
		//Reference vers le commentaire de laction a modifier
		private var commentaireAction : String;
		
		//Reference vers la nouvelle date de l action a modifier
		private var nouvelleDateAction : String;
		
		//Reference vers l'identifiant de l'action sélectionnée dans le menu
		private var idOpAction : int;
		
		/**
		 * Constructeur
		 * */	
		public function PanelResilierProduit()
		{
			super();			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		/**
		 * Met à jour le grid des ressources
		 * 
		 **/
		public function mettreAJourGridRessources():void{
			try{				
				getRessourcesFromNode();													
			}catch(e : Error){
				trace("Erreur mettreAJourGridRessources",e.message,e.getStackTrace());
			}
		}
		
		/**
		 * Reset l'affichage
		 * Efface les infos sur le produit
		 * Efface le filtre
		 * Fait disparaitre la section 'Actions Possibles'
		 * Efface le Tableau de l'historique
		 **/
		public function reset():void{
			try{				
				effacerInfosProduit();
				txtFiltre.text = "";
				desactiverActionsPossibles();
				effacerHistorique();
																	
			}catch(e : Error){
				trace("Erreur mettreAJourGridRessources",e.message,e.getStackTrace());
			}
		}
		
		/**
		 * Reset l'affichage.
		 * Efface les infos sur le produit.
		 * Efface le filtre.
		 * Fait disparaitre la section 'Actions Possibles'.
		 * Efface le tableau de l'historique.
		 * Efface le tabelau des ressources.
		 **/
		public function resetAll():void{
			try{				
				effacerInfosProduit();
				txtFiltre.text = "";
				desactiverActionsPossibles();
				effacerHistorique();
				effacerRessources();
																	
			}catch(e : Error){
				trace("Erreur mettreAJourGridRessources",e.message,e.getStackTrace());
			}
		}
		
		/**
		 * Initialise les parametre pour l' Ecran
		 * - l 'identifiant du noeud
		 * - le libelle du noeud
		 * - le type de perimetre du noeud
		 * @param params
		 * */
		public function setParams(params : RechercheMenuEvent):void{
			nodeId = params.nodeId;//l'identifiant du noeud 
			nodeLibelle = params.nodeLibelle;//le libellé du noeud
			nodePerimetre =	params.nodePerimetre;//le type de perimetre pour le noeud
		}
		
		
		/**
		 * met a jour l' id de la ressource a selectionner
		 * @param id l'IDINVENTAIRE_PRODIT de la ressource
		 * */
		public function setCurrentRessource(id : int):void{
			currentRessource = id;
		}
		
		/**
		 * Gere le changement de perimetre
		 * Sort du panel
		 * */
		public function onPerimetreChange():void{			
			btSortir.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
		}
		
//--- INITIALISATION -------------------------------------------------------------------------------------------------------------------
		
		
		//Initialisation de l'ecran apres l'evenement flexEvent.CREATION_COMPLETE
		//Affecte en place les ecouteurs d'évenement
		//Formate les données pour chaque tableau
		//Met en place le filtre pour le tabelau des ressources
		private function initIHM(fe : FlexEvent):void{	
			
			//-- grid historique ---										
		    myGrtidHistorique.sortableColumns = false;
		    myGrtidHistorique.dataProvider = tabHistorique;	
		   	   
		    
		    //myGrtidHistorique.addEventListener("ModifierAction",enregistrerModifAction);			   
		    DataGridColumn(myGrtidHistorique.columns[1]).labelFunction = formatDateDataTip;
		    DataGridColumn(myGrtidHistorique.columns[4]).labelFunction = formatDIDataTip;
		    //
		   
			//navigation
			btSortir.addEventListener(MouseEvent.CLICK,sortir);	

			//action
			btActionPossible.addEventListener(MouseEvent.CLICK,doAction);
			nsDuree.addEventListener(NumericStepperEvent.CHANGE,changerDateAction);
			dcDateAction.addEventListener(Event.CHANGE,changerDelaiAction);
			dcDateAction.selectedDate = new Date();
			
			//--- filtre ----------------------
			txtFiltre.addEventListener(Event.CHANGE,filtreTextChangeHandler);			
			cbDansInventaire.addEventListener(Event.CHANGE,filtreInvChangeHandler);
			
			
			//--- grid des ressources
			//DataGridColumn(myGridRessources.columns[0]).labelFunction = formatDataTip;
			myGridRessources.allowMultipleSelection = true;
			myGridRessources.addEventListener(Event.CHANGE,manageActionsPossibles);		
			DataGridColumn(myGridRessources.columns[1]).labelFunction = formatDataTip; 			
			myGridRessources.liveScrolling = false;						
			//--- Faire une Commande ----------
			
			/*btnCommander.addEventListener(MouseEvent.CLICK,displayCreatePanel);*/
			
			//Ccharge les ressources ne faisant pas partie d'une opération
			getRessourcesFromNode();
			
			addEventListener(FlexEvent.ADD,rafraichir);
		}
		
		//Rafraichit la page à chaque fois que celle ci doit est montrée
		private function rafraichir(fe : FlexEvent):void{
			getRessourcesFromNode();
		}
		
//--- FIN INITIALISATION ---------------------------------------------------------------------------------------------------------------

// FORMATAGE DES DONNEES ----------------------------------------------------------------------------------------------------------------------
		
		//Formate les dates du tableau 'Historique'
		//Voir l'attribut LabelFunction pour un DataGrid		
		private function formatDateDataTip(item : Object, column : DataGridColumn):String{			
			return DateFunction.formatDateAsString(new Date(item.DATE_ACTION));
		}		
		
		//Formate la colonne 'Dans inventaire' du tableau 'Historique'
		//Voir l'attribut LabelFunction pour un DataGrid
		private function formatDIDataTip(item : Object, column : DataGridColumn):String{			
			return (item.DANS_INVENTAIRE == 1)? "Oui" : "Non";
		}
				
		//Formate les numero de téléphone dans le Tableau des ressources
		//Voir l'attribut LabelFunction pour un DataGrid
		private function formatDataTip(item : Object, column : DataGridColumn):String{
			
			if ((String(item.SOUS_TETE).length == 10)&& parseInt(item.SOUS_TETE).toString().length == 9)
				return ConsoviewFormatter.formatPhoneNumber(item.SOUS_TETE);
			else
				return item.SOUS_TETE;
		}		
		
//FIN FORMATAGE -------------------------------------------------------------------------------------------------------------------------------

//GRID DES RESSOURCES --------------------------------------------------------------------------------------------------------------------------

		//Gere l'activation et la désactivation de la section 
		// 'Actions Possibles' apres une sélection dans le tableau des ressources
		private function manageActionsPossibles(ev : Event):void{
			if (myGridRessources.selectedIndex == -1){		
				
				effacerHistorique();				
				effacerInfosProduit();
				desactiverActionsPossibles();		
				
			}else{
				
				configBoxActionsPossibles(myGridRessources.selectedItems);
			}
		}
						
		//Gere le fitre sur le texte du tableau des ressources
		private function filtreTextChangeHandler(ev : Event):void{
			try{
				listeRessources.filterFunction = filterTextFunc;
				listeRessources.refresh();
				desactiverActionsPossibles();
				effacerHistorique();
				effacerInfosProduit();
			}catch(e : Error){
				trace("Erreur filtre liste ressources");
			}
		}
		
		//Gere le filtre (dans inventaire / hors inventaire) sur le tableau des ressources
		private function filtreInvChangeHandler(ev : Event):void{
			try{
				listeRessources.filterFunction = filterTextFunc;
				listeRessources.refresh();
				desactiverActionsPossibles();
				effacerHistorique();
				effacerInfosProduit();
				myGridRessources.selectedIndex = -1;
			}catch(e : Error){
				trace("Erreur filtre liste ressources");
			}
		}
				
		//Filtre le tableau des ressources suivant le texte taper dans le TextInput (txtFiltre)
		private function filterTextFunc(item : Object):Boolean{
			var matchText : Boolean = 	(String(item.SOUS_TETE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)  
							 			||
								 	  	(String(item.LIBELLE_PRODUIT).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
								 	  	||
								 	  	(String(item.LIBELLE_ETAT).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1);
					 	  	
			var matchInv : Boolean = (Number(item.DANS_INVENTAIRE) == 1);
			
			if (cbDansInventaire.selected){
				if (matchInv && matchText)				
					return true;
				else
					return false;
			}else{
				if (!matchInv && matchText)
					return true;
				else
					return false;				
			}			
			return false;
		}
		
		
		//Regarde si les ressources selectionnes sont dans le meme état
		//si oui retourne true, si non retourne false
		private function dansLeMemeEtat(tab : Array):Boolean{
			var len : int = tab.length;
			var idEtatRef : int = tab[0].IDINV_ETAT;
			 
			for (var i : int = 1; i < len; i++){
				if (tab[i].IDINV_ETAT != idEtatRef) return false;
			}
			
			return true;			
		}
		
		//retourne l'index de l'identifiant passé en param dans un arrayCollection 
		//les parametres sont dans l'ordre :
		// - ID l'identifiant dont on chereche l'index
		// - AC l'arrayCollection dans lequel on doit chercher
		// - COLNAME le nom de la colonne dans laquelle on doit chercher
		private function indexDe(id : int, ac : ArrayCollection,colname : String = "IDINVENTAIRE_PRODUIT"):int{									
			for (var i:int = 0; i < ac.length; i++){					
					 if (id == ac[i][colname]) return i;				 
			}			
			return -1;
		}

//FIN GRID RESSOURCES --------------------------------------------------------------------------------------------------------------------------

//COMMANDER PRODUIT ----------------------------------------------------------------------------------------------------------------------------
		
		
		//montre le Panel des saisies
		// private function displayCreatePanel(e:Event):void {
		//	var evtObj : RechercheMenuEvent = new RechercheMenuEvent("GlSelectionneComm");				
		//	evtObj.nodePerimetre = nodePerimetre;
		//	evtObj.nodeId = nodeId;
		//	evtObj.nodeLibelle = nodeLibelle;		
		//	/* btnCommander.enabled = true; */							
		//	dispatchEvent(evtObj);			
		//} 
		
		
//----------------------------------------------------------------------------------------------------------------------------------------------

//--- INFO PRODUIT -----------------------------------------------------------------------------------------------------------------------------
	 	
	 	
	 	//Effacre les infos. visiuels sur une ressource
	 	private function effacerInfosProduit():void{
	 		txtLigne.text = "";
	 		txtProduit.text = "";
	 		txtQte.text = "";
	 	}
	 	
	 	//Affiche les infos pour une ressource
	 	private function afficherInfosProduit():void{
	 		txtLigne.text = ConsoviewFormatter.formatPhoneNumber(myGridRessources.selectedItem.SOUS_TETE);
	 		txtProduit.text = myGridRessources.selectedItem.LIBELLE_PRODUIT;
	 		txtQte.text = myGridRessources.selectedItem.QTE;
	 		
	 		
	 	}
	 	
	 		 	
	 	//Affiche ou cache  le message "dans operation" suivant le parametre dansOpe
	 	//si dansOpe  != 0 alors on affiche le message sinon on ne l'affiche pas
	 	private function afficherMessageDansOperation(dansOpe : int):void{		
			if (dansOpe != 0){				 
				lblDansOpe.visible = true;				
			}else{				 
				lblDansOpe.visible = false;								
			}			
		}	
		
		
//--- FIN INFO PRODUIT -------------------------------------------------------------------------------------------------------------------------

//--- GRID HISTORIQUE ------------------------------------------------------------------------------------------------------------------
		
		
		//Effacer le tableau Historique
		private function effacerHistorique():void{
			try{
				tabHistorique.source = null;
				tabHistorique.refresh();
			}catch(e : Error){
				trace("pas d'historique a effacer");
			}
		}
		
		//Efface le tabelau des Ressources
		private function effacerRessources():void{
			try{
				listeRessources.source = null;
				listeRessources.refresh();
			}catch(e : Error){
				trace("pas d'historique a effacer");
			}
		}
		
		//Affiche l'historique de la ressource dont l'identifiant est passé en paramètre
		private function afficherHistorique(idressource : int):void{
			chargerHistorique(idressource);
		}
		
		//--- FIN GRID HISTORIQUE --------------------------------------------------------------------------------------------------------------
		
		//--- ACTIONS POSSIBLES ----------------------------------------------------------------------------------------------------------------
		
		//charge les actions possible du ou des produits selectionnés
		//param in : tabProduit le tabelau des ressources selectionnées
		private function configBoxActionsPossibles(tabProduit : Array):void{			
				//si un seul produits			
				if (tabProduit.length == 1){					
					//alors charger les actions possibles
					if (tabProduit[0].DANS_OPERATION != DANS_OPERATION){
						activerActionsPossibles(tabProduit[0].IDINV_ETAT);
						afficherMessageDansOperation(HORS_OPERATION);
					}else{
						desactiverActionsPossibles();		
						afficherMessageDansOperation(DANS_OPERATION);			
					}
					//charger les infos du produits  (libelle,sous-tete,qté)
					afficherInfosProduit();
					afficherHistorique(tabProduit[0].IDINVENTAIRE_PRODUIT);	
				
				}else if (tabProduit.length > 1){
					//si elles sont toutes dans le même état 
					trace("dansLeMemeEtat "+dansLeMemeEtat(tabProduit))
					if (dansLeMemeEtat(tabProduit)){
						
						//effacer les infos produits
						effacerInfosProduit();
						//alors charger actions possibles
						activerActionsPossibles(tabProduit[0].IDINV_ETAT);
						effacerHistorique();						
					}else{
						desactiverActionsPossibles();
						effacerHistorique();
						effacerInfosProduit();
					}
				} else{
					desactiverActionsPossibles();
					effacerHistorique();
					effacerInfosProduit();
				}						
		}
		
				
		//change la date de l'action suivant la durée (Handler du click sur le numeric Stepper)		
		private function changerDateAction(ev : NumericStepperEvent):void{			
			dcDateAction.selectedDate = new Date(dateCurrentInfoEtape.getFullYear(),dateCurrentInfoEtape.getMonth(),dateCurrentInfoEtape.getDate()+ev.value);
			
		}
		
		//change le delai suivant la date de la dernier action sur la ressource selectionnée 
		private function changerDelaiAction(ev : Event):void{		
			nsDuree.value = DateFunction.dateDiff("d",dcDateAction.selectedDate,dateCurrentInfoEtape);		
		}
						
		//formatage des actions possibles
		//param in tab un ArrayCollection contenant les diverses actions possibles
		private function formateActionsPossible(tab : ArrayCollection):ArrayCollection{
			var newArray : ArrayCollection = new ArrayCollection();			
			var ligne : Object;
			
			for (ligne in tab){
				if((tab[ligne].IDINV_ACTIONS != COMMANDER_PRODUIT) && 
					(tab[ligne].IDINV_ACTIONS != DEMANDE_RESILIATION)){
					newArray.addItem(tab[ligne]);
				}
			}
			return newArray
		}
		
		//Extrait la colonne IDINVENTAIRE_PRODUIT d'un tableau de ressources
		//param in un tableau contenant une liste de ressources.
		//param out un tableau d'IDINVENTAIRE_PRODUIT
		private function creerListeInventaire(prdtSelected : Array):Array{
			var len : int = prdtSelected.length;
			var newArray : Array =  new Array();
			
			for (var i : int = 0; i < len; i++){
				newArray.push(prdtSelected[i].IDINVENTAIRE_PRODUIT);
			}
			
			return newArray;
		}
		
		//Desactive le box actions possibles
		private function desactiverActionsPossibles():void{
			try{
				tabActionsPossible.source = null;
				tabActionsPossible.refresh();
				}catch(e : Error){
				trace("Pas d'action possible");
			}
				btActionPossible.enabled = false;
				btActionPossible.visible = false;
				txtCommentaire.editable = false;
				txtCommentaire.visible = false;
				dcDateAction.editable = false;
				dcDateAction.visible = false;
				boxAction.visible = false;
				formActPoss.visible = false;
				
		}
		
		//Active le box actions possibles et charge les actions possibles
		private function activerActionsPossibles(idEtat : int):void{			
			btActionPossible.enabled = true;
			btActionPossible.visible = true;
			txtCommentaire.editable = true;
			txtCommentaire.visible = true;
			dcDateAction.editable = true;
			dcDateAction.visible = true;
			formActPoss.visible = true;
			boxAction.visible = true;
			chargerActionPossible(idEtat);
		}
		
		//initialiser le box actions possibles
		private function initBoxActionsPossibles(info : Object):void{
			var selectableRange : Object = new Object();	 
						
			try{
				selectableRange.rangeStart = new Date(tabHistorique[0].DATE_ACTION);					
			}catch(re : RangeError){
				trace("[LAST_POSITION] erreur index = ");
				selectableRange.rangeStart =  new Date(1970,0,1);
			}catch(e : Error){
				trace("[LAST_POSITION]  pas d'étape avant celle ci ");
			}finally{
				selectableRange.rangeEnd = new Date(2999,0,1); 		
			}
			
			nsDuree.minimum = 0;
			nsDuree.value = parseInt(info.DUREE_ETAT);			
			dateCurrentInfoEtape = new Date(info.DATE_ACTION); 			
			dcDateAction.selectableRange = selectableRange;
			dcDateAction.selectedDate = dateCurrentInfoEtape; 
			if (isNaN(nsDuree.value)) nsDuree.value = DateFunction.dateDiff("d",new Date(),dateCurrentInfoEtape);
		}
			
		//Initialise le PopupMenuButton des actions possibles
		private function initMenu():void {
			myMenu = new Menu();
			myMenu.dataProvider = tabActionsPossible;
			myMenu.labelField = "LIBELLE_ACTION";
			
			try{
				myMenu.selectedIndex = 0;  	
				myMenu.addEventListener("itemClick", menuItemClickHandler);
			
				btActionPossible.label = myMenu.dataProvider[myMenu.selectedIndex].LIBELLE_ACTION;
				selectedAction = myMenu.dataProvider[myMenu.selectedIndex].IDINV_ACTIONS; 
			}catch(re : RangeError){
				trace("erreur d'index");
			}
				btActionPossible.popUp = myMenu;
		}

		//Definit le listener du Menu du PopupMenuButton Actions Possibles 
		private function menuItemClickHandler(event:MenuEvent):void {
			var label:String = event.item.LIBELLE_ACTION; 
			var idaction : int = event.item.IDINV_ACTIONS;       				
			btActionPossible.label = label;				
			selectedAction = idaction; 
			myMenu.selectedIndex = event.index;
			btActionPossible.close();				
		}
		
		//Gere le click sur le bouton du PopupMenuButton Actions Possibles
		private function doAction(me : MouseEvent):void{
			commentaireAction = txtCommentaire.text;			
			sauvegarderAction(selectedAction);
		}
				
		//--- FIN ACTIONS POSSIBLES ------------------------------------------------------------------------------------------------------------
		
		//--- NAVIGATION -----------------------------------------------------------------------------------------------------------------------
		
		
		//Handler du click sur le bouton 'sortir' sort du panel
		private function sortir(me : MouseEvent):void{			
			dispatchEvent(new Event("sortirRechercheResi"));
		}
		
		
		//--- FIN NAVIGATION -------------------------------------------------------------------------------------------------------------------
				
//--- REMOTINGS ------------------------------------------------------------------------------------------------------------------------		
		
		
		//Va chercher la liste des ressources du groupe de lignes dont l'identifiant est nodeId
		private function getRessourcesFromNode():void{
			myGridRessources.enabled = true;
			myGrtidHistorique.enabled = true;
			myGridRessources.dataProvider = null;
			myGrtidHistorique.dataProvider = null;
			desactiverActionsPossibles()
			
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
												  "fr.consotel.consoview.inventaire.cycledevie.ElementDInventaire",
												  "getListeRessources",
												  getLinesFromNodeResultHandler);
												  
			RemoteObjectUtil.callService(op,
										nodeId); 
		}
		
		
		//Handler du remoting lancé par la methode 'getRessourcesFromNode' charge la liste des ressources du noeud sélectionné
		private function getLinesFromNodeResultHandler(re : ResultEvent):void{			
			myGridRessources.enabled = true;
			myGrtidHistorique.enabled = true;			
			
			listeRessources = re.result as ArrayCollection;	
			myGridRessources.dataProvider = listeRessources;
			
			try{
				trace ("currentRessource = "+currentRessource);
				if (currentRessource > 0){
					var idx : int = indexDe(currentRessource,listeRessources);
					
					 myGridRessources.selectedIndex = idx;
					if (idx > 0) var b : Boolean = myGridRessources.scrollToIndex(idx);	
					myGridRessources.liveScrolling = false;							
					
				}else{
						myGridRessources.selectedIndex = 0;					
				}
				
				cbDansInventaire.selected = true;								
				txtFiltre.text = "";
				cbDansInventaire.dispatchEvent(new Event(Event.CHANGE));				
				myGridRessources.dispatchEvent(new Event(Event.CHANGE));
				
				
				
			}catch(e : Error){
				trace("error myGridRessources lastItem");
			}
			
			
			if (listeRessources.length > 0) {
				txtFiltre.enabled = true;				 			
			}
			else {				 
				//txtFiltre.enabled = false;	
				txtFiltre.text = "";
				effacerHistorique();	
				desactiverActionsPossibles();		
			}			
		}
		
		//Va chercher l'historique de la ressource dont l'identifiant est passé en parametre
		//param in idressource l'identifiant d'une ressource (IDINVENTAIRE_PRODUIT)
		private function chargerHistorique(idressource : int):void{
			opHisto = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																"getHistoriqueActionRessource",
																chargerHistoriqueResultHandler);				
			RemoteObjectUtil.callService(opHisto,idressource);		
		}
		
		//Formate les données de la colonne nombre de jours pour le tableau 'Historique'
		//param in source un ArrayCollection brut 
		//param out le meme ArrayCollection formaté		
		private function formatTabHistorique(source : ArrayCollection):ArrayCollection{
			
			for (var i : int = source.length -1 ; i > 0 ; i--){
				var tmp : Number = DateFunction.dateDiff("d",new Date(source[i-1].DATE_ACTION),new Date(source[i].DATE_ACTION))
				var methode : String = "d";				
				var lblJour : String = " jrs";
				
				if (tmp > 365) {
					methode = "y";
					lblJour = " a";									
				}else if (tmp > 30){
						methode = "m";
						lblJour = " m";
				}else{
					methode = "d"
					lblJour = " j";
				}
				
				source[i].DUREE  =  DateFunction.dateDiff(methode,new Date(source[i-1].DATE_ACTION),new Date(source[i].DATE_ACTION))
										+ lblJour;
				
			}
			return source;
		}
		
		//Handler du remoting lancé par la methode 'chargerHistorique' charge la liste des ressources du noeud sélectionné
		private function chargerHistoriqueResultHandler(re :ResultEvent):void{	
			if (re.result != null && (re.result as ArrayCollection).length > 0){				
				tabHistorique = formatTabHistorique(re.result as ArrayCollection); 				
				tabHistorique.refresh();
				HISTORIQUE_LAST_ELEMENT = Number(tabHistorique.length - 1);
				myGrtidHistorique.dataProvider = tabHistorique;	
				initBoxActionsPossibles(tabHistorique[0]);				
			}else if (re.result != null){
				var info : Object = new Object();
				info.DATE_ACTION = new Date();
				initBoxActionsPossibles(info);		
			}else{
				Alert.show("Une erreur c'est produite lors du chargement de l'historique");
			} 
		}			
		
		//Va chercher la liste des actions possibles pour un état
		//param in idEtat l'identifiant d'un état (IDINV_ETAT)
		private function chargerActionPossible(idEtat : int):void{		
 			opActionsPoss = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																			"getListeActionPossibles",
																			chargerActionPossibleResultHandler);				
			RemoteObjectUtil.callService(opActionsPoss,
										 idEtat);							
			
		}
		
		//Handler du remoting lancé par la methode 'chargerActionPossible' charge la liste des action possible pour un état et initialise le menu avec ces valuers
		private function chargerActionPossibleResultHandler(re : ResultEvent):void{			
			tabActionsPossible = formateActionsPossible(re.result as ArrayCollection);		
			callLater(initMenu);				
		}
		
		//Enregistre la action pour la ressource seleectionné
		//param in idAction  l'identifiant de l action à enregistrer(IDINV_ACTION)
		private function sauvegarderAction(idAction : int):void{

			var flagEtat : int;	
			var qte : int = 1;
			var lastAction : int = 0;
			
			
			currentRessource = myGridRessources.selectedItems[0].IDINVENTAIRE_PRODUIT;
			lastAction = myGridRessources.selectedItems[0].IDINV_ACTIONS;
			 
 			opDoAction = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																			"doActionSurRessource",
																			sauvegarderActionResultHandler);				
			RemoteObjectUtil.callService(opDoAction,
										 creerListeInventaire(myGridRessources.selectedItems),
										 idAction,
										 lastAction,
										 dcDateAction.text,
										 txtCommentaire.text,
										 LAST_ACTION,
										 myGridRessources.selectedItems[0].DANS_INVENTAIRE,
										 myGridRessources.selectedItems[0].QTE	)
										 
		}
		
		//Handler du remoting lancé par la methode 'sauvegarderAction' et met à jour le tableau des ressources
		private function sauvegarderActionResultHandler(re : ResultEvent):void{			
			if (((re.result > 0 ) && (myGridRessources.selectedItems.length == 1))
				||((re.result > 0 ) && dansLeMemeEtat(myGridRessources.selectedItems))){
					chargerHistorique( myGridRessources.selectedItems[0].IDINVENTAIRE_PRODUIT);		
					txtCommentaire.text = "";					
				}else{
					Alert.show("Une erreur c'est produite durant l'enregistrement");
				}			 
		
			getRessourcesFromNode();
			
		}
		
		//modifier une action
		/* private function enregistrerModificationAction(idOpaction : int):void{
			opUpDateAction = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																				"modifierAction",
																				modifierActionResultHandler);				

																	
			RemoteObjectUtil.callService(opUpDateAction
										   ,HORS_OPERATION
										   ,idOpaction												  
										   ,nouvelleDateAction
										   ,commentaireAction);	
		}
		
		private function modifierActionResultHandler(re : ResultEvent):void{
			if (parseInt(re.result.toString()) == 1){
				chargerHistorique(myGridRessources.selectedItems[0].IDINVENTAIRE_PRODUIT);
			}else{
				Alert.show("Une erreur s'est produite lors de la modification");
			}
				 
		} */
		//--- FIN REMOTINGS --------------------------------------------------------------------------------------------------------------------
		
	}
}

