package gestionparc.ihm.action.associer.actionAssocierLigneToEqp
{
	import gestionparc.ihm.action.ICommand;

	public class ActionAssocierLigneToEqpCommand implements ICommand
	{
		private var receiver:ActionAssocierLigneToEqpReceiver=null;

		public function ActionAssocierLigneToEqpCommand(rec:ActionAssocierLigneToEqpReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
