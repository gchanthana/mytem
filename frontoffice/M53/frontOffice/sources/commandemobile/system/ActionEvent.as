package commandemobile.system
{
	import flash.events.Event;

	public class ActionEvent extends Event
	{
		public var action:Action;
		
		public static const ACTION_CLICKED:String="actionClicked";
		public static const ACTION_PERFORMED:String="actionPerformed";
		
		public function ActionEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			var actionEvent:ActionEvent = new ActionEvent(ACTION_CLICKED);
			actionEvent.action = action;
			 
			return actionEvent; 
		}
	}
}