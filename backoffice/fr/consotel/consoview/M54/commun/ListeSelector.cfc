<cfcomponent displayname="ListeSelector" hint="Recuperer la liste des selecteurs" output="false">
	
	<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
	<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
	
	<cfif idLangue eq 0>
		<cfset idLangue = 3>
	</cfif>
	
	
	<!--- 
		recuperer le tableau de valeur de la periode
	 --->
	<cffunction name="getValuesMonoPeriode" access="remote" output="false" returntype="Query"
				hint="recuperer le tableau de valeur de la periode">		
		
		<cfset LOCAL.dateDebut	= lsDateFormat(SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_DEB,'dd/mm/yyyy')>
		<cfset LOCAL.dateFin 	= lsDateFormat(SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN,'dd/mm/yyyy')>
		
		<cfif DateDiff("m",SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_DEB,SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN) eq 0>
			<cfset LOCAL.newDateDebut = dateadd("m",-1,SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN)>
			<cfset LOCAL.dateDebut = lsDateFormat(LOCAL.newDateDebut,'dd/mm/yyyy')>
		</cfif>
		
		<cfset LOCAL.maxMonthToDisplay = SESSION.PERIMETRE.STRUCTDATE.PERIODS.MAX_MONTH_TO_DISPLAY>
		
		<cfstoredproc datasource="#Session.OFFREDSN#"  procedure="pkg_m54.get_MonoPeriodeSelector">		
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#LOCAL.dateDebut#" variable="p_datedeb">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#LOCAL.dateFin#" variable="p_datefin">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#LOCAL.maxMonthToDisplay#"	variable="p_nb_mois_max">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idLangue#" variable="p_langid">
			<cfprocresult name="qMonoPeriode">
		</cfstoredproc>
		
	    <cfreturn qMonoPeriode>
	</cffunction>
	
	<!--- 
		recuperer la liste des segments
	 --->
	<cffunction name="geListeSegment" access="remote" output="false" returntype="Query"
				hint="recuperer la liste des segments">
		
		<cfargument name="widget_paramid" type="numeric" required="true">
		
		<cfstoredproc datasource="#Session.OFFREDSN#"  procedure="pkg_m54.geListeSegment">		
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#widget_paramid#" 	variable="p_widget_paramid">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#idLangue#"   		variable="p_idLangue">
				<cfprocresult name="p_retour">
		</cfstoredproc>
		
        <cfreturn p_retour>
	        		
	</cffunction>
	
	<!--- 
		recuperer la liste des type de chart
	 --->
	<cffunction name="getListeTypeChart" access="remote" output="false" returntype="Query"
				hint="recuperer la liste des type de chart">
		
		<cfargument name="widget_paramid" type="numeric" required="true">
		
		<cfstoredproc datasource="#Session.OFFREDSN#"  procedure="pkg_m54.getListeTypeChart">		
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#widget_paramid#" 	variable="p_widget_paramid">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#idLangue#"   		variable="p_idLangue">
				<cfprocresult name="p_retour">
		</cfstoredproc>
		
        <cfreturn p_retour>
	        		
	</cffunction>
	
	<!--- 
		recuperer la liste des themes
	 --->
	<cffunction name="getListeTheme" access="remote" output="false" returntype="Query"
				hint="recuperer la liste des themes">
		
		<cfargument name="widget_paramid" type="numeric" required="true">
		
		<cfstoredproc datasource="#Session.OFFREDSN#"  procedure="pkg_m54.getListeTheme">		
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#widget_paramid#" 	variable="p_widget_paramid">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#idLangue#"   		variable="p_idLangue">
				<cfprocresult name="p_retour">
		</cfstoredproc>
			
	    <cfreturn p_retour>
	        		
	</cffunction>
	
	<!--- 
		recuperer la liste des unités data
	 --->
	<cffunction name="getListeUnitesData" access="remote" output="false" returntype="Query" hint="recuperer la liste des unités data">
		
		<cfargument name="widget_paramid" type="numeric" required="true">
		
		<cfstoredproc datasource="#Session.OFFREDSN#"  procedure="pkg_m54.getListeUnitesData">		
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#widget_paramid#" 	variable="p_widget_paramid">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#idLangue#"   		variable="p_idLangue">
				<cfprocresult name="p_retour">
		</cfstoredproc>
			
	    <cfreturn p_retour>
	        		
	</cffunction>

	
	
	
	<cffunction name="getListePool" access="remote" output="false" returntype="query" hint="recuperer la liste des pools">
		
		<cfargument name="widget_paramid" type="numeric" required="true">
		
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
        <cfset userID   = SESSION.USER.CLIENTACCESSID><!--- l'id de l'utilisateur connecté --->
		
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M331.get_PoolSelector">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#userID#" variable="p_idapp_login ">*
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idracine">
			<cfprocresult name="qPool">
		</cfstoredproc>				
        <cfreturn qPool>
	        		
	</cffunction>
	
</cfcomponent>