package intranet.gestionLoginClasse
{
	import flash.events.KeyboardEvent;
	import mx.events.FlexEvent;
	import flash.events.FocusEvent;
	
	public class ComposantInputAdrIp extends ComposantInputAdrIPIHM
	{
		private var _ipPart1 : String;
		private var _ipPart2 : String;
		private var _ipPart3 : String;
		private var _ipPart4 : String;
		
		public function ComposantInputAdrIp(_ipPart1 : String = "-1" ,_ipPart2 : String = "-1",_ipPart3 : String= "-1",_ipPart4 : String= "-1")
		{
				this._ipPart1 = _ipPart1;
				this._ipPart2 = _ipPart2;
				this._ipPart3 = _ipPart3;
				this._ipPart4 = _ipPart4;
				
				addEventListener(FlexEvent.CREATION_COMPLETE,initIHM)
		}
		private function initIHM(evt : FlexEvent):void
		{
			labErreur.setVisible(false);
			
			
		
			if (_ipPart1 != "-1")
			{
				inIp1.text = _ipPart1.toString();
				inIp2.text = _ipPart2.toString();
				inIp3.text = _ipPart3.toString();
				inIp4.text = _ipPart4.toString();
				
			}
			inIp1.restrict = "0123456789";
			inIp2.restrict = "0123456789";
			inIp3.restrict = "0123456789";
			inIp4.restrict = "0123456789";
			
						
			inIp1.maxChars = 3;
			inIp2.maxChars = 3;
			inIp3.maxChars = 3;
			inIp4.maxChars = 3;
						
			inIp1.addEventListener(KeyboardEvent.KEY_UP,gestionFocus1);
			inIp2.addEventListener(KeyboardEvent.KEY_UP,gestionFocus2);
			inIp3.addEventListener(KeyboardEvent.KEY_UP,gestionFocus3);
			inIp4.addEventListener(KeyboardEvent.KEY_UP,gestionFocus4);
			
			inIp1.addEventListener(FocusEvent.FOCUS_OUT,gestionMSG);
			inIp2.addEventListener(FocusEvent.FOCUS_OUT,gestionMSG);
			inIp3.addEventListener(FocusEvent.FOCUS_OUT,gestionMSG);
			inIp4.addEventListener(FocusEvent.FOCUS_OUT,gestionMSG);
		}
		private function gestionFocus1(evt : KeyboardEvent):void
		{
			if (inIp1.length==3){
				if(parseInt(inIp1.text) > 255)
				{
					labErreur.setVisible(true);
				}
				else
				{
					inIp2.setFocus();
					labErreur.setVisible(false);
				}				
			}
		}
		private function gestionFocus2(evt : KeyboardEvent):void
		{
			if  (inIp2.length==3){
				if(parseInt (inIp2.text) > 255)
				{
					labErreur.setVisible(true);
				}
				else
				{ 
					inIp3.setFocus();
					labErreur.setVisible(false);
				}				
			}
			
		}
		private function gestionFocus3(evt : KeyboardEvent):void
		{
			if  (inIp3.length==3){
				if(parseInt( inIp3.text) > 255)
				{
					labErreur.setVisible(true);
				}
				else
				{
					inIp4.setFocus();
					labErreur.setVisible(false);
				}				
			}
		}
		private function gestionFocus4(evt : KeyboardEvent):void
		{
			if  (inIp4.length==3){
				if(parseInt(inIp4.text)> 255)
				{
					labErreur.setVisible(true);
				}
				else
				{
					gestionMSG(null);
					//comboClasse.setFocus();
					//labErreur.setVisible(false);
				}				
			}
		}
		private function gestionMSG(evt : FocusEvent):void{
			if((parseInt( inIp1.text) > 255)||(parseInt(inIp2.text) > 255)||(parseInt (inIp3.text) > 255)||(parseInt( inIp4.text) > 255)||(parseInt (inIp1.text) <0)||(parseInt (inIp2.text) <0)||(parseInt (inIp3.text) <0)||(parseInt (inIp4.text) <0))
			{
				labErreur.setVisible(true);
			}
			else
			{
				labErreur.setVisible(false);
			}
		}
	}
}