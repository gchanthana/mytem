package rapportPilotage.service.monoperiodeselector
{
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;
	import rapportPilotage.event.RapportEvent;
	import rapportPilotage.vo.ValeurSelector;

	internal class MonoPeriodeSelectorModel  extends EventDispatcher
	{
		private var _valuesMonoPeriode :ArrayCollection;
	
		
		public function MonoPeriodeSelectorModel():void
		{
			_valuesMonoPeriode=new ArrayCollection();
		}
		public function get getValuesMonoPeriode():ArrayCollection
		{
			return _valuesMonoPeriode;
		}
		internal function updateValues(value:ArrayCollection):void
		{
			_valuesMonoPeriode.removeAll();
			
			var valeurSelector:ValeurSelector;
			
			for(var i:int=0;i<value.length;i++)
			{
				valeurSelector = new ValeurSelector();
				valeurSelector.fill(value[i]);
				_valuesMonoPeriode.addItem(valeurSelector); 
			}
			this.dispatchEvent(new RapportEvent(RapportEvent.USER_MONO_PERIODE_SELECTOR_EVENT));
		}
	}
}