package composants.tb.tableaux
{
	import flash.events.Event;
	
	import vo.TypeSegment;

	public class TableauChangeEvent extends Event
	{
		public var LIBELLE 			:String;
		private var _SEGMENT 			:String;
		public var QUANTITE 		:String;
		public var VOLUME 			:String;
		public var TYPE_THEME 		:String;
		public var SUR_THEME 		:String;
		public var MONTANT_TOTAL 	:String;
		public var TYPE 			:String;
		public var ID 				:String;
		public var THEME 			:String;
		public var NBAPPELS 		:String;
		public var IDPRODUIT 		:String;
		public var OPERATEUR 		:String;
		public var SOURCE 			:Object;
		public var IDENTIFIANT 		:String;
		public var ORDRE_AFFICHAGE 	:String;
		public var ID_LIBELLE 		:String;
		public var IDTYPE_THEME		:String;
		private var _IDSEGMENT_THEME	:Number;
		public var IDSUR_THEME		:Number;
		
		public function TableauChangeEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
			
		}
		
		public function get IDSEGMENT_THEME():Number
		{
			return _IDSEGMENT_THEME;
		}

		/*public function set IDSEGMENT_THEME(value:String):void
		{
			_IDSEGMENT_THEME = value;
		}*/

		public function get SEGMENT():String
		{	 
			return _SEGMENT;
		}

		public function set SEGMENT(value:String):void
		{
			_IDSEGMENT_THEME = TypeSegment.getIdSegment(value);
			_SEGMENT = value;
		}
		
	}
}