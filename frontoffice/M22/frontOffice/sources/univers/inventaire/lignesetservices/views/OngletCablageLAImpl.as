package univers.inventaire.lignesetservices.views
{
	import composants.controls.TextInputLabeled;
	
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	
	import mx.containers.FormItem;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.managers.PopUpManager;
	import mx.managers.PopUpManagerChildList;
	
	import univers.inventaire.lignesetservices.vo.OngletLAVo;
	import univers.inventaire.lignesetservices.vo.TrancheSDAVo;
	
	[Bindable]
	public class OngletCablageLAImpl extends BaseView implements IOngletCablage
	{
		public var NOM_SITE_INSTALL:Label;
		public var ADRESSE1_INSTALL:Label;
		public var ADRESSE2_INSTALL:Label;
		public var ZIPCODE_INSTALL:Label;
		public var VILLE_INSTALL:Label;
		
		public var REGLETTE1:TextInputLabeled;
		public var PAIRE1:TextInputLabeled;
		public var REGLETTE2:TextInputLabeled;
		public var PAIRE2:TextInputLabeled;
		public var BAT:TextInputLabeled;
		public var ETAGE:TextInputLabeled;
		public var SALLE:TextInputLabeled;
		public var NOEUD:TextInputLabeled;
		public var AMORCE:TextInputLabeled;
		public var CARTE_PABX:TextInputLabeled;
		public var COMMENTAIRE_CABLAGE:TextArea;
		
		public var NOMBRE_POSTES:TextInputLabeled;
		public var SECRET_IDENTIFIANT:CheckBox;
		public var PUBLICATION_ANNUAIRE:CheckBox;
		public var DISCRIMINATION:TextInputLabeled;
				
		public var libelle7 : FormItem;
		public var libelle8 : FormItem;		
		public var V7 : TextInputLabeled;
		public var V8 : TextInputLabeled;
		
		public var TRANCHES_SDA : DataGrid;
		
		public var btUpdate : Button;
		public var btUpdateTranche : Button;
		public var btAjouterSDA : Button;
		public var btSupprimerSDA : Button;
	
		protected var addTrancheWindow : AddTrancheView;
		
		public function OngletCablageLAImpl()
		{	
			super();
		}
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);
			btUpdateTranche.addEventListener(MouseEvent.CLICK,btUpdateTrancheClickHandler);
			btAjouterSDA.addEventListener(MouseEvent.CLICK,btAjouterSDAClickHandler);
			btSupprimerSDA.addEventListener(MouseEvent.CLICK,btSupprimerSDAClickHandler);
		}
		
				
		
		//======================== HANDLERS ==========================================
		protected function btUpdateClickHandler(me : MouseEvent):void{
			doMapping();
			gestionTechnique.gestionCablage.majInfosOnglet();
		}
		protected function btAjouterSDAClickHandler(me : MouseEvent):void{			
			afficherLaFenetreDEditionDeLaTranche(new TrancheSDAVo());
		}
		protected function btSupprimerSDAClickHandler(me : MouseEvent):void{
			if (TRANCHES_SDA.selectedItem != null){
				gestionTechnique.gestionCablage.selectedTrancheSda = TRANCHES_SDA.selectedItem as TrancheSDAVo;
				gestionTechnique.gestionCablage.delTranche();
				
				//TEST --------------->
				//gestionTechnique.gestionCablage.listeTraches.removeItemAt(gestionTechnique.gestionCablage.listeTraches.getItemIndex(TRANCHES_SDA.selectedItem));
				
			}
		}
		protected function btUpdateTrancheClickHandler(me : MouseEvent):void{
			if (TRANCHES_SDA.selectedItem != null){ 
				afficherLaFenetreDEditionDeLaTranche(TRANCHES_SDA.selectedItem as TrancheSDAVo);
			}
		}
		protected function addTrancheWindowValiderClickedHandler(ev : Event):void{
			if (addTrancheWindow != null && addTrancheWindow.trancheSDA != null){
				
				
				if (addTrancheWindow.trancheSDA.IDTRANCHE_SDA > 0){
					gestionTechnique.gestionCablage.selectedTrancheSda = addTrancheWindow.trancheSDA;
					gestionTechnique.gestionCablage.majTranche();
				}else{
					gestionTechnique.gestionCablage.selectedTrancheSda = addTrancheWindow.trancheSDA;
					gestionTechnique.gestionCablage.addTranche();	
				}
					
				PopUpManager.removePopUp(addTrancheWindow);
				addTrancheWindow = null;
			}
		}
		protected function addTrancheWindowAnnulerClickedHandler(ev : Event):void{
			
		}
		//======================== FIN HANDLERS ======================================
		
		//======================== PROTECTED =========================================
		protected function doMapping():void{
			if(gestionTechnique != null){
				var item : ItoXMLParams = gestionTechnique.gestionCablage.ongletVo;
								
				OngletLAVo(item).REGLETTE1=REGLETTE1.text;
				OngletLAVo(item).PAIRE1=PAIRE1.text;
				OngletLAVo(item).REGLETTE2=REGLETTE2.text;
				OngletLAVo(item).PAIRE2=PAIRE2.text;
				OngletLAVo(item).BAT=BAT.text;
				OngletLAVo(item).ETAGE=ETAGE.text;
				OngletLAVo(item).SALLE=SALLE.text;
				OngletLAVo(item).NOEUD=NOEUD.text;
				OngletLAVo(item).AMORCE=AMORCE.text;
				OngletLAVo(item).CARTE_PABX=CARTE_PABX.text;
				OngletLAVo(item).COMMENTAIRE_CABLAGE=COMMENTAIRE_CABLAGE.text;
				
				OngletLAVo(item).NOMBRE_POSTES=Number(NOMBRE_POSTES.text);
				OngletLAVo(item).SECRET_IDENTIFIANT=(SECRET_IDENTIFIANT.selected)?1:0;
				OngletLAVo(item).PUBLICATION_ANNUAIRE=(PUBLICATION_ANNUAIRE.selected)?1:0;
				OngletLAVo(item).DISCRIMINATION=DISCRIMINATION.text;
					
				OngletLAVo(item).V7 = V7.text;
				OngletLAVo(item).V8 = V8.text; 	
			}
			
		}
		//==================== FIN PROTECTED =========================================
		
		//==================== PRIVATE ===============================================
		private function afficherLaFenetreDEditionDeLaTranche(tranche : TrancheSDAVo):void{
			if (addTrancheWindow == null) addTrancheWindow = new AddTrancheView();
			
			tranche.IDSOUS_TETE = gestionTechnique.ligne.IDSOUS_TETE;
			addTrancheWindow.trancheSDA = tranche; 
			addTrancheWindow.modeEcriture = modeEcriture;
			
			addTrancheWindow.addEventListener(AddTrancheImpl.VALIDER_CLICKED,addTrancheWindowValiderClickedHandler);
			addTrancheWindow.addEventListener(AddTrancheImpl.ANNULER_CLICKED,addTrancheWindowAnnulerClickedHandler);
			PopUpManager.addPopUp(addTrancheWindow,DisplayObject(parentApplication),true,PopUpManagerChildList.PARENT);
		}
		//====================FIN PRIVATE ============================================
		
	}
}