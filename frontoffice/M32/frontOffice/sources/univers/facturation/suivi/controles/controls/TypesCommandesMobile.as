package univers.facturation.suivi.controles.controls
{
	public class TypesCommandesMobile
	{

		//											IDINV_TYPE_OPE | IDINV_ACTIONS_INITIALES | TYPE_OPERATION | 			DANS_INVENTAIRE_INIT | SID
		//---MOBILE
		public static const NOUVELLES_LIGNES		:Number = 1083;//--- 2066				Commande de nouvelles lignes mobiles	0				21644
		public static const EQUIPEMENTS_NUS			:Number = 1182;//--- 2066				Commande d'équipements mobile nus		0				21647
		public static const MODIFICATION_OPTIONS	:Number = 1282;//--- 2066				Modification d'options ligne mobile		1				21645
		public static const RESILIATION				:Number = 1283;//--- 2066				Résiliation d'un  abonnement mobile		1				21646
		public static const RENOUVELLEMENT			:Number = 1382;//--- 2066				Réengagement							1				21648
		public static const SUSPENSION				:Number = 1383;//--- 2066				Suspension d'un abonnement				1				24998
		public static const REACTIVATION			:Number = 1483;//--- 2066				Réactivation							1				25397
		
		//---FIXE
		public static const NVL_LIGNE_FIXE			:Number = 1583;//--- 2216				Commande de nouvelles lignes fixes		0				25954
		public static const EQU_NUS_FIXE			:Number = 1584;//--- 2216				Commande d'équipements fixe nus			0				25956
		public static const MODIFICATION_FIXE		:Number = 1585;//--- 2216				Modification d'options ligne fixe		1				25958
		public static const RESILIATION_FIXE		:Number = 1586;//--- 2216				Résiliation d'un  abonnement fixe		1				25960
		public static const RENOUVELLEMENT_FIXE		:Number = 1587;//--- 2216				Réengagement fixe						1				25962
		public static const SUSPENSION_FIXE			:Number = 1588;//--- 2216				Suspension d'un abonnement fixe			1				25964
		public static const REACTIVATION_FIXE		:Number = 1589;//--- 2216				Réactivation fixe						1				25966

		
		
		public function TypesCommandesMobile()
		{
		}
	}
}