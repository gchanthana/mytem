package intranet.GestionProduit
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class GestionProduit extends Canvas
	{
		public function GestionProduit()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init)
		}
		
		public function init(e:FlexEvent):void{
			initTab();
		}
		
// ON REMPLIT LES DIFFERENTS TABLEAUX - TABCONSO - TABABO
		public function initTab():void{			
			var tmpOperation:AbstractOperation= new AbstractOperation(); 
	// ABONNEMENT		
			tmpOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
			"fr.consotel.consoprod.gestionproduits.GestionProduits",
			"getListeTheme",
			Result,Fault);
			RemoteObjectUtil.callService(tmpOperation);
		}
		
		public function Result(e:ResultEvent):void{
			var t:ArrayCollection= e.result as ArrayCollection;
			var s:String;
			var a:Array;
			
			for(var i:int=0;i<t.length;i++){
				s=t[i].THEME;
				a=s.split('/');
				
				if(a[1] == "Abonnements"){
					if(a[0] == "Data"){
						s= a[0]+"     =>     "+a[2]+" /  "+a[3];
					}
					if(a[0] == "Fixe"){
						s= a[0]+"      =>     "+a[2]+" / "+a[3];
					}
					if(a[0] == "Mobile"){
						s= a[0]+"   =>     "+a[2]+" / "+a[3];
					}
					t[i].THEME=s;
					DataSample.TabAbo.addItem(t[i]);					
				}

				if(a[1] == "Consommations"){
					if(a[0] == "Data"){
						s= a[0]+"     =>     "+a[2]+" / "+a[3];
					}
					if(a[0] == "Fixe"){
						s= a[0]+"      =>     "+a[2]+" / "+a[3];
					}
					if(a[0] == "Mobile"){
						s= a[0]+"   =>     "+a[2]+" / "+a[3];
					}
					t[i].THEME=s;
					DataSample.TabConso.addItem(t[i]);				}
			}
		}	
		
		public function Fault(e:FaultEvent):void{
			trace("GestionProduit - initTab() :"+e.message);
		}	
	}
}