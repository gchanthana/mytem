package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasSousEqpAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;

	public class RuleDissocierSousEqpForEqp implements IRuleDisplayItem
	{
		public function RuleDissocierSousEqpForEqp()
		{
		}
		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem = new IsLigneFixe();
			var r2:IRuleDisplayItem = new HasSousEqpAssociated();
			
			if(r1.isDisplay()&& r2.isDisplay())
				return true;
			else
				return false;
		}
	}
}