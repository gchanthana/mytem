<cfcomponent output="false" alias="fr.consotel.consoview.facturation.suiviFacturation.controles.Produit">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="id" type="numeric" default="0">
	<cfproperty name="idCli" type="numeric" default="0">
	<cfproperty name="idCat" type="numeric" default="0">
	<cfproperty name="libelle" type="string" default="">
	<cfproperty name="segment" type="string" default="">
	<cfproperty name="themeId" type="numeric" default="0">
	<cfproperty name="themeLibelle" type="string" default="">
	<cfproperty name="operateurId" type="numeric" default="0">
	<cfproperty name="operateurLibelle" type="string" default="">
	<cfproperty name="typeTheme" type="string" default="">
	<cfproperty name="typeTrafic" type="string" default="">
	<cfproperty name="typeTraficId" type="string" default="">
	<cfproperty name="poids" type="numeric" default="0">
	<cfproperty name="pourControle" type="boolean" default="1">
	<cfproperty name="idVersion" type="numeric" default="0">
	<cfproperty name="nbVersions" type="numeric" default="0">
	<cfproperty name="prixUnitaire" type="numeric" default="0">
	<cfproperty name="prixRemise" type="numeric" default="0">
	<cfproperty name="montant" type="numeric" default="0">
	<cfproperty name="remiseContrat" type="numeric" default="0">
	<cfproperty name="dateDebut" type="date" default="">
	<cfproperty name="dateFin" type="date" default="">

	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.id = 0;
		variables.idCli = 0;
		variables.idCat = 0;
		variables.libelle = "";
		variables.segment = "";
		variables.themeId = 0;
		variables.themeLibelle = "";
		variables.operateurId = 0;
		variables.operateurLibelle = "";
		variables.typeTheme = "";
		variables.typeTrafic = "";
		variables.typeTraficId = "";
		variables.poids = 0;
		variables.pourControle = 1;
		variables.idVersion = 0;
		variables.nbVersions = 0;
		variables.prixUnitaire = 0;
		variables.prixRemise = 0;
		variables.montant = 0;
		variables.remiseContrat = 0;
		variables.dateDebut = "";
		variables.dateFin = "";
	</cfscript>

	<cffunction name="init" output="false" returntype="Produit">
		<cfreturn this>
	</cffunction>
	
	<cffunction name="getId" output="false" access="public" returntype="any">
		<cfreturn variables.Id>
	</cffunction>

	<cffunction name="setId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.Id = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getIdCli" output="false" access="public" returntype="any">
		<cfreturn variables.IdCli>
	</cffunction>

	<cffunction name="setIdCli" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IdCli = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getIdCat" output="false" access="public" returntype="any">
		<cfreturn variables.IdCat>
	</cffunction>

	<cffunction name="setIdCat" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IdCat = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.Libelle>
	</cffunction>

	<cffunction name="setLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.Libelle = arguments.val>
	</cffunction>
	
	<cffunction name="getSegment" output="false" access="public" returntype="any">
		<cfreturn variables.segment>
	</cffunction>

	<cffunction name="setSegment" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.segment = arguments.val>
	</cffunction>
	
	<cffunction name="getThemeId" output="false" access="public" returntype="any">
		<cfreturn variables.ThemeId>
	</cffunction>

	<cffunction name="setThemeId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.ThemeId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getThemeLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.ThemeLibelle>
	</cffunction>

	<cffunction name="setThemeLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.ThemeLibelle = arguments.val>
	</cffunction>

	<cffunction name="getOperateurId" output="false" access="public" returntype="any">
		<cfreturn variables.OperateurId>
	</cffunction>

	<cffunction name="setOperateurId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.OperateurId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getOperateurLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.OperateurLibelle>
	</cffunction>

	<cffunction name="setOperateurLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.OperateurLibelle = arguments.val>
	</cffunction>

	<cffunction name="getTypeTheme" output="false" access="public" returntype="any">
		<cfreturn variables.TypeTheme>
	</cffunction>

	<cffunction name="setTypeTheme" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.TypeTheme = arguments.val>
	</cffunction>

	<cffunction name="getTypeTrafic" output="false" access="public" returntype="any">
		<cfreturn variables.TypeTrafic>
	</cffunction>

	<cffunction name="setTypeTrafic" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.TypeTrafic = arguments.val>
	</cffunction>

	<cffunction name="getTypeTraficId" output="false" access="public" returntype="any">
		<cfreturn variables.TypeTraficId>
	</cffunction>

	<cffunction name="setTypeTraficId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.TypeTraficId = arguments.val>
	</cffunction>

	<cffunction name="getPoids" output="false" access="public" returntype="any">
		<cfreturn variables.Poids>
	</cffunction>

	<cffunction name="setPoids" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.Poids = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPourControle" output="false" access="public" returntype="any">
		<cfreturn variables.PourControle>
	</cffunction>

	<cffunction name="setPourControle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsBoolean(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.PourControle = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid boolean"/>
		</cfif>
	</cffunction>

	<cffunction name="getIdVersion" output="false" access="public" returntype="any">
		<cfreturn variables.IdVersion>
	</cffunction>

	<cffunction name="setIdVersion" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IdVersion = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getNbVersions" output="false" access="public" returntype="any">
		<cfreturn variables.nbVersions>
	</cffunction>

	<cffunction name="setNbVersions" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.nbVersions = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getPrixUnitaire" output="false" access="public" returntype="any">
		<cfreturn variables.PrixUnitaire>
	</cffunction>

	<cffunction name="setPrixUnitaire" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.PrixUnitaire = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getPrixPrixRemise" output="false" access="public" returntype="any">
		<cfreturn variables.PrixRemise>
	</cffunction>

	<cffunction name="setPrixRemise" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.PrixRemise = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	
	<cffunction name="getMontant" output="false" access="public" returntype="any">
		<cfreturn variables.montant>
	</cffunction>
	
	
	
	<cffunction name="setMontant" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.montant= arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	
	<cffunction name="getRemiseContrat" output="false" access="public" returntype="any">
		<cfreturn variables.RemiseContrat>
	</cffunction>

	<cffunction name="setRemiseContrat" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.RemiseContrat = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateDebut" output="false" access="public" returntype="any">
		<cfreturn variables.DateDebut>
	</cffunction>

	<cffunction name="setDateDebut" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DateDebut = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateFin" output="false" access="public" returntype="any">
		<cfreturn variables.DateFin>
	</cffunction>

	<cffunction name="setDateFin" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DateFin = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="setProprietes" output="false" access="public" returntype="void">
		<cfargument name="proprietes" type="any" required="true">
		
		<cfscript>
		//Initialize the CFC with the properties values.
			setId(proprietes.IDPRODUIT_CLIENT);
			setIdCat(proprietes.IDPRODUIT_CATALOGUE);
			setLibelle(proprietes.LIBELLE_PRODUIT);
			setThemeId(proprietes.IDTHEME_PRODUIT);
			setThemeLibelle(proprietes.THEME_LIBELLE);
			setOperateurId(proprietes.OPERATEURID);
			setOperateurLibelle(proprietes.OPNOM);
			setTypeTheme(proprietes.TYPE_THEME);
			
	//		setTypeTrafic(proprietes.TYPE_TRAFFIC);
	//		setTypeTraficId(proprietes.);
			setPoids(proprietes.POIDS);
			setPourControle(proprietes.FLAG_CLIENT);
			setIdVersion(proprietes.IDPRODUIT_CLIENT_VERSION);
			setNbVersions(proprietes.NB_VERSIONS);
			setPrixUnitaire(proprietes.PRIX_UNIT);
			setPrixRemise(proprietes.PRIX_REMISE);
			
			setMontant(proprietes.MONTANT);
			setRemiseContrat(proprietes.REMISE);
			setDateDebut(proprietes.DATEDEBUTVALIDITE);
			setDateFin(proprietes.DATEFINVALIDITE);
		</cfscript>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/06/2007
		
		Description : Met à jour le tague pour le controle du produit (0 ou 1)
		
		Param in 
		Param out
			Number 1 si ok sinon -1
	
	--->
	<cffunction name="updateTague" access="public" output="false" returntype="numeric">
		<!--- TODO: PKG_CV_FACTURATION.UPDATE_TAGUEPRODUIT --->
		<!--- 
			PROCEDURE			
				PKG_CV_FACTURATION.UPDATE_TAGUEPRODUIT
			PARAM
				in
				p_idProduitClient	INTEGER
				p_value				INTEGER
				
				out
				p_result 	INTEGER
		 --->
		 <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_FACTURATION.UPDATE_ETATS_FACTURE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idProduitClient" value="#this.getId()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_value" value="#this.getPourControle()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="result"/>		
		</cfstoredproc>		
		<cfreturn result>
	</cffunction>
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/06/2007
		
		Description : charge le produit
		
		Param in 
		Param out
			Produt
	
	--->
	<cffunction name="load" access="public" output="false" returntype="Produit">
		<!--- TODO: PKG_CV_FACTURATION.GET_PRODUIT --->
		
		<!--- 
			PROCEDURE			
				PKG_CV_FACTURATION.GET_PRODUIT
			PARAM
				in
				p_idProduitClient	INTEGER				
				
				out
				p_result 	QUERY
		 --->
		 <!--- <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_FACTURATION.UPDATE_ETATS_FACTURE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idProduitClient" value="#this.getId()#"/>
			<cfprocresult name="proprietes">
		</cfstoredproc>	 --->	
		
		
		
		<cfreturn THIS>
	</cffunction>
	
</cfcomponent>