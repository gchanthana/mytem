<cfcomponent displayname="RechObject" hint="Objet utilisï¿½ pour la sauvegarde de la recherche" output="false" alias="fr.consotel.consoview.rechercheglobale.RechObject">
 
	<cfproperty name="APP_LOGINID" displayname="APP_LOGINID" hint="le l'identifiant de l'utilisateur" type="numeric" default="0" />
	<cfproperty name="IDRACINE" displayname="IDRACINE" hint="L'identifiant du groupe racine" type="numeric" default="0" />
	<cfproperty name="LIBELLE_GROUPE" displayname="LIBELLE_GROUPE" hint="Le libellï¿½ du groupe" type="string" default="" />
	<cfproperty name="LIGNES" displayname="LIGNES" hint="La liste des lignes" type="string" default="" />
	<cfproperty name="ID" displayname="ID" hint="L'identifiant du groupe de recherche" type="numeric" default="" />
	   
	
	<cfscript>
		//Initialise le CFC avec les valeurs par defauts
		variables.ID = 0;
		variables.APP_LOGINID = 0;
		variables.IDRACINE = 0;
		variables.LIBELLE_GROUPE = "";
		variables.LIGNES = "";
	</cfscript>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/23/2007
		
		Description : Initialise le composant
		
		Methode non utilisï¿½e
	
	--->
	<cffunction name="init" hint="Initialise le composant" access="public" output="false" returntype="RechObject">	
		<cfargument name="params" required="false" type="any">
		<cfscript>			
			load(params);			
			return this;
		</cfscript>
	</cffunction>	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 9/7/2007
		
		Description : Accesseurs
	
	--->	
	<cffunction name="getAPP_LOGINID" access="public" output="false" returntype="numeric">
		<cfreturn session.user.CLIENTACCESSID />
	</cffunction>

	<cffunction name="setAPP_LOGINID" access="public" output="false" returntype="void">
		<cfargument name="APP_LOGINID" type="numeric" required="true" />
		<cfset variables.APP_LOGINID = arguments.APP_LOGINID />
		<cfreturn />
	</cffunction>


	<cffunction name="getID" access="public" output="false" returntype="numeric">
		<cfreturn variables.ID/>
	</cffunction>

	<cffunction name="setID" access="public" output="false" returntype="void">
		<cfargument name="ID" type="numeric" required="true" />
		<cfset variables.ID = arguments.ID />
		<cfreturn />
	</cffunction>



	<cffunction name="getIDRACINE" access="public" output="false" returntype="numeric">
		<cfreturn session.perimetre.ID_GROUPE />
	</cffunction>

	<cffunction name="setIDRACINE" access="public" output="false" returntype="void">
		<cfargument name="IDRACINE" type="numeric" required="true" />
		<cfset variables.IDRACINE = arguments.IDRACINE />
		<cfreturn />
	</cffunction>

	<cffunction name="getLIBELLE_GROUPE" access="public" output="false" returntype="string">
		<cfreturn variables.LIBELLE_GROUPE />
	</cffunction>

	<cffunction name="setLIBELLE_GROUPE" access="public" output="false" returntype="void">
		<cfargument name="LIBELLE_GROUPE" type="string" required="true" />
		<cfset variables.LIBELLE_GROUPE = arguments.LIBELLE_GROUPE />
		<cfreturn />
	</cffunction>
	
	<cffunction name="getLIGNES" access="public" output="false" returntype="string">
		<cfreturn variables.LIGNES/>
	</cffunction>

	<cffunction name="setLIGNES" access="public" output="false" returntype="void">
		<cfargument name="LIGNES" type="string" required="true" />
		<cfset variables.LIGNES = arguments.LIGNES/>
		<cfreturn />
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/23/2007
		
		Description : Enregistre une recherche sous forme de GroupeLigne avec :
					- TYPE_ORGA = SAVE
					- TYPE_DECOUPAGE = 2 (GroupeLigne)		
		Param out
			idGroupeClient si ok sion -1
	
	--->
	<cffunction name="save" displayname="save" hint="Crï¿½ï¿½ un groupe de ligne de type SAVE" access="public" output="false" returntype="string">
		<cfset var qSave = "">
		<cfset var pResult = -1>	 
		<cfset var pRtAffect = -1>
		<cfset var pRtAddNode = -1>
		<cfset retString = "">
	 	
	 	<!--- Creation d'une nouvelle orga de type SAVE'--->
 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.add_organisation">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getIDRACINE()#" null="false">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="true">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#getLIBELLE_GROUPE()#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="Sauvegarde" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="4" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_CHAR" value="SAV" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="0" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="0" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="0" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="-1" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="pResult">			
		</cfstoredproc>
					
		<cfif pResult gt 0>
			<cfset idGroupeSource = session.perimetre.ID_PERIMETRE>
			<cfset idGroupeRacine = session.perimetre.ID_GROUPE>
			<cfset pRtAccess = 1>
			
			
			<cfif idGroupeSource neq idGroupeRacine>
				<!--- Affectation des droits d'acces --->						
			 	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GLOBAL.COPY_NODES_ACCES_V2">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getAPP_LOGINID()#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupeSource#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#pResult#" null="false">
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="pRtAccess">
				</cfstoredproc>	
			</cfif>
			
			<cfif pRtAccess gt 0>
				<!--- Creation du noeud de sauvegarde --->
		 		<cfset libelle = "Rï¿½sultat - " & getLIBELLE_GROUPE()>
		 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.add_node">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#pResult#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="true">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="0" null="false">
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="pRtAddNode">
				</cfstoredproc>
				
				
				<cfif pRtAddNode gt 0>
				<!--- Affectation des lignes --->
					<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.Affecte_liste_lignes_v2">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#pRtAddNode#" null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#getLIGNES()#" null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="1" null="false">
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="pRtAffect">
					</cfstoredproc>	
				<cfelse>					
					<cfset retString  = retString & pRtAddNode>
					<cfreturn retString>
				</cfif>			
			<cfelse>
				<cfset retString  = retString & pRtAccess>
				<cfreturn retString>
			</cfif>				
		<cfelse>
			<cfset retString  = retString & pResult>
			<cfreturn retString>
		</cfif>
		
		<cfset retString = pResult & "," & pRtAddNode>
		<cfreturn retString>	
	</cffunction>
	

	
	<!--- Suppression  d'une orga de type SAVE'--->
	<cffunction name="delete" displayname="delete" hint="supprime un groupe de ligne de type SAVE" access="public" output="false" returntype="Numeric">				
 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_consoview_v3.remove_orga">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getID()#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="SAV" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="pResult">
		</cfstoredproc>			
	 	<cfreturn pResult/>
	</cffunction>
	
	
	<cffunction name="load" displayname="load" hint="load" access="public" output="true" returntype="void">
		<cfargument name="params" type="any" required="true" />			
		<cfreturn />
	</cffunction>
	
</cfcomponent>