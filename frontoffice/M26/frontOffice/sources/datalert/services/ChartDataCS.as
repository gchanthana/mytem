package datalert.services
{
	import composants.util.ChartDataUtil;
	
	import datalert.event.DatalertEvent;
	import datalert.vo.ChartDataVo;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class ChartDataCS extends EventDispatcher
	{
		public var listeChartData : ArrayCollection;
		public var dayStart:Date;
		public var dayEnd:Date;
		
		public function ChartDataCS()
		{
		}
		
		public function getChartDataPerHour(day:Date, idSoustete:int):void
		{			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M26.DatalertChart",
				"getDataPerHour",
				getChartHourData_handler,null);
			
			RemoteObjectUtil.callService(opData,day,
												idSoustete);
		}
		
		private function getChartHourData_handler(re : ResultEvent):void
		{
			var tmp:Array = (re.result as ArrayCollection).source;			
			listeChartData = new ArrayCollection();
			
			for(var i : int=0; i < tmp.length;i++)
			{	
				var  item : ChartDataVo = new ChartDataVo();
				item.HOUR=tmp[i].HOUR;
				item.DATADOMESTIC=tmp[i].DOMESTICDATA/1024;
				item.DATAROAMING=tmp[i].ROAMINGDATA/1024;
				item.DATAWIFI=tmp[i].WIFIDATA/1024;
				
				listeChartData.addItem(item);
			}		
			listeChartData = ChartDataUtil.toFullListPerHour(listeChartData);
			dispatchEvent(new DatalertEvent(DatalertEvent.UPDATE_DATA_CHART_EVENT,true));
		}
		
		public function getChartDataPerDay(dayStart:Date,dayEnd:Date, idSoustete:int):void
		{			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M26.DatalertChart",
				"getDataPerDay",
				getChartDayData_handler,null);
			this.dayStart = dayStart;
			this.dayEnd = dayEnd;//this.dayStart et this.dayEnd ne servent qu'a ajouter les jours vides par la suite
			RemoteObjectUtil.callService(opData,dayStart,
												dayEnd,
												idSoustete);
		}
		
		private function getChartDayData_handler(re : ResultEvent):void
		{
			var tmp:Array = (re.result as ArrayCollection).source;			
			listeChartData = new ArrayCollection();
			
			for(var i : int=0; i < tmp.length;i++)
			{	
				var  item : ChartDataVo = new ChartDataVo();
				item.DAY=tmp[i].DAY;
				item.DATADOMESTIC=tmp[i].DOMESTICDATA/1024;
				item.DATAROAMING=tmp[i].ROAMINGDATA/1024;
				item.DATAWIFI=tmp[i].WIFIDATA/1024;
				
				listeChartData.addItem(item);
			}		
			listeChartData = ChartDataUtil.toFullListPerDay(listeChartData, dayStart, dayEnd);
			dispatchEvent(new DatalertEvent(DatalertEvent.UPDATE_DATA_CHART_EVENT,true));
		}
	}
}