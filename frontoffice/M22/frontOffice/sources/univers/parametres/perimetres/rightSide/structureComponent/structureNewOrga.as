package univers.parametres.perimetres.rightSide.structureComponent
{
    import flash.events.Event;
    import flash.events.MouseEvent;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.events.CloseEvent;
    import mx.events.FlexEvent;
    import mx.events.ListEvent;
    import mx.managers.PopUpManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;
    import mx.utils.ObjectUtil;

    public class structureNewOrga extends structureNewOrgaIHM
    {
        private var _orgaData:ArrayCollection;
        private var _structureRef:Object;
        private var _AnalytiConsoViewModuleObjectalue:String;
        public static const LAST_TYPE_NIVEAU_ANU:String = "COLLABORATEUR";

        public function structureNewOrga()
        {
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
            // To change with the real value
            _AnalytiConsoViewModuleObjectalue = "999999999"; //5
        }

        public function setStructureRef(ref:Object):void
        {
            _structureRef = ref;
        }

        protected function initIHM(event:Event):void
        {
            fillOrganisation()
            PopUpManager.centerPopUp(this);
            //cbxCopyOrga.addEventListener(MouseEvent.CLICK, refreshWindow);
            //btnValid.addEventListener(MouseEvent.CLICK, validWinClicked);
            //btnCancel.addEventListener(MouseEvent.CLICK, closeWinClicked);
            cmbOrganisationType.addEventListener(Event.CHANGE, selectionChange);
            cbxCopyLines.addEventListener(MouseEvent.CLICK, selectionChange);
            btnCancelExistant.addEventListener(MouseEvent.CLICK, closeWinClicked);
            btnValidExistant.addEventListener(MouseEvent.CLICK, btValiderClickHandler);
            cmpCreateModele.addEventListener("NouveauModeleAnnuler", annulerHandler);
            cmpCreateModele.addEventListener("NouveauModeleValider", cmpCreateModeleValiderHandler);
            cmpSelectModele.addEventListener("ModeleExistantAnnuler", annulerHandler);
            cmpSelectModele.addEventListener("ModeleExistantValider", cmpSelectModeleValiderHandler);
            this.addEventListener(CloseEvent.CLOSE, closeWin);
            txtName.addEventListener(Event.CHANGE, txtNameChangeHandler);
            cmbOrganisationType.addEventListener(ListEvent.CHANGE, cmbOrganisationChangeHandler);
        }

        private function fillOrganisation():void
        {
            var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetreStructure",
                                                                                "getOrganisation", fillOrganisationResult);
            RemoteObjectUtil.callService(op, idGroupeMaitre);
        }

        private function fillOrganisationResult(result:ResultEvent):void
        {
            var collec:ArrayCollection = ArrayCollection(result.result);
            // suppression du choix annuaire 
            for(var i:int = 0; i < collec.length; i++)
            {
                if(collec.getItemAt(i).TYPE_ORGA == "ANU")
                {
                    collec.removeItemAt(i);
                    break;
                }
            }
            cmbOrganisation.labelField = "LIBELLE_GROUPE_CLIENT";
            cmbOrganisation.dataProvider = ArrayCollection(collec);
        }

        protected function selectionChange(event:Event):void
        {
        /* if (cmbOrganisationType.selectedItem != null){
           if (cmbOrganisationType.selectedItem.value == _AnalytiConsoViewModuleObjectalue
           && cbxCopyOrga.selected == true && cbxCopyLines.selected)
           formItemCpyAssign.visible = true;
           else
           formItemCpyAssign.visible = false;
         } */
        }

        protected function cmbOrganisationChangeHandler(le:ListEvent):void
        {
            if(cmbOrganisationType.selectedItem != null && cmbOrganisationType.selectedItem.value == 6)
                checkDoubleOrgaANU();
        }

        protected function txtNameChangeHandler(ev:Event):void
        {
            txtName.clearStyle("borderColor");
        }

        private function closeWin(event:CloseEvent):void
        {
            PopUpManager.removePopUp(this);
            //winProperties.remo
        }

        private function closeWinClicked(event:MouseEvent):void
        {
            PopUpManager.removePopUp(this);
            //winProperties.remo
        }

        private function annulerHandler(ev:Event):void
        {
            PopUpManager.removePopUp(this);
        }

        protected function refreshWindow(event:MouseEvent):void
        {
            if(cbxCopyOrga.selected == true)
            {
                formItemCopyOrga.visible = true;
                formItemAffectLines.visible = true;
                if(cmbOrganisationType.selectedItem.value == _AnalytiConsoViewModuleObjectalue && cbxCopyLines.selected)
                    formItemCpyAssign.visible = true;
            }
            else
            {
                formItemCopyOrga.visible = false;
                formItemAffectLines.visible = false;
                formItemCpyAssign.visible = false;
            }
        }

        private function cmpCreateModeleValiderHandler(ev:Event):void
        {
            if(verifierFormulaire())
            {
                if(isCompatibleTypeOrgaModele(cmpCreateModele.nouveauModele as XML))
                {
                    validerOrga(Number(cmpCreateModele.nouveauModele.@IDORGA_NIVEAU));
                }
                else
                {
                    Alert.show(resourceManager.getString('M22', 'Le_type_d_organisation_s_lectionn__et_le_mod_le_choisit_ne_sont_pas_compatibles_') + ' \n' +
                               resourceManager.getString('M22', 'Pour_une_organisation_Annuaire_le_dernier_niveau_du_modèle_doit_être_de_type') + LAST_TYPE_NIVEAU_ANU +
                               ".", "Erreur");
                }
            }
        }

        private function cmpSelectModeleValiderHandler(ev:Event):void
        {
            if(verifierFormulaire())
            {
                if(isCompatibleTypeOrgaModele(cmpSelectModele.nouveauModele as XML))
                {
                    validerOrga(Number(cmpSelectModele.nouveauModele.@IDORGA_NIVEAU));
                }
                else
                {
                    Alert.show(resourceManager.getString('M22', 'Le_type_d_organisation_s_lectionn__et_le_mod_le_choisit_ne_sont_pas_compatibles_') + ' \n' +
                               resourceManager.getString('M22', 'Pour_une_organisation_Annuaire_le_dernier_niveau_du_modèle_doit_être_de_type') + LAST_TYPE_NIVEAU_ANU +
                               ".", "Erreur");
                }
            }
        }

        private function btValiderClickHandler(me:MouseEvent):void
        {
            if(verifierFormulaire())
            {
                isCompatibleOrgaSourceSeclectedTyepOrga(Number(cmbOrganisation.selectedItem.IDORGA_NIVEAU));
            }
        }

        private function verifierFormulaire():Boolean
        {
            if(txtName.text != null && txtName.text.length > 0)
            {
                return true;
            }
            else
            {
                txtName.setStyle("borderColor", "red");
                Alert.show(resourceManager.getString('M22', 'Le_libell__est_obligatoire_'), resourceManager.getString('M22', 'Erreur'));
                return false
            }
        }

        private function validerOrga(idOrgaNiveau:Number = 0):void
        {
            var idRacine:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var name:String = txtName.text;
            var comment:String = txtComment.text;
            var type:Number = cmbOrganisationType.selectedItem.value;
            var type_orga:String = getTypeOrgaByDecoupage(type);
            if(type_orga == "ANU")
                type = 4;
            var operateurId:Number = 0;
            var copylines:Number = 0;
            var organisationSource:Number = 0;
            var typeAffectation:String = "";
            var idorga_niveau:Number = idOrgaNiveau;
            if(cmbModeles.selectedItem.value == 3)
            {
                idorga_niveau = cmbOrganisation.selectedItem.IDORGA_NIVEAU;
                if(cbxCopyLines.selected == true)
                    copylines = 1;
                try
                {
                    operateurId = cmbOrganisation.selectedItem.OPERATEURID;
                }
                catch(error:Error)
                {
                }
                organisationSource = cmbOrganisation.selectedItem.IDGROUPE_CLIENT;
                if(cmbOrganisationType.selectedItem.value == _AnalytiConsoViewModuleObjectalue)
                    typeAffectation = typeAssign.selectedValue.toString();
            }
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetreStructure",
                                                                                "addOrganisation", addOrganisationResult);
            RemoteObjectUtil.callService(op, idRacine, idorga_niveau, name, comment, type, type_orga, operateurId, copylines, organisationSource, typeAffectation);
        }

        private function getTypeOrgaByDecoupage(type:Number):String
        {
            switch(type)
            {
                case 4:
                    return "CUS";
                case 6:
                    return "ANU";
            }
            return "NULL";
        }

        private function addOrganisationResult(event:ResultEvent):void
        {
            if(event.result > 0)
            {
                trace(ObjectUtil.toString(event.token.message.body));
                var item:Object = new Object();
                //si le parametre est superieur a 0 alors c'est une copie d'organisation -> le NTY = 1 sinon NTY = 0 
                item.NTY = (event.token.message.body[8] > 0) ? 1 : 0;
                item.STC = 1;
                item.LIBELLE_GROUPE_CLIENT = txtName.text;
                var type:Number = cmbOrganisationType.selectedItem.value;
                item.TYPE_ORGA = getTypeOrgaByDecoupage(type);
                item.IDGROUPE_CLIENT = event.result;
                item.IDORGA_NIVEAU = event.token.message.body[1];
                _structureRef.refreshOrganisation(item);
                PopUpManager.removePopUp(this);
            }
            else
            {
                Alert.show(resourceManager.getString('M22', 'Une_erreur_s_est_produite_lors_de_l_enregistrement_'), resourceManager.getString('M22', 'Erreur'));
            }
        }

        private function checkDoubleOrgaANU():void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionmodele.GestionModeleGateWay",
                                                                                "checkDoubleOrgaANU", checkDoubleOrgaANUResultHandler);
            RemoteObjectUtil.callService(op, "ANU");
        }

        private function checkDoubleOrgaANUResultHandler(re:ResultEvent):void
        {
            if(re.result > 0)
            {
                //Alert.show("Vous ne pouvez créer qu'une seule organisation de type annuaire", "Erreur");
                cmbOrganisationType.selectedIndex = 0;
            }
            else if(re.result == -1)
            {
                Alert.show("Erreur");
            }
        }

        private function isCompatibleTypeOrgaModele(modele:XML):Boolean
        {
            if(cmbOrganisationType.selectedItem.value == 6) //si on a choisit de creer une organisation annuaire
            {
                var nodeObj:Object;
                var node:XMLList;
                node = modele.children();
                while(node.length() > 0)
                {
                    nodeObj = new Object();
                    nodeObj.NIVEAU = String(node[0].@NUMERO_NIVEAU);
                    nodeObj.TYPE_NIVEAU = String(node[0].@TYPE_NIVEAU);
                    node = node[0].children();
                }
                if(String(nodeObj.TYPE_NIVEAU).toUpperCase() == LAST_TYPE_NIVEAU_ANU)
                {
                    return true;
                }
                else
                    return false
            }
            else
            {
                return true;
            }
        }

        private function isCompatibleOrgaSourceSeclectedTyepOrga(idOrgaNiveau:Number):void
        {
            if(cmbOrganisationType.selectedItem.value == 6)
            {
                var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionmodele.GestionModeleGateWay",
                                                                                    "isCompatibleOrgaSourceSeclectedTyepOrga", isCompatibleOrgaSourceSeclectedTyepOrgaResultHandler);
                RemoteObjectUtil.callService(op, idOrgaNiveau);
            }
            else
            {
                validerOrga();
            }
        }

        private function isCompatibleOrgaSourceSeclectedTyepOrgaResultHandler(re:ResultEvent):void
        {
            if(re.result > 0)
            {
                validerOrga();
            }
            else
            {
                Alert.show(resourceManager.getString('M22', 'Le_type_d_organisation_s_lectionn__et_le_mod_le_choisit_ne_sont_pas_compatibles_') + ' \n' +
                           resourceManager.getString('M22', 'Pour_une_organisation_Annuaire_le_dernier_niveau_du_modèle_doit_être_de_type') + LAST_TYPE_NIVEAU_ANU +
                           '.', resourceManager.getString('M22', 'Erreur'));
            }
        }
    }
}