package gestionparc.ihm.fiches.popup
{
	import composants.ui.TitleWindowBounds;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.TypeLigneVO;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	public class PopupChooseTypeLigneImpl extends TitleWindowBounds
	{
		//--------------- VARIABLES ----------------//
		
		public var dgChooseTypeLigne:DataGrid;
		public var tiFiltre			:TextInput;
		public var btnValider		:Button;
		public var btnAnnuler		:Button;
		[Bindable]
		public var btnValidEnabled	:Boolean = false;
		
		private var _itemCurrent	:TypeLigneVO = null;
		private var _listeTypeLigne	:ArrayCollection = new ArrayCollection();
		
		
		//--------------- METHODES ----------------//
		
		/* constructeur */
		public function PopupChooseTypeLigneImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		/* initialisation */
		private function init(fe:FlexEvent):void
		{
			tiFiltre.addEventListener(Event.CHANGE,saisieFiltreHandler);
			btnValider.addEventListener(MouseEvent.CLICK,validerChooseTypeLigneHandler);
			btnAnnuler.addEventListener(MouseEvent.CLICK,annulerHandler);
			dgChooseTypeLigne.addEventListener(ListEvent.CHANGE,currentItemChangeHandler);
		}
		
		/* */
		private function currentItemChangeHandler(le:ListEvent):void
		{
			_itemCurrent = le.currentTarget.selectedItem;
			setSelectedItem(_itemCurrent);
		}
		
		/* */
		private function setSelectedItem(obj:TypeLigneVO):void
		{
			for each(var item:TypeLigneVO in listeTypeLigne)
			{
				item.SELECTED = false;
				if(obj.IDTYPE_LIGNE == item.IDTYPE_LIGNE)
				{
					item.SELECTED = true;
					_itemCurrent = item;
				}
				listeTypeLigne.itemUpdated(item);
			}
			
			btnValidEnabled = true;
		}
		
		/* */
		public function setTypeLigne(item:TypeLigneVO):void
		{
			setSelectedItem(item);
		}
		
		/* lors de la saisie du filtre */
		private function saisieFiltreHandler(e:Event):void
		{
			listeTypeLigne.filterFunction = filtreSaisieTypeLigne;
			listeTypeLigne.refresh();
		}
		
		/* systeme de filtre */
		private function filtreSaisieTypeLigne(item:Object):Boolean
		{
			if (item.LIBELLE_TYPE_LIGNE != null && (item.LIBELLE_TYPE_LIGNE as String).toLowerCase().search((tiFiltre.text).toLowerCase()) != -1)
				return true;
			else
				return false;
		}
		
		/* au click sur annuler */
		private function annulerHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		/* au click sur valider */
		private function validerChooseTypeLigneHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event("VALID_CHOIX_TYPELIGNE",true));
			annulerHandler(null);
		}
		
		//--------------- GETETRS - SETTERS ----------------//
		
		[Bindable]
		public function get listeTypeLigne():ArrayCollection
		{
			return _listeTypeLigne;
		}
		public function set listeTypeLigne(value:ArrayCollection):void
		{
			_listeTypeLigne = value;
		}
		
		public function get itemCurrent():TypeLigneVO
		{
			return _itemCurrent;
		}
		public function set itemCurrent(value:TypeLigneVO):void
		{
			_itemCurrent = value;
		}
	}
}