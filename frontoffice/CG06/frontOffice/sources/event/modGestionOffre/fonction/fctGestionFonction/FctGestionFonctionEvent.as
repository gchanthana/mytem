package event.modGestionOffre.fonction.fctGestionFonction
{
	import flash.events.Event;
	
	public class FctGestionFonctionEvent extends Event
	{
		public static const LISTE_NIVEAUX_EVENT : String = "LISTE_NIVEAUX_EVENT";
		public static const LISTE_FONCTIONS_EVENT : String = "LISTE_FONCTIONS_EVENT";
		public static const LISTE_DROITSPE_EVENT : String = "LISTE_DROITSPE_EVENT";
		
		public function FctGestionFonctionEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}