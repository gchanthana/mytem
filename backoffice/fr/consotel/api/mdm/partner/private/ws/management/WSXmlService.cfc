<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.management.WSXmlService" implements="fr.consotel.api.mdm.partner.private.ws.IWSXmlService"
hint="Implémentation fournissant les méthodes du Service Xml Zenprise. Les méthodes de cette implémentation sont soumises à la vérification de l'authentification">
	<!--- fr.consotel.api.mdm.partner.private.ws.management.WSXmlService --->
	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSXmlService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="asJavaNullValue" returntype="Any" hint="Retourne la valeur nulle Java si stringValue est une chaine vide et stringValue sinon">
		<cfargument name="stringValue" type="String" required="true">
		<cfif TRIM(ARGUMENTS["stringValue"]) EQ "">
			<cfreturn javaCast("NULL",0)>
		<cfelse>
			<cfreturn ARGUMENTS["stringValue"]>
		</cfif>
	</cffunction>
	
	<!--- XML Service --->
	<cffunction access="public" name="addXMLFile" returntype="Void" hint="Ajoute un fichier XML">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="newFile" type="String" required="true" hint="Nom à donner au fichier">
		<cfargument name="fileText" type="String" required="true" hint="Contenu du fichier">
		<cfargument name="comment" type="String" required="true" hint="Commentaires à associer au fichier">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var xmlService=zdmRepository.getXmlService()>
		<cftry>
			<cfset xmlService.addXMLFile(ARGUMENTS["osFamily"],ARGUMENTS["newFile"],ARGUMENTS["fileText"],ARGUMENTS["comment"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="updateXMLFile" returntype="Void" hint="Met à jour un fichier XML">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom actuel du fichier">
		<cfargument name="newFile" type="String" required="true" hint="Nom à donner au fichier ou une chaine vide pour garder le nom fileName">
		<cfargument name="fileText" type="String" required="true" hint="Contenu du fichier">
		<cfargument name="comment" type="String" required="true" hint="Commentaires à associer au fichier">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var xmlService=zdmRepository.getXmlService()>
		<cftry>
			<cfset xmlService.updateXMLFile(ARGUMENTS["osFamily"],ARGUMENTS["fileName"],asJavaNullValue(ARGUMENTS["newFile"]),ARGUMENTS["fileText"],ARGUMENTS["comment"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removeXMLFile" returntype="Void" hint="Supprime un fichier XML">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var xmlService=zdmRepository.getXmlService()>
		<cftry>
			<cfset xmlService.removeXMLFile(ARGUMENTS["osFamily"],ARGUMENTS["fileName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getXMLFileInfo" returntype="Array"
	hint="Retourne les infos du fichier dans un tableau contenant les éléments suivants dans l'ordre : Nom, Contenu, Commentaires, Date de création ou d'upload, Date de modification">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var xmlService=zdmRepository.getXmlService()>
		<cftry>
			<cfreturn xmlService.getXMLFileInfo(ARGUMENTS["osFamily"],ARGUMENTS["fileName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getXMLFileList" returntype="Array" hint="Retourne la liste des fichier XML dans un tableau">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var xmlService=zdmRepository.getXmlService()>
		<cftry>
			<cfreturn xmlService.getXMLFileList()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="validateXML" returntype="Boolean" hint="Retourne TRUE si le contenu XML est lève une exception sinon">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileText" type="String" required="true" hint="Contenu à valider">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var xmlService=zdmRepository.getXmlService()>
		<cftry>
			<cfreturn xmlService.validateXML(ARGUMENTS["osFamily"],ARGUMENTS["fileText"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>