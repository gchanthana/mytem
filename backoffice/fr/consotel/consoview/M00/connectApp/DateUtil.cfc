<cfcomponent name="dateUtils" output="false">    
	
	<!---
	inspired
	http://www.bennadel.com/blog/882-ask-ben-finding-the-last-monday-of-the-month-in-coldfusion.htm
	--->
	<cffunction
    name="getLastDayOfMonth"
    access="public"
    returntype="date"
    output="false"
    hint="Returns the date of the last given weekday of the given month.">
     
    <!--- Define arguments. --->
    <cfargument
    name="Date"
    type="date"
    required="true"
    hint="Any date in the given month we are going to be looking at."
    />
    

    <!--- Define the local scope. --->
    <cfset var LOCAL = StructNew() />
     
    <!--- Get the current month based on the given date. --->
    <cfset LOCAL.ThisMonth = CreateDateTime(
    Year( ARGUMENTS.Date ),
    Month( ARGUMENTS.Date ),
    1,Hour( ARGUMENTS.Date ),Minute( ARGUMENTS.Date ),Second( ARGUMENTS.Date )
    ) />
     
    <!---
    Now, get the last day of the current month. We
    can get this by subtracting 1 from the first day
    of the next month.
    --->
    <cfset LOCAL.LastDay = (
    DateAdd( "m", 1, LOCAL.ThisMonth ) -
    1
    ) />
    

	<cfreturn DateConvert("local2utc",LOCAL.LastDay)>
    </cffunction>
	
	<!---
		ADD OR SUBSTRACT TIME TO DATE
	--->
	<cffunction name="addTimeToDate" access="public" output="false" returntype="string">
		
		<cfargument name="unit" type="string" required="true">
		<cfargument name="number" type="numeric" required="true" hint="time to add or substract">
		<cfargument name="date" type="string" required="true">
		
		<cfreturn gotToFirstDayOfDate(dateadd(ARGUMENTS.unit,ARGUMENTS.number,ARGUMENTS.date))>
	</cffunction>
	
	<!--- GO TO THE FIRST DAY OF A DATE --->
	<cffunction name="gotToFirstDayOfDate" access="public" output="false" returntype="string">
		
		<cfargument name="date" type="string" required="true">
		
		<cfset LOCAL.dateDebut = CreateDate(year(ARGUMENTS.date), month(ARGUMENTS.date), 1)> <!--- toujours revenir au debut du mois de la date de factu --->
	
		<cfreturn LOCAL.dateDebut>
	</cffunction>
	
</cfcomponent>