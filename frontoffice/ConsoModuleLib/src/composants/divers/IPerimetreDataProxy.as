package composants.divers {
	public interface IPerimetreDataProxy {
		/**
		 * Loads node children and add each of them as child of node parameter object.
		 * */
		function loadNodeChildren(node:XML):void;
	}
}