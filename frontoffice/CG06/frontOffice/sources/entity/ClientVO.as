package entity
{
	import com.as3xls.xls.ExcelFile;

	[Bindable]
	public class ClientVO
	{
		private var _SIRET:String;
		private var _ADRESSE:String;
		private var _NOM:String;
		private var _ID:int;
		// utiliser dans GestionClient (commentaire à effacer une fois la Gestion Client est terminé)
		private var _SELECTED:Boolean = false;
		private var _NBRECORD:int;
		
		private var _master:String;
		private var _application:String;
		private var _nbreCollecte:int;
		private var _login:int;
		private var _distributeur:String;
		
		public function ClientVO()
		{
			
		}
		
		public function fill(obj:Object):Boolean
		{
			try
			{
				if(obj.hasOwnProperty('CLIENT_ADRESSE'))
					this._ADRESSE = obj.CLIENT_ADRESSE;
				if(obj.hasOwnProperty('CLIENT_ID'))
					this._ID = obj.CLIENT_ID;
				if(obj.hasOwnProperty('CLIENT_NAME'))
					this._NOM = obj.CLIENT_NAME;
				if(obj.hasOwnProperty('CLIENT_SIRET'))
					this._SIRET = obj.CLIENT_SIRET;
				
				if(obj.hasOwnProperty('NBRECORD'))
					this.NBRECORD = obj.NBRECORD;
				if(obj.hasOwnProperty('MASTER'))
					this.master = obj.MASTER;
				if(obj.hasOwnProperty('APPLICATION'))
					this.application = obj.APPLICATION;
				if(obj.hasOwnProperty('NB_COLLECTE'))
					this.nbreCollecte = obj.NB_COLLECTE;
				if(obj.hasOwnProperty('LOGIN_ID'))
					this.login = obj.LOGIN_ID;
				if(obj.hasOwnProperty('DISTRIBUTEUR'))
					this.distributeur = obj.DISTRIBUTEUR;
				
				return true;
			}
			catch(e:Error)
			{
				trace("Erreur pendant le chargement du ClientVO");
				return false
			}
			return false
		}
		
		public function get NOM():String { return _NOM; }
		public function set NOM(value:String):void
		{
			if (_NOM == value)
				return;
			_NOM = value;
		}
		public function get ADRESSE():String { return _ADRESSE; }
		public function set ADRESSE(value:String):void
		{
			if (_ADRESSE == value)
				return;
			_ADRESSE = value;
		}
		public function get SIRET():String { return _SIRET; }
		public function set SIRET(value:String):void
		{
			if (_SIRET == value)
				return;
			_SIRET = value;
		}
		public function get ID():int { return _ID; }
		public function set ID(value:int):void
		{
			if (_ID == value)
				return;
			_ID = value;
		}
		
		public function get NBRECORD():int { return _NBRECORD; }
		
		public function set NBRECORD(value:int):void
		{
			if (_NBRECORD == value)
				return;
			_NBRECORD = value;
		}
		
		public function get SELECTED():Boolean { return _SELECTED; }
		
		public function set SELECTED(value:Boolean):void
		{
			if (_SELECTED == value)
				return;
			_SELECTED = value;
		}
		
		public function get login():int { return _login; }
		
		public function set login(value:int):void
		{
			if (_login == value)
				return;
			_login = value;
		}
		
		
		public function get nbreCollecte():int { return _nbreCollecte; }
		
		public function set nbreCollecte(value:int):void
		{
			if (_nbreCollecte == value)
				return;
			_nbreCollecte = value;
		}
		
		
		public function get application():String { return _application; }
		
		public function set application(value:String):void
		{
			if (_application == value)
				return;
			_application = value;
		}
		
		public function get master():String { return _master; }
		
		public function set master(value:String):void
		{
			if (_master == value)
				return;
			_master = value;
		}
		
		public function get distributeur():String { return _distributeur; }
		
		public function set distributeur(value:String):void
		{
			if (_distributeur == value)
				return;
			_distributeur = value;
		}
	}
}