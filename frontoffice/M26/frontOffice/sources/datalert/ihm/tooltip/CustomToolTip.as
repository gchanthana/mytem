package datalert.ihm.tooltip
{
	
	import mx.core.*;
	import mx.containers.*;
	
	[Bindable]
	public class CustomToolTip extends VBox implements IToolTip
	{
		private var _text:String = "";
		
		public function CustomToolTip()
		{
			mouseEnabled = false;
			mouseChildren = false;
			setStyle("paddingLeft", 10);
			setStyle("paddingTop", 10);
			setStyle("paddingBottom", 10);
			setStyle("paddingRight", 10);
		}
		
		public function get text():String {     
			return this._text;
		}
		public function set text(value:String):void {
			this._text = value;
		}
	}	
}