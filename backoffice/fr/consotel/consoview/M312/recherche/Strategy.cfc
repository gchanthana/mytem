<!--- =========================================================================
Name: Strategy
Original Author: 
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent displayname="Strategy" hint="This class declares an interface common to all supported algorithms. Context uses this interface to call the algorithm defined by a ConcreteStrategy.">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="Strategy" displayname="Strategy init()" hint="Initialize the Strategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(numeric, string, string, string)">
		<cfargument name="idGroupe" required="true" type="numeric" default="" displayname="numeric ID" 				hint="Initial value for the IDproperty."/>
		<cfargument name="idCliche" required="true" type="numeric" default="" displayname="numeric ID" 				hint="Initial value for the IDproperty."/>
		<cfargument name="chaine"   required="true" type="string"  default="" displayname="la chaine de recherche" 	hint="Initial value for the chaine property."/>
		<cfargument name="niveau"   required="true" type="string"  default="" displayname="la chaine de recherche" 	hint="Initial value for the chaine property."/>
		<cfargument name="iddatedeb" 	type="numeric" 	required="true">
		<cfargument name="iddatefin"   type="numeric" 	required="true">
			
			<cfabort showerror="This Method is Abstract and needs to be overridden">

	</cffunction>	
	
	<cffunction name="getLignes" access="public" returntype="query" output="false" displayname="query getData(numeric, string, string)">
		<cfargument name="idGroupe" required="true" type="numeric" default="" displayname="numeric ID" 				hint="Initial value for the IDproperty."/>
		<cfargument name="idCliche" required="true" type="numeric" default="" displayname="numeric ID" 				hint="Initial value for the IDproperty."/>
		<cfargument name="niveau"   required="true" type="string"  default="" displayname="la chaine de recherche" 	hint="Initial value for the chaine property."/>
		<cfargument name="iddatedeb" 	type="numeric" 	required="true">
		<cfargument name="iddatefin"   type="numeric" 	required="true">
			
			<cfabort showerror="This Method is Abstract and needs to be overridden">
			
	</cffunction>	
	
	<cffunction name="getStrategy" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)">
		
			<cfabort showerror="This Method is Abstract and needs to be overridden">
		
	</cffunction>	
</cfcomponent>