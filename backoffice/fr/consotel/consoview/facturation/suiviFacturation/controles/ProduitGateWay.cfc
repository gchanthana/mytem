<cfcomponent output="false">
	<cfproperty name="idGroupeMaitre" type="numeric" default="0">
	<cfproperty name="idGroupeClient" type="numeric" default="0">
	<cfproperty name="type_perimetre" type="string" default="Groupe">
	   
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idGroupeMaitre = session.perimetre.ID_GROUPE;
		variables.idGroupeClient = session.perimetre.ID_PERIMETRE;
		variables.type_perimetre = session.perimetre.TYPE_PERIMETRE;
	</cfscript>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : GETTER
	
	--->
	<cffunction name="getIdGroupeMaitre" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeMaitre>
	</cffunction>
	
	<cffunction name="getIdGroupeClient" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeClient>
	</cffunction>
	
	<cffunction name="getTypePerimetre" output="false" access="public" returntype="string">
		<cfreturn variables.type_perimetre>
	</cffunction>
	
	
	<cffunction name="getGrpProduitsGroupePerimetre" access="remote" output="false" returntype="any">
		<cfargument name="pRecherche" type="ParamsRecherche" required="true" />
		
		<cfset alisteGrpProduits = Evaluate("getGrpProduits#getTypePerimetre()#AsQuery(pRecherche)")>
		<cfset qlisteGrpProduitsAbos = alisteGrpProduits[1]>
		<cfset qlisteGrpProduitsConsos = alisteGrpProduits[2]>
		
		<!--- =============== TOTAUX ============== --->
		<cfset montantTotal = 0>
		<cftry>
			<cfquery name="totalAbos" dbtype="query">
				select sum(MONTANT) as TOTAL from qlisteGrpProduitsAbos
			</cfquery>
			<cfquery name="totalConsos" dbtype="query">
				select sum(MONTANT) as TOTAL from qlisteGrpProduitsConsos
			</cfquery>	
			<cfif totalAbos.recordCount gt 0>
				<cfset montantTotal = montantTotal + totalAbos.TOTAL[1]>
				<cfelse>
				<cfset montantTotal = 0>
			</cfif>
			<cfif totalConsos.recordCount gt 0>
				<cfset montantTotal = montantTotal + totalConsos.TOTAL[1]>				
			</cfif>	
		<cfcatch type="any">
			<cfset montantTotal = 0>
		</cfcatch>	
		</cftry>
		<!--- =============== FIN TOTAUX ========== --->
		
		
		<!--- =============== GRP PRODUITS ABOS ========== --->
		<cfset tabProduitsAbos = arrayNew(1)>	
		
		<cfloop query="qlisteGrpProduitsAbos">
			<cfset grpProduitsAbos = createObject("component","GroupeProduitControle")>	
			<cfscript>		
				grpProduitsAbos.setId(qlisteGrpProduitsAbos.IDGRP_CTL);
				grpProduitsAbos.setLibelle(qlisteGrpProduitsAbos.GRP_CTL);
				grpProduitsAbos.setSegment(formatSegment(qlisteGrpProduitsAbos.GRP_SEGMENT));				
				grpProduitsAbos.setOperateurId(pRecherche.getOperateurId());
				if (montantTotal eq 0){
					grpProduitsAbos.setPoids(0);
				}else{
					grpProduitsAbos.setPoids(qlisteGrpProduitsAbos.MONTANT/montantTotal*100);
				}
			  	grpProduitsAbos.setPourControle(qlisteGrpProduitsAbos.A_CONTROLER);
				grpProduitsAbos.setPrixUnitaire(qlisteGrpProduitsAbos.TARIF_BRUT);
				grpProduitsAbos.setPrixRemise(qlisteGrpProduitsAbos.TARIF_REMISE);
				grpProduitsAbos.setMontant(qlisteGrpProduitsAbos.MONTANT); 
				grpProduitsAbos.setNbVersion(1); 
				grpProduitsAbos.setRemiseContrat(qlisteGrpProduitsAbos.PCT_REMISE); 
				grpProduitsAbos.setDateDebut(qlisteGrpProduitsAbos.DATEDEB); 
				grpProduitsAbos.setDateFin(qlisteGrpProduitsAbos.DATEFIN); 
				grpProduitsAbos.setType("ABO");
			</cfscript>
			<cfset arrayAppend(tabProduitsAbos,grpProduitsAbos)>
		</cfloop>
		
		<!--- =============== FIN GRP PRODUITS ABOS ========== --->
		
		
		<!--- =============== GRP PRODUITS CONSOS ========== --->
		<cfset qlisteGrpProduitsConsos = alisteGrpProduits[2]>
		<cfset tabProduitsConsos = arrayNew(1)>
		<cfloop query="qlisteGrpProduitsConsos">
			<cfset grpProduitsConsos = createObject("component","GroupeProduitControle")>			
			<cfscript>		
			//Initialize the CFC with the properties values.
				grpProduitsConsos.setId(qlisteGrpProduitsConsos.IDGRP_CTL);
				grpProduitsConsos.setLibelle(qlisteGrpProduitsConsos.GRP_CTL);
				grpProduitsConsos.setSegment(formatSegment(qlisteGrpProduitsConsos.GRP_SEGMENT));				
				grpProduitsConsos.setOperateurId(pRecherche.getOperateurId());
				if (montantTotal eq 0){
					grpProduitsConsos.setPoids(0);
				}else{
					grpProduitsConsos.setPoids(qlisteGrpProduitsConsos.MONTANT/montantTotal*100);
				}
			  	grpProduitsConsos.setPourControle(qlisteGrpProduitsConsos.A_CONTROLER);
				grpProduitsConsos.setPrixUnitaire(qlisteGrpProduitsConsos.TARIF_BRUT);
				grpProduitsConsos.setPrixRemise(qlisteGrpProduitsConsos.TARIF_REMISE);
				grpProduitsConsos.setMontant(qlisteGrpProduitsConsos.MONTANT); 
				grpProduitsConsos.setNbVersion(1); 
				grpProduitsConsos.setRemiseContrat(qlisteGrpProduitsConsos.PCT_REMISE); 
				grpProduitsConsos.setDateDebut(qlisteGrpProduitsConsos.DATEDEB); 
				grpProduitsConsos.setDateFin(qlisteGrpProduitsConsos.DATEFIN); 
				grpProduitsConsos.setType("CONSO");
			</cfscript>
			<cfset arrayAppend(tabProduitsConsos,grpProduitsConsos)>
		</cfloop>
		<!--- =============== FIN PRODUITS CONSOS ========== --->
		
		<cfset tabProduits = arrayNew(1)>		
		<cfset arrayappend(tabProduits,tabProduitsAbos)>
		<cfset arrayappend(tabProduits,tabProduitsConsos)>
				
		<cfreturn tabProduits>
	</cffunction>
	
	<cffunction name="formatSegment" access="private" output="false" returntype="string">
		<cfargument name="nSegment" type="numeric" required="true">
		<cfswitch expression="#nSegment#">
			<cfcase value="1">
				<cfreturn "Fixe">
			</cfcase>
			<cfcase value="2">
				<cfreturn "Mobile">
			</cfcase>
			<cfcase value="3">
				<cfreturn "Data">
			</cfcase>
			<cfdefaultcase>
				<cfreturn "Segment inconnu">
			</cfdefaultcase>
		</cfswitch>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Fournit la liste des groupe de produits élaborés par Consotel pour une Racine.
					  
		Param in 
			ParamsRecherche
		Params out
			query
			  		  	
	--->
	<cffunction name="getGrpProduitsGroupeAsQuery" access="remote" output="false" returntype="array">
		<cfargument name="paramsRecherche" type="ParamsRecherche" required="true"/>
		<!---pkg_cv_grcl_facturation.liste_grp_ctl_abos( p_operateurid => 	:p_operateurid,
			                                             p_libelle => 		:p_libelle,
			                                             p_idracine => 		:p_idracine,
			                                             p_retour => 		:p_retour);
		--->											
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation_V3.liste_grp_ctl_abos">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeMaitre" 	value="#THIS.getIdGroupeMaitre()#" null="#iif((paramsRecherche.getCatalogueClient() eq 0), de("yes"), de("no"))#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl" 			value="0" null="true"/>
			<cfprocresult name="qListeGrpProduitsAbos"/>        
		</cfstoredproc>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.liste_grp_ctl_Consos">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in"   variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeMaitre" 	value="#THIS.getIdGroupeMaitre()#" null="#iif((paramsRecherche.getCatalogueClient() eq 0), de("yes"), de("no"))#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl"		 	value="0" null="true"/>
			<cfprocresult name="qListeGrpProduitsConsos"/>        
		</cfstoredproc>
		
		<cfset tabQListeGrpProduits = arraynew(1)>
		<cfset arrayappend(tabQListeGrpProduits,qListeGrpProduitsAbos)>
		<cfset arrayappend(tabQListeGrpProduits,qListeGrpProduitsConsos)>		
		<cfreturn tabQListeGrpProduits>
		
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Fournit la liste des groupe de produits élaborés par Consotel pour un groupe de ligne.
					  
		Param in 
			ParamsRecherche
		Params out
			query
			  		  	
	--->
	<cffunction name="getGrpProduitsGroupeLigneAsQuery" access="Remote" output="false" returntype="array">
		<cfargument name="paramsRecherche" type="ParamsRecherche" required="true"/>
		<!---pkg_cv_glig_facturation.liste_grp_ctl_abos( p_operateurid => 	:p_operateurid,
			                                             p_libelle => 		:p_libelle,
			                                             p_idracine => 		:p_idGroupeLigne,
			                                             p_retour => 		:p_retour);
		--->											
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_glig_facturation_V3.liste_grp_ctl_abos">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeLigne" 		value="#THIS.getIdGroupeClient()#" null="#iif((paramsRecherche.getCatalogueClient() eq 0), de("yes"), de("no"))#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl" 			value="0" null="true"/>
			<cfprocresult name="qListeGrpProduitsAbos"/>        
		</cfstoredproc>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_glig_facturation_V3.liste_grp_ctl_Consos">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeLigne" 		value="#THIS.getIdGroupeClient()#" null="#iif((paramsRecherche.getCatalogueClient() eq 0), de("yes"), de("no"))#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl"		 	value="0" null="true"/>
			<cfprocresult name="qListeGrpProduitsConsos"/>        
		</cfstoredproc>
		
		<cfset tabQListeGrpProduits =  arraynew(1)>
		<cfset arrayappend(tabQListeGrpProduits,qListeGrpProduitsAbos)>
		<cfset arrayappend(tabQListeGrpProduits,qListeGrpProduitsConsos)>		
		<cfreturn tabQListeGrpProduits>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Fournit la liste des produits tagués par l'acheteur qui apparaissent dans les factures émisent sur la periode
					  et qui sont facturé é 100% sur ces factures.
		Param in 
			ParamsRecherche
		Params out
			Produit[]	
	--->
	<cffunction name="getProduitsTagues" access="Remote" output="false" returntype="Produit[]">
		<cfargument name="paramsRecherche" type="ParamsRecherche" required="true" />
		<cfset qlisteProuitCliTagues = getProduitsTaguesAsQuery(paramsRecherche)>	
		<cfset tabProduitsCliTagues = arrayNew(1)>			
		<cfset i = 1>
		<cfloop query="qlisteProuitCliTagues">
			<cfset produitCliTague = createObject("component","Produit").setProprietes(qlisteProuitCliTagues[i])>
			<cfset arrayAppend(tabProduitsCliTagues,produitCliTague)>
			<cfset i = i +1>
		</cfloop>
		<cfreturn tabProduitsCliTagues>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Fournit la liste des produits client tagués par l'acheteur qui apparaissent dans les factures émisent sur la periode
					  et qui sont facturé é 100% sur ces factures.
		Param in 
			ParamsRecherche
		Params out
			query	
	--->
	<cffunction name="getProduitsTaguesAsQuery" access="Remote" output="false" returntype="Query">
		<cfargument name="paramsRecherche" type="ParamsRecherche" required="true" />
		<!--- TODO: PKG_CV_GRCL_FACTURATION.GET_PRODUITSCLIENTTAGUES --->
		<!--- 
			PROCEDURE			
				PKG_CV_FACTURATION.GET_PRODUITSCLIENTTAGUES
			PARAM	
			in			
				p_idGroupe_client		INTEGER
				p_chaine 				VAR_CHAR
				p_dateDebut				DATE
				p_dateFin				DATE
				p_themeId		INTEGER
			out				  	
				p_retour				QUERY				
		 
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION_V3.GET_PRODUITSCLIENTTAGUES">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupe_client" 	value="#THIS.getIdGroupeClient()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_perimetre" 	value="#THIS.getTypePerimetre()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_DATE"  	 	type="in" 	variable="p_dateDebut" 			value="#paramsRecherche.getDateDebut()#"/>
			<cfprocparam cfsqltype="CF_SQL_DATE"  	 	type="in" 	variable="p_dateFin" 			value="#paramsRecherche.getDateFin()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_themeId" 			value="#paramsRecherche.getThemeId()#"/>
			<cfprocresult name="qListeProduits"/>        
		</cfstoredproc>--->
		<cfset qListeProduits = queryNew("")>
		<cfreturn qListeProduits>
	</cffunction>
	
	
	<!---
	Calcul les couts de chaque groupe de produit sur les x derniers mois 
	--->
	<cffunction name="calculerCoutSurXDerniersMois" access="Remote" output="false" returntype="array">
		<cfargument name="pRecherche" type="ParamsRecherche" required="true" />
		<cfargument name="nbMois" type="numeric" required="false" default="6"/>
		
		<cfset alisteGrpProduits = Evaluate("calculerCoutSurXDerniersMois#getTypePerimetre()#AsQuery(pRecherche,nbMois)")>
		<cfset qlisteGrpProduitsAbos = alisteGrpProduits[1]>
		<cfset qlisteGrpProduitsConsos = alisteGrpProduits[2]>
		
		<!--- =============== TOTAUX ============== --->
		<cfset montantTotal = 0>
		<cftry>
			<cfquery name="totalAbos" dbtype="query">
				select sum(MONTANT) as TOTAL from qlisteGrpProduitsAbos
			</cfquery>
			<cfquery name="totalConsos" dbtype="query">
				select sum(MONTANT) as TOTAL from qlisteGrpProduitsConsos
			</cfquery>	
			<cfif totalAbos.recordCount gt 0>
				<cfset montantTotal = montantTotal + totalAbos.TOTAL[1]>
				<cfelse>
				<cfset montantTotal = 0>
			</cfif>
			<cfif totalConsos.recordCount gt 0>
				<cfset montantTotal = montantTotal + totalConsos.TOTAL[1]>				
			</cfif>	
		<cfcatch type="any">
			<cfset montantTotal = 0>
		</cfcatch>	
		</cftry>
		<!--- =============== FIN TOTAUX ========== --->
		
		
		<!--- =============== GRP PRODUITS ABOS ========== --->
		<cfset tabProduitsAbos = arrayNew(1)>	
		
		<cfloop query="qlisteGrpProduitsAbos">
			<cfset grpProduitsAbos = createObject("component","GroupeProduitControle")>	
			<cfscript>		
				grpProduitsAbos.setId(qlisteGrpProduitsAbos.IDGRP_CTL);
				grpProduitsAbos.setLibelle(qlisteGrpProduitsAbos.GRP_CTL);
				grpProduitsAbos.setSegment(formatSegment(qlisteGrpProduitsAbos.GRP_SEGMENT));				
				grpProduitsAbos.setOperateurId(pRecherche.getOperateurId());
				if (montantTotal eq 0){
					grpProduitsAbos.setPoids(0);
				}else{
					grpProduitsAbos.setPoids(qlisteGrpProduitsAbos.MONTANT/montantTotal*100);
				}
			  	grpProduitsAbos.setPourControle(qlisteGrpProduitsAbos.A_CONTROLER);
				grpProduitsAbos.setPrixUnitaire(qlisteGrpProduitsAbos.TARIF_BRUT);
				grpProduitsAbos.setPrixRemise(qlisteGrpProduitsAbos.TARIF_REMISE);
				grpProduitsAbos.setMontant(qlisteGrpProduitsAbos.MONTANT); 
				grpProduitsAbos.setNbVersion(1); 
				grpProduitsAbos.setRemiseContrat(qlisteGrpProduitsAbos.PCT_REMISE); 
				grpProduitsAbos.setDateDebut(qlisteGrpProduitsAbos.DATEDEB); 
				grpProduitsAbos.setDateFin(qlisteGrpProduitsAbos.DATEFIN); 
				grpProduitsAbos.setType("ABO");
			</cfscript>
			<cfset arrayAppend(tabProduitsAbos,grpProduitsAbos)>
		</cfloop>
		
		<!--- =============== FIN GRP PRODUITS ABOS ========== --->
		
		
		<!--- =============== GRP PRODUITS CONSOS ========== --->
		<cfset qlisteGrpProduitsConsos = alisteGrpProduits[2]>
		<cfset tabProduitsConsos = arrayNew(1)>
		<cfloop query="qlisteGrpProduitsConsos">
			<cfset grpProduitsConsos = createObject("component","GroupeProduitControle")>			
			<cfscript>		
			//Initialize the CFC with the properties values.
				grpProduitsConsos.setId(qlisteGrpProduitsConsos.IDGRP_CTL);
				grpProduitsConsos.setLibelle(qlisteGrpProduitsConsos.GRP_CTL);
				grpProduitsConsos.setSegment(formatSegment(qlisteGrpProduitsConsos.GRP_SEGMENT));				
				grpProduitsConsos.setOperateurId(pRecherche.getOperateurId());
				if (montantTotal eq 0){
					grpProduitsConsos.setPoids(0);
				}else{
					grpProduitsConsos.setPoids(qlisteGrpProduitsConsos.MONTANT/montantTotal*100);
				}
			  	grpProduitsConsos.setPourControle(qlisteGrpProduitsConsos.A_CONTROLER);
				grpProduitsConsos.setPrixUnitaire(qlisteGrpProduitsConsos.TARIF_BRUT);
				grpProduitsConsos.setPrixRemise(qlisteGrpProduitsConsos.TARIF_REMISE);
				grpProduitsConsos.setMontant(qlisteGrpProduitsConsos.MONTANT); 
				grpProduitsConsos.setNbVersion(1); 
				grpProduitsConsos.setRemiseContrat(qlisteGrpProduitsConsos.PCT_REMISE); 
				grpProduitsConsos.setDateDebut(qlisteGrpProduitsConsos.DATEDEB); 
				grpProduitsConsos.setDateFin(qlisteGrpProduitsConsos.DATEFIN); 
				grpProduitsConsos.setType("CONSO");
			</cfscript>
			<cfset arrayAppend(tabProduitsConsos,grpProduitsConsos)>
		</cfloop>
		<!--- =============== FIN PRODUITS CONSOS ========== --->
		
		<cfset tabProduits = arrayNew(1)>		
		<cfset arrayappend(tabProduits,tabProduitsAbos)>
		<cfset arrayappend(tabProduits,tabProduitsConsos)>
				
		<cfreturn tabProduits>
	</cffunction>
	
	<cffunction name="calculerCoutSurXDerniersMoisGroupeAsQuery" access="Remote" output="false" returntype="array">
		<cfargument name="paramsRecherche" type="ParamsRecherche" required="true" />
		<cfargument name="nbMois" type="numeric" required="false" default="6"/>
		
		<!--- TODO: PKG_CV_GRCL_FACTURATION.CALCUL_COUT_XMOIS --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.liste_grp_ctl_abos_cout">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeMaitre" 	value="#THIS.getIdGroupeMaitre()#" >
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl" 			value="0" null="true"/>
			<cfprocresult name="qListeCoutsAbos"/>        
		</cfstoredproc>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation_V3.liste_grp_ctl_Consos_cout">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in"   variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeMaitre" 	value="#THIS.getIdGroupeMaitre()#" >
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl"		 	value="0" null="true"/>
			<cfprocresult name="qListeCoutsConsos"/>        
		</cfstoredproc>

		<cfset tabQListeCouts = arraynew(1)>
		<cfset arrayappend(tabQListeCouts,qListeCoutsAbos)>
		<cfset arrayappend(tabQListeCouts,qListeCoutsConsos)>
				
		<cfreturn tabQListeCouts>
		
	</cffunction>
	
	<cffunction name="calculerCoutSurXDerniersMoisGroupeLigneAsQuery" access="Remote" output="false" returntype="array">
		<cfargument name="paramsRecherche" type="ParamsRecherche" required="true" />
		<cfargument name="nbMois" type="numeric" required="false" default="6"/>
		
		 
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.liste_grp_ctl_abos_cout">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeLigne" 		value="#THIS.getIdGroupeClient()#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl" 			value="0" null="true"/>
			<cfprocresult name="qListeCoutsAbos"/>        
		</cfstoredproc>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation_V3.liste_grp_ctl_Consos_cout">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in"   variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeLigne" 		value="#THIS.getIdGroupeClient()#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl"		 	value="0" null="true"/>
			<cfprocresult name="qListeCoutsConsos"/>        
		</cfstoredproc>
		
		<cfset tabQListeCouts = arraynew(1)>
		<cfset arrayappend(tabQListeCouts,qListeCoutsAbos)>
		<cfset arrayappend(tabQListeCouts,qListeCoutsConsos)>
				
		<cfreturn tabQListeCouts>
		
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : 	Met é jour les tarifs du produit.
						Retourne 1 en cas de succes, sinon -1
		Param in 
			Produit
		Params out
			numeric	
	--->
	<cffunction name="loadProduit" access="Remote" output="false" returntype="GroupeProduitControle">
		<cfargument name="myProduit" type="GroupeProduitControle" required="true"/>
		<cfreturn myProduit.load()/>
	</cffunction>
	
	<cffunction name="getListeProduitsGroupeDeProduit" access="remote" output="false" returntype="Produit[]">
		<cfargument name="gpeProduit" type="LigneFacturation" required="true" />
		<cfset qlisteProuit = getListeProduitsGroupeDeProduitAsQuery(gpeProduit)>	
		<cfset tabProduitsCat = arrayNew(1)>
		<cfloop query="qlisteProuit">
			<cfset produit = createObject("component","Produit")>
			<cfscript>
				produit.setIdCat(qlisteProuit.IDPRODUIT_CATALOGUE);
			</cfscript>	
			<cfset arrayAppend(tabProduitsCat,produit)>
		</cfloop>
		<cfreturn tabProduitsCat>
	</cffunction>
	
	<!---		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : 	Liste des produits d'un groupe de produit
						
		Param in 
			gpe_Produit(produit)
		Params out
			produit[]	
	--->
	<cffunction name="getListeProduitsGroupeDeProduitAsQuery" access="Remote" output="false" returntype="query">
		<cfargument name="gpeProduit" type="LigneFacturation" required="true"/>
      	<!---  PROCEDURE liste_produit_grp (       p_idgrp_ctl                                    IN integer,
                                           		   p_retour                                       OUT sys_refcursor);
	  	--->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION_V3.liste_produit_grp">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idgrp_ctl" 			value="#gpeProduit.getId()#"/>
			<cfprocresult name="qListeProduits"/>        
		</cfstoredproc>		
		<cfreturn qListeProduits/>
	</cffunction>
	
	
	<!---
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : 	Met é jour le Tague 'pour le contréle'.
						Retourne 1 en cas de succes, sinon -1
		Param in 
			Produit
		Params out
			numeric	
	--->
	<cffunction name="taguerProduitClient" access="Remote" output="false" returntype="numeric">
		<cfargument name="grpProduit" type="GroupeProduitControle" required="true" />
		<cfreturn 1/>
	</cffunction>
	
	<!---
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : 	Tague le groupe produit du groupe, pour le contréle.						
						Retourne 1 en cas de succes, sinon -1
		Param in 
			GroupeProduitControle
		Params out
			numeric	
	--->
	<cffunction name="taguerProduitGroupe" access="Remote" output="false" returntype="numeric">
		<cfargument name="grpProduit" type="GroupeProduitControle" required="true" />
		<cfreturn grpProduit.taguer(getIdGroupeMaitre())>
	</cffunction>
	
	<!---
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Détague le groupe de produit du groupe pour le contréle,
					  Retourne 1 en cas de succes, sinon -1.
		Params
			In
				Produit
			Out 
				Numeric 1 si ok sinon -1
	--->
	<cffunction name="detaguerProduitGroupe" access="remote" output="false" returntype="Numeric">		
		<cfargument name="grpProduit" type="GroupeProduitControle" required="true"/>
		<cfreturn grpProduit.detaguer(getIdGroupeMaitre())>
	</cffunction>
</cfcomponent>
