<cfcomponent author="Cedric" extends="fr.saaswedo.utils.remoting.RemotingClient"
hint="Utilitaire de remoting (SOAP). Le username et le password doivent etre renseignés pour que les crédentiels soient définis">
	<cffunction access="public" name="invoke" returntype="Struct" output="false" hint="Instancie le WebService puis effectue l'appel de la méthode
	et retourne le résultat comme spécifié par la fonction parent. L'instance du WebService (Axis Stub) sera le résultat si procedure est une chaine vide.
	Toute exception causée par le WebService est capturée puis loggée (sans stockage) avant d'etre ajoutée dans la structure retournée">
		<cfargument name="service" type="Struct" required="true" hint="Informations concernant le service">
		<cfargument name="procedure" type="String" required="true" hint="Nom de la méthode à appeler">
		<cfargument name="parameters" type="Array" required="true" hint="Liste des paramètres à fournir à la procédure">
		<cfset var result={}>
		<cftry>
			<cfset var endpoint=SUPER.getEndpoint(ARGUMENTS.service)>
			<cfif LEN(TRIM(endpoint)) GT 0>
				<cfset var procParams=ARGUMENTS.parameters>
				<cfset var parameterCount=arrayLen(procParams)>
				<!--- Instanciation du WebService (SOAP) et définition des crédentiels --->
				<cfset var stub=createObject("webservice",endpoint)>
				<cfset setStubCredentials(ARGUMENTS.service,stub)>
				<!--- Appel du WebService --->
				<cfif LEN(TRIM(ARGUMENTS.procedure)) GT 0>
					<cfinvoke webservice="#stub#" method="#ARGUMENTS.procedure#" returnvariable="wsResult">
						<cfloop index="i" from="1" to="#parameterCount#">
							<cfset var param=procParams[i]>
							<cfset var paramAttributes={name=SUPER.getParamName(param)}>
							<cfif SUPER.isParamValueDefined(param)>
								<cfset paramAttributes.value=SUPER.getParamValue(param)>
							<cfelse>
								<cfset paramAttributes.omit=TRUE>
							</cfif>
							<cfinvokeargument attributecollection="#paramAttributes#">
						</cfloop>
					</cfinvoke>
					<cfset result[SUPER.STATUS()]=isDefined("wsResult") ? SUPER.DEFINED():SUPER.UNDEFINED()>
					<cfif result[SUPER.STATUS()] EQ SUPER.DEFINED()>
						<cfset result[SUPER.RESULT()]=wsResult>
					</cfif>
					<!--- Request et réponse SOAP --->
					<cfset setClientRequest(result,stub)>
					<cfset setServerResponse(result,stub)>
				<!--- Pas d'appel du WebService (Procédure non renseignée): Pas de requete ni de réponse SOAP --->
				<cfelse>
					<cfset result[SUPER.STATUS()]=SUPER.DEFINED()>
					<cfset result[SUPER.RESULT()]=stub>
				</cfif>
			<cfelse>
				<cfthrow type="Custom" message="Unable to invoke service" detail="Endpoint is undefined or empty">
			</cfif>
			<cfcatch type="Any">
				<cfset var errorMessage=CFCATCH.message>
				<cfset result=(isDefined("stub") EQ TRUE) ? getErrorResult(CFCATCH,stub):getErrorResult(CFCATCH)>
				<cfif LEN(TRIM(CFCATCH.detail)) GT 0>
					<cfset errorMessage=errorMessage & " [Detail:" & CFCATCH.detail & "]">
				</cfif>
				<cflog type="error" text="SOAP invoke() error: #errorMessage#">
			</cfcatch>
			<cffinally>
				<cfreturn result>
			</cffinally>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="FAULT" returntype="Numeric" hint="Valeur associée à STATUS() pour une erreur du WebService (AxisFault)">
		<cfreturn -2>
	</cffunction>
	
	<cffunction access="public" name="init" returntype="fr.saaswedo.utils.remoting.SoapClient" hint="Constructeur">
		<cfset SUPER.init()>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="setStubCredentials" returntype="void" hint="Appelée par invoke() pour ajouter définir les crédentiels du stub">
		<cfargument name="service" type="Struct" required="true" hint="Informations concernant le service">
		<cfargument name="stub" type="Any" required="true" hint="Instance du WebService (Axis Stub) provenant de invoke()">
		<cfset var username=SUPER.getUsername(ARGUMENTS.service)>
		<cfset var password=SUPER.getPassword(ARGUMENTS.service)>
		<cfif (LEN(TRIM(username)) GT 0) AND (LEN(TRIM(password)) GT 0)>
			<cfset var stubInstance=ARGUMENTS.stub>
			<cfset stubInstance.setUsername(username)>
			<cfset stubInstance.setPassword(password)>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="setClientRequest" returntype="void" hint="Appelée par invoke() pour ajouter le SOAP Request (XML)
	dans le résultat qui sera retourné par invoke() si elle est accessible et qu'elle n'a pas encore été ajoutée dedans">
		<cfargument name="invokeResult" type="Struct" required="true" hint="Structure provenant de invoke() et qui sera retournée par invoke()">
		<cfargument name="stub" type="Any" required="true" hint="Instance du WebService (Axis Stub) provenant de invoke()">
		<cfset var result=ARGUMENTS.invokeResult>
		<cftry>
			<cfif NOT structKeyExists(result,SUPER.CLIENT_REQUEST())>
				<cfset result[SUPER.CLIENT_REQUEST()]=getSoapRequest(ARGUMENTS.stub)>
			</cfif>
			<cfcatch type="Any">
				<cfset var errorMessage=CFCATCH.message>
				<cfif LEN(TRIM(CFCATCH.detail)) GT 0>
					<cfset errorMessage=errorMessage & " [Detail:" & CFCATCH.detail & "]">
				</cfif>
				<!--- Si la requete client ne peut pas etre obtenue alors c'est un avertissement non bloquant (i.e Pas d'erreur) --->
				<cflog type="warning" text="Unable to add SOAP request in invoke() result: #errorMessage#">
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="setServerResponse" returntype="void" hint="Appelée par invoke() pour ajouter la réponse SOAP (XML)
	dans le résultat qui sera retourné par invoke() si elle est accessible et qu'elle n'a pas encore été ajoutée dedans">
		<cfargument name="invokeResult" type="Struct" required="true" hint="Structure provenant de invoke() et qui sera retournée par invoke()">
		<cfargument name="stub" type="Any" required="true" hint="Instance du WebService (Axis Stub) provenant de invoke()">
		<cfset var result=ARGUMENTS.invokeResult>
		<cftry>
			<cfif NOT structKeyExists(result,SUPER.SERVER_RESPONSE())>
				<cfset result[SUPER.SERVER_RESPONSE()]=getSoapResponse(ARGUMENTS.stub)>
			</cfif>
			<cfcatch type="Any">
				<cfset var errorMessage=CFCATCH.message>
				<cfif LEN(TRIM(CFCATCH.detail)) GT 0>
					<cfset errorMessage=errorMessage & " [Detail:" & CFCATCH.detail & "]">
				</cfif>
				<!--- Si la réponse client ne peut pas etre obtenue alors c'est un avertissement non bloquant (i.e Pas d'erreur) --->
				<cflog type="warning" text="Unable to add SOAP response in invoke() result: #errorMessage#">
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="getErrorResult" returntype="Struct"
	hint="Appelée par invoke() pour retourner affecter l'exception error à la clé RESULT(). Le paramètre stub est fourni s'il est défini.
	Cette fonction vérifie si les clés CLIENT_REQUEST(),SERVER_RESPONSE() sont renseignées et les renseigne à partir de stub si ce n'est pas le cas">
		<cfargument name="error" type="Any" required="true" hint="Exception ColdFusion durant la fonction invoke()">
		<cfargument name="stub" type="Any" required="false" hint="Instance du WebService (Axis Stub)">
		<cfset var errorResult={}>
		<cfset var cfException=ARGUMENTS.error>
		<cfset errorResult[SUPER.STATUS()]=SUPER.ERROR()>
		<cfif structKeyExists(ARGUMENTS,"stub") AND isDefined("ARGUMENTS.stub")>
			<cfif NOT structKeyExists(errorResult,CLIENT_REQUEST())>
				<cfset setClientRequest(errorResult,ARGUMENTS.stub)>
			</cfif>
			<cfif NOT structKeyExists(errorResult,SERVER_RESPONSE())>
				<cfset setServerResponse(errorResult,ARGUMENTS.stub)>
			</cfif>
			<!--- Erreur de type AxisFault --->
			<cfif structKeyExists(cfException,"faultString")>
				<cfset errorResult[SUPER.STATUS()]=FAULT()>
			</cfif>
		</cfif>
		<!--- L'exception error est affectée à la clé RESULT() --->
		<cfset errorResult[SUPER.RESULT()]=ARGUMENTS.error>
		<cfreturn errorResult>
	</cffunction>
</cfcomponent>