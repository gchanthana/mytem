package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.extension
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;

	public class RuleExtGeneric implements IRuleDisplayItem
	{
		public function RuleExtGeneric()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem=new IsLigneFixe();
			
			return r1.isDisplay();
		}
	}
}
