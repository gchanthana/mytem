package composants.tb.graph
{
	import flash.text.TextFieldAutoSize;
	
	import mx.charts.LegendItem;
	import mx.core.IUITextField;
	import mx.core.UITextField;
	import mx.core.mx_internal;
	
	use namespace mx_internal;
	
	public class WrapLegendItem extends LegendItem
	{
		private var nameLabel:IUITextField;
		
		public function WrapLegendItem()
		{
			super();
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			nameLabel = UITextField(this.getChildAt(numChildren-1))
			nameLabel.autoSize = TextFieldAutoSize.LEFT;
			nameLabel.wordWrap = true;
			// you can change the width of the text by changing this value
			// some higher values work for some reason and chop bits off the text
			// must be something wrong with measure()
			nameLabel.width = 100;
		}
		
		// overriding to ALWAYS SET TO percentHeight to ZERO
		override public function set percentHeight(value:Number):void
		{
			super.percentHeight = 0;
		}
		
		override public function get label():String
		{
			return _label;
		}
		private var labelChanged:Boolean;
		override public function set label(value:String):void
		{
			_label = value;
			labelChanged = true;
			invalidateSize();
		}
		private var _label:String = '';
		
		override protected function measure():void
		{
			var txt:String = _label;
			
			var itemW:Number = 0;
			var itemH:Number = 0;
			
			if (txt == null || txt == "" || txt.length < 2)
				txt = "Wj";
			
			if (labelChanged)
			{
				labelChanged = false;
				nameLabel.htmlText = txt;
			}
			nameLabel.width = width - UITextField.TEXT_WIDTH_PADDING - getMarkerWidth() - getStyle("paddingLeft") - getStyle("paddingRight") - getStyle("horizontalGap");
			
			var textW:Number = nameLabel.width;
			var textH:Number = nameLabel.height;
			
			if (nameLabel.textWidth > 0 || getMarkerWidth() > 0)
			{
				itemW += getStyle("paddingLeft") + getStyle("paddingRight");
				itemH += getStyle("paddingTop") + getStyle("paddingBottom");
				
				itemW += getStyle("horizontalGap") + getMarkerWidth() + textW;
				itemH += Math.max(textH, getMarkerHeight());
			}
			
			measuredWidth = itemW;
			measuredHeight = itemH;
		}
		
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
		}
		
		private function getMarkerWidth():Number
		{
			var out:Number = getStyle("markerWidth");
			if (isNaN(out))
				out = 10;
			return out;
		}
		private function getMarkerHeight():Number
		{
			var out:Number = getStyle("markerHeight");
			if (isNaN(out))
				out = 15;
			return out;
		}
	}
}