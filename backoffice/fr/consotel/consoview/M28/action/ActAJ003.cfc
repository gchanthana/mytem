﻿<cfcomponent extends="AbstractAction">
	
	<cffunction name="executeAct" access="public" returntype="Struct">
		<cfargument name="data" type="struct" required="true">
		
		<cftry>
			
				<cfstoredproc procedure="pkg_m28.IUFonction" datasource="ROCOFFRE" >
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.nomFonction#">
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.commentFct#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#data.idCategorie#">
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.keyFct#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#data.idFonction#">
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.droit_niveau#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out">
				</cfstoredproc>
				
				<cfset qResult = structNew()>
				<cfset qResult.LISTFCTS = p_retour>
				<cfreturn CreateReponse("AJ003", 1, qResult)>	
			
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ003", 0, ThrowCfCatchError("AJ003",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
				
	</cffunction>

</cfcomponent>