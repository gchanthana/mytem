<!--- =========================================================================
Classe: LignesUtils
Auteur: samuel.divioka
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="ProduitsUtils" hint="" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="ProduitsUtils" hint="Remplace le constructeur de ProduitsUtils.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->


	<!--- 
	fournit les article du catalogue operateur 
	 --->
	<cffunction name="fournirListeProduitsCatalogueOperateur" access="public" returntype="query" output="false" hint="" >
		<cfargument name="idoperateur" type="numeric" required="true">
		<cfset codeLangue=session.user.globalization>
		
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		<cfset idGestionnaire = Session.USER.CLIENTACCESSID>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_COMMANDE.L_PRODUITS_SEGMENT_V3_1">	
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idoperateur#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_app_loginid" value="#idGestionnaire#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idgroupe_client" value="#idRacine#">
			<cfprocparam  cfsqltype="CF_SQL_VARCHAR"  type="in" value="#codeLangue#">
			<cfprocresult name="result">		  
		</cfstoredproc>		 		
		<cfreturn result/>
	</cffunction>
	
	<cffunction name="ajouterProduitAMesFavoris" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="idProduitCatalogue" type="numeric" required="true">
		
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		<cfset idGestionnaire = Session.USER.CLIENTACCESSID>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.ADDPRODFAVORI">	
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idRacine#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idGestionnaire#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idProduitCatalogue#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="out" variable="result">		  
		</cfstoredproc>		 		
		<cfreturn result/>
	</cffunction>	
	
	<cffunction name="supprimerProduitDeMesFavoris" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="idProduitCatalogue" type="numeric" required="true">
		
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		<cfset idGestionnaire = Session.USER.CLIENTACCESSID>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.DELPRODFAVORI">	
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idRacine#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idGestionnaire#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idProduitCatalogue#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="out" variable="result">
		</cfstoredproc>		 		
		<cfreturn result/>
	</cffunction>
	
	<cffunction name="fournirListeProduitsFavoris" access="public" returntype="query" output="false" hint="" >
		<cfargument name="idoperateur" type="numeric" required="true">
		
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		<cfset idGestionnaire = Session.USER.CLIENTACCESSID>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GESTIONNAIRE.GETPRODFAVORI">	
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idRacine#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_operateurid" value="#idGestionnaire#">
			<cfprocresult name="result">		  
		</cfstoredproc>		 		
		<cfreturn result/>
	</cffunction>
	
	
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>