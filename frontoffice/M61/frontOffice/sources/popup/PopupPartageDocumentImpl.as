package popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.RadioButton;
	import mx.controls.RadioButtonGroup;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.resources.ResourceManager;
	
	import classe.Login;
	import classe.PopupWindows;
	
	import composants.util.ConsoviewAlert;
	
	import events.container.ContainerEvent;
	
	import service.container.ContainerService;

	public class PopupPartageDocumentImpl extends PopupWindows
	{
		public var rbg				:RadioButtonGroup;
		public var rbnotall			:RadioButton;
		public var rball			:RadioButton;
		public var vbUser			:HBox;
		public var dtgUtilisateur	:DataGrid;
		public var ckxSelectAll		:CheckBox;
		public var ti_Filtre		:TextInput;
		public var dgc_fullname		:DataGridColumn;
		
		[Bindable] public var cs:ContainerService;
	// contient la liste des logins sélectionnés dans cet écran
		private var _listeLoginSelected:ArrayCollection;
	// Contient la liste des documents sélectionnés sur l'écran principal	
		public var listeDocSelected:ArrayCollection;
	// app_loginid de la personne connectée
		public var app_loginid:int
	// nombre de login sélectionné dans cet écran
		[Bindable] protected var nbLoginSelected:int = 0;
		
		public function PopupPartageDocumentImpl()
		{
			super();
			listeLoginSelected = new ArrayCollection();
		}

		protected function creationCompleteHandler():void
		{
			cs.addEventListener(ContainerEvent.VALIDATE_EVENT,validate);
			var idracine:int = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			cs.getlisteLoginShare(idracine)
		}
		protected function ckxSelectAllHandler():void
		{
			for each(var log:Login in cs.listeLogin)
			{
				log.SELECTED = ckxSelectAll.selected
				addSelectedUser(log);
			}
			calculNbLogin();
		}
		protected function rbgChangeHandler():void
		{
			if(rbg.selectedValue == "notall")
			{
				vbUser.visible = true;
				vbUser.includeInLayout = true;
			}
			else
			{
				vbUser.visible = false;
				vbUser.includeInLayout = false;
			}
		}
		public function SelectionLoginHandler():void
		{
			calculNbLogin();
		}
		
		public function addSelectedUser(selectedObj:Login):void
		{
			if(listeLoginSelected)
			{
				var here:Boolean = false;
				
				for (var j:int = 0; j < listeLoginSelected.length; j++) 
				{
					if(selectedObj.APP_LOGINID == listeLoginSelected[j].APP_LOGINID)
					{
						here = true;
						break;
					}
				}
					
				if(here == false)
					listeLoginSelected.addItem(selectedObj);
			}
		}
		
		
		private function calculNbLogin():void
		{
			nbLoginSelected = 0;
			for each(var log:Login in cs.listeLogin)
			{
				if(log.SELECTED)
					nbLoginSelected++;
			}
			
			if(nbLoginSelected == cs.listeLogin.length)
				ckxSelectAll.selected = true; 
			else
				ckxSelectAll.selected = false;
			if(nbLoginSelected == 0)
				ckxSelectAll.selected = false;
		}
		protected function btnValiderHandler():void
		{
			ti_Filtre.text = '';
			onChangeTextHandler(null);
			if(listeLoginSelected.length > 0)
			{
				cs.shareDoc(listeLoginSelected,listeDocSelected,app_loginid);
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M61','Veuillez_s_lectionner_un_ou_plusieurs_utilisateurs_'), ResourceManager.getInstance().getString('M61','Attention'), null);
			
		}
		private function validate(e:ContainerEvent):void
		{
			validateWindow();
		}
		
		protected function onChangeTextHandler(evt:Event):void
		{
			cs.listeLogin.filterFunction = function filtrerParPersonne(item:Object):Boolean
			{
				if((item.NOMCOMPLET != null && item.NOMCOMPLET.toString().toLowerCase().search(ti_Filtre.text) !=-1)
					||(item.LOGIN != null && item.LOGIN.toString().toLowerCase().search(ti_Filtre.text) !=-1))
				{
					return true;
				}
				else
					return false;		
			}
			
			cs.listeLogin.refresh();
			calculNbLogin();
		}
		
		/**
		 * Supprimer un seul utilisateur sélectionné
		 */
		public function deleteSelectedUser(selectObj:Login):void
		{
			if(listeLoginSelected.length > 0)
			{
				for (var i:int = 0; i < listeLoginSelected.length; i++) 
				{
					if(listeLoginSelected[i].APP_LOGINID == selectObj.APP_LOGINID)
					{
						listeLoginSelected.removeItemAt(listeLoginSelected.getItemIndex(selectObj));
						selectObj.SELECTED = false;
						break;
					}
				}
			}
		}
		
		/**
		 * Supprimer tout les utilisateurs sélectionnés
		 */
		protected function onDeleteAllSelectedHandler(event:MouseEvent):void
		{
			if(listeLoginSelected.length >0)
			{
				for (var i:int = 0; i < listeLoginSelected.length; i++) 
					listeLoginSelected[i].SELECTED = false;
				
				listeLoginSelected.removeAll();
				
			}
			// Effacer le filtrage texte
			ti_Filtre.text = '';
			onChangeTextHandler(null);
		}
		
		[Bindable]
		public function get listeLoginSelected():ArrayCollection
		{
			return _listeLoginSelected;
		}
		
		public function set listeLoginSelected(value:ArrayCollection):void
		{
			_listeLoginSelected = value;
		}
		
	}
}