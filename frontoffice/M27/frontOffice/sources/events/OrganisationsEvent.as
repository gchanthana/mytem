package events
{
	import flash.events.Event;
	
	public class OrganisationsEvent extends Event
	{

		public static const ORGANISATIONSCLIENTES					:String = "ORGANISATIONSCLIENTES";
		
		public function OrganisationsEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new OrganisationsEvent(type, bubbles, cancelable);
		}

	}
}
