package gestionparc.ihm.action.mdm.actionGeolocalisation
{
	import gestionparc.ihm.action.ICommand;

	public class ActionGeolocalisationCommand implements ICommand
	{

		private var receiver:ActionGeolocalisationReceiver=null;

		public function ActionGeolocalisationCommand(rec:ActionGeolocalisationReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
