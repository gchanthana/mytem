<cfcomponent output="false" alias="fr.consotel.consoview.inventaire.cycledevie.Reclamation">
	
	<cfproperty name="IDINV_OPERATIONS" type="numeric" default="0">	
	<cfproperty name="IDINV_TYPEOP" type="string" default="">
	<cfproperty name="LIBELLE_OPERATIONS" type="string" default="">
	
	<cfproperty name="ID_SOCIETE" type="numeric" default="0">
	<cfproperty name="ID_CONTACT" type="numeric" default="0">
	
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		this.IDINV_OPERATIONS = 0;
		this.IDINV_TYPEOP = 0;
		this.LIBELLE_OPERATIONS = "";
		this.ID_SOCIETE = 0;
		this.ID_CONTACT = 0;
		thie.DATE_REF = "";
	</cfscript>
	
	<cffunction name="getIDINV_OPERATIONS" output="false" access="public" returntype="any">
		<cfreturn this.IDINV_OPERATIONS>
	</cffunction>

	<cffunction name="setIDINV_OPERATIONS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDINV_OPERATIONS = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getDATE_REF" output="false" access="public" returntype="any">
		<cfreturn this.DATE_REF>
	</cffunction>

	<cffunction name="setDATE_REF" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif arguments.val NEQ "" >
			<cfset this.DATE_REF = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid Date"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getIDINV_TYPEOP" output="false" access="public" returntype="any">
		<cfreturn this.IDINV_TYPEOP>
	</cffunction>

	<cffunction name="setIDINV_TYPEOP" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDINV_TYPEOP = arguments.val - 1>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getID_SOCIETE" output="false" access="public" returntype="any">
		<cfreturn this.ID_SOCIETE>
	</cffunction>

	<cffunction name="setID_SOCIETE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.ID_SOCIETE = arguments.val>
		<cfelse>
			<cfset this.ID_SOCIETE = 0>
		</cfif>
	</cffunction>

	
	<cffunction name="getID_CONTACT" output="false" access="public" returntype="any">
		<cfreturn this.ID_CONTACT>
	</cffunction>

	<cffunction name="setID_CONTACT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.ID_CONTACT = arguments.val>
		<cfelse>
			<cfset this.ID_CONTACT = 0>
		</cfif>
	</cffunction>
	
	<cffunction name="getLIBELLE_OPERATIONS" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_OPERATIONS>
	</cffunction>

	<cffunction name="setLIBELLE_OPERATIONS" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_OPERATIONS = arguments.val>
	</cffunction>

	<cffunction name="envoyer" access="public" returntype="numeric">		
		<cfset obj=createObject("component","fr.consotel.consoview.parcview.inventaire.commande.MAIL.DemandeNouveauxProduits")>
		<cfset fileName = obj.createPDFFile(THIS)>
			<!--- failto="support@consotel.fr" --->
		
		<cfset userMail = session.user.Email>
		
		<cfif fileName neq "Error">
			 <cfmail from="support@consotel.fr"
					to="#userMail#"
					subject="Commande : #getLIBELLE_COMMANDE()#"
					type="html"
					charset="utf-8"
					wraptext="72"
					replyto="#userMail#"
				
					server="#APPLICATION.MAIL_SEVRER#">	
			 	
			 	<cfmailparam file = "#fileName#">

			Veuillez trouver ci-joint le bordereau de Rï¿½clamtion 
			Cordialement.
			
			</cfmail>
			<cfreturn 1> 
		<cfelse>
			<cfreturn -1> 
		</cfif>
	</cffunction>
	
	<cffunction name="afficher" access="public" output="true">		
		<cfargument name="format" type="string" default="PDF">
		<cfset obj=createObject("component","fr.consotel.consoview.inventaire.cycledevie.export."& format &".BordereauDeReclamation")>
		<cfset file = obj.afficher(THIS)>		
	</cffunction>
</cfcomponent>