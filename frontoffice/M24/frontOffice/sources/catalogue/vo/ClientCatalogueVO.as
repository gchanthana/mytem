package catalogue.vo
{
	[Bindable]
	public class ClientCatalogueVO
	{
		private var _LIBELLE_CLIENT:String;
	
		private var _ID_CLIENT:Number;
		private var _MAIL_CLIENT:String;
		
		public var boolPrevenir : Boolean;
		public var boolAddInCatClient : Boolean ;
		public var assigned : Boolean =true;
		public var _value:Number;
		public var SELECTED:Boolean = false;
		
		public function ClientCatalogueVO()
		{ 
		}
		public function get ID_CLIENT():Number
		{
			return _ID_CLIENT;
		}
		
		public function get value():Number
		{
			return _value;
		}
		public function get LIBELLE_CLIENT():String
		{
			return _LIBELLE_CLIENT;
		}
		
		public function set LIBELLE_CLIENT(value:String):void
		{
			_LIBELLE_CLIENT = value;
		}
		public function set ID_CLIENT(value:Number):void
		{
			_ID_CLIENT = value;
		}
		
		public function set value(value:Number):void
		{
			_value = value;
		}

		public function set MAIL_CLIENT(value:String):void
		{
			_MAIL_CLIENT = value;
		}

		public function get MAIL_CLIENT():String
		{
			return _MAIL_CLIENT;
		}
	}
}