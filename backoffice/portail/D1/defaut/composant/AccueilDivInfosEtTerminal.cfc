<cfcomponent extends="portail.class.specialClass.AbstractComposant" 
			 implements="portail.class.specialClass.InterfaceComposant" 
			 output="false" 
			 displayname="Composant" 
			 hint="Classe de composant dans le template D1" >
	
	
	<!--- PSEUDO CONSTRUCTEUR --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<!--- empty --->
	</cffunction>
	
	
	<!---  --->
	<cffunction name="getAccueilDivInfosEtTerminal" access="public" output="true" returnType="string"
				displayname="" 
				hint="Génère la div contenant les infos d'un colaborateur et de son terminal / template D1" >
		
		<cfargument name="idSousTete" required="false" type="numeric" default=0 >
		
		<cfset segment = 1>
		<cfset idTerminal = 0>
		<cfset idId = 0>
		<cfset idSim = 0>
		<cfset image = '<img src="img/NoVisuelSmall.jpg" title="Aucune image disponible" alt="Aucune image disponible" style="" >'>
		
		
		<!--- SI IDSOUS_TETE EST CELUI PAR DEFAUT -> IDSOUS_TETE = 0 --->
		<cfif idSousTete eq 0>
		
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
						
					<cfset idTerminal = SESSION.queryMesTerminaux['IDTERMINAL'][currentRow] >
					<cfset idID = SESSION.queryMesTerminaux['ID'][currentRow] >
					<cfset idSIM = SESSION.queryMesTerminaux['IDSIM'][currentRow] >
					<cfset idSousTete = SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] >
					<cfset path = SESSION.queryMesTerminaux['PATH_LOW'][currentRow] >
					<cfset typeModele = SESSION.queryMesTerminaux['T_MODELE'][currentRow] >
					
					<cfif path neq "/">
						<cfset image = '<img src="http://images.consotel.fr/#path#" title="#typeModele#" alt="#typeModele#">'>
					<cfelse>
						<cfset image = '<img src="img/NoVisuelSmall.jpg" title="Aucune image disponible" alt="Aucune image disponible" style="" >'>
					</cfif>
					<cfbreak>
					
				</cfif>
			</cfloop>
				
		<cfelse><!--- SI IDSOUS_TETE EST SPECIFIÉ EN PARAMETRE --->
		
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] eq idSousTete >
					
					<cfset idTerminal = SESSION.queryMesTerminaux['IDTERMINAL'][currentRow] >
					<cfset idID = SESSION.queryMesTerminaux['ID'][currentRow] >
					<cfset idSIM = SESSION.queryMesTerminaux['IDSIM'][currentRow] >
					<cfset idSousTete = SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] >
					<cfset path = SESSION.queryMesTerminaux['PATH_LOW'][currentRow] >
					<cfset typeModele = SESSION.queryMesTerminaux['T_MODELE'][currentRow] >
					
					<cfif path neq "/">
						<cfset image = '<img src="http://images.consotel.fr/#path#" title="#typeModele#" alt="#typeModele#">'>
					<cfelse>
						<cfset image = '<img src="img/NoVisuelSmall.jpg" title="Aucune image disponible" alt="Aucune image disponible" style="" >'>
					</cfif>
					<cfbreak>					
				</cfif>
			</cfloop>
		
		</cfif>
		
		
		<!--- --------- --->
		<!--- MES INFOS --->
		<!--- --------- --->
		
		<cfif NOT isDefined("SESSION.queryMesInfos") >
			<cfset query = createObject("component","fr.consotel.consoview.M111.fiches.FicheCollaborateur").getInfosFicheCollaborateur(SESSION.IDEMPLOYE,SESSION.IDPOOL,0) >
			<!--- ON RECUPERE LE 2eme ARRAY RETOURNÉ --->
			<cfset SESSION.queryMesInfos = query[2]>
		</cfif>
		
		<!--- INITIALISATION VARIABLES INFOS USER --->
		<cfset color = "color1">
		<cfset nom = " -- ">
		<cfset prenom = " -- " >
		<cfset email = " -- " >
		<cfset fonction = " -- " >
		<cfset telFixe = " -- " >
		<cfset faxx = " -- " >
		<cfset matricule = " -- " >
		
		<cfif NOT SESSION.queryMesInfos["NOM"][1] eq "" >
			<cfset nom = SESSION.queryMesInfos["NOM"][1] >
		</cfif>
		<cfif NOT SESSION.queryMesInfos["PRENOM"][1] eq "" >
			<cfset prenom = SESSION.queryMesInfos["PRENOM"][1] >
		</cfif>
		<cfif NOT SESSION.queryMesInfos["EMAIL"][1] eq "" >
			<cfset email = SESSION.queryMesInfos["EMAIL"][1] >
		</cfif>
		<cfif NOT SESSION.queryMesInfos["FONCTION_EMPLOYE"][1] eq "" >
			<cfset fonction = SESSION.queryMesInfos["FONCTION_EMPLOYE"][1] >
		</cfif>
		<cfif NOT SESSION.queryMesInfos["TELEPHONE_FIXE"][1] eq "" >
			<cfset telFixe = SESSION.queryMesInfos["TELEPHONE_FIXE"][1] >
		</cfif>
		<cfif NOT SESSION.queryMesInfos["FAX"][1] eq "" >
			<cfset faxx = SESSION.queryMesInfos["FAX"][1] >
		</cfif>
		<cfif NOT SESSION.queryMesInfos["MATRICULE"][1] eq "" >
			<cfset matricule = SESSION.queryMesInfos["MATRICULE"][1] >
		</cfif>
		
		
		
		<cfset divMesInfosAndTerminal = '
				<div class="divInfo" style="">
					<div>
						<div class="pictoInfo" style="float:left;height:30px;width:40px;"><img src="img/mytem/mes_infos.png"></div>
						<div class="intituleInfo" style="">Mes infos</div>
						<div class="clear" style=""></div>
					</div>
					<div>
						<div class="pictoInfo" style="float:left;height:30px;width:50px;"></div>
						<div class="divTableInfo" style="">
							<table class="tableInfo" style="" width="100%">
								<tbody>
									<tr style="">
										<td class="divInfoIntitule" style="">Nom </td>
										<td class="divInfoValeur" style="">:&nbsp;#nom#</td>
									</tr>
									<tr style="">
										<td class="divInfoIntitule" style="">Prénom</td>
										<td class="divInfoValeur" style="">:&nbsp;#prenom#</td>
									</tr>
									<tr style="">
										<td class="divInfoIntitule" style="">Email</td>
										<td class="divInfoValeur" style="">:&nbsp;#email#</td>
									</tr>
									<tr style="">
										<td class="divInfoIntitule" style="">Fonction</td>
										<td class="divInfoValeur" style="">:&nbsp;#fonction#</td>
									</tr>
									<tr style="">
										<td class="divInfoIntitule" >Matricule</td>
										<td class="divInfoValeur" style="">:&nbsp;#matricule#</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="clear" style=""></div>
					</div>
				</div>
		'>
		
		
		<!--- ------------ --->
		<!--- MON TERMINAL --->
		<!--- ------------ --->
		
		<cfif NOT isDefined("SESSION.queryInfoFicheTerminal") || (isDefined("SESSION.clone_IdSous_tete") ) && SESSION.clone_IdSous_tete neq idSousTete >
			<cfset queryDetailTerminal = createObject("component","fr.consotel.consoview.M111.fiches.FicheTerminal").getInfosFicheTerminal(idTerminal,segment,SESSION.IDPOOL,idID) >
			<!--- ON RECUPERE LE 2EME QUERY DE L'ARRAY --->
			<cfset SESSION.queryInfoFicheTerminal = queryDetailTerminal[2]>
		</cfif>
		
		<cfif NOT isDefined("SESSION.queryInfoSim") || (isDefined("SESSION.clone_IdSous_tete") ) && SESSION.clone_IdSous_tete neq idSousTete>
			<cfset querySim = createObject("component","fr.consotel.consoview.M111.fiches.FicheSim").getInfosFicheSim(val(idSIM),val(idSousTete),SESSION.USER.CLIENTID,idID) >
			<cfset SESSION.queryInfoSim = querySim[2]>
			<cfset SESSION.clone_IdSous_tete = idSoustete>
		</cfif>
		
		<cfset date_achat = " -- " >
		<cfset date_renouvell = " -- " >
		
		<cfif SESSION.queryInfoFicheTerminal.DATE_ACHAT neq "">
			<cfset date_achat = LSDateFormat(SESSION.queryInfoFicheTerminal.DATE_ACHAT, "dd/mm/yyyy") >
		</cfif>
		
		<cfif SESSION.queryInfoSim.DATE_RENOUVELLEMENT neq "">
			<cfset date_renouvell = LSDateFormat(SESSION.queryInfoSim.DATE_RENOUVELLEMENT, "dd/mm/yyyy") >
		</cfif>
		
		
		<cfset divMesInfosAndTerminal &= '
				<div class="divTerminal" style="">
					<div>
						<div class="pictoTerminal" style=""><img src="img/mytem/mon_terminal.png"></div>
						<div class="intituleTerminal" style="" >Mon terminal</div>
						<div class="clear" style=""></div>
					</div>
					<div>
						<div class="pictoTerminal" style=""></div>
						<div class="divIMGAccueil" style="">
							<table class="tableIMGAccueil" width="100%" >
								<tr>
									<td align="center" style="width:115px;height:200px;">#image#</td>	
								</tr>
							</table>
						</div>
						<div class="divTableTerminal" style="">
							<table class="tableTerminal" style="" width="100%">
								<tbody>
									<tr style="">
										<td colspan="2" class="bold" style="">#SESSION.queryInfoFicheTerminal.MARQUE#</td>
									</tr>
									<tr style="">
										<td colspan="2" class="bold" style="">#SESSION.queryInfoFicheTerminal.MODELE#</td>
									</tr>
									<tr style="">
										<td class="tdTerminalIntitule" style="">Date d''achat</td>
										<td class="tdTerminalValeur" style="">:&nbsp;#date_achat#</td>
									</tr>
									<tr style="">
										<td class="tdTerminalIntitule" style="">Date renouvellement</td>
										<td class="tdTerminalValeur" style="">:&nbsp;#date_renouvell#</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="clear" style=""></div>
					</div>
				</div>
		'>

		<cfreturn divMesInfosAndTerminal>
	</cffunction>
	
	
</cfcomponent>