package univers.inventaire.inventaire.creation.nouvelleResources.commande
{
	import composants.util.TextInputLabeled;
	import composants.mail.MailBoxIHM;
	import composants.mail.gabarits.InfosObject;
	import composants.util.CvDateChooser;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.Canvas;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.Image;
	import mx.controls.TextArea;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	
	[Bindable]
	public class EditerInfosCommandeImpl extends Canvas
	{
		//---Composants graphiques--------------------------------------------------
		public var txtLibelle : TextInputLabeled;
		
		public var txtRefClient : TextInputLabeled;
		
		public var txtRefOperateur : TextInputLabeled;
		
		public var txtCommentaires : TextArea;
					
		public var txtOperateur : TextInputLabeled;
		
		public var txtLivraisonPrevue : TextInputLabeled;
		
		public var txtCible : TextInputLabeled;
		
		public var imgChangeCibleOrga : Image;
		
		public var imgChangeContact : Image;
		
		public var imgMail : Image;
		
		public var btUpdate : Button;
		
		public var popUp : TitleWindow;	
		
		public var dcDateLivraisonPrevue : CvDateChooser;
		
		private var _cpMail : MailBoxIHM		
		//----------------------------------------------------------------
		
		private var _infosMail : InfosObject;
			
		private var _modeEcriture : Boolean = true;
		public function get modeEcriture():Boolean{
			return _modeEcriture;
		}
		public function set modeEcriture(mode : Boolean):void
		{
			if(mode != _modeEcriture)
			{
				_modeEcriture = mode;
				invalidateProperties();
				invalidateSize();
				invalidateDisplayList();
			}
		}
		
		
		private var _commande : Commande;
		public function set commande(value : Commande):void{
			_commande = value;
		}
		public function get commande():Commande{
			return _commande
		}
		
		private var _panier : ElementCommande;
		public function set panier(value : ElementCommande):void{
			_panier = value
		}
		public function get panier():ElementCommande{
			return _panier
		}
		
		public function EditerInfosCommandeImpl()
		{
			super();
			
			
		}
		
		override protected function commitProperties():void
		{
			if(modeEcriture)
			{
				currentState = "";
			}
			else
			{
				currentState = "disabled";
			}
		}
		//---HANDLERS --------------------------------------------------
		protected function contactCreationCompleteHandler(event : Event):void{
			
		}
		protected function distributeurCreationCompleteHandler(event : Event):void{
			
		}
		protected function agenceCreationCompleteHandler(event : Event):void{
			
		}
		
		protected function imgChangeCibleOrgaClickHandler(me : MouseEvent):void{
			
		}
		
		protected function imgChangeContactClickHandler(me : MouseEvent):void{
		
		}
		
		protected function imgMailClickHandler(event : MouseEvent):void{
			
			
		}
		
		protected function cpMailBoxMailEnvoyeHandler(event : Event):void{
					}
		
		protected function btUpdateClickHandler(me : MouseEvent):void{
			if(txtLibelle.text.length > 0){
				
				commande.libelle = txtLibelle.text;
				commande.commentaire = txtCommentaires.text;
				commande.refClient = txtRefClient.text;
				commande.refOperateur = txtRefOperateur.text;
				commande.updateCommande();	
				
			}else{
				Alert.okLabel = "Fermer";
				Alert.buttonWidth = 100;
				Alert.show("Le libellé de la commande est obligatoire");
			}
		}
		
		protected function cpContactsChangeHandler(ev : Event):void
		{
		}
		
		protected function cpOrganisationChangeHandler(ev : Event):void
		{
		}
	}
}