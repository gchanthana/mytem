<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction"
			displayname="ActAS005" hint="supprimer la pièce jointe d'une actualité" output="false">
	
	<!--- 
		Supprimer la pièce jointe d'une actualité
	 --->
	<cffunction name="executeAct" access="remote" returntype="Struct"
				hint="supprimer la pièce jointe d'une actualité" output="false">
		
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.deleteFileActualite">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idarticle" type="in" value="#data.IDACTUALITE#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR2" variable="p_uuid" type="in" value="#data.UUID#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out">
			</cfstoredproc>
						
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			
			<cfset createObject("component","fr.consotel.consoview.M28.upload.UploaderFile").deleteFTP(data.UUID)>
			
			<cfreturn CreateReponse("AS005", 1, qResult)>
		<cfcatch type="Any">
			<cfreturn CreateReponse("AS005", 0, ThrowCfCatchError("AS005",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>