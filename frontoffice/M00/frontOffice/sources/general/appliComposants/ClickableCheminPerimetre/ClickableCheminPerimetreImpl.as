package general.appliComposants.ClickableCheminPerimetre
{
	import mx.containers.HBox;
	import mx.controls.Label;
	
	import general.appliComposants.CvPerimetreNodeVO;
	import general.appliComposants.NodeRenderer.NodeRendererCVIHM;

	public class ClickableCheminPerimetreImpl extends HBox
	{
		protected const LARGEURMAX:int = 400;
		private var _etc_added:Boolean =false;
		
		[Bindable]public var lblChemin:String = "";		
		
		public function ClickableCheminPerimetreImpl()
		{
			super();
		}
/*	------------------------------------------------------------------------------------------
									FONCTION
------------------------------------------------------------------------------------------  */		
		public function updateAffichage(listeNode:Array):void
		{
			this.removeAllChildren()
			lblChemin = ""
			if (listeNode != null && listeNode.length > 0)
			{
				var larg:int = 0
					
				for each(var node:CvPerimetreNodeVO in listeNode)
				{
					var nd:NodeRendererCVIHM = new NodeRendererCVIHM()
					nd.TITRE = node.LBL
					nd.NID = node.NID
					nd.ENABLED = node.HASRIGHT
					this.addChildAt(nd,0);
					lblChemin = node.LBL+"/"+lblChemin
					invalidateDisplayList()
				}
				var nd1:NodeRendererCVIHM = this.getChildAt(this.getChildren().length-1) as NodeRendererCVIHM
				nd1.ISLAST = true
			}
			else
				lblChemin = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth,unscaledHeight)
			if(unscaledWidth > LARGEURMAX)
			{
				this.removeChildAt(0);
				if(!_etc_added)
				{
					var etc:Label = new Label();
					etc.text = "... /";
					this.addChildAt(etc,0);
					_etc_added = true;
				}
				invalidateDisplayList()
			}
		}
/*	------------------------------------------------------------------------------------------
									GETTER SETTER 
------------------------------------------------------------------------------------------  */	
	}
}