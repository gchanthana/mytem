package gestionparc.ihm.action.associer.actionAssocierLigneSimToEqp
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.equipement.FicheAssocierLigneSimToEqp;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierLigneSimToEqpReceiver
	{
		private var fiche:FicheAssocierLigneSimToEqp;
		
		public function ActionAssocierLigneSimToEqpReceiver()
		{
		}
		public function action():void
		{
			displayIHM();
		}
		private function displayIHM():void
		{
			fiche = new FicheAssocierLigneSimToEqp();
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}