package univers.inventaire.equipements.lignes.mainComponent
{
	import flash.events.Event;
	import mx.events.FlexEvent;
	import flash.events.MouseEvent;
	
	public class WinConfirmDelete extends WinAssignLines
	{
		public function WinConfirmDelete()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		override public function initIHM(event:Event):void
		{
			super.initIHM(event);
			txtMessage.text = "Voulez vous supprimer toute la descendance de ce noeud ?";
		}
		
		override public function acceptRequest(event:MouseEvent):void
		{
			_ref.acceptDeleteNodesChilds();
			closeWin();
		}
				
	}
}