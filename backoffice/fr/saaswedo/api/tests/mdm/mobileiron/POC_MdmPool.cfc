<cfcomponent author="Cedric" hint="POC de chargement du parc MDM (MobileIron)">
	<cffunction access="public" name="import" returntype="void" hint="Importe tous les devices enrollés du parc MDM">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Infos serveur MDM">
		<cfset var IsoToDate=CreateObject("component","fr.saaswedo.api.tests.mdm.IsoDateUtil")>
		<cfset var iOS_Device=FALSE>
		<cfset var android_Device=FALSE>
		<!--- v1 : Import synchrone séquentiel des devices par pages de 50 devices --->
		<cfset var infos=ARGUMENTS.mdmInfos>
		<cfset var MAX_DEVICE_PER_PAGE=50>
		<cfset var deviceList=[]>
		<!--- Identifiant du payload --->
		<cfset var uuid=createUUID()>
		<!--- Numéro de page et nombre de devices importés pour la page en cours --->
		<cfset var pageNum=1>
		<cfset var importedCount=0>
		<!--- Liste de payload (i.e Liste de liste de devices) : Chaque payload correspond à une page (i.e Liste de devices) --->
		<cfset var payloadList=[]>
		<cfset var initTick=getTickCount()>
		<cfset var wsTickCount=0><!--- Tick: Instanciation du Web Service --->
		<!--- Récupération de la liste des devices --->
		<cfset var startTick=initTick>
		<cfset var deviceIdList=getActiveDevices(infos)>
		<cfset var allDeviceTickCount=getTickCount() - startTick><!--- Tick: Récupération de tous les devices (+ filtrage) --->
		<cfset var loadDeviceListDuration=allDeviceTickCount / 1000>
		<cflog type="information" text="[MobileIron] MDM devices list loaded (#loadDeviceListDuration# secs)">
		<cfset var deviceCount=arrayLen(deviceIdList)>
		<cfset var payloadStartTick=getTickCount()>
		<cfloop index="i" from="1" to="#deviceCount#">
			<cfset startTick=getTickCount()>
			<cfset var udidKey="iPhone UDID">
			<cfset var iccidKey="">
			<cfset var imeiKey="_">
			<cfset var wifiMacKey="_">
			<cfset var deviceId=deviceIdList[i]>
			<cfset var managedStatus=deviceId.status>
			<!--- Filtrage du statut d'enrollment --->
			<cfif (compareNoCase(managedStatus,"ACTIVE") EQ 0) OR (compareNoCase(managedStatus,"MANAGED") EQ 0)>
				<cfset var isSupportedOS=FALSE>
				<cfset var device={
					marque=deviceId.manufacturer,modele=deviceId.model,platform=deviceId.platform,imei="",num_serie="",collaborateur="",matricule="",
					username_mdm=deviceId.principal,id_mdm=deviceId.uuid,udid="",num_sim="",num_tel=" " & deviceId.currentPhoneNumber,id_activesync="",
					operateur=deviceId.operator,mcc=" 0",mnc=" 0",imsi="",rownum=1
				}>
				<!--- Champs MYT-652 --->
				<cfset structAppend(device,{
					principal="N.A",country="N.A",manufacturer=deviceId.manufacturer,email_address="N.A",status_code="N.A",employee_owned="N.A",
					compliance="N.A",client_version="N.A",mdm_enabled="N.A",last_connected_at="N.A",activeSync_UUID="N.A",
					activeSync_lastSync_attempt="N.A",wifimac="N.A",device_encryption="N.A",last_MDM_CheckIn="N.A",registered_on="N.A"
				},TRUE)>
				<!--- Propriétés dont les clés dépendent de l'OS --->
				<cfif findNoCase("iOS",device.platform) GT 0>
					<cfset iOS_Device=TRUE>
					<cfset android_Device=FALSE>
					<cfset iccidKey="iPhone ICCID">
					<cfset imeiKey="iPhone IMEI">
					<cfset wifiMacKey="WiFiMAC">
					<cfset isSupportedOS=TRUE>
				<cfelseif findNoCase("Android",device.platform) GT 0>
					<cfset android_Device=TRUE>
					<cfset iOS_Device=FALSE>
					<cfset iccidKey="Android ICCID">
					<cfset imeiKey="imei">
					<cfset wifiMacKey="wifi_mac_addr">
					<cfset isSupportedOS=TRUE>
				<cfelse>
					<cfset android_Device=FALSE>
					<cfset iOS_Device=FALSE>
					<cfset isSupportedOS=FALSE>
				</cfif>
				<cfif isSupportedOS>
					<!--- Infos de chaque device --->
					<cfset var deviceProps=[]>
					<cfset var deviceDetails=deviceId.details>
					<cfif (arrayLen(deviceDetails) GT 0) AND isStruct(deviceDetails[1]) AND structKeyExists(deviceDetails[1],"entry")>
						<cfset deviceProps=deviceDetails[1]["entry"]>
						<cfset var propCount=arrayLen(deviceProps)>
						<!--- Parcours de la liste des propriétés de chaque device pour en garder certaines (e.g PhoneNumber,ICCID) --->
						<cfloop index="j" from="1" to="#propCount#">
							<cfset var deviceProperty=deviceProps[j]>
							<cfif compareNoCase(deviceProperty.key,"SERIAL") EQ 0>
								<cfset device.num_serie=" " & deviceProperty.value>
							<cfelseif compareNoCase(deviceProperty.key,imeiKey) EQ 0>
								<cfset device.imei=" " & deviceProperty.value>
							<cfelseif compareNoCase(deviceProperty.key,udidKey) EQ 0>
								<cfset device.udid=deviceProperty.value>
							<cfelseif compareNoCase(deviceProperty.key,iccidKey) EQ 0>
								<cfset device.num_sim=deviceProperty.value>
							<cfelseif compareNoCase(deviceProperty.key,"SIM MCC") EQ 0>
								<cfset device.mcc=" " & deviceProperty.value>
							<cfelseif compareNoCase(deviceProperty.key,"SIM MNC") EQ 0>
								<cfset device.mnc=" " & deviceProperty.value>
							<cfelseif compareNoCase(deviceProperty.key,"imsi") EQ 0>
								<cfset device.imsi=" " & deviceProperty.value>
							<!--- Champs MYT-652 --->
							<cfelseif compareNoCase(deviceProperty.key,"Client_version") EQ 0>
								<cfset device.client_version=" " & deviceProperty.value>
							<cfelseif compareNoCase(deviceProperty.key,"mdm_enabled") EQ 0>
								<cfset device.mdm_enabled=" " & deviceProperty.value>
							<cfelseif compareNoCase(deviceProperty.key,wifiMacKey) EQ 0>
								<cfset device.wifimac=" " & deviceProperty.value>
							<cfelseif compareNoCase(deviceProperty.key,"prv_ActiveSyncDeviceID")>
								<cfset device.activeSync_UUID=" " & deviceProperty.value>
							<cfelseif compareNoCase(deviceProperty.key,"prv_device_encryption")>
								<cfset device.device_encryption=" " & deviceProperty.value>							
							</cfif>
						</cfloop>
					</cfif>
					<!--- Champs MYT-652 --->					
					<cfif structKeyExists(deviceId,"countryName")>
						<cfset device.country=" " & deviceId.countryName>
					</cfif>									
					<cfif structKeyExists(deviceId,"principal")>
						<cfset device.principal=" " & deviceId.principal>
					</cfif>
					<cfif structKeyExists(deviceId,"emailAddress")>
						<cfset device.email_address=" " & deviceId.emailAddress>
					</cfif>
					<cfif structKeyExists(deviceId,"statusCode")>
						<cfset device.status_code=VAL(deviceId.statusCode)>
					</cfif>
					<cfif structKeyExists(deviceId,"employeeOwned")>
						<cfset device.employee_owned=deviceId.employeeOwned>
					</cfif>
					<cfif structKeyExists(deviceId,"compliance")>
						<cfset device.compliance=deviceId.compliance>
					</cfif>
					<cfif structKeyExists(deviceId,"mdmManaged") AND (iOS_Device EQ TRUE)>
						<cfset device.mdm_enabled=deviceId.mdmManaged>
					</cfif>
					<cfif structKeyExists(deviceId,"lastConnectedAt")>
						<cfset device.last_connected_at=IsoToDate.ISOToDateTime(deviceId.lastConnectedAt)>
					</cfif>
					<cfif structKeyExists(deviceId,"registeredAt")>
						<cfset device.registered_on=IsoToDate.ISOToDateTime(deviceId.registeredAt)>
					</cfif>
					<!--- MAJ de la valeur du SERIAL pour Android --->
					<cfif android_Device EQ TRUE>
						<cfif LEN(TRIM(device.num_serie)) EQ 0>
							<cfset device.num_serie=device.imei>
						</cfif>
					</cfif>
					<!--- Ajout du device dans la liste : Si la liste n'est pas remplie --->
					<cfif importedCount LT MAX_DEVICE_PER_PAGE>
						<cfset importedCount=importedCount+1>
						<cfset device.rownum=importedCount>
						<cfset var perDeviceTickCount=getTickCount() - startTick><!--- Tick: Pour chaque device --->
						<cfset device.perDeviceTickCount=perDeviceTickCount>
						<cfset deviceList[importedCount]=device>
					</cfif>
				</cfif>
			</cfif>
			<!--- Ajout du payload de chaque page remplie ou quand il n'y a plus de device à ajouter --->
			<cfif arrayLen(deviceList) GT 0>
				<cfif (importedCount EQ MAX_DEVICE_PER_PAGE) OR (i EQ deviceCount)>
					<cfset var importDate=NOW()>
					<cfset var payload={
						mdm_vendor=infos.MDM_TYPE,version_flux="1.0",mdm_url=getMdmEndpointURL(infos),
						date_extraction=lsDateFormat(importDate,"dd/mm/yyyy") & " " & lsTimeFormat(importDate,"HH:mm:ss"),
						UUID_chargement=uuid,page_number=pageNum,nb_devices=importedCount,IDRACINE=infos.IDGROUPE,devices=deviceList
					}>
					<cfset var perPayloadTickCount=getTickCount() - payloadStartTick><!--- Tick: Pour chaque payload --->
					<cfset payload.perPayloadTickCount=perPayloadTickCount>
					<cfset payload.wsTickCount=wsTickCount>
					<cfset payload.allDeviceTickCount=allDeviceTickCount>						
					<cfset importPayload(payload)>
					<!--- Création d'une nouvelle liste s'il y a des device restants --->
					<cfif importedCount LT deviceCount>
						<cfset pageNum=pageNum+1>
						<cfset importedCount=0>
						<cfset deviceList=[]>
						<cfset payloadStartTick=getTickCount()>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>
		<cfset var totalTick=(getTickCount() - initTick) / 1000>
		<cflog type="information" text="MDM Pool imported (#totalTick# secs)">
	</cffunction>
	
	<cffunction access="private" name="importPayload" returntype="void" hint="Import du payload dans la base">
		<cfargument name="payload" type="Struct" required="true" hint="Payload : Une page de liste de devices">
		<cfset var procedureName="PKG_MDM.import_parc_mdm">
		<cftry>
			<cfset var startTick=getTickCount()>
			<cfset var jsonPayload=serializeJSON(ARGUMENTS.payload)>
			<cfset var jsonTick=getTickCount() - startTick><!--- Tick : Sérialisation JSON --->
			<cflog type="information" text="[POC_MdmPool] MDM Pool payload JSON-Serialized (#jsonTick# ms)">
			<cfset startTick=getTickCount()>
			<cfstoredproc datasource="ROCOFFRE" procedure="#procedureName#">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#jsonPayload#">
				<cfprocresult name="qImportMdmPool">
			</cfstoredproc>
			<cfset var dbTick=(getTickCount() - startTick) / 1000><!--- Tick : Insertion dans la base --->
			<cflog type="information" text="[POC_MdmPool] MDM Pool payload imported (#dbTick# secs)">
			<cfif qImportMdmPool.recordcount GT 0>
				<cfif VAL(qImportMdmPool["STATUS"][1]) GT 0>
					<cfset var errorMsg=qImportMdmPool["MESSAGE"][1]>
					<cfthrow type="Custom" message="#procedureName# error: #errorMsg#">
				</cfif>
			</cfif>
			<cfcatch type="Any">
				<cflog type="Error" text="[POC_MdmPool] Payload import error: #CFCATCH.message# (#CFCATCH.detail#)">
				<cfmail from="monitoring@saaswedo.com" to="cedric.rapiera@saaswedo.com"
				priority="highest" type="html" subject="[POC MDM] Erreur">
					<cfoutput>
						<b>Exception :</b><br>#CFCATCH.message# (#CFCATCH.detail#)<hr>
					</cfoutput>
					<cfif isDefined("jsonPayload")>
						<cfoutput>
							<hr><b>JSON :</b><br>#jsonPayload#<hr>
						</cfoutput>
					</cfif>
					<cfif isDefined("qImportMdmPool")>
						<cfdump var="#qImportMdmPool#" label="#procedureName#"><hr>
					</cfif>
					<cfdump var="#CFCATCH.tagContext#" label="Tag context">
				</cfmail>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="getActiveDevices" returntype="Array">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Infos serveur MDM">
		<cfset var devices=[]>
		<cfset var infos=ARGUMENTS.mdmInfos>
		<cfset var rootURL=getMdmEndpointURL(infos)>
		<cfset var devicesEndpoint=rootURL & "/api/v1/dm/devices">
		<cfhttp method="get" url="#devicesEndpoint#" username="#infos.USERNAME#" password="#infos.PASSWORD#">
			<cfhttpparam type="header" name="accept" value="application/json">
			<cfhttpparam type="formfield" name="status" value="ACTIVE">
		</cfhttp>
		<cfif structKeyExists(CFHTTP,"Responseheader") AND structKeyExists(CFHTTP.Responseheader,"Status_Code")>
			<cfif structKeyExists(CFHTTP,"fileContent")>
				<cfset var stringResponseContent=getStringContent(CFHTTP.fileContent)>
				<cfif isJSON(stringResponseContent)>
					<cfset var jsonResponse=deserializeJSON(stringResponseContent)>
				</cfif>
				<cfif VAL(CFHTTP.ResponseHeader.Status_Code) EQ 200>
					<cfif structKeyExists(jsonResponse,"devices") AND structKeyExists(jsonResponse.devices,"device")>
						<cfset devices=jsonResponse.devices.device>
						<cfif not IsArray(devices)>
							<cfset device = devices>
							<cfset devices = ArrayNew(1)>
							<cfset ArrayAppend(devices,device)>
						</cfif>
					</cfif>
				<cfelse>				
					<cfthrow type="Custom" errorCode="ERROR" message="Error : #stringResponseContent#">
				</cfif>
			</cfif>
		<cfelse>				
			<cfthrow type="Custom" errorCode="ERROR" message="Error : #CFHTTP.ErrorDetail#">
		</cfif>
		<cfreturn devices>
	</cffunction>
	
	<!--- Traitements réponses HTTP --->
	<cffunction access="private" name="getStringContent" returntype="String" hint="Retourne la désérialisation String du contenu httpContent.
	Ecrit un log de warning si httpContent est de type complexe différent de java.io.ByteArrayOutputStream">
		<cfargument name="httpContent" type="Any" required="true" hint="Contenu de la réponse HTTP (CFHTTP.FileContent)">
		<cfset var content=ARGUMENTS.httpContent>
		<cfif isSimpleValue(content) AND isValid("String",content)>
			<cfreturn content>
		<cfelse>
			<!--- Conversion en String. Ne pas comparer avec getClass().getName() car héritage possible --->
			<cfif isInstanceOf(content,"java.io.ByteArrayOutputStream")>
				<cfreturn content.toString()>
			<cfelse>
				<cfset var code="MDM.ERROR">
				<cfthrow type="Custom" errorCode="#code#" message="Error converting HTTP response to String">
			</cfif>
		</cfif>
	</cffunction>
	
	<!--- Accès au Web Service du MDM --->
	
	<cffunction access="private" name="getRemoteService" returntype="Any">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Infos serveur MDM">
		<cfargument name="serviceName" type="String" required="true" hint="Identifiant de la racine">
		<cfset var infos=ARGUMENTS.mdmInfos>
		<cfset var stubEndpoint=getMdmEndpointURL(infos) & "/services/" & ARGUMENTS.serviceName & "?wsdl">
		<cfset var stub=createObject("webservice",stubEndpoint)>
		<cfif LEN(TRIM(mdmInfos.USERNAME)) GT 0>
			<cfset stub.setUsername(mdmInfos.USERNAME)>
			<cfset stub.setPassword(mdmInfos.PASSWORD)>
		</cfif>
		<cfreturn stub>
	</cffunction>
	
	<cffunction access="private" name="getMdmEndpointURL" returntype="String" hint="Retourne l'URL d'accès à l'API du serveur MDM">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Infos serveur MDM">
		<cfset var infos=ARGUMENTS.mdmInfos>
		<cfreturn (infos.USE_SSL ? "https":"http") & "://" & infos.MDM>
	</cffunction>
</cfcomponent>