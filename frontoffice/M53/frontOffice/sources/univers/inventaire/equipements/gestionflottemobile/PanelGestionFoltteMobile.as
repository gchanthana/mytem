package univers.inventaire.equipements.gestionflottemobile
{
	import composants.util.ConsoviewFormatter;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.system.System;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.containers.ViewStack;
	import mx.controls.Alert;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.DataGridEvent;
	import mx.events.FlexEvent;
	import mx.events.ItemClickEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.commande.ihm.InventaireCommandesImpl;
	import univers.inventaire.equipements.gestionflottemobile.fiches.ihm.FicheEvent;
	import univers.inventaire.equipements.gestionflottemobile.importmasse.ImportMasse;
	import univers.inventaire.equipements.gestionflottemobile.menugestionflottemobile.MenuFlotteMobDefault;
	import univers.inventaire.equipements.gestionflottemobile.menugestionflottemobile.MenuFlotteMobNDI;
	import univers.inventaire.equipements.gestionflottemobile.menugestionflottemobile.MenuFlotteMobSIM;
	import univers.inventaire.equipements.gestionflottemobile.menugestionflottemobile.MenuFlotteMobTerminal;
	import univers.inventaire.equipements.gestionflottemobile.menugestionflottemobile.MenuFlotteMobUtilisateur;
	import univers.inventaire.equipements.gestionflottemobile.menugestionflottemobile.MenuGestionFlotteMobileIHM;
	
	public class PanelGestionFoltteMobile extends PanelGestionFoltteMobileIHM 
	{ 

		
		[Bindable]
		protected var rechercheData: ArrayCollection;
		
	 	public static var texteItem : String;// Libell�?s© de l'item selectionn�?©
		//public static var texteItemSelection : String;// Libell�?© de l'item selectionn�?©
		public static var objLigneSelected : DgRechercheItemVo;// Ligne selectionnée
		public static var idPoolGestionnaire : int; // Id du pool sélectionné dans interface
		public static var globalColor : int;//Couleur globale des panels
		public static const VALIDER_EVENT:String = "POPUP_VALIDER_EVENT"; // Signifie Validation d'un popUp
		public static const ANNULER_EVENT:String = "POPUP_ANNULER_EVENT"; // Signifie Annulation d'un popUp
		public static const SHOW_TERM_SIM_EVENT:String = "POPUP_SHOW_TERM_SIM_EVENT";			
		//Donnée Source de la comboBox de trie :		
		private var donneeComboBoxTrie : Array = [ 	{label:"Aucun", data:1},
													{label:"Collaborateurs sans téléphone ni ligne", data:2},
													{label:"Collaborateurs avec SIM sans téléphone", data:3}, 
													{label:"Collaborateurs avec plus d'un téléphone", data:4}, 
													{label:"Cartes SIM en spare (SIM sans ligne)", data:5},  
													{label:"Lignes/SIM en stock", data:6}, 
													{label:"Téléphones en stock", data:7}, 
													{label:"Equipements au rebut", data:8}, 
													{label:"Collaborateurs hors de l'entreprise", data:9},
													{label:"Equipements non restitués", data:10},
												];
						
		//Menu en fonction de l'item selectionn�?©
		private var objMenuFlotteMobDefault : MenuFlotteMobDefault;
		private var objMenuFlotteMobSIM :MenuFlotteMobSIM;
		private var objMenuFlotteMobTerminal : MenuFlotteMobTerminal;
		private var objMenuFlotteMobNDI:MenuFlotteMobNDI;
		private var objMenuFlotteMobUtilisateur:MenuFlotteMobUtilisateur;  
		//Menu en SURVOL
		private var objMenuFlotteMobSIM_tooltip :MenuFlotteMobSIM;
		private var objMenuFlotteMobTerminal_tooltip : MenuFlotteMobTerminal;
		private var objMenuFlotteMobNDI_tooltip:MenuFlotteMobNDI;
		private var objMenuFlotteMobUtilisateur_tooltip:MenuFlotteMobUtilisateur; 
		
		private var bxImgMenue_tooltip : ViewStack = new ViewStack(); 
		   
		private var colPoolGestionnaire : ArrayCollection = new ArrayCollection();

		/*private var idCollab : int;
		private var idSIM : int;
		private var idLigne : int;
		private var idTerminal : int;*/
		
		
		//Le panel de gestion de l'annuaire
		
		public function PanelGestionFoltteMobile()
		{
			//TODO: implement function
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		

		
		private function initIHM(fe : FlexEvent):void{
			
			bxImgMenue_tooltip.resizeToContent = true;
			
			//ecouteur mise à jour d'une fiche
			addEventListener(FicheEvent.FICHE_UPDATED,ficheUpdatedHandler,true);
			
			//Alert.show("--Sur ce groupe Vous êtes en mode : "+CvAccessManager.getSession().CURRENT_ACCESS+" et WRITE_MODE = "+ConsoViewSessionObject.WRITE_MODE);
			if(CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE)
			{
				//Cr�?©ation des menus
				objMenuFlotteMobSIM = new MenuFlotteMobSIM(this);
				objMenuFlotteMobTerminal = new MenuFlotteMobTerminal(this);
				objMenuFlotteMobNDI= new MenuFlotteMobNDI(this);
				objMenuFlotteMobUtilisateur= new MenuFlotteMobUtilisateur(this);
				
				//ecouteur menu
				objMenuFlotteMobSIM.addEventListener(MenuFlotteMobDefault.BT_HISTORIQUE_CLIKED,getHistorique);
				objMenuFlotteMobTerminal.addEventListener(MenuFlotteMobDefault.BT_HISTORIQUE_CLIKED,getHistorique);
				objMenuFlotteMobNDI.addEventListener(MenuFlotteMobDefault.BT_HISTORIQUE_CLIKED,getHistorique);
				objMenuFlotteMobUtilisateur.addEventListener(MenuFlotteMobDefault.BT_HISTORIQUE_CLIKED,getHistorique); 
				
				//Ajout des menus
				bxImgMenue.addChild(objMenuFlotteMobSIM);
				bxImgMenue.addChild(objMenuFlotteMobTerminal);
				bxImgMenue.addChild(objMenuFlotteMobNDI);
				bxImgMenue.addChild(objMenuFlotteMobUtilisateur);
				
				//Création des menus SURVOL/TOOLTIP
				objMenuFlotteMobSIM_tooltip = new MenuFlotteMobSIM(this,true);
				objMenuFlotteMobTerminal_tooltip = new MenuFlotteMobTerminal(this,true);
				objMenuFlotteMobNDI_tooltip= new MenuFlotteMobNDI(this,true);
				objMenuFlotteMobUtilisateur_tooltip= new MenuFlotteMobUtilisateur(this,true);
				
				//ecouteur menu SURVOL/TOOLTIP
				objMenuFlotteMobSIM_tooltip.addEventListener(MenuFlotteMobDefault.BT_HISTORIQUE_CLIKED,getHistorique);
				objMenuFlotteMobTerminal_tooltip.addEventListener(MenuFlotteMobDefault.BT_HISTORIQUE_CLIKED,getHistorique);
				objMenuFlotteMobNDI_tooltip.addEventListener(MenuFlotteMobDefault.BT_HISTORIQUE_CLIKED,getHistorique);
				objMenuFlotteMobUtilisateur_tooltip.addEventListener(MenuFlotteMobDefault.BT_HISTORIQUE_CLIKED,getHistorique); 
				
				
				//Ajout des menus SURVOL/TOOLTIP
				bxImgMenue_tooltip.addChild(objMenuFlotteMobSIM_tooltip);
				bxImgMenue_tooltip.addChild(objMenuFlotteMobTerminal_tooltip);
				bxImgMenue_tooltip.addChild(objMenuFlotteMobNDI_tooltip);
				bxImgMenue_tooltip.addChild(objMenuFlotteMobUtilisateur_tooltip);
				//bxImgMenue_tooltip.percentWidth=100;
				
				//Ecouteur sur le DG
				dgRecherche.dgListe.addEventListener(ListEvent.ITEM_CLICK,dgRechercheChangeHandler);
				
				//Export CSV
				imgExport.addEventListener(MouseEvent.CLICK,exportCSV);			
			
				
				//Import en masse de terminaux
				btAddTerminal.addEventListener(MouseEvent.CLICK,importMasseTerm);
				//Import en masse des SIM
				btAddSim.addEventListener(MouseEvent.CLICK,importmasseSIM);

			}
			
			
			btGererCommande.addEventListener(MouseEvent.CLICK,btGererCommandeClickHandler);
			objMenuFlotteMobDefault = new MenuFlotteMobDefault();
			bxImgMenue.addChild(objMenuFlotteMobDefault);
			//Selection du menu vide
			bxImgMenue.selectedChild = objMenuFlotteMobDefault;
		
			//Ecouteur sur comBoBox de trie :
			comboTri.dataProvider=donneeComboBoxTrie;
			comboTri.addEventListener(Event.CHANGE,comboBoxHandler);
			globalColor = uint(panelRecherche.getStyle("headerColors")[0]);
			//Ecouteur sur la loupe
			imgRechercher.addEventListener(MouseEvent.CLICK,imgRechercherClickHandler);
			//Ecouteur su la croix rouge
			imgViderTab.addEventListener(MouseEvent.CLICK,viderTabClickHandler);
			//Ecouteur sur le Enter 
			txtRecherche.addEventListener(FlexEvent.ENTER,txtRechercheEnterHandler);
			//Ecouteur pour le filtre
			//txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			//Presse papier
			dgRecherche.dgListe.addEventListener(DataGridEvent.ITEM_EDIT_BEGINNING,dgResultatsEditBeginningHandler);
			//ComboGestionnaire
			initComboGestionnaire();
			//bouton dans le canvas :
			bt_fermer_canvas.addEventListener(MouseEvent.CLICK,cache_popUp);
			//écouteur d'évenement sur la combo box id pool gestionnaire
			comboPoolGestionnaire.addEventListener(Event.CHANGE,comboGestionaireHandler);	
			vue.selectedVue = 'Accueil';
			btBarVue.addEventListener(ItemClickEvent.ITEM_CLICK,btBarVueclickHandler);	
			
			cpInventaireCommandes.addEventListener(InventaireCommandesImpl.BT_ADD_TERMINAL_CLICKED,cpIventaireCommandeEventsHandler);
			cpInventaireCommandes.addEventListener(InventaireCommandesImpl.BT_FERMER_CLICKED,cpIventaireCommandeEventsHandler);		
		}
		
		private function cpIventaireCommandeEventsHandler(event:Event):void
		{
			switch(event.type)
			{
				case InventaireCommandesImpl.BT_ADD_TERMINAL_CLICKED:
				{
					importMasseTerm(null);
					break;
				}
				case InventaireCommandesImpl.BT_FERMER_CLICKED:
				{
					viewStack.selectedIndex = 0;
					break;
				}
			}
		}
		
		private function importMasseTerm(evt : MouseEvent):void
		{
			var panel_importMasse : ImportMasse = new ImportMasse("TERMINAL");
			PopUpManager.addPopUp(panel_importMasse,this,true);
			PopUpManager.centerPopUp(panel_importMasse);
		}
		private function importmasseSIM(evt : MouseEvent):void
		{
			var panel_importMasse : ImportMasse = new ImportMasse("SIM");
			PopUpManager.addPopUp(panel_importMasse,this,true);
			PopUpManager.centerPopUp(panel_importMasse);
		}

		
	 	private var savedIndex:int = 99999;
        private function btBarVueclickHandler(event:ItemClickEvent):void {
            if (event.index == savedIndex) {
               
            }
            else {
                savedIndex = event.index;
                vue.selectedVue = event.item.name;			
				if(comboTri.selectedIndex != -1)
				{
					rechercheData= null;
					dgRecherche.dataProvider = null;
					imgRechercherClickHandler(null);
				}
            }    
        }
        
        private function ficheUpdatedHandler(event:FicheEvent):void
        {
        	imgRechercherClickHandler(null);
        }
		
		private function comboGestionaireHandler(evt : Event):void
		{
			if(/* comboPoolGestionnaire.selectedIndex == 0 || */ comboPoolGestionnaire.selectedIndex == -1 )
			{
				viderTabClickHandler(null);
			}
			else
			{
				/* if(comboPoolGestionnaire.selectedIndex == 1){ // Provisoire = tous
					idPoolGestionnaire = -2;
					comboBoxHandler(null);
				} 
				else
				{
				idPoolGestionnaire = comboPoolGestionnaire.selectedItem.IDPOOL;
				comboBoxHandler(null);
				}*/
				
				
				comboBoxHandler(null);
			}
		}
		
		private function cache_popUp(evt : MouseEvent):void
		{
			movePauseMove;
			contract.play();
		} 
		        
       private function initComboGestionnaire():void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"fournirPoolsDuGestionnaire",
																		process_init_combo_pool_gestionnaire);
			RemoteObjectUtil.callService(op);
		}
		private function process_init_combo_pool_gestionnaire (evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				
				var values:ArrayCollection = evt.result as ArrayCollection;							
				var cursor : IViewCursor = values.createCursor();
				
				colPoolGestionnaire = new ArrayCollection();
				
				while(!cursor.afterLast)
				{
					var obj:Object = new Object();
					obj.IDPOOL = cursor.current.IDPOOL;
					obj.LIBELLE_POOL = cursor.current.LIBELLE_POOL;
					obj.CODE_INTERNE_POOL = cursor.current.CODE_INTERNE_POOL;
					obj.COMMENTAIRE_POOL = cursor.current.COMMENTAIRE_POOL;
					obj.IDPROFIL = cursor.current.IDPROFIL;
					obj.LIBELLE_PROFIL = cursor.current.LIBELLE_PROFIL;
					obj.ACCES_COLLABORATEUR = cursor.current.ACCES_COLLABORATEUR;
					obj.GEST_CATALOGUE_FOURNIS = cursor.current.GEST_CATALOGUE_FOURNIS;
					obj.GEST_CATALOGUE_CLIENT = cursor.current.GEST_CATALOGUE_CLIENT;
					
					colPoolGestionnaire.addItem(obj);
					
					cursor.moveNext();
				}
				
				/* colPoolGestionnaire.addItemAt({LIBELLE_POOL:"Aucun",data:-1},0);
				colPoolGestionnaire.addItemAt({LIBELLE_POOL:"Tous",data:-2},1); */
				
				comboPoolGestionnaire.dataProvider = colPoolGestionnaire;
				comboPoolGestionnaire.validateProperties();
				comboPoolGestionnaire.selectedIndex = 0;
				idPoolGestionnaire=comboPoolGestionnaire.selectedItem.IDPOOL;
			}
			else
			{
				trace("Erreur getPoolGestionnaire");
			}
		}
		private function exportCSV(evt : MouseEvent):void
		{
			if (dgRecherche.dataProvider == null) return;
			
			if((dgRecherche.dataProvider as ArrayCollection).length >0)
			{
				var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
				var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
				var GROUPE_INDEX :int= perimetreObj.PERIMETRE_INDEX; // idPerimetre ( groupe client) 
				var txtSearch : String = txtRecherche.text;
				var methodeName: String;
				var titre_doc: String;
				switch (comboTri.selectedItem.data){
					case 1:
					methodeName = "getEquipements_V3";
					titre_doc = "equipement";
					break;
					case 2:
					methodeName = "Liste_emp_sans_equipements_v3";
					titre_doc = "Liste_emp_sans_equipements";
					break;
					case 3:
					methodeName = "Liste_emp_SIM_sans_term_v3";
					titre_doc = "Liste_emp_SIM_sans_term";
					break;
					case 4:
					methodeName = "Liste_emp_pls_term_v3";
					titre_doc = "Liste_emp_pls_term";
					break;
					case 5:
					methodeName = "Liste_SIM_sans_ligne_v3";
					titre_doc = "Liste_SIM_sans_ligne";
					break;
					case 6:
					methodeName = "Liste_SIM_sans_emp_v3";
					titre_doc = "Liste_SIM_sans_emp";
					break;
					case 7:
					methodeName = "Liste_terminaux_sans_emp_v3";
					titre_doc = "Liste_terminaux_sans_emp";
					break;
					case 8:
					methodeName = "get_equip_rebut";
					titre_doc = "equipement au rebut";
					break;
					case 9:
					methodeName = "get_collab_rebut";
					titre_doc = "collaborateurs hors entreprise";
					break;
					case 10:
					methodeName = "get_collab_rebut_av_equip";
					titre_doc = "collaborateurs hors entreprise avec équipements non restitués";
					break;
				}
				var idracine:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
               // var txtSearch : String = txtRecherche.text;
                var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
                                                                                    	"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
                                                                                      	"exporterEnCSV",
                                                                                      	exporterListeResultHandler);
                RemoteObjectUtil.callService(opData,idracine,txtSearch,methodeName,titre_doc,PanelGestionFoltteMobile.idPoolGestionnaire,rbgAfficher.selectedValue);      

			}
				
		}
		private function exporterListeResultHandler(re : ResultEvent):void{
                  if (String(re.result) != "error"){
                        displayExport(String(re.result));                    
                  }else{
                   Alert.show("Erreur export");                   
          }
  		}
        private function displayExport(name : String):void {             
	            var url:String = ConsoViewModuleObject.NonSecureUrlBackoffice + "/fr/consotel/consoview/inventaire/equipement/flotte/csv/exportCSV.cfm";
	            var request:URLRequest = new URLRequest(url);         
	           	var variables:URLVariables = new URLVariables();
	            variables.FILE_NAME = name;
	            request.data = variables;
	            request.method = URLRequestMethod.POST;
                navigateToURL(request,"_blank");
         }
		private function comboBoxHandler (evt : Event):void{
			
			
			if(comboPoolGestionnaire.selectedIndex<0)
			{
				movePauseMove.stop();
				movePauseMove.play();
			}
			else
			{
			 	idPoolGestionnaire = comboPoolGestionnaire.selectedItem.IDPOOL;
				var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
				var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
				var GROUPE_INDEX :int= perimetreObj.PERIMETRE_INDEX; // idPerimetre ( groupe client) 
				var txtSearch : String = txtRecherche.text;
				
				
				switch (comboTri.selectedItem.data)
				{
					case 1:
					rechercher();  
					break;
					case 2:
					comboFiltreFunction_Liste_emp_sans_equipements_v1(txtSearch,GROUPE_INDEX);
					break;
					case 3:
					comboFiltreFunction_Liste_emp_SIM_sans_term_v1(txtSearch,GROUPE_INDEX);
					break;
					case 4:
					comboFiltreFunction_Liste_emp_pls_term_v1(txtSearch,GROUPE_INDEX);
					break;
					case 5:
					comboFiltreFunction_Liste_SIM_sans_ligne_v2(txtSearch,GROUPE_INDEX);
					break;
					case 6:
					comboFiltreFunction_Liste_SIM_sans_emp_v2(txtSearch,GROUPE_INDEX);
					break;
					case 7:
					comboFiltreFunction_Liste_terminaux_sans_emp_v2(txtSearch,GROUPE_INDEX);
					break;
					case 8:
					comboFiltreFunction_Liste_equip_rebut(txtSearch,GROUPE_INDEX);
					break;
					case 9:
					comboFiltreFunction_Liste_collab_rebut(txtSearch,GROUPE_INDEX);
					break;
					case 10:
					comboFiltreFunction_Liste_collab_rebut_av_equip(txtSearch,GROUPE_INDEX);
					break;
				}
			}
		}
		private function comboFiltreFunction_Liste_emp_sans_equipements_v1( txtSearch : String, GROUPE_INDEX : int):void{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"Liste_emp_sans_equipements_v3",
							rechercherResultHandler);
			
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,txtSearch,idPoolGestionnaire,rbgAfficher.selectedValue,rbgAfficher.selectedValue);
		}
		private function comboFiltreFunction_Liste_emp_pls_term_v1( txtSearch : String, GROUPE_INDEX : int):void{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"Liste_emp_pls_term_v3",
							rechercherResultHandler);
			
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,txtSearch,idPoolGestionnaire,rbgAfficher.selectedValue,vue.selectedVue);
			
		}
		private function comboFiltreFunction_Liste_emp_SIM_sans_term_v1( txtSearch : String, GROUPE_INDEX : int):void{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"Liste_emp_SIM_sans_term_v3",
							rechercherResultHandler);
			 
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,txtSearch,idPoolGestionnaire,rbgAfficher.selectedValue,rbgAfficher.selectedValue);
			
		}
		private function comboFiltreFunction_Liste_SIM_sans_ligne_v2( txtSearch : String, GROUPE_INDEX : int):void{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"Liste_SIM_sans_ligne_v3",
							rechercherResultHandler);
			 
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,txtSearch,idPoolGestionnaire,rbgAfficher.selectedValue);
			
		}  
		private function comboFiltreFunction_Liste_SIM_sans_emp_v2(txtSearch : String, GROUPE_INDEX : int):void{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"Liste_SIM_sans_emp_v3",
							rechercherResultHandler);
			 
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,txtSearch,idPoolGestionnaire,rbgAfficher.selectedValue,rbgAfficher.selectedValue);
			
		}  
		private function comboFiltreFunction_Liste_terminaux_sans_emp_v2( txtSearch : String, GROUPE_INDEX : int):void{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"Liste_terminaux_sans_emp_v3",
							rechercherResultHandler);
			 
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,txtSearch,idPoolGestionnaire,rbgAfficher.selectedValue,rbgAfficher.selectedValue);
			
		}
		private function comboFiltreFunction_Liste_equip_rebut( txtSearch : String, GROUPE_INDEX : int):void{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"get_equip_rebut",
							rechercherResultHandler);
			 
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,txtSearch,idPoolGestionnaire,rbgAfficher.selectedValue,rbgAfficher.selectedValue);
			
		}
		private function comboFiltreFunction_Liste_collab_rebut( txtSearch : String, GROUPE_INDEX : int):void{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"get_collab_rebut",
							rechercherResultHandler);
			 
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,txtSearch,idPoolGestionnaire,rbgAfficher.selectedValue,rbgAfficher.selectedValue);
			
		}
		private function comboFiltreFunction_Liste_collab_rebut_av_equip( txtSearch : String, GROUPE_INDEX : int):void{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"get_collab_rebut_av_equip",
							rechercherResultHandler);
			 
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,txtSearch,idPoolGestionnaire,rbgAfficher.selectedValue,rbgAfficher.selectedValue);
			
		}
		
		
		protected function formateLigne(item : Object, column : DataGridColumn):String{			
			return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
		}
		
		private function setDataProvidr(data : ArrayCollection):void{
			rechercheData= data;
			dgRecherche.dataProvider = rechercheData;
			rechercheData.refresh()
		}
		
		
		/*Effacer l'item selectionn�?© / Afficher le menu par default / Declanche la proc�?©dure*/
		private function imgRechercherClickHandler(me : MouseEvent):void{
			
			clearItem();
			//comboTri.selectedIndex=0;
			bxImgMenue.selectedChild = objMenuFlotteMobDefault;
			//rechercher();
			idPoolGestionnaire = comboPoolGestionnaire.selectedItem.IDPOOL;
			comboBoxHandler(null);
		}
		private function viderTabClickHandler(me : Event):void{
			clearItem();
			bxImgMenue.selectedChild = objMenuFlotteMobDefault;
			rechercheData= null;
			
			dgRecherche.dataProvider = null;
			txtRecherche.text="";
		
			imgRechercher.setFocus();
			txtRecherche.setFocus();
			comboTri.selectedIndex=0;
			
		}
		
		private function txtRechercheEnterHandler(fe : FlexEvent):void{
			clearItem();
			bxImgMenue.selectedChild = objMenuFlotteMobDefault;
			comboBoxHandler(null);
		}
		private function rechercher():void{	
			
			var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
			var PERIMETRE_INDEX : int = perimetreObj.PERIMETRE_INDEX; // idPerimetre ( groupe client) 
		 
		 
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"getEquipements_V3",
							rechercherResultHandler);
		 
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,txtRecherche.text,idPoolGestionnaire,rbgAfficher.selectedValue,vue.selectedVue);
		}
		private function rechercherResultHandler(re : ResultEvent):void{
			if (re.result)
			{	
				var newColl:ArrayCollection = new ArrayCollection();
				var tmpArr:Array = (re.result as ArrayCollection).source;
				var i:int;
				for(i=0; i < tmpArr.length;i++){	
					
					var  item : DgRechercheItemVo = new DgRechercheItemVo();
					
					item.IDEMPLOYE=tmpArr[i].IDEMPLOYE;
					item.IDSIM=tmpArr[i].IDSIM;
					item.IDSOUS_TETE=tmpArr[i].IDSOUS_TETE;
					item.IDTERMINAL=tmpArr[i].IDTERMINAL;
					item.IDCONTRAT_ABO = tmpArr[i].IDCONTRAT_ABO;
					item.IDDISTRIBUTEUR=tmpArr[i].IDDISTRIBUTEUR;
					
					item.COLLABORATEUR= (tmpArr[i].COLLABORATEUR)?tmpArr[i].COLLABORATEUR:"";
					item.DISTRIBUTEUR=	(tmpArr[i].DISTRIBUTEUR)?tmpArr[i].DISTRIBUTEUR:"";
					item.IMEI=(tmpArr[i].IMEI)?tmpArr[i].IMEI:"";
					item.LIGNE=(tmpArr[i].LIGNE)?tmpArr[i].LIGNE:"";
					item.MARQUE=(tmpArr[i].MARQUE)?tmpArr[i].MARQUE:"";
					item.MODELE=(tmpArr[i].TERMINAL)?tmpArr[i].TERMINAL:"";
					item.NUM_SIM=(tmpArr[i].NUM_SIM)?tmpArr[i].NUM_SIM:"";
					item.OPERATEUR=(tmpArr[i].OPERATEUR)?tmpArr[i].OPERATEUR:"";
					item.TERMINAL=(tmpArr[i].TERMINAL)?tmpArr[i].TERMINAL:"";
					
					item.MATRICULE = (tmpArr[i].MATRICULE)?tmpArr[i].MATRICULE:" ";					
					item.NIVEAU_COLLABORATEUR = (tmpArr[i].NIVEAU_COLLABORATEUR)?tmpArr[i].NIVEAU_COLLABORATEUR:"";
					item.NIVEAU_TERMINAL = (tmpArr[i].NIVEAU_TERMINAL)?tmpArr[i].NIVEAU_TERMINAL:"";
					item.EMAIL = (tmpArr[i].EMAIL)?tmpArr[i].EMAIL:"";
					
					item.CODE_INTERNE = (tmpArr[i].CODE_INTERNE )?tmpArr[i].CODE_INTERNE :"";
					item.REFERENCE_EMPLOYE = (tmpArr[i].REFERENCE_EMPLOYE != null)?tmpArr[i].REFERENCE_EMPLOYE:"";
					item.IDENTIFIANT_EMPLOYE = (tmpArr[i].IDENTIFIANT_EMPLOYE)?tmpArr[i].IDENTIFIANT_EMPLOYE:"";
					
					item.NUM_CONTRAT_ABO = (tmpArr[i].NUM_CONTRAT_ABO)?tmpArr[i].NUM_CONTRAT_ABO:"";
					
					item.RIX_TERMINAL = (tmpArr[i].RIX_TERMINAL)?tmpArr[i].RIX_TERMINAL:0;
					item.NBJR_FIN = (tmpArr[i].NBR_JRS_FIN_GARANTIE)?tmpArr[i].NBR_JRS_FIN_GARANTIE:0;
					item.LIBELLE_TYPE_LIGNE = (tmpArr[i].LIBELLE_TYPE_LIGNE)?tmpArr[i].LIBELLE_TYPE_LIGNE:"";
					item.NB_JRS_FIN_LIGNE = (tmpArr[i].NB_JR_FIN_LG)?tmpArr[i].NB_JR_FIN_LG:0;
					
					item.ORGA_CLIENT = "";
					
					item.DATE_ENTREE = (tmpArr[i].DATE_ENTREE != null)?tmpArr[i].DATE_ENTREE:null;
					item.DATE_SORTIE = (tmpArr[i].DATE_SORTIE != null)?tmpArr[i].DATE_SORTIE:null;
					item.DATE_ELLIGIBILITE = (tmpArr[i].DATE_ELLIGIBILITE)?tmpArr[i].DATE_ELLIGIBILITE:null;
					item.DATE_FIN_GARANTIE = (tmpArr[i].DATE_FIN_GARANTIE )?tmpArr[i].DATE_FIN_GARANTIE :null;
					item.DATE_COMMANDE = (tmpArr[i].DATE_COMMANDE != null)?tmpArr[i].DATE_COMMANDE :null;
					
												
					newColl.addItem(item);
				}
				
				setDataProvidr(newColl);
				rechercheData = newColl;
				
			}		
			bxImgMenue.selectedChild = objMenuFlotteMobDefault;
		}
		
		private function txtFiltreChangeHandler(ev : Event):void{
			
			clearItem();
			bxImgMenue.selectedChild = objMenuFlotteMobDefault;
			
			try{
				rechercheData.filterFunction = filtrerLeGrid;
				rechercheData.refresh();
			}catch(e :Error){
				trace("Erreur Gestion Flotte GSM");
			}

		}
		private function filtrerLeGrid(item : Object):Boolean{			
			return false
		}
		
		//index du dernier item selectionné
		private var indexLastItem : int = -1;
		private var objLastItem : DgRechercheItemVo;
		
		private var typeColonneLastIndex : int = -1 ;
	 
		/*
		1->Collab
		2->Ligne
		3->Term
		4->Sim
		*/
				
		private function dgRechercheChangeHandler(event : ListEvent):void{
			var isClickable : Boolean = false;
			
			trace();
			var selectedColumne:String = DataGridColumn(event.itemRenderer.styleName).dataField.toUpperCase();
			
			if(event!=null)
			{
			
				if ((event.rowIndex != -1))
				{
					clearItem();
					
					switch (selectedColumne) 
					{ 
					  case "COLLABORATEUR"://1 						
						texteItem = dgRecherche.dgListe.selectedItem.COLLABORATEUR;
						
						if(dgRecherche.dgListe.selectedItem.COLLABORATEUR != null && dgRecherche.dgListe.selectedItem.COLLABORATEUR != "")
						{							
							isClickable=true;
							//idCollab=dgRecherche.dgListe.selectedItem.IDEMPLOYE;
							bxImgMenue.selectedChild=objMenuFlotteMobUtilisateur;
							indexLastItem = dgRecherche.dgListe.selectedIndex;
							objLastItem = DgRechercheItemVo(dgRecherche.dgListe.selectedItem);
							dgRecherche.dgListe.selectedItem.SELECTION_COLLAB = true;
							typeColonneLastIndex = 1;
							bxImgMenue_tooltip.selectedChild=objMenuFlotteMobUtilisateur_tooltip;
							afficheMenuToolTip(objMenuFlotteMobUtilisateur_tooltip);
													
						}
						else
						{
							isClickable=false;
					  	  	bxImgMenue.selectedChild = objMenuFlotteMobDefault;
					  	  	
					  	}
					  break;
					  case "IMEI": 
						
						texteItem = dgRecherche.dgListe.selectedItem.MODELE+"("+dgRecherche.dgListe.selectedItem.IMEI+")";
						if(dgRecherche.dgListe.selectedItem.IMEI != null && dgRecherche.dgListe.selectedItem.IMEI != "")
						{
							isClickable=true;
							bxImgMenue.selectedChild=objMenuFlotteMobTerminal;
							bxImgMenue_tooltip.selectedChild=objMenuFlotteMobTerminal_tooltip;
							
							indexLastItem = dgRecherche.dgListe.selectedIndex;
							objLastItem = DgRechercheItemVo(dgRecherche.dgListe.selectedItem);
							dgRecherche.dgListe.selectedItem.SELECTION_TERM = true;
							typeColonneLastIndex = 3;
							afficheMenuToolTip(objMenuFlotteMobTerminal_tooltip);
					 	}
					 	else
						{
							isClickable=false;
					  	  	bxImgMenue.selectedChild = objMenuFlotteMobDefault;
						}
					  break; 
					  case "LIGNE": 
					  	
						texteItem = dgRecherche.dgListe.selectedItem.LIGNE;
						if(dgRecherche.dgListe.selectedItem.LIGNE != null && dgRecherche.dgListe.selectedItem.LIGNE != "")
						{
							isClickable=true;
						  	bxImgMenue.selectedChild=objMenuFlotteMobNDI;
						  	bxImgMenue_tooltip.selectedChild=objMenuFlotteMobNDI_tooltip;
						  	
						  	indexLastItem = dgRecherche.dgListe.selectedIndex;
						  	objLastItem = DgRechercheItemVo(dgRecherche.dgListe.selectedItem);
							dgRecherche.dgListe.selectedItem.SELECTION_LIGNE = true;
							typeColonneLastIndex = 2;
							afficheMenuToolTip(objMenuFlotteMobNDI_tooltip);
						}
						else
						{
							isClickable=false;
					  	  	bxImgMenue.selectedChild = objMenuFlotteMobDefault;
						}
					  break; 
					  
					  case "NUM_SIM": 
						
						texteItem = dgRecherche.dgListe.selectedItem.NUM_SIM;
						if(dgRecherche.dgListe.selectedItem.NUM_SIM != null && dgRecherche.dgListe.selectedItem.NUM_SIM != "" )
						{
							isClickable=true;
							bxImgMenue.selectedChild = objMenuFlotteMobSIM;
							bxImgMenue_tooltip.selectedChild=objMenuFlotteMobSIM_tooltip;
							indexLastItem = dgRecherche.dgListe.selectedIndex;
							objLastItem = DgRechercheItemVo(dgRecherche.dgListe.selectedItem);
							dgRecherche.dgListe.selectedItem.SELECTION_SIM = true;
							typeColonneLastIndex = 4;
							afficheMenuToolTip(objMenuFlotteMobSIM_tooltip);
						}
						else
						{
							isClickable=false;
					  	  	bxImgMenue.selectedChild = objMenuFlotteMobDefault;
						}
						
						 
					  break; 
					  default: 
					  	  isClickable=false;
					  	  bxImgMenue.selectedChild = objMenuFlotteMobDefault;
					  	  texteItem = null;
					}
					objLigneSelected = (dgRecherche.dataProvider as ArrayCollection).getItemAt(dgRecherche.dgListe.selectedIndex) as DgRechercheItemVo;
					
				}
			}
		}
		private function afficheMenuToolTip(menu_toolTip : MenuGestionFlotteMobileIHM ):void
		{
			
			
			//selectedChild : Sinon pb de taille avec le viewstack. à voire
			//var obj_menuTooltip : MenuTooltip = new MenuTooltip(menu_toolTip);
			var obj_menuTooltip : MenuTooltip = new MenuTooltip(bxImgMenue_tooltip);
			var pt:Point = new Point(mouseX, mouseY);
            pt = contentToGlobal(pt);
            obj_menuTooltip.x=pt.x+5;
			obj_menuTooltip.y=pt.y+10;
			PopUpManager.addPopUp(obj_menuTooltip,this,false);
		}
		
		private function clearItem():void
		{
			if(indexLastItem != -1)
			{
				switch (typeColonneLastIndex) 
				{ 
					case 1: 
					objLastItem.SELECTION_COLLAB = false;
					break;
					case 2: 
					objLastItem.SELECTION_LIGNE = false;
					break;
					case 3: 
					objLastItem.SELECTION_TERM = false;
					break;
					case 4: 
					objLastItem.SELECTION_SIM = false;
					break;
				}
			}
		}
		public function actualise():void
		{
			clearItem();
			rechercher();
		}
		
		protected function dgResultatsEditBeginningHandler(dge : DataGridEvent):void{			
		     if (dge.itemRenderer != null && dge.itemRenderer.data != null)
		     {
		     	if(dge.itemRenderer.data[dge.dataField]!=null)
		     	{
			     var txt : String = dge.itemRenderer.data[dge.dataField];
		    	 System.setClipboard(txt); 
		    	 lblClipBoard.text = "Presse papier : " + txt;
       			}
       		}
       		dge.preventDefault();
	    }
	    
	    private function getHistorique(evt : MouseEvent):void
	    {
	    	 
	    	switch (typeColonneLastIndex) 
				{ 
					case 1: 
						//objLastItem.SELECTION_COLLAB = false;
						initHistorique(dgRecherche.dgListe.selectedItem.IDEMPLOYE,0,0,0);
					break;
					case 2: 
						//objLastItem.SELECTION_LIGNE = false;
						initHistorique(0,dgRecherche.dgListe.selectedItem.IDSOUS_TETE,0,0);
					break;
					case 3: 
						//objLastItem.SELECTION_TERM = false;
						initHistorique(0,0,dgRecherche.dgListe.selectedItem.IDTERMINAL,0);
					break;
					case 4: 
						//objLastItem.SELECTION_SIM = false;
						initHistorique(0,0,0,dgRecherche.dgListe.selectedItem.IDSIM);
					break; 
				}   
	    }
	    
	    
	    private function initHistorique(idEmp : int,idSousTete : int, idTerm : int,idSim : int):void
	    {
	    	//Alert.show(idEmp+"---"+idTerm+"---"+idSim+"---"+idSousTete);
	    	var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
	 
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"getHistorique",
							getHistoResult);
			//Alert.show(ObjectUtil.toString(xmlParam));
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,idEmp,idTerm,idSim,idSousTete);
	    		
	    }
	    
	    private function btAnnuaireClickHandler(event:MouseEvent):void
	    {
	    	//viewStack.selectedIndex = 2;
	    }
	    
	    private function getHistoResult(re : ResultEvent):void
	    {
	    	//Alert.show("-->"+ObjectUtil.toString(re.result));
	    	if (re.result)
			{	
				var panelHisto : PanelHistorique = new PanelHistorique(re.result as ArrayCollection);
	    		PopUpManager.addPopUp(panelHisto,this,true);
	    		PopUpManager.centerPopUp(panelHisto);
			}
	    }
	    /*
	    * ******************************************************************************************************************************************************
	    * ******************************************************************************************************************************************************
	    * ********************************************************Requetes pour la COMBOBOX*********************************************************************
	    * ******************************************************************************************************************************************************
	    */
	    private function get_sim_ligne():void
	    {
	    	//idEmployé a zéro
	    	var objUtilGetListeDisp : UtilGetListeDispo = new UtilGetListeDispo(1,0,0,1,"aucun",rechercherResultHandler);	
	    }
	    	    
	    /*
	    *Commande Mobile functions
	    */
	    private function btGererCommandeClickHandler(me:MouseEvent): void
	    {
	    	viewStack.selectedIndex = 1;
	    }	  
	    // Les accesseurs ci dessous sont utilisés par les menu-tool-tip pour récupérer le label-fixe en bas du grid principal et afficher une description de l'action sur le survol d'un bt
	   public function get objMenuFlotteMobSIM_fixe():MenuFlotteMobSIM
	   {
	   	 return objMenuFlotteMobSIM;
	   }
	   public function get objMenuFlotteMobTerminal_fixe():MenuFlotteMobTerminal
	   {
	   	 return objMenuFlotteMobTerminal;
	   }
	   public function get objMenuFlotteMobNDI_fixe():MenuFlotteMobNDI
	   {
	   	 return objMenuFlotteMobNDI;
	   }
	   public function get objMenuFlotteMobUtilisateur_fixe():MenuFlotteMobUtilisateur
	   {
	   	 return objMenuFlotteMobUtilisateur;
	   }
	    
	  }
}
                                                                                                                                                                                                                                                             
