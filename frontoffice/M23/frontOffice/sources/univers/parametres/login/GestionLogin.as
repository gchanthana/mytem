package univers.parametres.login {
	
	import mx.events.FlexEvent;
	
	import univers.UniversFunctionItem;
	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	import univers.parametres.parametrecompte.event.GestionProfilEvent;
	import univers.parametres.parametrecompte.event.GestionTypeCommandeEvent;
		
	public class GestionLogin extends GestionLoginIHM implements UniversFunctionItem {
		
		public function GestionLogin()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCOmpleteHandler);
		}
		private function creationCOmpleteHandler(evt : FlexEvent):void
		{
			//Mise a jour des données de l'onglet POOL:
			addEventListener(GestionPoolEvent.UPDATE_PROFIL_LOGIN,info_pool_change_handler);
			addEventListener(GestionTypeCommandeEvent.TYPE_COMMANDE_CHANGE,info_type_commande_change_handler);
			//Mise à jour des données de l'onglet login:
			//addEventListener(GestionProfilEvent.PROFIL_USER_CHANGE,info_profil_change_handler);
		}
		public function afterPerimetreUpdated():void
		{
			
		}
		public function onPerimetreChange():void
		{
			
		}
		public function info_pool_change_handler(evt : GestionPoolEvent):void
		{
			ongletGestionPool.ongletListeProfilCompte.initData();
			ongletGestionPool.ongletListePool.initData();
		}
		public function info_type_commande_change_handler(evt : GestionTypeCommandeEvent):void
		{
			ongletGestionPool.ongletListeTypeCommandeCompte.init();
		}
		/*public function info_profil_change_handler(evt : GestionProfilEvent):void
		{
		//	ongletLogin.
		}*/
		
		
		
	}
	
}
		