﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" author="Soufiane" hint="retourne la liste des certifications ou type de contrat exemple Gold , Platinium">

	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="ne contient rien">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.List_certification">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#getCodeLangue()#">
				<cfprocresult name="p_List_certification" >
			</cfstoredproc>
				
			<cfif isQuery(p_List_certification)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_List_certification>
				<cfreturn CreateReponse("AL022", 1, data)/>
			<cfelseif isnumeric(p_List_certification)>
				<cfset err = ThrowCfCatchError(p_List_certification, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL022", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AL022", 0, ThrowCfCatchError("AL022",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
</cfcomponent>