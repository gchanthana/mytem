package rapportPilotage.utils
{
	import flash.display.DisplayObject;
	
	import mx.containers.TitleWindow;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	public class ManagerPopUp extends TitleWindow
	{
		
		// permet d'ajouter un écouteur d'evenement sur le popup 
		 				
		public function ManagerPopUp():void
		{
			this.addEventListener(CloseEvent.CLOSE,closeHandler);
		}

		//afficher le pop up 
		
		public function showPopUp():void
		{	
			PopUpManager.addPopUp(this,Application.application as DisplayObject,true);
			PopUpManager.centerPopUp(this);	
			
		}
		// fermer le pop up 
		
		public function closeHandler(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
			this.x=0;
			this.y=0;
			//ModuleRapport.isClose=true;
		}
		
	}
}