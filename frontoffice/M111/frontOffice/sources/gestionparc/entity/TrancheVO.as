package gestionparc.entity
{

	public class TrancheVO
	{
		private var _libelle:String="";
		private var _typeTranche:String="";
		private var _id:int;
		private var _nbNum:int;
		private var _numDepart:int;
		private var _numFin:int;
		private var _idsous_tete:int;

		public function TrancheVO()
		{
		}

		public function get idsous_tete():int
		{
			return _idsous_tete;
		}

		public function set idsous_tete(value:int):void
		{
			_idsous_tete=value;
		}

		public function get numFin():int
		{
			return _numFin;
		}

		public function set numFin(value:int):void
		{
			_numFin=value;
		}

		public function get numDepart():int
		{
			return _numDepart;
		}

		public function set numDepart(value:int):void
		{
			_numDepart=value;
		}

		public function get nbNum():int
		{
			return _nbNum;
		}

		public function set nbNum(value:int):void
		{
			_nbNum=value;
		}

		public function get id():int
		{
			return _id;
		}

		public function set id(value:int):void
		{
			_id=value;
		}

		public function get typeTranche():String
		{
			return _typeTranche;
		}

		public function set typeTranche(value:String):void
		{
			_typeTranche=value;
		}

		public function get libelle():String
		{
			return _libelle;
		}

		public function set libelle(value:String):void
		{
			_libelle=value;
		}

		public function fillTrancheVO(obj:Object):void
		{
			this.libelle=obj.LIBELLE_TRANCHE;
			this.typeTranche=obj.TYPE_TRANCHE;
			this.nbNum=obj.NB_TRANCHE;
			this.numDepart=obj.TRANCHE_DEB;
			this.numFin=obj.TRANCHE_FIN;
			this.id=obj.IDTRANCHE_SDA;
			this.idsous_tete=obj.IDSOUS_TETE;
		}

		public static function returnTrancheVO(obj:Object):TrancheVO
		{
			var tranche:TrancheVO=new TrancheVO();
			tranche.libelle=obj.LIBELLE_TRANCHE;
			tranche.typeTranche=obj.TYPE_TRANCHE;
			tranche.nbNum=obj.NB_TRANCHE;
			tranche.numDepart=obj.TRANCHE_DEB;
			tranche.numFin=obj.TRANCHE_FIN;
			tranche.id=obj.IDTRANCHE_SDA;
			tranche.idsous_tete=obj.IDSOUS_TETE;

			return tranche;
		}
	}
}
