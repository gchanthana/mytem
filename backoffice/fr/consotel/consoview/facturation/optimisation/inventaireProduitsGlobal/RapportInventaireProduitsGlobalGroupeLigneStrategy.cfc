<!---
Package : fr.consotel.consoview.facturation.optimisation.inventaireProduitsGlobal
--->
<cfcomponent name="RapportInventaireProduitsGlobalGroupeLigneStrategy">
	<cffunction name="getReportData" access="private" returntype="query" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset RapportParams.date_m = LSDateFormat(ParseDateTime(RapportParams.DATEDEB),"yyyy/mm/dd")>
		<cfset RapportParams.date_m2=
				LSDateFormat(DateAdd("m",RapportParams.periodicite,ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
        <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_REP_V3.REP_INV_PRODUITS_LIG">
	        <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#RapportParams.DATE_M#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#RapportParams.DATE_M2#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetReportData">
        </cfstoredproc>
		<cfreturn qGetReportData>
	</cffunction>
	
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset NEW_DATEDEB = parseDateTime(RapportParams.DATEDEB)>
		<!---====== FORMATAGE DES DATES DE DEBUT ET DE FIN ======--->
		<cfset DATE_YEAR_YYYY = datePart("YYYY",rapportParams.DATEDEB)>
		<cfset DATE_MONTH_M = datePart("M",rapportParams.DATEDEB)>
		<cfset rapportParams.DATEDEB =
				LsDateFormat(createDate(DATE_YEAR_YYYY,DATE_MONTH_M,1),"YYYY/MM/DD")>
		<!---====================================================--->
		<cfset qGetData = getReportData(ID_PERIMETRE,RapportParams)>
		<cfif #qGetData.recordcount# gt 0>
			<cfinclude template="./inventaire_produits_xls.cfm">
		<cfelse>
			<cfoutput>
				<center><strong><h2>Aucunes donnÃ©es rÃ©pondant ï¿½ votre demande</h2></strong></center>
			</cfoutput>
		</cfif>
	</cffunction>
</cfcomponent>
